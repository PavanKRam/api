### Install PIP

```sudo apt-get install python-pip```

### After you have cloned the repo, install virtual environment. 

```sudo pip install virtualenv```

### When it's installed, it's time to create a virtual environment.

```virtualenv myenv```

### When myenv gets created, activate the environment by typing:

```source myenv/bin/activate```

### Install all the dependencies
```pip install -r requirements.pip```  

### Ignore if mysqlclient successfully installs, else you will need to run the following command and then install mysqlclient:

```sudo apt-get install libmysqlclient-dev```

### Install apache module

```sudo apt-get install libapache2-mod-wsgi```

### Generate static files
```python manage.py collectstatic```

### Create environment file
```cp .env.default .env```

### Install NewRelic Agent (globally, not in virtualenv)

```sudo pip install newrelic==2.90.0.75```

### Create newrelic ini file in api/backend/server/apache folder
```newrelic-admin generate-config LICENSE-KEY newrelic.ini```

### Make migrations
```python manage.py makemigrations```

### Migrate default admin app migrations to DB
```python manage.py migrate admin```

### Fake migrate all other apps migrations
```python manage.py migrate --fake```

### Run celery worker (optional on localhost)
```/path-to-python-virtualenv/bin/celery worker --app=server.celery_app --workdir=/path-to-api-repo/api/backend -l info```

### Configure trained model and other related files
```Download data from here https://drive.google.com/open?id=18G7a-E4B_9y7NRkKfnWDi6sovLNOYj2p```
```Go to download section of this repository (Second last section on left hand side)```
```Dowmload and run model_data_config.py file using```
```python model_data_config.py (Make sure both these files are under same folder)```