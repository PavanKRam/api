from django.conf.urls import url
from rest_framework.routers import DefaultRouter
from .views import *
from main.calendar.resources import calendar, SellerCarInspectionCalender

router = DefaultRouter()

router.register(r'^listings', ListingViewSet)
urlpatterns = [
    url(r'^filters/$', FiltersAPIView.as_view()),
    url(r'^carselection/$' , CarSelectionView.as_view() ),
    url(r'^search_filters/$', SearchFiltersAPIView.as_view()),
    url(r'^home_page/$', HomePageCars.as_view()),
    url(r'^get_similar_cars/$', SimilarCars.as_view()),
    url(r'^subheader_filters/$', SubheaderFiltersAPIView.as_view()),
    url(r'^recently_visited_cars/$', RecentlyVisitedCars.as_view()),
    url(r'^album_images/$', AlbumImagesAPIVIew.as_view()),
    url(r'^calendar/', calendar),
    url(r'^seller_car_inspection_calender/$', SellerCarInspectionCalender.as_view()),
    url(r'^similar_test_drive_cars/$', SimilarTestDriveCars.as_view()),
    url(r'^price_engine/$', PriceEngineAPIView.as_view()),
] + router.urls
