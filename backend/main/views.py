# -*- coding: utf-8 -*-
"""
Module holding viewsets for models contained by
truebil main app.
"""

from __future__ import unicode_literals

import random
import re
from datetime import datetime, date
from urlparse import urlparse

import django_filters
import pytz
from django.contrib.auth.models import AnonymousUser
from django.db.models import F, Q
from django.http import HttpResponseForbidden
from django_filters import rest_framework as filters
from rest_framework import mixins, status, viewsets, pagination
from rest_framework.response import Response
from rest_framework.views import APIView

from authn.permissions import IsAuthenticatedOrReadOnly
from connect.email import get_email_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from connect.sms import get_sms_data
from connect.spreadsheet import get_seller_spreadsheet_data
from truebil_management.models import SeoKeyword
from user.crf.helpers import is_freemium_eligible
from user.models import Buyer, InterestedSeller, Seller
from user.user_helpers import get_user_subscriptions, get_user_source, get_seller_visit_info, \
    get_sellerpage_listing_image
from user.utils.messages import INVALID_CITY, USER_NOT_LOGGED_IN, REQUIRED_FIELDS_ABSENT, INVALID_LISTING
from user.utils.response import get_error_response
from .data_science.functions import price_engine, listing_price_prediction, get_raw_price, \
    get_listing_refurbishment_cost, get_listing_procurement_score
from .main_helpers import get_recommendation_params
from .models import ListingPriceChange, InterestedSellerListings
from .quick_ops import find_between, get_price_range_from_slug, get_price_engine_response, decode_to_utf_8
from .recommendation_helpers import get_popular_model_order, \
    get_popular_recommendation, get_similar_cars, get_recommendations
from .serializers import *
from .utils.constants import LISTING_ACTIVE_STATUS, TYRE_IMAGES, TRENDING_MODELS, TYRE_CAPTION_SECTION_MAP, \
    TYRE_CAPTION_COMPONENT_MAP, EXCLUDED_CAPTIONS_FOR_GALLERY, CAPTION_IDS, CARS_FOR_DASHBOARD
from .utils.messages import INVALID_LISTING, INVALID_PATCH_REQUEST
from .utils.sample_inspection_report import SAMPLE_INSPECTION_REPORT_TYRE_IMGS, SAMPLE_INSPECTION_REPORT_ERROR_IMGS
from .utils.serializer_helpers import get_serializer_data, get_invalid_numbers, get_super_saver_car_ids, \
    get_make_model_variants


class TbPagination(pagination.PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100

    @staticmethod
    def remove_reco_params(link):
        if link:
            parse_url = urlparse(link)
            all_query_params = parse_url.query.split('&')
            query_string = ''
            for query_param in all_query_params:
                check_query_param = query_param.split('=')
                if check_query_param[0] in ['buyer_id', 'cars_seen']:
                    pass
                else:
                    query_string += query_param + '&'

            parse_url = parse_url._replace(query=query_string[:-1])
            return parse_url.geturl()
        else:
            return None

    def get_previous_link(self):
        url = super(TbPagination, self).get_previous_link()
        return self.remove_reco_params(url)

    def get_next_link(self):
        url = super(TbPagination, self).get_next_link()
        return self.remove_reco_params(url)


class ListingParentViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    """Parent viewset class for viewsets dealing with Listings"""

    queryset = Listing.objects.all()
    serializer_class = ListingSerializer

    def get_serializer_context(self):
        buyer = None
        is_user_logged_in = False
        is_subscribed_buyer = False
        freemium_eligible = False

        if type(self.request.user) is not AnonymousUser:
            is_user_logged_in = True
            mobile = self.request.user.mobile
            buyer = Buyer.objects.filter(mobile=mobile).order_by('-created_at').first()
            is_subscribed_buyer = get_user_subscriptions(self.request.user.id, subscription_type='buyer')
            if buyer:
                freemium_eligible = is_freemium_eligible(buyer.id)
        else:
            buyer_id = self.request.query_params.get('buyer_id', None)
            if buyer_id:
                try:
                    buyer = Buyer.objects.get(id=buyer_id)
                except Buyer.DoesNotExist:
                    pass

        attachments_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)

        return {
            'request': self.request,
            'buyer': buyer,
            'is_user_logged_in': is_user_logged_in,
            'is_subscribed_buyer': is_subscribed_buyer,
            'cdn_url': get_cdn_url(),
            'super_saver_car_ids': get_super_saver_car_ids(),
            'attachment_qs': attachments_qs,
            'invalid_numbers': get_invalid_numbers(),
            'is_buyer_crf_exhausted': not (freemium_eligible or is_subscribed_buyer),
            'source': get_user_source(self.request.META)
        }


class TraversalFilter(django_filters.Filter):
    """Custom filter class for implementing '__' lookup type"""

    def filter(self, qs, value):
        if value not in (None, ''):
            value = decode_to_utf_8(value)
            integers = [int(v) for v in value.split(',')]
            return qs.filter(**{'%s__%s' % (self.name, self.lookup_expr): integers})
        return qs


class SlugFilter(django_filters.Filter):
    """Custom filter class for slug filter type"""

    query_param_string = ''

    slug_map = {
        'model': 'variant__model_id',
        'make': 'variant__model__make_id',
        'body_type': 'body_type_id',
        'is_inventory': 'is_inventory',
    }

    def __init__(self):
        django_filters.Filter.__init__(self)
        SlugFilter.query_param_string = self.query_param_string

    def filter(self, qs, value):
        if value not in (None, ''):
            SlugFilter.query_param_string = ''
            kwargs = dict()
            split_by_city_name = value.rsplit('-', 1)
            city_name = split_by_city_name[1]
            if city_name.lower() != 'india':
                kwargs['locality__city__name__iexact'] = city_name.lower()

            rest_of_the_slug = split_by_city_name[0]
            relevant_slug = find_between(rest_of_the_slug, 'used-', '-cars-in')
            seo_filters = None
            if relevant_slug:
                filter_list = str(relevant_slug).split(' ')
                seo_filters = SeoKeyword.objects.filter(filter_name__in=filter_list)

            qs = qs.filter(**kwargs)
            if seo_filters:
                for seo_filter in seo_filters:
                    qs, SlugFilter.query_param_string = self.filter_data(qs, seo_filter, SlugFilter.query_param_string)
                return qs
            else:
                price_range = get_price_range_from_slug(relevant_slug)
                if price_range['price_min'] and price_range['price_max']:
                    SlugFilter.query_param_string += 'price_min=' + str(price_range['price_min']) + '&price_max=' + \
                                                     str(price_range['price_max']) + '&'
                    return qs.filter(**{'%s__%s' % ('price', 'range'): [price_range['price_min'],
                                                                        price_range['price_max']]})
                elif price_range['price_max']:
                    SlugFilter.query_param_string += 'price_max=' + str(price_range['price_max']) + '&'
                    return qs.filter(**{'%s__%s' % ('price', 'lte'): price_range['price_max']})
                elif price_range['price_min']:
                    SlugFilter.query_param_string += 'price_min=' + str(price_range['price_min']) + '&'
                    return qs.filter(**{'%s__%s' % ('price', 'gte'): price_range['price_min']})
        return qs

    def filter_data(self, qs, seo_filter, query_string):
        if seo_filter.filter_category in ['model', 'make', 'body_type', 'is_inventory']:
            query_string += str(seo_filter.filter_category) + '=' + str(seo_filter.filter_id) + '&'
            return qs.filter(**{'%s' % (self.slug_map[seo_filter.filter_category]): seo_filter.filter_id}), query_string
        elif seo_filter.filter_category == 'fuel_type':
            fuel_id = FuelType.objects.get(name=seo_filter.filter_id).id
            query_string += 'fuel=' + str(fuel_id) + '&'
            fuel_filter = FuelFilter(name='fuel_type_id_primary', lookup_expr='in')
            return fuel_filter.filter_data(qs, [str(seo_filter.filter_id)]), query_string
        elif seo_filter.filter_category == 'is_value_for_money':
            query_string += 'value_for_money=' + str(seo_filter.filter_id) + '&'
            value_for_money_filter = ListingFilter()
            return value_for_money_filter.filter_value_for_money_cars(qs, 'price',
                                                                      int(seo_filter.filter_id)), query_string


class FuelFilter(django_filters.Filter):
    """Custom filter class for fuel type"""

    def get_filtered_qs(self, qs, primary_fuel_filter, secondary_fuel_filter):
        return qs.filter(
            Q(**{'%s__%s' % (self.name, self.lookup_expr): primary_fuel_filter}) |
            Q(**{'%s__%s' % ('fuel_type_id_secondary', self.lookup_expr): secondary_fuel_filter})
        )

    def filter_data(self, qs, fuel_types):
        return self.get_filtered_qs(qs, fuel_types, fuel_types)

    def filter(self, qs, value):
        value = decode_to_utf_8(value)
        integers = [int(v) for v in value.split(',')]
        active_fuel_qs = FuelType.objects.filter(pk__in=integers, is_active=True)
        primary_fuel_qs = active_fuel_qs.filter(is_secondary=False).values('name')
        secondary_fuel_qs = active_fuel_qs.filter(is_secondary=True).values('name')
        return self.get_filtered_qs(qs, primary_fuel_qs, secondary_fuel_qs)


class ListingFilter(filters.FilterSet):
    """Custom filter class for filtering listings"""

    id = TraversalFilter(name='pk', lookup_expr='in')
    city_id = TraversalFilter(name='locality__city__id', lookup_expr='in')
    model = TraversalFilter(name='variant__model__id', lookup_expr='in')
    make = TraversalFilter(name='variant__model__make__id', lookup_expr='in')
    body_type = TraversalFilter(name='body_type__id', lookup_expr='in')
    colors = TraversalFilter(name='color__id', lookup_expr='in')
    regions = TraversalFilter(name='locality__region__id', lookup_expr='in')
    transmission = TraversalFilter(name='transmission_type__id', lookup_expr='in')
    years = django_filters.DateFilter(name='manufacturing_date', method='filter_year')
    features = TraversalFilter(name='features__id', lookup_expr='in')
    accessories = TraversalFilter(name='accessories__id', lookup_expr='in')
    value_for_money = django_filters.NumberFilter(name='price', method='filter_value_for_money_cars')
    truescores = django_filters.NumberFilter(name='rating', lookup_expr='gte')
    mileage = django_filters.NumberFilter(name='mileage', method='filter_mileage')
    price_min = django_filters.NumberFilter(name='price', lookup_expr='gte')
    price_max = django_filters.NumberFilter(name='price', lookup_expr='lte')
    fuel = FuelFilter(name='fuel_type_id_primary', lookup_expr='in')
    owners = TraversalFilter(name='owners', lookup_expr='in')
    slug = SlugFilter()

    @staticmethod
    def filter_year(queryset, name, value):
        if not value:
            return queryset

        if value == '2011-12-31':
            lookup = '__'.join([name, 'lte'])
        else:
            lookup = '__'.join([name, 'gte'])

        return queryset.filter(**{lookup: value})

    @staticmethod
    def filter_value_for_money_cars(queryset, name, value):
        if not value:
            return queryset

        filter_by = '%s__%s' % (name, 'lte')
        if value == 1:
            return queryset.filter(Q(Q(**{filter_by: 0.9 * F('carwale_price')}) & Q(is_inventory=False)) |
                                   Q(Q(**{filter_by: 1.06 * F('carwale_price')}) & Q(is_inventory=True)))
        else:
            return queryset

    @staticmethod
    def filter_mileage(queryset, name, value):
        if not value:
            return queryset

        if value == 80001:
            lookup = '__'.join([name, 'gt'])
        else:
            lookup = '__'.join([name, 'lte'])

        return queryset.filter(**{lookup: value})

    class Meta:
        model = Listing
        fields = ('id', 'is_inventory', 'status', 'city_id', 'model', 'body_type', 'colors', 'regions',
                  'transmission', 'years', 'features', 'accessories', 'owners', 'value_for_money', 'truescores', 'price_min',
                  'price_max', 'fuel', 'mileage', 'make', 'slug')


class ListingViewSet(ListingParentViewSet, mixins.RetrieveModelMixin, mixins.UpdateModelMixin):
    """Viewset class for Listing model"""

    pagination_class = TbPagination
    filter_backends = (filters.DjangoFilterBackend,)
    filter_class = ListingFilter
    recommended_type = ''

    def get_serializer_class(self):
        if self.action == 'list':
            return ListingSerializer
        elif self.action == 'retrieve':
            return DedicatedSerializer
        else:
            return ListingSerializer

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """

        if self.action == 'list':
            sort_param = self.request.query_params.get('sort', None)
            sort_by_dict = {
                'dd': '-sort_by', 'da': 'sort_by', 'pa': 'price', 'pd': '-price', 'ra': 'rating', 'rd': '-rating',
                'ma': 'mileage', 'md': '-mileage', 'rega': 'manufacturing_date', 'regd': '-manufacturing_date'
            }

            if sort_param not in sort_by_dict.keys():
                recommendation = get_recommendations(get_recommendation_params(self.request))
                self.recommended_type = recommendation['type']
                recommendation_list = recommendation['list']

                listings = ','.join(map(str, recommendation_list))
                listing_qs = Listing.objects.filter(id__in=recommendation_list, status__in=LISTING_ACTIVE_STATUS).extra(
                    select={'manual': 'FIELD(listings.id,' + listings + ' )'},
                    order_by=('manual',)
                )
            else:
                order_params = [sort_by_dict[sort_param]]
                listing_qs = self.queryset.filter(status__in=LISTING_ACTIVE_STATUS).order_by(*order_params)

            listing_qs = ListingSerializer.setup_eager_loading(listing_qs)
            return listing_qs.distinct()
        elif self.action == 'retrieve':
            return self.queryset
        else:
            return HttpResponseForbidden()

    def list(self, request, *args, **kwargs):
        slug = self.request.query_params.get('slug', None)
        city_id = self.request.query_params.get('city_id', None)
        city = None

        if slug or city_id:
            slug_city = slug.split('-')[-1] if slug else None
            city = City.objects.filter(Q(name=slug_city) | Q(id=city_id))

        if not city:
            return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)

        response = super(ListingViewSet, self).list(request, args, kwargs)
        slug = SlugFilter()
        query_string = slug.query_param_string
        if request.query_params.get('slug'):
            response.data['query_string'] = query_string.rstrip('&')
        response.data['recommended_type'] = self.recommended_type
        return response

    def partial_update(self, request, *args, **kwargs):
        new_price = int(request.data.get('new_price', None))
        original_price = int(request.data.get('original_price', None))
        truebil_recommended_price = request.data.get('truebil_recommended_price', None)
        min_recommended_price = None
        max_recommended_price = None
        if truebil_recommended_price:
            truebil_recommended_price = truebil_recommended_price.split("-")
            min_recommended_price = truebil_recommended_price[0]
            max_recommended_price = truebil_recommended_price[1]

        listing_id = kwargs.get('pk', None)

        if listing_id:
            email_data = list()
            try:
                price_change_successful = False
                if new_price < original_price:
                    change_percentage = ((original_price - new_price)/original_price)*100
                    if change_percentage <= 30:
                        updated_price = round(new_price, -3)
                        listing = Listing.objects.get(id=listing_id)
                        listing.price = updated_price
                        listing.save(update_fields=['price'])
                        price_change_successful = True
                        ListingPriceChange.objects.create(listing=listing, previous_price=original_price,
                                                          updated_price=new_price, source_update='seller_dashboard')
                        email_data = [
                            get_email_data(type_='price_decrease', send_to='price.review.group@truebil.com',
                                           subject='Price decreased', car_id=listing_id, name=request.user.name,
                                           mobile=request.user.mobile, original_price=original_price, new_price=new_price,
                                           updated_price=updated_price, min_recommended_price=min_recommended_price,
                                           max_recommended_price=max_recommended_price)]

                if not price_change_successful:
                    email_data = [
                        get_email_data(type_='price_decrease', send_to='price.review.group@truebil.com',
                                       subject='Price Review Required', car_id=listing_id, name=request.user.name,
                                       mobile=request.user.mobile, original_price=original_price, new_price=new_price,
                                       updated_price="Did not update price", min_recommended_price=min_recommended_price,
                                       max_recommended_price=max_recommended_price)]

                sms_data = [get_sms_data(type_='price_change_request', mobile=request.user.mobile,
                                         name=request.user.name)]

                mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data)
                add_gearman_data('mailer', mailer_data)

                return Response({'status': True, 'price_change_successful': price_change_successful})
            except Listing.DoesNotExist:
                return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(get_error_response(INVALID_PATCH_REQUEST), status=status.HTTP_400_BAD_REQUEST)


class FiltersAPIView(APIView):
    def get(self, request, format=None):
        """
        Returns filters

        ---

        parameters:
            - name: city_id
            - description: ID of the city
            - required: false
            - type: integer

        responseMessages:
            - code: 400
              message: City does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        serializer_class = FiltersSerializer
        try:
            city_id = request.query_params.get('city_id', '')
            filter_city = False
            city = City
            if city_id:
                city = City.objects.get(id=city_id)
                filter_city = True
        except City.DoesNotExist:
            response_data = {'success': False, 'message': INVALID_CITY}
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        filters_data = get_serializer_data(request, serializer_class, city, filter_city)
        is_filter_selected = filters_data.pop('is_filter_selected')
        response_data = {'success': True, 'filters': filters_data, 'is_filter_selected': is_filter_selected}
        return Response(response_data)


class SearchFiltersAPIView(APIView):
    def get(self, request, format=None):
        """
        Returns search filters

        ---

        parameters:
            - name: city_id
            - description: ID of the city
            - required: false
            - type: integer

        responseMessages:
            - code: 400
              message: City does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        serializer_class = SearchFiltersSerializer
        try:
            city_id = request.query_params.get('city_id', '')
            filter_city = False
            city = City
            if city_id:
                city = City.objects.get(id=city_id)
                filter_city = True
        except City.DoesNotExist:
            response_data = {'success': False, 'message': INVALID_CITY}
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        search_filters_data = get_serializer_data(request, serializer_class, city, filter_city)
        response_data = {'success': True, 'filters': search_filters_data}
        return Response(response_data)


class AlbumImagesAPIVIew(APIView):
    def get(self, request):
        """
        API View to retrieve listing album image urls

        ---

        parameters:
            - name: listing_id
              description: ID of the listing
              required: true
              type: integer
            - name: dimension
              description: 125, 360 or 800
              required: false
              type: string

        responseMessages:
            - code: 400
              message: Required field not entered.
              message: Listing does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml

        """

        listing_id = request.query_params.get('listing_id', None)
        dimension = request.query_params.get('dimension', '360')

        if not listing_id:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        try:
            image_size = dimension if dimension in ['125', '360', '800'] else '360'
            actual_data, album_images = self.get_album_images(listing_id, image_size)

            response = {
                'album_images': album_images,
                'sample_data': not actual_data
            }
            return Response(response)

        except (Listing.DoesNotExist, ValueError):
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

    def get_album_images(self, listing_id, image_size):
        listing_obj = Listing.objects.get(id=listing_id)
        is_freemium_buyer = False
        if type(self.request.user) is not AnonymousUser:
            mobile = self.request.user.mobile
            buyer = Buyer.objects.filter(mobile=mobile).first()
            if buyer:
                is_freemium_buyer = is_freemium_eligible(buyer.id)

        show_inspection_report = can_show_inspection_report(self.request, listing_obj) or is_freemium_buyer
        img_version = listing_obj.img_version
        attachment_qs = Attachment.objects.filter(listing_id=listing_id). \
            order_by('caption__inspection_category_id', 'caption__id', 'inspection_section_id'). \
            prefetch_related('caption', 'caption__inspection_category')
        listing_comments = listing_obj.comments.filter(type__exact='positive'). \
            prefetch_related('inspection_section').values('inspection_section_id', 'text')
        component_error_comment_map = get_component_error_comment_map()
        listing_error_qs = listing_obj.listing_errors.prefetch_related('inspection_error', 'inspection_component')
        listing_component_errors = [
            str(listing_error.inspection_component.id) + "-" + str(listing_error.inspection_error.id)
            for listing_error in listing_error_qs
        ]

        cdn_url = get_cdn_url()
        album_images = {}
        album_category_images = {'images': []}
        previous_category_id = None
        first_err_img = None
        tyre_images = {'images': []} if show_inspection_report else SAMPLE_INSPECTION_REPORT_TYRE_IMGS
        error_images = {'images': []} if show_inspection_report else SAMPLE_INSPECTION_REPORT_ERROR_IMGS

        for attachment in attachment_qs:
            url_first_half = '{0}{1}-{2}-{3}.jpg'.format(cdn_url, listing_id, attachment.file_number, image_size)
            url = url_first_half if not img_version else ('{}?v={}'.format(url_first_half, img_version))

            caption = attachment.caption
            if caption:
                if caption.id in EXCLUDED_CAPTIONS_FOR_GALLERY:
                    continue

                if caption.id in TYRE_IMAGES:
                    if show_inspection_report:
                        positive_comments = []
                        for comment in listing_comments:
                            if comment['inspection_section_id'] == TYRE_CAPTION_SECTION_MAP[caption.id]:
                                positive_comments.append(comment['text'])

                        negative_comments = []
                        for error in listing_component_errors:
                            if error.startswith(str(TYRE_CAPTION_COMPONENT_MAP[caption.id]) + "-") and \
                                    error in component_error_comment_map.keys() and \
                                    component_error_comment_map[error].comment_negative:
                                negative_comments.append(component_error_comment_map[error].comment_negative)

                        tyre_images['category_id'] = 8
                        tyre_images['category_name'] = 'Tyre photos'
                        image = {
                            'caption': caption.name.replace(' Tread', ''),
                            'url': url,
                            'comments': {
                                'positive': positive_comments,
                                'negative': negative_comments if negative_comments else None
                            }
                        }
                        tyre_images['images'].append(image)
                    else:
                        continue
                else:
                    category_id = caption.inspection_category_id

                    if previous_category_id is not None and previous_category_id != category_id:
                        album_category_images = {'images': [], 'category_id': '', 'category_name': ''}

                    album_category_images['category_id'] = category_id
                    album_category_images['category_name'] = caption.inspection_category.name
                    image = {
                        'caption': caption.name,
                        'url': url
                    }
                    album_category_images['images'].append(image)
                    album_images[category_id] = album_category_images
                    previous_category_id = category_id
            else:
                if attachment.inspection_error_id and attachment.inspection_component_id:
                    if show_inspection_report:
                        comp_err = str(attachment.inspection_component_id) + '-' + str(attachment.inspection_error_id)
                        error_images['category_id'] = 6
                        error_images['category_name'] = 'Error Photos'
                        image = {
                            'caption': component_error_comment_map[comp_err].comment_negative,
                            'url': url
                        }
                        error_images['images'].append(image)
                    else:
                        if first_err_img is None:
                            comp_err = str(attachment.inspection_component_id) + '-' + str(
                                attachment.inspection_error_id)
                            first_err_img = [
                                {
                                    'category_id': 6,
                                    'category_name': 'Error Photos',
                                    'images': [{
                                        'caption': component_error_comment_map[comp_err].comment_negative,
                                        'url': url
                                    }]
                                }
                            ]
                        else:
                            continue
                else:
                    continue

        all_album_images = list(album_images.values())
        all_album_images = (all_album_images + first_err_img) if first_err_img is not None else all_album_images
        all_album_images = (all_album_images + [error_images]) if 'category_id' in error_images else all_album_images
        all_album_images = (all_album_images + [tyre_images]) if 'category_id' in tyre_images else all_album_images
        return show_inspection_report, all_album_images


class SimilarCars(APIView):
    """
    An API View to get similar cars
    """

    def get(self, request, format=None):
        """
        An API View to get similar cars

        ---

        Methods:
             - GET:
                parameters:
                    - name: listing_id
                      description: Listing ID
                      required: true
                      type: integer
                    - name: city_id
                      description: City ID
                      required: false
                      type: integer

                consumes:
                    - application/json
                    - application/xml
                produces:
                    - application/json
                    - application/xml

            """

        listing_id = request.query_params.get('listing_id', None)
        city_id = request.query_params.get('city_id', None)
        seen_cars = request.query_params.get('cars_seen', None)

        try:
            listing = Listing.objects.values('id', 'status', 'locality__city_id').get(id=listing_id)
        except Listing.DoesNotExist:
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

        listing_city_id = listing['locality__city_id']
        if city_id and int(city_id) == listing_city_id:
            city_name = City.objects.values_list('name', flat=True).get(id=city_id)
        else:
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

        similar_cars = get_similar_cars(listing, city_id, direct_only=True)['list']
        similar_car_ids = similar_cars

        if seen_cars:
            seen_car_ids = [int(v) for v in decode_to_utf_8(seen_cars).split(',')]
            similar_car_ids = [x for x in similar_cars if x not in seen_car_ids]

        if len(similar_car_ids) > 0:
            similar_car_ids = similar_car_ids[:3]
        else:
            similar_car_ids = similar_cars[:3]

        response = []
        attachment_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
        for car_id in similar_car_ids[:3]:
            try:
                listing = Listing.objects.get(id=car_id)
                listings_data = ListingSerializer(listing, context={'show_one_url': True, 'cdn_url': get_cdn_url(),
                                                                    'attachment_qs': attachment_qs,
                                                                    'invalid_numbers': get_invalid_numbers()})
                response.append(listings_data.data)
            except Listing.DoesNotExist:
                pass

        home_obj = HomePageCars()
        recommendation_link = home_obj.get_link_suffix(city_name)

        data = {
            'link': recommendation_link,
            'count': len(similar_car_ids),
            'title': "All Recommendations",
            'results': response
        }
        return Response(data)


class RecentlyVisitedCars(APIView, mixins.UpdateModelMixin):
    """
    An API View to get recently visited cars
    """

    permission_classes = [IsAuthenticatedOrReadOnly]

    @staticmethod
    def get_listing_response(seen_car_ids):
        attachment_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
        listings = ','.join(map(str, seen_car_ids[:3]))
        seen_listings_qs = Listing.objects.filter(id__in=seen_car_ids, status__in=LISTING_ACTIVE_STATUS).extra(
            select={'manual': 'FIELD(listings.id,' + listings + ' )'},
            order_by=('manual',)
        )
        seen_listings_qs = ListingSerializer.setup_eager_loading(seen_listings_qs)
        seen_listings = ListingSerializer(seen_listings_qs, context={
            'cdn_url': get_cdn_url(), 'attachment_qs': attachment_qs, 'show_one_url': True}, many=True)
        return seen_listings.data

    def get_cars_seen_by_user(self, seen_car_ids, city_name):
        link = None
        response = self.get_listing_response(seen_car_ids[:CARS_FOR_DASHBOARD])
        home_obj = HomePageCars()
        link_suffix = home_obj.get_link_suffix(city_name)
        recently_visited_link = home_obj.get_link(link_suffix + '/?id=', seen_car_ids[:20])
        count = len(seen_car_ids)
        if count >= 3:
            count = len(seen_car_ids[:20])
            link = recently_visited_link

        return response, count, link

    def get(self, request, format=None):
        """
        An API View to get recently visited cars

        ---

        Methods:
             - GET:
                parameters:
                    - name: cars_seen
                      description: List of cars that the user has seen
                      required: false
                      type: list
                    - name: city_id
                      description: City ID
                      required: false
                      type: integer

                consumes:
                    - application/json
                    - application/xml
                produces:
                    - application/json
                    - application/xml

            """

        cars_seen = request.query_params.get('cars_seen', None)
        city_id = request.query_params.get('city_id', None)

        if city_id is None:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)
        else:
            city_name = City.objects.get(id=city_id).name

            link_title = "All Recently Viewed Cars"
            if type(request.user) is AnonymousUser:
                seen_car_ids = [] if any(cars_seen == param_value for param_value in ['', None]) else \
                    [int(car_id) for car_id in cars_seen.split(',')]
                data = self.get_cars_seen_by_user(seen_car_ids, city_name)
            else:
                user_activities = request.user.user_activities.filter(is_seen=True)
                listings_seen_by_users = user_activities.values_list(
                    'listing_id', flat=True).order_by('-created_at')
                data = self.get_cars_seen_by_user(map(int, listings_seen_by_users[:3]), city_name)

            response = data[0]
            count = data[1]
            link = data[2]

            response_data = {
                'link': link,
                'count': count,
                'title': link_title,
                'results': response
            }
            return Response(response_data)


class HomePageCars(APIView):
    """
    An API View to get various cars in recommendation sections present on Home Page
    """

    attachments_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
    config = get_cdn_url()

    def get_response_data(self, queryset):
        queryset = ListingSerializer.setup_eager_loading(queryset)
        listings_data = ListingSerializer(
            queryset,
            context={'show_one_url': True, 'cdn_url': self.config, 'attachment_qs': self.attachments_qs,
                     'super_saver_car_ids': get_super_saver_car_ids(),
                     'is_user_logged_in': not isinstance(self.request.user, AnonymousUser), 'request': self.request},
            many=True
        )

        return listings_data.data

    @staticmethod
    def get_final_data(link_suffix, count, title, results):
        return {
            'link': link_suffix,
            'count': count,
            'title': title,
            'results': results
        }

    @staticmethod
    def get_top_trending_cars(city_name, model_order):
        top_trending_qs = Model.objects.filter(id__in=TRENDING_MODELS)
        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(model_order)])
        ordered_trending_models = top_trending_qs.order_by(preserved)

        response = []
        for model in ordered_trending_models:
            show_price = "{:.1f}".format(math.ceil(model.cutoff_price / float(10000)) / 10)
            price_in_lakhs = int(float(show_price) * 100000)
            filter_in_link = '/?price_max=' + str(price_in_lakhs) + '&model='
            models_in_filter = str(model.id)
            if model.connected_models:
                model_list = re.split('\W+', model.connected_models, flags=re.UNICODE)
                list_of_model_strings = [str(n) for n in model_list]
                models_in_filter += ',' + ','.join(map(str, list_of_model_strings))

            filter_in_link += models_in_filter
            link = 'used-cars-in-' + city_name.lower() + filter_in_link
            response.append({
                'model_id': model.id,
                'name': model.name,
                'cutoff_price': unit_converter(model.cutoff_price),
                'link': link,
                'actual_cutoff_price': model.cutoff_price
            })
        return response

    def get_newly_added_matching_cars(self, recommended_cars):
        listing_qs = Listing.objects.filter(id__in=recommended_cars, status__in=LISTING_ACTIVE_STATUS).order_by(
            '-created_at')[:3]
        recommendation_listing_result_data = self.get_response_data(listing_qs)
        return recommendation_listing_result_data, len(listing_qs)

    def get_value_for_money_cars(self, listings):
        listing_qs = Listing.objects.filter(pk__in=listings, price__gt=200000, price__lt=400000, is_inventory=True,
                                            status__in=LISTING_ACTIVE_STATUS)[:3]
        recommendation_listing_result_data = self.get_response_data(listing_qs)
        return recommendation_listing_result_data, len(listing_qs)

    def get_top_rated_cars(self, listings):
        listing_qs = Listing.objects.filter(pk__in=listings, rating__gte=4.5, is_inventory=True,
                                            status__in=LISTING_ACTIVE_STATUS).order_by('-rating')[:3]
        recommendation_listing_result_data = self.get_response_data(listing_qs)
        return recommendation_listing_result_data, len(listing_qs)

    def get_premium_cars(self, listings):
        listing_qs = Listing.objects.filter(pk__in=listings, is_inventory=True, price__gt=900000,
                                            status__in=LISTING_ACTIVE_STATUS).order_by('-price')[:3]
        recommendation_listing_result_data = self.get_response_data(listing_qs)
        return recommendation_listing_result_data, len(listing_qs)

    @staticmethod
    def get_link_suffix(city_name):
        return '/used-cars-in-' + city_name.lower()

    @staticmethod
    def get_link(link_prefix, listing_id_list):
        if link_prefix and listing_id_list:
            return link_prefix + ",".join([str(car_id) for car_id in listing_id_list])
        elif link_prefix and not listing_id_list:
            return link_prefix

    def get(self, request, format=None):
        """
        An API View to get similar cars

        ---

        Methods:
             - GET:
                parameters:
                    - name: cars_seen
                      description: Listing IDs of the cars seen by user
                      required: false
                      type: list
                    - name: sellers_contacted
                      description: Listing IDs of the cars contacted by user
                      required: false
                      type: list
                    - name: city_id
                      description: City ID
                      required: false
                      type: integer

                consumes:
                    - application/json
                    - application/xml
                produces:
                    - application/json
                    - application/xml

        """

        city_id = request.query_params.get('city_id', None)
        if city_id:
            city_name = City.objects.get(id=city_id).name
        else:
            return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)

        slug = self.get_link_suffix(city_name)
        popular_models_order = get_popular_model_order(city_id)
        top_trending_data = self.get_top_trending_cars(city_name, popular_models_order)

        params = get_recommendation_params(self.request)
        if (params['seen_car_ids'] is None or len(params['seen_car_ids'])) < 3 and not params['buyer_id']:
            city_popular_listings = get_popular_recommendation(city_id)['list'][:200]
            value_for_money_cars_data, cars_count = self.get_value_for_money_cars(city_popular_listings)
            value_for_money_cars = self.get_final_data(slug + '?price_min=200000&price_max=400000',
                                                       cars_count, 'Value for money cars picked for you',
                                                       value_for_money_cars_data)

            top_rated_cars_data, car_count = self.get_top_rated_cars(city_popular_listings)
            top_rated_cars = self.get_final_data(slug + '?truescores=4.5', car_count, 'Top Rated by Truebil',
                                                 top_rated_cars_data)

            premium_cars_data, premium_car_count = self.get_premium_cars(city_popular_listings)
            premium_cars = self.get_final_data(slug + '?price_min=900000', premium_car_count,
                                               'Premium used cars', premium_cars_data)

            return Response(({'status': True,
                              'top_trending': top_trending_data,
                              'popular_cars': random.sample(top_trending_data, len(top_trending_data)),
                              'is_revisited_user': False,
                              'value_for_money_cars': value_for_money_cars,
                              'top_rated_by_truebil': top_rated_cars,
                              'premium_used_cars': premium_cars
                              }))
        else:
            recommended_cars = get_recommendations(params)['list'][:200]
            actual_number_of_cars_recommended = len(recommended_cars)

            listing_qs = Listing.objects.filter(id__in=recommended_cars[:3], status__in=LISTING_ACTIVE_STATUS)
            recommendation_response = self.get_response_data(listing_qs)

            all_recommendation_data = self.get_final_data(slug, actual_number_of_cars_recommended,
                                                          "All Recommendations", recommendation_response)

            value_for_money_cars_data, cars_count = self.get_value_for_money_cars(recommended_cars)
            value_for_money_cars = self.get_final_data(slug + '?price_min=200000&price_max=400000',
                                                       cars_count, 'Value for money cars picked for you',
                                                       value_for_money_cars_data)

            newly_added_cars, cars_count = self.get_newly_added_matching_cars(recommended_cars)
            newly_added_cars_response = self.get_final_data(slug + '?sort=dd', cars_count,
                                                            "Newly added cars you’ll like", newly_added_cars)
            return Response(({'status': True,
                              'our_recommendations_for_you': all_recommendation_data,
                              'is_revisited_user': True,
                              'popular_search_in_budget': random.sample(top_trending_data, len(top_trending_data)),
                              'newly_added_cars_you_will_like': newly_added_cars_response,
                              'value_for_money_cars_picked_for_you': value_for_money_cars
                              }))


class SubheaderFiltersAPIView(APIView):
    def get(self, request, format=None):
        """
        Returns subheader filters

        ---

        parameters:
            - name: city_id
            - description: ID of the city
            - required: false
            - type: integer

        responseMessages:
            - code: 400
              message: City does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        serializer_class = SubheaderFiltersSerializer
        try:
            city_id = request.query_params.get('city_id', '')
            filter_city = False
            city = City
            if city_id:
                city = City.objects.get(id=city_id)
                filter_city = True
        except City.DoesNotExist:
            response_data = {'success': False, 'message': INVALID_CITY}
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)

        subheader_filters_data = get_serializer_data(request, serializer_class, city, filter_city)
        response_data = {'success': True, 'filters': subheader_filters_data}
        return Response(response_data)


class SimilarTestDriveCars(APIView):
    """
    An API View to get similar cars for test drive
    """

    def get(self, request, format=None):
        """
        An API View to get similar cars for test drive

        ---

        Methods:
             - GET:
                parameters:
                    - name: city_id
                      description: City ID
                      required: false
                      type: integer

                consumes:
                    - application/json
                    - application/xml
                produces:
                    - application/json
                    - application/xml

            """

        params = get_recommendation_params(request)
        buyer_visit_obj = None

        if type(self.request.user) is AnonymousUser:
            return Response(get_error_response(USER_NOT_LOGGED_IN), status=status.HTTP_400_BAD_REQUEST)

        if params['buyer_id']:
            buyer_visit_obj = BuyerVisit.objects.filter(buyer_id=params['buyer_id']).order_by('created_at')[:1]

        if params['city_id']:
            try:
                city_name = City.objects.get(id=params['city_id']).name
            except City.DoesNotExist:
                return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)
        elif buyer_visit_obj:
            city_name = buyer_visit_obj.city.name
        else:
            return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)

        recommended_car_ids = get_recommendations(params)['list']
        if buyer_visit_obj:
            all_listings_in_buyer_visit = set(BuyerVisitListing.objects.filter(buyer_visit=buyer_visit_obj,
                                                                               is_active=True).values_list('listing_id',
                                                                                                           flat=True))
            recommended_car_ids = [listing_id for listing_id in recommended_car_ids if listing_id not in
                                   all_listings_in_buyer_visit]

        recommended_test_drive_listing_qs = Listing.objects.filter(pk__in=recommended_car_ids, is_inventory=True,
                                                                   status__in=LISTING_ACTIVE_STATUS)[:10]
        attachments_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
        recommended_test_drive_listing_qs = ListingSerializer.setup_eager_loading(recommended_test_drive_listing_qs)
        recommended_test_drive_listing_data = ListingSerializer(recommended_test_drive_listing_qs, context={
            'cdn_url': get_cdn_url(), 'attachment_qs': attachments_qs}, many=True)
        home_obj = HomePageCars()
        similar_cars_link = home_obj.get_link_suffix(city_name)

        data = {
            'status': True,
            'link': similar_cars_link,
            'count': len(recommended_test_drive_listing_qs),
            'title': "Test Drive Recommendations",
            'results': recommended_test_drive_listing_data.data
        }
        return Response(data)


class CarSelectionView(APIView):
    """
        An API View to get Quote from the Seller Page
    """
    permission_classes = [IsAuthenticatedOrReadOnly, ]

    def get(self, request):
        """
        API View to feed the Sell page search .
        ---

        parameters:
            - none
        responseMessages:
            - code: 200
              status: true
              message: returns a dictionary of make-model-variant ,owner, year . Depending
                       on the status of the inspection scheduled returns inspection status,
                      inspection details and inspector, details if assigned .
        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml

        """
        user = request.user
        response = dict()

        if type(user) is not AnonymousUser:
            entry_in_seller = Seller.objects.filter(mobile=user.mobile).order_by('-created_at').first()

            if entry_in_seller:
                response['image_url'] = get_sellerpage_listing_image(user.mobile)

            entry_in_seller_visit = InterestedSellerListings.objects.filter(~Q(seller_visit=None),
                                                                            interested_seller__mobile=user.mobile). \
                order_by('-created_at').first()

            if entry_in_seller_visit:
                seller_visit_obj = entry_in_seller_visit.seller_visit
                current_time = get_timezone_aware_current_datetime()

                timezone = pytz.timezone('Asia/Kolkata')
                temp_end_of_inspection_time = timezone.localize(
                    datetime.datetime.strptime(str(seller_visit_obj.visit_date), '%Y-%m-%d'))
                end_of_inspection_time = timezone.localize(datetime.datetime.combine(temp_end_of_inspection_time,
                                                                                     seller_visit_obj.visit_time_slot.end_time))
                processing_period = end_of_inspection_time + timedelta(days=10)

                if seller_visit_obj.status == 'booked':

                    if end_of_inspection_time > current_time:
                        response['inspection_details'] = get_seller_visit_info(seller_visit_obj)
                        response['is_inspected'] = True
                        return Response(response)
                    elif end_of_inspection_time < current_time < processing_period:
                        response['status'] = True
                        response['car_inspection_done'] = True
                        return Response(response, status=status.HTTP_200_OK)
                    elif current_time > processing_period:
                        pass

                # this else will be used later when reschedule and cancellation will be done
                else:
                    pass

        response['make'] = get_make_model_variants()
        response['year'] = range(2001, date.today().year + 1)
        response['owner'] = [1, 2, 3]
        response['is_inspected'] = False
        return Response(response, status=status.HTTP_200_OK)

    def post(self, request):
        """
        An API View to show the 'Predicted Price' for a seller's car
        ---

        parameters:
            - name: variant_id
              description: Variant Id
              required: true
              type: integer
            - name: year
              description: Year of the car
              required: true
              type: integer
            - name: mileage
              description: Km driven
              required: true
              type: integer
            - name: owner
              description: Tells the owner number for the car
              required: true
              type: integer

        responseMessages:
            - code: 201
              status: true
              message: the price engine returned null
            - code: 200
              status:201
              message: returns a dictionary including predicted price, count of interested buyers ,
                       seller's car information
            - code: 400
              status: false
        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        variant_id = request.data.get('variant_id')
        year = request.data.get('year')
        mileage = request.data.get('mileage')
        owner = request.data.get('owner')
        response = dict()

        user = request.user
        interested_seller_object = InterestedSeller.objects.filter(mobile=user.mobile)

        if interested_seller_object.exists():
            interested_seller_object = interested_seller_object[0]
        else:
            interested_seller_object = InterestedSeller.objects.create(
                name=user.name,
                email=user.email,
                mobile=user.mobile,
                city=user.city
            )
        try:
            variant = Variant.objects.get(id=variant_id)
            color = Color.objects.get(name='White')
        except (Variant.DoesNotExist, Color.DoesNotExist):
            return Response({'success': False}, status=status.HTTP_400_BAD_REQUEST)

        now = timezone.now()
        three_days_duration = timezone.now() - timedelta(days=7)
        buyer_queryset = BuyerListing.objects.filter(listing__variant__model=variant.model.id,
                                                     created_at__range=(three_days_duration, now))
        count_of_buyers = max(buyer_queryset.count(), random.randint(15, 25))
        price_list = price_engine(variant.id, year, mileage, owner, color.name, user.city_id)
        car_name_for_info = variant.name.replace(variant.model.make.name, '').lstrip()
        model_name = variant.model.name

        response['buyer_interest'] = {'count': count_of_buyers, 'car_model': model_name, 'days': 3}
        response['info'] = {'year': year, 'car': car_name_for_info, 'mileage': mileage}

        if price_list:
            type_of_price = list()
            for types in price_list:
                type_of_price.append(str(types[0]) + '-' + str(types[1]))

            interested_seller_listing = InterestedSellerListings.objects.create(
                interested_seller=interested_seller_object,
                variant=variant, mileage=mileage,
                year=year, owner=owner,
                color=color, rough=type_of_price[0],
                ok=type_of_price[1], excellent=type_of_price[2]
            )
            response['predicted_price'] = get_price_engine_response(price_list)

        else:
            interested_seller_listing = InterestedSellerListings.objects.create(
                interested_seller=interested_seller_object,
                variant=variant, mileage=mileage,
                year=year, owner=owner,
                color=color)
            response['message'] = 'The price engine returned null'

        spreadsheet_data = list()
        spreadsheet_data.append(get_seller_spreadsheet_data(user, interested_seller_listing))
        mailer_data = prepare_mailer_data(spreadsheet_data=spreadsheet_data)
        add_gearman_data('mailer', mailer_data)
        response['status'] = True
        return Response(response, status=status.HTTP_201_CREATED)


class PriceEngineAPIView(APIView):

    def get(self, request):
        """
        An API View to show the 'Raw Predicted Price' of a car
        ---

        parameters:

            - name: listing_id
              description: Listing Id
              required: true
              type: integer

             When listing_id is not given

            - name: variant_id
              description: Variant Id
              required: true
              type: integer
            - name: year
              description: Year of the car
              required: true
              type: integer
            - name: mileage
              description: Km driven
              required: true
              type: integer
            - name: owner
              description: Tells the number of owners for the car
              required: true
              type: integer
            - name: color_id
              description: Color_id of the car
              required: true
              type: integer
            - name: city_id
              description: City_id to which the car belongs to
              required: true
              type: integer
            - name: rating
              description: Condition of the car
              required: true
              type: integer

        responseMessages:
            - code: 400
              message: Required field not entered.
            - code: 200
              message: Successfully predicted the price.
        """
        listing_id = request.query_params.get('listing_id', None)
        variant_id = request.query_params.get('variant_id', None)
        year = request.query_params.get('year', None)
        mileage = request.query_params.get('mileage', None)
        owner = request.query_params.get('owner', None)
        color_id = request.query_params.get('color_id', None)
        city_id = request.query_params.get('city_id', None)
        rating = request.query_params.get('rating', None)

        response = dict()
        if listing_id:
            if Listing.objects.filter(pk=listing_id).exists() is False:
                return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)
        elif None in [variant_id, year, mileage, owner, color_id, city_id, rating]:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        if listing_id:
            price = listing_price_prediction(listing_id, raw_price=True)
            response['procurement_score'] = get_listing_procurement_score(listing_id)[0]
            response['refurbishment_cost'] = unit_converter(get_listing_refurbishment_cost(listing_id))
        else:
            color = Color.objects.values('name').get(pk=color_id)
            price_sets = price_engine(variant_id, year, mileage, owner, color['name'], city_id)
            price = get_raw_price(price_sets, rating) if price_sets else None

        response['status'] = True
        response['predicted_price'] = price
        response['predicted_price_shorthand'] = unit_converter(price)
        return Response(response, status=status.HTTP_200_OK)

