# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import Accessory


class AccessoryAdmin(admin.ModelAdmin):
    class Meta:
        model = Accessory

admin.site.register(Accessory, AccessoryAdmin)
