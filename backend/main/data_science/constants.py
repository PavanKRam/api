LISTING_DETAILS_QUERY = """(SELECT price,mileage,year(manufacturing_date) AS year FROM listings WHERE id = %s)"""

MODELWISE_LISTING_PRICES_QUERY = """(SELECT listings.id,listings.price, variants.model_id, listings.is_inventory FROM 
listings JOIN variants ON listings.variant_id = variants.id )"""

MODEL_INVENTORY_COUNT_QUERY = """(SELECT model_id,count(*) AS inventory_count FROM listings JOIN variants ON 
variants.id = listings.variant_id WHERE is_inventory=1 and status='active' GROUP BY model_id)"""

LISTING_FEATURES_QUERY = """(SELECT l.variant_id,l.carwale_price,year(l.manufacturing_date) AS year,l.mileage,owners,
 c.name AS color,lo.city_id,l.rating FROM listings l JOIN localities lo ON l.locality_id=lo.id JOIN colors c ON
  l.color_id = c.id WHERE l.id = %s )"""

AUCTION_IDS_DETAILS_QUERY = """(SELECT listing_auctions.listing_id,dealer_listing_bids.dealer_id,
 dealer_listing_bids.amount,users.name, users.mobile FROM listing_auctions JOIN dealer_listing_bids ON 
 listing_auctions.id=dealer_listing_bids.listing_auction_id JOIN dealers ON dealers.id=dealer_listing_bids.dealer_id 
 JOIN users ON dealers.user_id = users.id WHERE listing_auctions.listing_id = %s)"""

REFURBISHMENT_COST_QUERY = """(SELECT listing_errors.listing_id, ifnull(component_errors.refurbishment_cost,0) as 
refurbishment_cost FROM component_errors JOIN listing_errors ON component_errors.inspection_component_id = 
listing_errors.inspection_component_id AND component_errors.inspection_error_id = listing_errors.inspection_error_id
 WHERE listing_errors.listing_id = %s)"""