# -*- coding: utf-8 -*-

import math
from datetime import datetime

import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler

from constants import (LISTING_DETAILS_QUERY, MODELWISE_LISTING_PRICES_QUERY, MODEL_INVENTORY_COUNT_QUERY,
                       LISTING_FEATURES_QUERY, AUCTION_IDS_DETAILS_QUERY, REFURBISHMENT_COST_QUERY)
from ds_helpers import (get_orp_json, get_price_coeff_json, get_pkl_file_data, get_dealer_details_csv,
                        get_model_demand_factor_csv, pandas_cached_query)
from main.quick_ops import get_timezone_aware_current_datetime


def price_engine(variant_id, year, mileage, owner, color, city_id):
    # type: (int, int, int, int, str, int) -> list or None
    """
        :rtype: list: list of predicted price range on success else None
        :param variant_id: car variant id
        :param year: car manufactured year
        :param mileage: car mileage
        :param owner: car owner in number
        :param color: car color
        :param city_id: user city id
        :return:
    """

    orp_json_data = get_orp_json()
    price_coeff_json_data = get_price_coeff_json()

    try:
        # Extracting orp, model_name, fuel_type, transmission  for the given variant
        orp = orp_json_data[str(variant_id)]['ORP']
        model_name = orp_json_data[str(variant_id)]['model_name']
        fuel_type = orp_json_data[str(variant_id)]['fuel_type']
        transmission = orp_json_data[str(variant_id)]['transmission']
        coeff = price_coeff_json_data[model_name]
    except KeyError:
        return None

    # import scaler pickel file
    pkl_file_name = str(model_name).replace(' ', '') + '.pkl'
    scaler = get_pkl_file_data(pkl_file_name)
    if not scaler:
        return None

    # age function
    beta = coeff['beta']

    current_year = get_timezone_aware_current_datetime().year
    age = current_year - int(year)

    if age < 0:
        return None
    age_rv = math.exp(beta * age)

    # mileage function
    avg_margin = 0
    if fuel_type == 'Petrol':
        avg_margin = 12000
    elif fuel_type == 'Diesel':
        avg_margin = 14000
    else:
        return None

    # Rounding of Mileage in multiples of 1000's
    mileage = float(mileage)
    mileage /= 1000
    mileage = math.floor(mileage)
    mileage *= 1000
    mileage_cons_1 = mileage - (float(avg_margin) * age)

    # owner function
    owner = int(owner)

    # color function
    color = str(color)
    color_high = 0
    if color in ['White', 'Silver', 'Black']:
        color_high = 1

    # data normalization
    data_1 = [[age_rv, mileage_cons_1, owner, color_high, 1, 0]]
    norm_data_1 = scaler.transform(data_1)

    data_2 = [[age_rv, mileage_cons_1, owner, color_high, 0, 0]]
    norm_data_2 = scaler.transform(data_2)

    data_3 = [[age_rv, mileage_cons_1, owner, color_high, 0, 1]]
    norm_data_3 = scaler.transform(data_3)

    orp = int(orp)

    # onRoadPrice updation based on city_id   Delhi ->2 , Banglore -> 5
    city_id = int(city_id)
    if city_id == 5:
        orp *= 1.1
    elif city_id == 2:
        orp *= 0.9


    # reduction calculation excellent
    reduct_value_1 = coeff['constant'] + coeff['age'] * norm_data_1[0][0] + \
                     coeff['mlc'] * norm_data_1[0][1] + \
                     coeff['owner'] * norm_data_1[0][2]  + \
                     coeff['colorH'] * norm_data_1[0][3] + \
                     coeff['excellent'] * norm_data_1[0][4] + coeff['fair'] * norm_data_1[0][5]

    # reduction calculation good
    reduct_value_2 = coeff['constant'] + coeff['age'] * norm_data_2[0][0] + \
                     coeff['mlc'] * norm_data_2[0][1] + \
                     coeff['owner'] * norm_data_2[0][2]  + \
                     coeff['colorH'] * norm_data_2[0][3] + \
                     coeff['excellent'] * norm_data_2[0][4] + coeff['fair'] * norm_data_2[0][5]

    # reduction calculation bad
    reduct_value_3 = coeff['constant'] + coeff['age'] * norm_data_3[0][0] + \
                     coeff['mlc'] * norm_data_3[0][1] + \
                     coeff['owner'] * norm_data_3[0][2] + \
                     coeff['colorH'] * norm_data_3[0][3] + \
                     coeff['excellent'] * norm_data_3[0][4] + coeff['fair'] * norm_data_3[0][5]

    # Variable to ensure that price_ranges will never be negative nor below a specified range
    temp = 0.1
    if reduct_value_3 < temp or reduct_value_2 < temp or reduct_value_1 < temp:
        reduct_value_1 = temp + 0.05
        reduct_value_2 = temp + 0.03
        reduct_value_3 = temp

    # Variable to ensure that Price is never greater than ORP (for rare corner cases)
    # Also ensures that price changes with year when other features are kept constant
    if reduct_value_1 >= 1 or reduct_value_2 >= 1 or reduct_value_3 >= 1:
        reduct_value_1 = 0.95 * (math.exp(-(float(age) / (float(current_year) - float(2000)))))
        reduct_value_2 = reduct_value_1 - 0.04
        reduct_value_3 = reduct_value_2 - 0.04

    # Final prices calculation
    final_price_1 = orp * reduct_value_1
    final_price_2 = orp * reduct_value_2
    final_price_3 = orp * reduct_value_3

    prices = [0.95 * final_price_1, 0.95 * final_price_2, 0.95 * final_price_3]

    price_ranges = []

    for price in prices:
        price_ranges.append(tuple([price - (0.05 * price), price + (0.05 * price)]))

    price_ranges = sorted(price_ranges)

    return price_ranges


def get_listing_potential_dealers(listing_id):
    """
    select top potential dealers matching with the features of the listing id

    :param listing_id: listing_id (integer)
    :return: potential_dealers_list: potential dealers (list)

    """

    listing_details = pandas_cached_query(LISTING_DETAILS_QUERY % listing_id)
    price = [float(listing_details['price'][0]) / 100000]
    mileage = [listing_details['mileage'][0]]
    present_year = datetime.now().year
    age = [present_year - listing_details['year'][0]]

    dealer_details = pd.read_csv(get_dealer_details_csv())

    item_features = pd.DataFrame()
    item_features['price_slab'] = pd.cut(price, [0.0, 2.0, 4.0, 6.0, 8.0, float("inf")])
    item_features['mileage_slab'] = pd.cut(mileage, [0, 100000, float("inf")])
    item_features['age_slab'] = pd.cut(age, [0, 5, 10, 15])
    item_features = pd.get_dummies(item_features,
                                   columns=['price_slab', 'mileage_slab', 'age_slab']).values.flatten()

    # Scores multiplication of listing features and dealers to select most preferred ones
    scores = np.array([])
    for dealer in range(0, dealer_details.shape[0]):
        dealer_features = dealer_details.iloc[dealer:dealer + 1, 0:-2].values.astype('float32').flatten()
        scores = np.append(scores, np.dot(dealer_features, item_features).astype('float32'))
    dealer_details['score'] = scores

    top_dealers = dealer_details.sort_values('score', ascending=False)
    dealers_list = []
    dealer_name_and_contacts = top_dealers[['Dealer_Contact_number', 'Dealer_Name']].values
    for dealer in dealer_name_and_contacts:
        potential_dealer = dict()
        potential_dealer['name'] = dealer[1]
        potential_dealer['mobile'] = dealer[0]
        dealers_list.append(potential_dealer)

    potential_dealers_list = dealers_list

    return potential_dealers_list


def get_listing_procurement_score(listing_id):
    """
    calculates procurement score of the listing

    :rtype list: list of original and modified procurement scores
    :param listing_id: listing_id (integer)
    :return: procurement_score : original procurement score
            listing_procurement_score : normalised procurement score

    """
    price_engine_price = listing_price_prediction(listing_id)
    refurbishment_cost = get_listing_refurbishment_cost(listing_id)

    listings = pandas_cached_query(MODELWISE_LISTING_PRICES_QUERY).fillna(0)
    model_inventory_count = pandas_cached_query(MODEL_INVENTORY_COUNT_QUERY)

    demand_score = pd.read_csv(get_model_demand_factor_csv(), usecols=[0, 4], names=['model_id', 'demand_value'])[
                   1:].dropna().astype('float64')
    absent_model_ids = pd.DataFrame(
        np.setdiff1d(listings['model_id'].drop_duplicates().values, demand_score['model_id'].values.astype('int32')),
        columns=['model_id'])
    absent_model_ids['Demand_Value'] = 0

    # Calculation of opportunity cost of a listing
    listings['opportunity_cost_initial'] = listings['price'] * 0.15
    listings['holdup_cost'] = listings['price'] * 0.01
    listings['capital_per_month'] = 0.01
    listings['cost_price'] = (listings['price'] - refurbishment_cost) * (1 - 0.15)
    listings['profit_margin'] = ((price_engine_price - listings['cost_price']) / listings['cost_price']) * listings[
        'price']
    listings['opportunity_cost'] = listings[["opportunity_cost_initial", "profit_margin"]].min(axis=1)
    listings['phy'] = (listings['opportunity_cost'] - listings['price'] * listings['capital_per_month']) / (
            listings['opportunity_cost'] + listings['holdup_cost'])

    demand_score['demand_value'] = demand_score['demand_value'] * 4
    demand = pd.concat([demand_score, absent_model_ids]).astype('float64')

    # Normalizing procurement scores to avoid negative values
    listing_details = listings.merge(demand, how='left', on='model_id')
    listing_details['optimum_inventory_count'] = listing_details.phy * listing_details.demand_value
    listing_details = listing_details.merge(model_inventory_count, how='left', on='model_id').fillna(0).drop_duplicates(
        subset=['id'])
    listing_details['listing_procurement_score'] = 0

    not_null_optimum_indices = listing_details[listing_details['optimum_inventory_count'] != 0].index.values
    listing_details.loc[not_null_optimum_indices, 'listing_procurement_score'] = \
        (1 - (listing_details.loc[not_null_optimum_indices, 'inventory_count']
              / listing_details.loc[not_null_optimum_indices, 'optimum_inventory_count']) ** 2) * \
        ((listing_details.loc[not_null_optimum_indices, 'optimum_inventory_count'] /
          max(listing_details['optimum_inventory_count'])) ** 0.33)

    negative_procurement_scores_listings = listing_details[listing_details['listing_procurement_score'] < 0]
    scaler = MinMaxScaler(feature_range=(0, 0.5))
    scaler.fit(negative_procurement_scores_listings['listing_procurement_score'].values.reshape(-1, 1))
    listing_details.loc[negative_procurement_scores_listings.index.values, 'listing_procurement_score'] = \
        scaler.transform(negative_procurement_scores_listings['listing_procurement_score'].values.reshape(-1, 1))
    procurement_score_val = \
    listing_details[listing_details['id'] == int(listing_id)]['listing_procurement_score'].fillna(0).values[0]

    # Modification of procurement score to maintain feasible price ranges
    modified_procurement_score = 0.85 if procurement_score_val < 0.85 else procurement_score_val
    procurement_scores = [modified_procurement_score, procurement_score_val]

    return procurement_scores


def get_raw_price(price_sets, rating):
    """
       Returns raw price corresponding to rating

     :param price_sets: list of tuples
     :param rating: float
     :return: price : integer

     """
    for i in range(len(price_sets)):
        price_value = (price_sets[i][0] + price_sets[i][1]) / 2
        price_value = float(price_value) / 0.95
        price_sets[i] = int(price_value)
    if rating <= 3.56:
        price = price_sets[0]
    elif rating <= 4:
        price = price_sets[1]
    else:
        price = price_sets[2]

    return price


def listing_price_prediction(listing_id, raw_price=False):
    """
    calculates predicted price of an existing listing

    :rtype list: list of tuples
    :param listing_id: listing_id (integer)
    :param  raw_price : True -if called from price_engine end point default False (boolean)
    :return: price_ranges : predicted prices

    """

    listing_details = pandas_cached_query(LISTING_FEATURES_QUERY % listing_id)

    listing_details = listing_details.loc[:,
                      ['variant_id', 'year', 'mileage', 'owners', 'color', 'city_id', 'rating', 'carwale_price'
                       ]].fillna(value={'variant_id': 803, 'mileage': 50000, 'color': 'Golden', 'city_id': 1,
                                        'owners': 1, 'year': 2014, 'rating': 3.5, 'carwale_price': 340000})

    variant_id = listing_details['variant_id'][0]
    year = listing_details['year'][0]
    mileage = listing_details['mileage'][0]
    owner = listing_details['owners'][0]
    color = listing_details['color'][0]
    city_id = listing_details['city_id'][0]
    carwale_price = listing_details['carwale_price'][0]

    price_sets = price_engine(variant_id, year, mileage, owner, color, city_id)

    if price_sets is None:
        price = None if raw_price else carwale_price
    else:
        price = get_raw_price(price_sets, listing_details['rating'][0])

    if raw_price:
        return price

    suggested_price_increment_factor = 0.12
    suggested_price = int(price * (1 + suggested_price_increment_factor))

    return suggested_price


def auction_id_details(listing_id):
    """
    returns auction details of the listing id

    :rtype data frame
    :param listing_id: listing_id (integer)
    :return: auction_id_data: auction details of the listing id

    """
    try:
        auction_id_data = pandas_cached_query(AUCTION_IDS_DETAILS_QUERY % listing_id)
    except IndexError:
        auction_id_data = pd.DataFrame(columns=['listing_id', 'dealer_id', 'amount', 'name', 'mobile'])

    return auction_id_data


def get_listing_top_offers(listing_id):
    """
    return top offers of dealers on the listing id

    :rtype dictionary of dealer details
    :param listing_id: listing_id (integer)
    :return: top_offers_dict: top offers on the listing
    """

    auction_ids_details = auction_id_details(listing_id)
    top_offers_dict = []

    for dealer in np.unique(auction_ids_details['dealer_id'].values):
        dealer_dict = {}
        dealer_details = auction_ids_details[auction_ids_details['dealer_id'] == dealer][['amount', 'name', 'mobile']]
        dealer_dict['name'] = dealer_details['name'].values[0]
        dealer_dict['price'] = max(dealer_details['amount'].values)
        dealer_dict['mobile'] = dealer_details['mobile'].values[0]

        top_offers_dict.append(dealer_dict)

    return top_offers_dict


def get_listing_seller_price(listing_id):
    """
    calculates price to be shown to the seller

    :param listing_id: listing_id (integer)
    :return: seller_show_price : price to be shown to the seller (integer)

    """
    auction_ids_details = auction_id_details(listing_id)
    start_bidding_price_val = get_listing_start_bidding_price(listing_id)

    if auction_ids_details.empty is False:
        auction_id = auction_ids_details['listing_id'][0]
        auction_id_prices = auction_ids_details[auction_ids_details['listing_id'] == auction_id]['amount'].values
        auction_id_prices = -np.sort(-auction_id_prices)[:3]
        price_division_factor = [0.7, 0.2, 0.1]
        factor_sum = sum(price_division_factor[auction_id_prices.size - 1:3])
        if factor_sum != 0:
            price_division_factor[auction_id_prices.size - 1:] = [factor_sum]

        price = np.multiply(auction_id_prices, price_division_factor).astype('int')
        auction_price = np.sum(price)
        dealer_show_price = int(0.88 * auction_price)
        seller_show_price = int(max(start_bidding_price_val, dealer_show_price))

    else:
        seller_show_price = start_bidding_price_val

    if math.isnan(seller_show_price):
        seller_show_price = 0

    return seller_show_price


def get_listing_refurbishment_cost(listing_id):
    """
    calculates overall refurbishment cost of a listing

    :param listing_id: listing_id (integer)
    :return: refurbishment_cost: cost of refurbishment of the listing (integer)
    """
    listing_refurbishment_details = pandas_cached_query(REFURBISHMENT_COST_QUERY % listing_id)
    try:
        refurbishment_cost = listing_refurbishment_details.groupby(
            'listing_id')['refurbishment_cost'].sum().reset_index()['refurbishment_cost'][0]
    except IndexError:
        refurbishment_cost = 0

    return refurbishment_cost


def get_listing_start_bidding_price(listing_id):
    """
    calculates start bidding price on a listing

    :param listing_id: listing_id (integer)
    :return: start_bidding_price_val: start bidding price of a listing(integer)
    """
    price_engine_price = listing_price_prediction(listing_id)
    refurbishment_cost = get_listing_refurbishment_cost(listing_id)
    procurement_score_data = get_listing_procurement_score(listing_id)
    procurement_worthiness_index = procurement_score_data[0]

    start_bidding_price_val = int(0.92 * procurement_worthiness_index * (price_engine_price - refurbishment_cost))
    if math.isnan(start_bidding_price_val): start_bidding_price_val = 0

    return start_bidding_price_val


