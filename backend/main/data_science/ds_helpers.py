# -*- coding: utf-8 -*-
"""
Write helper functions used in machine_learning.functions here
"""
import json
from hashlib import sha1

import pandas as pd
from cachalot.settings import Settings
from django.core.cache import cache
from django.db import connection
from sklearn.externals import joblib

from server.settings import BASE_DIR


def get_file_data(file_path, is_json=False):
    try:
        with open(file_path) as data:
            if is_json:
                return json.load(data)
            return data
    except (IOError, ValueError):
        return None


def get_orp_json():
    orp_file_path = BASE_DIR + '/main/data_science/trained_data/on_road_price.json'
    return get_file_data(orp_file_path, is_json=True)


def get_price_coeff_json():
    price_coeff_file_path = BASE_DIR + '/main/data_science/trained_data/pricing_coefficients.json'
    return get_file_data(price_coeff_file_path, is_json=True)


def get_pkl_file_data(pkl_filename):
    full_pkl_path = BASE_DIR + '/main/data_science/trained_data/pklcoeff/' + pkl_filename
    try:
        return joblib.load(full_pkl_path)
    except IOError:
        return None


def get_dealer_details_csv():
    dealer_details_csv_path = BASE_DIR + '/main/data_science/trained_data/dealer_details.csv'
    return dealer_details_csv_path


def get_model_demand_factor_csv():
    model_demand_factor_csv_path = BASE_DIR + "/main/data_science/trained_data/model_demand_factor.csv"
    return model_demand_factor_csv_path


def pandas_cached_query(query):
    key = sha1(query).hexdigest()
    if key in cache:
        return cache.get(key)
    else:
        data = pd.read_sql_query(query, connection)
        cache.set(key, data, timeout=Settings.CACHALOT_TIMEOUT)
        return data
