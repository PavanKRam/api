import datetime
import math
import re
import sys
import time
from datetime import datetime, timedelta
from urllib import unquote

import pytz
from django.db.models import Max, Min, Q
from django.utils import timezone
from intervaltree import IntervalTree

from truebil_management.models import Config
from user.models import BuyerListingOffer
from .data_science.ds_helpers import get_orp_json
from .models import Listing, ListingStatusTrack
from .utils.constants import LISTING_ACTIVE_STATUS
from .utils.filters_conf import HIGH_DEMAND_MODELS


def get_listing_min_max_price():
    active_listings_qs = Listing.objects.filter(status__in=LISTING_ACTIVE_STATUS)
    min_price_of_active_listings = active_listings_qs.aggregate(Min('price'))
    max_price_of_active_listings = active_listings_qs.aggregate(Max('price'))

    return min_price_of_active_listings['price__min'], max_price_of_active_listings['price__max']


def get_cdn_url():
    return Config.objects.get(key="cdnurl").value


def get_asset_url():
    return Config.objects.get(key="assetsurl").value


def get_date_difference_in_minutes(datetimestamp):
    # Convert to Unix timestamp
    d1_ts = time.mktime(datetimestamp.timetuple())
    d2_ts = time.mktime(timezone.now().timetuple())
    return int(d2_ts - d1_ts) / 60


def unit_converter(amount, shorthand_price=False):
    try:
        amount = float(amount)
    except (ValueError, TypeError):
        return None

    thousand = 'K'
    lakh = 'L' if shorthand_price else 'Lakh'

    if amount >= 10000000:
        return "{:,.2f}".format(amount / 10000000) + ' ' + 'Cr'
    elif amount >= 100000:
        if amount > 9999499:
            return '1 Cr'
        elif shorthand_price:
            return "{:,.2f}".format(amount / 100000) + lakh
        else:
            return "{:,.2f}".format(amount / 100000) + ' ' + lakh
    else:
        if amount > 99994:
            return '1' + ' ' + lakh
        elif shorthand_price and amount > 1000:
            return "{:,.2f}".format(amount / 1000) + thousand
        else:
            return "{:,}".format(int(amount))


def ordinal(number):
    suffixes = {1: 'st', 2: 'nd', 3: 'rd'}

    if 10 <= number % 100 <= 20:
        suffix = 'th'
    else:
        # keeping 'th' suffix as default
        suffix = suffixes.get(number % 10, 'th')

    return str(number) + suffix


def valid_mobile_number(mobile):
    return re.findall(r'^[6789]\d{9}$', str(mobile))


def get_timezone_aware_current_datetime():
    return datetime.now(pytz.timezone('Asia/Kolkata'))


def get_refurbished_date(date):
    """
    Convert date and return refurbishment date. Format date: 26June (Monday)
    """

    now = get_timezone_aware_current_datetime()
    date = date if date > now else now + timedelta(days=3)
    return {
        "formatted_date": date.strftime("%d %B") + " (" + date.strftime("%A") + ")",
        "date": date
    }


def get_recommended_car_price(is_eligible_inventory, carwale_price):
    """
    Set recommended car price
    """

    recommended_price = dict()
    if is_eligible_inventory:
        if carwale_price <= 500000:
            recommended_price['min'] = round(0.94 * carwale_price, -3)
            recommended_price['max'] = round(1.04 * carwale_price, -3)
        elif carwale_price <= 800000:
            recommended_price['min'] = round(0.95 * carwale_price, -3)
            recommended_price['max'] = round(1.03 * carwale_price, -3)
        else:
            recommended_price['min'] = round(0.96 * carwale_price, -3)
            recommended_price['max'] = round(1.02 * carwale_price, -3)
    else:
        recommended_price['min'] = round(0.85 * carwale_price, -3)
        recommended_price['max'] = round(0.9 * carwale_price, -3)

    return recommended_price


def get_buyer_offer(listing, buyer, invalid_numbers):
    if listing.status in ['active', 'refurbishing']:
        offer_data_on_listing = get_offer_data_on_listing(listing, invalid_numbers)
        offer_data = {
            "offer_count": offer_data_on_listing['offer_count'],
            "top_offer": offer_data_on_listing['max_offered_price'],
            "offered_by_me": None,
            "min_allowed_offer": offer_data_on_listing['min_allowed_offer']
        }

        if buyer:
            offer_on_listing = listing.offers.filter(buyer=buyer, is_active=True, is_verified=True)

            if offer_on_listing:
                offer_data["offered_by_me"] = offer_on_listing.latest('created_at').offered_price

        return offer_data
    else:
        return {
            "offer_count": None,
            "top_offer": None,
            "offered_by_me": None,
            "min_allowed_offer": None
        }


def get_offer_data_on_listing(listing, invalid_numbers):
    offers_on_listing = BuyerListingOffer.objects.filter(listing=listing, is_active=True, is_verified=True)
    relevant_offers_on_listing = offers_on_listing.exclude(buyer__mobile__in=invalid_numbers)

    offer_data = dict()
    if relevant_offers_on_listing.exists():
        offer_data['count'] = relevant_offers_on_listing.count()
        offer_data['max_offer'] = relevant_offers_on_listing.aggregate(Max('offered_price'))['offered_price__max']
        offer_data['max_offer_date'] = relevant_offers_on_listing.filter(offered_price=offer_data['max_offer']). \
            latest('created_at').created_at

    return get_make_offer(listing, offer_data)


def get_make_offer(listing, offer_data):
    offer_count = offer_data.get('count', 0)
    offer_count += (listing.id % 4) + 1
    max_recommeded_price = int(round(1.06 * listing.inventory_offered_price, -3)) if listing.inventory_offered_price \
        else int(round(0.9 * listing.carwale_price, -3))

    listing_price = listing.price
    truescore = listing.rating
    genuine_offer = offer_data.get('max_offer', 0)
    genuine_offer_date = offer_data.get('max_offer_date', None)
    price_factor_dict = IntervalTree()

    price_factor_dict[0: 200001] = {"factor": 0.7, "dummy_factor": 0.83}
    price_factor_dict[200001: 500001] = {"factor": 0.75, "dummy_factor": 0.88}
    price_factor_dict[500001: 1000001] = {"factor": 0.8, "dummy_factor": 0.89}
    price_factor_dict[1000001: 1500001] = {"factor": 0.85, "dummy_factor": 0.92}
    price_factor_dict[1500001: sys.maxint] = {"factor": 0.9, "dummy_factor": 0.94}

    factor = sorted(price_factor_dict[listing_price])[0].data['factor']
    dummy_factor = sorted(price_factor_dict[listing_price])[0].data['dummy_factor']

    if truescore >= 4.5:
        truescore_bracket = 1
    elif 3.5 < truescore < 4.5:
        truescore_bracket = 2
    else:
        truescore_bracket = 3

    is_car_favourable = listing.is_inventory or (truescore_bracket is 1) or \
                        (listing.variant.model.id in HIGH_DEMAND_MODELS and truescore_bracket is 2)

    dummy_max_offer = max(factor * listing_price, min(max_recommeded_price, dummy_factor * listing_price))

    listing_statuses = ListingStatusTrack.objects.filter(listing=listing,
                                                         status_to__in=['active', 'requestedInspection',
                                                                        'refurbishing'])

    if listing_statuses.exists():
        active_from = listing_statuses.latest('changed_at').changed_at
        active_days = abs((active_from - timezone.now()).days)
    else:
        active_days = timezone.now().day
    active_days = min(30, active_days) if is_car_favourable else min(active_days, 60)

    if active_days <= 5:
        offer_count += active_days * 2 if is_car_favourable else active_days
    elif 5 < active_days <= 15:
        offer_count += active_days if is_car_favourable else math.floor(active_days / 2)
    else:
        offer_count += math.floor(active_days / 2) if is_car_favourable else math.floor(active_days / 4)

    if genuine_offer >= dummy_max_offer:
        if genuine_offer_date is None or genuine_offer_date.date() != timezone.now().date():
            dummy_max_offer = genuine_offer + min(genuine_offer * 0.01, 5000)
        else:
            dummy_max_offer = genuine_offer

    max_offered_price = min(listing_price, dummy_max_offer)
    min_allowed_offer = listing_price * factor

    return {
        "offer_count": offer_count,
        "max_offered_price": get_rounded_offered_price(max_offered_price),
        "factor": factor,
        "min_allowed_offer": min_allowed_offer
    }


def get_rounded_offered_price(price):
    val = price % 1000

    if val == 0:
        return price
    else:
        if val < 500:
            return math.floor(price / 1000.0) * 1000
        else:
            return math.ceil(price / 1000.0) * 1000


def get_car_details(listing_id):
    listing = Listing.objects.get(id=listing_id)
    car_name = listing.variant.name
    listing_status = listing.status
    listing_registration_number = listing.registration_number

    return {
        'car_name': car_name,
        'listing_status': listing_status,
        'registration_number': listing_registration_number
    }


def get_cars_unavailable_for_test_drive(cars_added_to_test_drive, visit_date):
    refurbished_cars_added_to_test_drive = Listing.objects.filter(id__in=cars_added_to_test_drive,
                                                                  status='refurbishing')
    now = timezone.now()
    cars_unavailable_for_test_drive = refurbished_cars_added_to_test_drive.filter(
        Q(expected_live_date__gt=visit_date) | Q(expected_live_date__lt=now.date()))

    return cars_unavailable_for_test_drive


def haversine(point1, point2):
    """
    Wiki : https://en.wikipedia.org/wiki/Haversine_formula
    Calculate the great-circle distance between two points on the Earth surface.

    :input: two 2-tuples, containing the latitude and longitude of each point
    in decimal degrees.

    Example: haversine((45.7597, 4.8422), (48.8567, 2.3508))

    :output: Returns the distance between the two points.
    The default unit is kilometers.
    """
    avg_earth_radius = 6371  # in km
    # unpack latitude/longitude
    lat1, lng1 = point1
    lat2, lng2 = point2

    # convert all latitudes/longitudes from decimal degrees to radians
    lat1, lng1, lat2, lng2 = map(math.radians, (lat1, lng1, lat2, lng2))

    # calculate haversine
    lat = lat2 - lat1
    lng = lng2 - lng1
    hav = math.sin(lat * 0.5) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(lng * 0.5) ** 2
    # Great-circle distance between two point on sphere = 2R sin inverse(under-root haversine)
    distance = 2 * avg_earth_radius * math.asin(math.sqrt(hav))
    return distance


def find_between(s, first, last):
    try:
        start = s.index(first) + len(first)
        end = s.index(last, start)
        return s[start:end]
    except ValueError:
        return ""


def get_price_range_from_slug(relevant_slug):
    price_range = [int(slug) for slug in relevant_slug.split('-') if slug.isdigit()]
    price_range = [number * 100000 for number in price_range]
    response = {
        'price_min': '',
        'price_max': ''
    }

    if 'lakh' in relevant_slug:
        if re.search(r'\d+-lakh-to-\d+-lakh', relevant_slug):
            response['price_min'] = price_range[0]
            response['price_max'] = price_range[1]
        elif re.search(r'under-\d+-lakh', relevant_slug):
            response['price_max'] = price_range[0]
        elif re.search(r'\d+-lakh-above', relevant_slug):
            response['price_min'] = price_range[0]

    return response


def get_market_price(listing):
    orp_json = get_orp_json()
    depicted_market_price = listing.depicted_market_price
    try:
        orp = long(orp_json[str(listing.variant_id)]['ORP'] * 0.95)
        if depicted_market_price:
            return depicted_market_price if orp < listing.price else min(orp, depicted_market_price)
    except KeyError:
        pass

    return depicted_market_price


def get_price_engine_response(price_list):
    """
        Input list of tuples,
        Returns a dictionary of okay , good and excellent price for a car model ( min-max for each type)
    """
    temp_list = []
    price_type = ['okay', 'good', 'excellent']
    for price in price_list:
        price_dict = dict()
        price_dict['min'] = unit_converter(price[0], shorthand_price=True)
        price_dict['max'] = unit_converter(price[1], shorthand_price=True)
        temp_list.append(price_dict)
    predicted_price = dict(zip(price_type, temp_list))

    return predicted_price


def decode_to_utf_8(params):
    return unquote(params).decode('utf8')
