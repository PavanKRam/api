import numpy as np
from sklearn.metrics.pairwise import cosine_similarity

from main_helpers import get_city_super_saver_listings, get_city_listing_details
from server.utils import ModelData
from utils.constants import LISTING_ACTIVE_STATUS
from .models import Listing


def get_recommended_listings(predicted_listings, city_id, recommendation_type):
    """
    :param predicted_listings : recommended listings passed in order
    :param city_id: city_id (integer)
    :param recommendation_type: type of recommendation
    :return: list of sorted recommended listings
    """
    city_super_saver_listings = get_city_super_saver_listings(city_id)
    city_active_inventory_listings = ModelData.active_listings[int(city_id)]['inventory']
    inventory_similar_listings = predicted_listings[np.in1d(predicted_listings, city_active_inventory_listings)]
    marketplace_similar_listings = predicted_listings[~np.in1d(predicted_listings, city_active_inventory_listings)]

    if inventory_similar_listings.size < 20:
        top_inventory_size = 20
    else:
        buffer_inventory_size = 20 + 0.2 * (inventory_similar_listings.size - 20)
        top_inventory_size = int(buffer_inventory_size)

    top_inventory_similar_listings = inventory_similar_listings[:top_inventory_size]
    inventory_similar_listings = inventory_similar_listings[top_inventory_size:]

    marketplace_index_count = 0
    recommended_listings = np.array([], dtype='int')
    recommended_listings = np.append(recommended_listings, top_inventory_similar_listings)

    for j in range(inventory_similar_listings.shape[0]):
        recommended_listings = np.append(recommended_listings, marketplace_similar_listings[marketplace_index_count:
                                                                                            marketplace_index_count + 4]
                                         )
        recommended_listings = np.append(recommended_listings, inventory_similar_listings[j])
        marketplace_index_count = marketplace_index_count + 4

    recommended_listings = np.append(recommended_listings, marketplace_similar_listings[marketplace_index_count:])

    super_saver_index_count = 0
    sorted_recommended_listings = np.array([], dtype='int32')

    if city_super_saver_listings:
        for g in range(len(city_super_saver_listings)):
            sorted_recommended_listings = np.append(sorted_recommended_listings, recommended_listings
                                                    [super_saver_index_count:super_saver_index_count + 3])
            sorted_recommended_listings = np.append(sorted_recommended_listings, city_super_saver_listings[g])
            super_saver_index_count = super_saver_index_count + 3

    else:
        sorted_recommended_listings = recommended_listings

    sorted_recommended_listings = np.append(sorted_recommended_listings, recommended_listings[super_saver_index_count:])

    return {'list': list(sorted_recommended_listings), 'type': recommendation_type}


def get_personal_recommendation(buyer_id, city_id, direct_only=False):
    """
    :param city_id: city_id (integer)
    :param buyer_id : buyer_id (integer)
    :param direct_only: recommends for only inventory cars
    :return: list of ids of personalised recommendations
    """
    trained_model = ModelData.trained_model
    item_features = ModelData.item_features
    user_features = ModelData.user_features
    city_active_inventory_listings = ModelData.active_listings[int(city_id)]['inventory']
    city_active_marketplace_listings = ModelData.active_listings[int(city_id)]['marketplace']

    # Predicting Scores
    city_inventory_scores = trained_model.predict(buyer_id, city_active_inventory_listings, item_features=
                                                  item_features, user_features=user_features)
    city_inventory_ids = city_active_inventory_listings[np.argsort(-city_inventory_scores)]

    if direct_only:
        return {'list': city_inventory_ids, 'type': 'personal'}

    city_marketplace_scores = trained_model.predict(buyer_id, city_active_marketplace_listings, item_features=
                                                    item_features, user_features=user_features)
    city_marketplace_ids = city_active_marketplace_listings[np.argsort(-city_marketplace_scores)]
    all_city_listings = np.append(city_inventory_ids, city_marketplace_ids)

    return get_recommended_listings(all_city_listings, city_id, 'personal')


def get_popular_recommendation(city_id):
    """
    :param city_id: city_id (integer)
    :return: list of ids of popular recommendations
    """
    return {'list': list(ModelData.popular_listings[int(city_id)]['popular_listings']), 'type': 'popular'}


def get_popular_model_order(city_id):
    """
    :param city_id: city_id (integer)
    :return: recommended order of popular models in the city
    """
    return ModelData.popular_listings[int(city_id)]['popular_models']


def get_similar_cars(listing, city_id, direct_only=False):
    """
    :param listing: listing_id (integer)
    :param city_id: city_id (integer)
    :param direct_only: listings needed direct promotion
    :return: list of ids of similar recommendations
    """
    listing_status = listing['status']
    listing_id = listing['id']

    city_active_listings = get_city_listing_details(city_id, listing_id=listing_id)
    listing_car_features = city_active_listings[0]

    if listing_status not in LISTING_ACTIVE_STATUS:
        listing_car_features = get_city_listing_details(city_id, listing_id=listing_id, status=listing_status)
    else:
        city_active_listings = city_active_listings[1:]

    city_active_listings_features = np.array(city_active_listings)[:, 2:17]
    listing_car_features = np.array([listing_car_features[2:17]])
    city_active_listings_ids = np.array(city_active_listings)[:, 0]

    similarity_indices = np.argsort(
        -cosine_similarity(city_active_listings_features, listing_car_features).reshape(1, -1))

    similar_listings = city_active_listings_ids[similarity_indices].flatten()

    if direct_only:
        return get_recommended_listings(similar_listings, city_id, 'similar')
    else:
        return {'list': list(similar_listings), 'type': 'similar'}


def get_seen_similar_recommendations(seen_car_ids, city_id):
    """
    :param city_id: city_id (integer)
    :param seen_car_ids: list of last seen ids
    :return: list of ids of similar to seen recommendations
    """
    all_listings = []

    listings = Listing.objects.filter(id__in=seen_car_ids).values('id', 'status')
    for listing in listings:
        similar_ids = get_similar_cars(listing, city_id, direct_only=False)['list']
        all_listings.extend(similar_ids)

    return get_recommended_listings(np.array(all_listings), city_id, 'seen car')


def get_fallback_listings(city_id):
    """
    :param city_id: city_id (integer)
    :return: list of ids with direct promotion
    """
    listings = np.array(Listing.objects.filter(
                            status__in=LISTING_ACTIVE_STATUS,
                            locality__city_id=city_id
                        ).values_list('id', flat=True))
    return get_recommended_listings(listings, city_id, 'fallback')


def get_recommendations(params):
    """
    :param params: basic data for recommendation functions
    :return: list of recommended ids with direct promotion
    """
    buyer_id = params['buyer_id']
    city_id = params['city_id']
    seen_car_ids = params['seen_car_ids']

    try:
        if buyer_id and int(buyer_id) < ModelData.max_buyer_id:
            recommendation = get_personal_recommendation(buyer_id, city_id)
        elif seen_car_ids is not None and len(seen_car_ids) >= 3:
            recommendation = get_seen_similar_recommendations(seen_car_ids[-3:], city_id)
        else:
            recommendation = get_popular_recommendation(city_id)
    except:
        try:
            recommendation = get_popular_recommendation(city_id)
        except:
            recommendation = get_fallback_listings(city_id)

    return recommendation
