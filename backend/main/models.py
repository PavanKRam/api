# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from main.utils.constants import LISTING_STATUS_CHOICES
from user.models import BaseModel
from connect.signals import push_truebil_data_to_crm


class Accessory(models.Model):
    name = models.CharField(max_length=50)
    errors = models.CharField(max_length=100, blank=True, null=True)
    comments = models.CharField(max_length=200, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'accessories'
        verbose_name_plural = 'accessories'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Battery(models.Model):
    name = models.CharField(max_length=50)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'batteries'
        verbose_name_plural = 'batteries'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InspectionError(models.Model):
    name = models.CharField(max_length=100)
    detail = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'inspection_errors'
        verbose_name_plural = 'inspection_errors'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InspectionSection(models.Model):
    name = models.CharField(max_length=45)
    inspection_category = models.ForeignKey(to='InspectionCategory', related_name='inspection_sections')

    class Meta:
        db_table = 'inspection_sections'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InspectionCategory(models.Model):
    name = models.CharField(max_length=45)

    class Meta:
        db_table = 'inspection_categories'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Comment(models.Model):
    type = models.CharField(max_length=15)
    text = models.CharField(max_length=300)
    inspection_section = models.ForeignKey(to='InspectionSection', related_name='inspection_section_comments',
                                           blank=True, null=True)

    class Meta:
        db_table = 'comments'

    def __unicode__(self):
        return '%s - %s' % (self.type, self.text)


class InspectionComponent(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    inspection_section = models.ForeignKey(to='InspectionSection', related_name='inspection_components')
    alias = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        db_table = 'inspection_components'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Attachment(models.Model):
    listing = models.ForeignKey(to='Listing', related_name='listing_attachments', blank=True, null=True)
    file_number = models.IntegerField(blank=True, null=True)
    inspection_error = models.ForeignKey(to='InspectionError', related_name='error_attachments', blank=True, null=True)
    inspection_component = models.ForeignKey(to='InspectionComponent', related_name='component_attachments', blank=True,
                                             null=True)
    inspection_section = models.ForeignKey(to='InspectionSection', related_name='section_attachments', blank=True,
                                           null=True)
    caption = models.ForeignKey(to='Caption', blank=True, null=True)

    class Meta:
        db_table = 'attachments'

    def __unicode__(self):
        return '%s - %s' % (self.listing_id, self.file_number)


class Caption(models.Model):
    name = models.CharField(max_length=100)
    inspection_category = models.ForeignKey(to='InspectionCategory', related_name='caption_categories')

    class Meta:
        db_table = 'captions'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Feature(models.Model):
    FEATURE_TYPE_CHOICES = (
        ('Safety', 'Safety'),
        ('Comfort', 'Comfort'),
    )

    name = models.CharField(max_length=50)
    errors = models.CharField(max_length=100, blank=True, null=True)
    comments = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=6, choices=FEATURE_TYPE_CHOICES, blank=True, null=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'features'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Make(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        db_table = 'makes'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class BodyType(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        db_table = 'body_types'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Model(models.Model):
    make = models.ForeignKey(to='Make', related_name='models')
    body_type = models.ForeignKey(to='BodyType', related_name='body_type_models')
    name = models.CharField(max_length=128)
    similar_models = models.TextField(null=True, blank=True)
    cutoff_price = models.IntegerField(null=True, blank=True)
    matching_models = models.TextField(null=True, blank=True)
    connected_models = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        db_table = 'models'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Variant(models.Model):
    model = models.ForeignKey(to='Model', related_name='variants')
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'variants'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Color(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    hex = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'colors'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InspectionRequest(BaseModel):
    user = models.ForeignKey(to='user.User', related_name='inspection_requests')
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'inspection_requests'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.user)


class CarInspector(models.Model):
    name = models.CharField(max_length=45, blank=True, null=True)
    mobile = models.BigIntegerField(blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    experience = models.IntegerField(blank=True, null=True)
    cars_listed = models.SmallIntegerField(blank=True, null=True)
    city = models.ForeignKey(to='user.City', related_name='car_inspectors', blank=True, null=True)

    class Meta:
        db_table = 'car_inspectors'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InsuranceCompany(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        db_table = 'insurance_companies'
        verbose_name_plural = 'insurance_companies'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class TransmissionType(models.Model):
    name = models.CharField(max_length=45)

    class Meta:
        db_table = 'transmission_types'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class FuelType(models.Model):
    name = models.CharField(max_length=50)
    is_active = models.BooleanField(default=False)
    is_secondary = models.BooleanField(default=False)

    class Meta:
        db_table = 'fuel_types'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Listing(BaseModel):

    LISTING_POLICY_TYPE_CHOICES = (
        ('0 Depreciation', '0 Depreciation'),
        ('Comprehensive', 'Comprehensive'),
        ('Third Party', 'Third Party'),
        ('Lapsed', 'Lapsed'),
    )

    LISTING_FUEL_TYPE_CHOICES = (
        ('Petrol', 'Petrol'),
        ('Diesel', 'Diesel'),
    )

    LISTING_FUEL_TYPE_SECONDARY_CHOICES = (
        ('LPG', 'LPG'),
        ('CNG', 'CNG'),
    )

    LISTING_SECONDARY_FUEL_COMMENTS_CHOICES = (
        ('Company Fitted', 'Company Fitted'),
        ('Externally Fitted', 'Externally Fitted'),
    )

    LISTING_ATTACHMENT_STATUS_CHOICES = (
        ('unprocessed', 'unprocessed'),
        ('processed', 'processed'),
        ('processing', 'processing'),
    )

    LISTING_SELLING_REASON_CHOICES = (
        ('Bought another used car', 'Bought another used car'),
        ('Bought another new car', 'Bought another new car'),
        ('Will buy another used car', 'Will buy another used car'),
        ('Will buy another new car', 'Will buy another new car'),
        ('Relocating to different city', 'Relocating to different city'),
        ('Moving Abroad', 'Moving Abroad'),
        ('Others', 'Others'),
    )

    LISTING_EMISSION_NORM_CHOICES = (
        ('BS I', 'BS I'),
        ('BS II', 'BS II'),
        ('BS III', 'BS III'),
        ('BS IV', 'BS IV'),
    )

    LISTING_SERVICING_CHOICES = (
        ('Authorised', 'Authorised'),
        ('Unauthorised', 'Unauthorised'),
    )

    variant = models.ForeignKey(to='Variant', related_name='variant_listings')
    seller = models.ForeignKey(to='user.Seller', related_name='seller_listings')
    inspection_date = models.DateField(blank=True, null=True)
    engine_number = models.CharField(max_length=45, blank=True, null=True)
    chassis_number = models.CharField(max_length=45, blank=True, null=True)
    price = models.IntegerField(blank=True, null=True)
    inspector_quoted_price = models.IntegerField(blank=True, null=True)
    negotiable = models.BooleanField(default=False)
    policy_type = models.CharField(max_length=16, choices=LISTING_POLICY_TYPE_CHOICES, blank=True, null=True)
    policy_validity = models.DateField(blank=True, null=True)
    registration_number = models.CharField(max_length=45, blank=True, null=True)
    registration_date = models.DateField(blank=True, null=True)
    manufacturing_date = models.DateField(blank=True, null=True)
    rto = models.ForeignKey(to='user.RegionalTransportOffice', related_name='rto_listings', blank=True, null=True)
    rc_type = models.SmallIntegerField(blank=True, null=True)
    attachment_max_id = models.IntegerField(blank=True, null=True)
    features = models.ManyToManyField(to='Feature', through='ListingFeature', related_name='listing_features',
                                      blank=True)
    accessories = models.ManyToManyField(to='Accessory', through='ListingAccessory', related_name='listing_accessories',
                                         blank=True)
    comments = models.ManyToManyField(to='Comment', through='CommentsListing', related_name='listing_comments',
                                      blank=True)
    fuel_type_id_primary = models.CharField(max_length=6, choices=LISTING_FUEL_TYPE_CHOICES, blank=True,
                                            null=True)
    fuel_type_id_secondary = models.CharField(max_length=3, choices=LISTING_FUEL_TYPE_SECONDARY_CHOICES,
                                              blank=True, null=True)
    secondary_fuel_comments = models.CharField(max_length=16, choices=LISTING_SECONDARY_FUEL_COMMENTS_CHOICES,
                                               blank=True, null=True)
    owners = models.IntegerField(blank=True, null=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, blank=True, null=True)
    mileage = models.IntegerField(blank=True, null=True)
    shape_id = models.IntegerField(blank=True, null=True)
    comment_positive = models.TextField(blank=True, null=True)
    comment_negative = models.TextField(blank=True, null=True)
    color = models.ForeignKey(to='Color', related_name='listings', blank=True, null=True)
    attachment_status = models.CharField(max_length=16, choices=LISTING_ATTACHMENT_STATUS_CHOICES, blank=True,
                                         null=True)
    body_type = models.ForeignKey(to='BodyType', related_name='body_type_listings', blank=True, null=True)
    transmission_type = models.ForeignKey(to='TransmissionType', related_name='transmission_type_listings', null=True,
                                          blank=True)
    status = models.CharField(max_length=16, choices=LISTING_STATUS_CHOICES, blank=True, null=True)
    inspection_requested_user = models.ForeignKey(to='user.User',
                                                     related_name='inspection_requested_listings', blank=True,
                                                     null=True)
    is_inventory = models.BooleanField(default=False)
    is_shown_to_dealers = models.BooleanField(default=False)
    is_eligible_inventory = models.BooleanField(default=False)
    inventory_offered_price = models.IntegerField(blank=True, null=True)
    depicted_market_price = models.IntegerField(blank=True, null=True)
    folder_name = models.IntegerField(blank=True, null=True)
    img_version = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    sort_by = models.DateTimeField(blank=True, null=True)
    modified_at = models.DateTimeField()
    car_inspector = models.ForeignKey(to='CarInspector', blank=True, null=True)
    ibb_price = models.IntegerField(blank=True, null=True)
    carwale_price = models.IntegerField(blank=True, null=True)
    carwale_price_excellent_individual = models.IntegerField(blank=True, null=True)
    carwale_price_excellent_dealer = models.IntegerField(blank=True, null=True)
    carwale_price_good_individual = models.IntegerField(blank=True, null=True)
    carwale_price_good_dealer = models.IntegerField(blank=True, null=True)
    carwale_price_fair_individual = models.IntegerField(blank=True, null=True)
    carwale_price_fair_dealer = models.IntegerField(blank=True, null=True)
    inspection_rating = models.DecimalField(max_digits=3, decimal_places=2, blank=True, null=True)
    selling_date = models.DateField(blank=True, null=True)
    is_mvc = models.BooleanField(default=False)
    ibb_dealer_fair_min_price = models.IntegerField(blank=True, null=True)
    ibb_dealer_fair_max_price = models.IntegerField(blank=True, null=True)
    ibb_individual_fair_min_price = models.IntegerField(blank=True, null=True)
    ibb_individual_fair_max_price = models.IntegerField(blank=True, null=True)
    ibb_dealer_good_min_price = models.IntegerField(blank=True, null=True)
    ibb_dealer_good_max_price = models.IntegerField(blank=True, null=True)
    ibb_individual_good_min_price = models.IntegerField(blank=True, null=True)
    ibb_individual_good_max_price = models.IntegerField(blank=True, null=True)
    ibb_dealer_excellent_min_price = models.IntegerField(blank=True, null=True)
    ibb_dealer_excellent_max_price = models.IntegerField(blank=True, null=True)
    ibb_individual_excellent_min_price = models.IntegerField(blank=True, null=True)
    ibb_individual_excellent_max_price = models.IntegerField(blank=True, null=True)
    locality = models.ForeignKey(to='user.Locality', blank=True, null=True)
    is_rc_in_seller_name = models.BooleanField(default=True)
    is_rc_verified = models.BooleanField(default=True)
    reason_for_selling = models.CharField(max_length=64, choices=LISTING_SELLING_REASON_CHOICES, blank=True, null=True)
    rto_endorsed = models.BooleanField(default=False)
    insurance_endorsed = models.BooleanField(default=False)
    emission_standard_norm = models.CharField(max_length=32, choices=LISTING_EMISSION_NORM_CHOICES, blank=True,
                                              null=True)
    insurance_company = models.ForeignKey(to='InsuranceCompany', related_name='insurance_company_listings',
                                             blank=True, null=True)
    idv_amount = models.IntegerField(blank=True, null=True)
    is_insurance_verified = models.BooleanField(default=False)
    is_extended_warranty = models.BooleanField(default=False)
    is_amc = models.BooleanField(default=False)
    is_rsa = models.BooleanField(default=False)
    extended_warranty_validity_date = models.DateField(blank=True, null=True)
    extended_warranty_validity_kms = models.IntegerField(blank=True, null=True)
    amc_validity_date = models.DateField(blank=True, null=True)
    rsa_validity_date = models.DateField(blank=True, null=True)
    serviced_at = models.CharField(max_length=12, choices=LISTING_SERVICING_CHOICES, blank=True, null=True)
    last_service_date = models.DateField(blank=True, null=True)
    next_service_date = models.DateField(blank=True, null=True)
    is_hypothecation = models.BooleanField(default=False)
    hfi_bank_id = models.IntegerField(blank=True, null=True)
    is_bank_noc = models.BooleanField(default=False)
    other_reason_for_selling = models.TextField(blank=True, null=True)
    quick_sell_offered = models.BooleanField(default=False)
    negative_comments = models.CharField(max_length=500, blank=True, null=True)
    positive_comments = models.CharField(max_length=500, blank=True, null=True)
    inventory_added_date = models.DateTimeField(blank=True, null=True)
    eligible_inventory_date = models.DateTimeField(blank=True, null=True)
    is_eligible_warranty = models.BooleanField(default=False)
    expected_live_date = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'listings'

    def __unicode__(self):
        return '%s - %s - %s' % (self.id, self.variant.name, self.seller.name)


class CommentsListing(models.Model):
    listing = models.ForeignKey(to='Listing', related_name='listing_comments')
    comment = models.ForeignKey(to='Comment', related_name='comment_listings')

    class Meta:
        db_table = 'comments_listings'

    def __unicode__(self):
        return '%s - %s' % (self.listing.id, self.comment.text)


class ListingFeature(models.Model):
    listing = models.ForeignKey(to='Listing', related_name='listing_features')
    feature = models.ForeignKey(to='Feature', related_name='feature_listings')
    comment = models.CharField(max_length=200, blank=True, null=True)
    error = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'listing_features'

    def __unicode__(self):
        return '%s - %s' % (self.listing.variant.name, self.feature.name)


class ListingAccessory(models.Model):
    listing = models.ForeignKey(to='Listing', related_name='listing_accessories')
    accessory = models.ForeignKey(to='Accessory', related_name='accessory_listings')
    comment = models.CharField(max_length=45, blank=True, null=True)
    error = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'listing_accessories'

    def __unicode__(self):
        return '%s - %s' % (self.listing.variant.name, self.accessory.name)


class ListingAdditionalFields(models.Model):
    listing = models.OneToOneField(to='Listing', related_name='listing_additional_fields', primary_key=True)
    secondary_fuel_license_expiration_date = models.DateField(blank=True, null=True)
    battery = models.ForeignKey(to='Battery', related_name='listing_battery', blank=True, null=True)
    battery_expiry_date = models.DateField(blank=True, null=True)
    battery_capacity_percentage = models.IntegerField(blank=True, null=True)
    rc_person_relation_with_seller = models.CharField(max_length=14, blank=True, null=True)
    service_station_name = models.TextField(blank=True, null=True)
    next_service_due_km = models.IntegerField(blank=True, null=True)
    is_non_listing = models.IntegerField(blank=True, null=True)

    class Meta:
        db_table = 'listing_additional_fields'

    def __unicode__(self):
        return '%s' % self.listing.id


class ListingStatusTrack(BaseModel):
    SOURCE_UPDATE_CHOICES = (
        ('seller_dashboard', 'seller_dashboard'),
        ('cargo', 'cargo'),
        ('sheet', 'sheet'),
        ('cron', 'cron'),
        ('dealer_dashboard', 'dealer_dashboard'),
        ('dedicated_page', 'dedicated_page')
    )

    listing = models.ForeignKey(to='Listing', related_name='listing_tracks', blank=True, null=True)
    status_to = models.CharField(max_length=32, choices=LISTING_STATUS_CHOICES, blank=True, null=True)
    source = models.CharField(max_length=16, choices=SOURCE_UPDATE_CHOICES, blank=True, null=True)
    changed_at = models.DateTimeField()

    class Meta:
        db_table = 'listing_status_track'

    def __unicode__(self):
        return '%s - %s' % (self.listing.id, self.status_to)


class ListingSoldReportLog(BaseModel):
    SOURCE_UPDATE_CHOICES = (
        ('seller_dashboard', 'seller_dashboard'),
        ('dedicated_page', 'dedicated_page'),
        ('dealer_dashboard', 'dealer_dashboard')
    )
    LISTING_SOLD_STATUS_CHOICES = (
        ('truebiled', 'truebiled'),
        ('soldByOthers', 'soldByOthers')
    )

    listing = models.ForeignKey(to='Listing', related_name='listing_sold_report_logs', blank=True, null=True)
    user = models.ForeignKey(to='user.User', related_name='listing_sold_report_logs', blank=True, null=True)
    status = models.CharField(max_length=16, choices=LISTING_SOLD_STATUS_CHOICES, blank=True, null=True)
    source = models.CharField(max_length=16, choices=SOURCE_UPDATE_CHOICES, blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'listing_sold_report_logs'

    def __unicode__(self):
        return '%s - %s' % (self.listing.id, self.user.id)


class ListingError(models.Model):
    listing = models.ForeignKey(to='Listing', related_name='listing_errors', blank=True, null=True)
    inspection_error = models.ForeignKey(to='InspectionError', related_name='listing_inspection_errors')
    inspection_component = models.ForeignKey(to='InspectionComponent', related_name='listing_inspection_components')
    comment = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'listing_errors'

    def __unicode__(self):
        return '%s - %s' % (self.listing.id, self.comment)


class ListingPriceChange(BaseModel):
    SOURCE_UPDATE_CHOICES = (
        ('seller_dashboard', 'seller_dashboard'),
        ('cargo', 'cargo'),
        ('sheet', 'sheet'),
        ('dealer_dashboard', 'dealer_dashboard')
    )

    listing = models.ForeignKey(to='Listing', related_name='listing_price_changes')
    previous_price = models.IntegerField(blank=True, null=True)
    updated_price = models.IntegerField(blank=True, null=True)
    source_update = models.CharField(max_length=16, choices=SOURCE_UPDATE_CHOICES, blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'listing_price_changes'

    def __unicode__(self):
        return '%s - %s - %s' % (self.listing.id, self.previous_price, self.updated_price)


class ComponentError(models.Model):
    inspection_component = models.ForeignKey(to='InspectionComponent', related_name='inspection_components')
    inspection_error = models.ForeignKey(to='InspectionError', related_name='inspection_errors')
    comment_negative = models.CharField(max_length=100, blank=True, null=True)
    weight = models.DecimalField(max_digits=2, decimal_places=1, blank=True, null=True)
    refurbishment_cost = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_neutral = models.BooleanField(default=False)

    class Meta:
        db_table = 'component_errors'

    def __unicode__(self):
        return '%s - %s' % (self.inspection_component.id, self.comment_negative)


class CategoryRating(models.Model):
    listing = models.ForeignKey(to='Listing', related_name='listing_category_ratings')
    category = models.ForeignKey(to='InspectionCategory', related_name='category_ratings', blank=True, null=True)
    rating = models.DecimalField(max_digits=3, decimal_places=2, blank=True, null=True)

    class Meta:
        db_table = 'category_ratings'

    def __unicode__(self):
        return '%s - %s' % (self.listing.id, self.category.id)


class SuperSaverListing(BaseModel):
    city = models.ForeignKey(to='user.City', related_name='city_super_savers')
    listing = models.ForeignKey(to='Listing', related_name='listing_super_savers')
    rank = models.IntegerField()
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'supersaver_listings'

    def __unicode__(self):
        return '%s - %s' % (self.listing.variant.name, self.rank)


class InstaveritasVerification(models.Model):
    registration_number = models.CharField(max_length=45)
    chassis_number = models.CharField(max_length=45, blank=True, null=True)
    engine_number = models.CharField(max_length=45, blank=True, null=True)
    fuel_type = models.CharField(max_length=20, blank=True, null=True)
    registration_state = models.CharField(max_length=20, blank=True, null=True)
    vehicle_company = models.CharField(max_length=45, blank=True, null=True)
    vehicle_model = models.CharField(max_length=45, blank=True, null=True)
    vehicle_type = models.CharField(max_length=45, blank=True, null=True)
    owners_name = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    json_payload = models.TextField(blank=True, null=True)
    registration_date = models.DateField()
    registering_authority = models.CharField(max_length=45, blank=True, null=True)

    class Meta:
        db_table = 'instaveritas_verifications'

    def __unicode__(self):
        return '%s' % self.registration_number


class InterestedSellerListings(BaseModel):

    interested_seller = models.ForeignKey(to='user.InterestedSeller')
    seller_visit = models.ForeignKey(to='user.SellerVisit', related_name='interested_seller_listings')
    variant = models.ForeignKey(to='Variant', related_name='interested_seller_listings')
    year = models.IntegerField()
    mileage = models.IntegerField()
    owner = models.IntegerField()
    color = models.ForeignKey(to='main.Color', related_name='interested_seller_listings')
    rough = models.CharField(max_length=50)
    ok = models.CharField(max_length=50)
    excellent = models.CharField(max_length=50)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'interested_seller_listings'

    def __unicode__(self):
        return '%s - %s ' % (self.id, self.variant)