"""
Module holding serializer classes for models
in truebil main module.
"""

import collections
import datetime
import json
import math
from datetime import timedelta

import pretty
from django.db.models import Case, When
from django.utils import timezone
from rest_framework import serializers

from main.models import (Accessory, Feature, Make, BodyType, Model, Variant, Color,
                         InspectionRequest, CarInspector, Listing, InsuranceCompany, Attachment,
                         FuelType, TransmissionType, InspectionCategory, CategoryRating, ListingAdditionalFields,
                         InstaveritasVerification)
from main.quick_ops import (get_cdn_url, unit_converter, ordinal, get_asset_url, get_buyer_offer, get_market_price,
                            get_timezone_aware_current_datetime, get_recommended_car_price)
from main.utils.constants import SHOWCASE_IMAGES, RC_INSURANCE_CAPTION_IDS
from main.utils.filters_conf import GROOVE_INSPECTION_COMPONENTS, POPULAR_MODELS
from main.utils.sample_inspection_report import get_sample_inspection_report
from main.utils.serializer_helpers import (get_active_makes_models, get_prices_data,
                                           get_truebil_direct, get_value_for_money,
                                           get_km_drivens, get_truescores, get_owners, get_years,
                                           subheader_active_makes, build_url_slug, can_show_inspection_report,
                                           get_year_data_for_subheaders, get_variant_name, get_groove_percentage,
                                           get_component_error_comment_map, is_car_shortlisted, get_model_name)
from user.crf.helpers import validate_buyer_for_seller_data
from user.models import (City, Region, BuyerListing, UserActivity, BuyerListingOffer, BuyerVisitListing, BuyerVisit,
                         SellerInventoryRequest)
from user.serializers import RegionSerializer


class AccessorySerializer(serializers.ModelSerializer):
    """Serializer class for Accessory model"""

    class Meta:
        model = Accessory
        fields = ('id', 'name', 'errors', 'comments', 'is_active')


class FeatureSerializer(serializers.ModelSerializer):
    """Serializer class for Feature model"""

    def get_is_checked(self, obj):
        filter_features = self.context.get('filter_features', '')

        is_checked = False
        if str(obj.id) in filter_features:
            is_checked = True

        return is_checked

    is_checked = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Feature
        fields = ('id', 'name', 'is_checked')


class MakeSerializer(serializers.ModelSerializer):
    """Serializer class for Make model"""

    class Meta:
        model = Make
        fields = ('id', 'name')


class BodyTypeSerializer(serializers.ModelSerializer):
    """Serializer class for BodyType model"""

    def get_is_checked(self, obj):
        filter_body_types = self.context.get('filter_body_types', '')

        is_checked = False
        if str(obj.id) in filter_body_types:
            is_checked = True

        return is_checked

    is_checked = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return build_url_slug(obj.name, self.context.get('city_name', 'india'))

    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = BodyType
        fields = ('id', 'name', 'is_checked', 'url')


class FuelTypeSerializer(serializers.ModelSerializer):
    """Serializer class for FuelType model"""

    def get_is_checked(self, obj):
        filter_fuel_types = self.context.get('filter_fuel_types', '')

        is_checked = False
        if str(obj.id) in filter_fuel_types:
            is_checked = True

        return is_checked

    is_checked = serializers.SerializerMethodField(read_only=True)

    def get_url(self, obj):
        return build_url_slug(obj.name, self.context.get('city_name', 'india'))

    url = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = FuelType
        fields = ('id', 'name', 'is_checked', 'url')


class TransmissionTypeSerializer(serializers.ModelSerializer):
    """Serializer class for Transmission model"""

    def get_is_checked(self, obj):
        filter_transmission = self.context.get('filter_transmission', '')

        is_checked = False
        if str(obj.id) in filter_transmission:
            is_checked = True

        return is_checked

    is_checked = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = TransmissionType
        fields = ('id', 'name', 'is_checked')


class ModelSerializer(serializers.ModelSerializer):
    """Serializer class for Model model"""

    class Meta:
        model = Model
        fields = ('id', 'name')


class VariantSerializer(serializers.ModelSerializer):
    """Serializer class for Model model"""

    model = serializers.PrimaryKeyRelatedField(queryset=Model.objects.all())

    class Meta:
        model = Variant
        fields = ('id', 'model', 'name')


class ColorSerializer(serializers.ModelSerializer):
    """Serializer class for BodyType model"""

    def get_is_checked(self, obj):
        filter_colors = self.context.get('filter_colors', '')

        is_checked = False
        if str(obj.id) in filter_colors:
            is_checked = True

        return is_checked

    is_checked = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Color
        fields = ('id', 'name', 'hex', 'is_checked')


class InspectionRequestSerializer(serializers.ModelSerializer):
    """Serializer class for InspectionRequest model"""

    class Meta:
        model = InspectionRequest
        fields = ('id', 'user_id', 'created_id')


class CarInspectorSerializer(serializers.ModelSerializer):
    """Serializer class for CarInspector model"""

    city = serializers.PrimaryKeyRelatedField(queryset=City.objects.all())

    class Meta:
        model = CarInspector
        fields = ('id', 'name', 'mobile', 'email', 'experience', 'cars_listed', 'city')


class InsuranceCompanySerializer(serializers.ModelSerializer):
    """Serializer class for InsuranceCompany model"""

    class Meta:
        model = InsuranceCompany
        fields = ('id', 'name')


class ListingSerializer(serializers.ModelSerializer):
    """Serializer class for Listing model"""

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('variant', 'seller', 'locality')
        return queryset

    @staticmethod
    def get_car_link(obj):
        variant_name_slug = obj.variant.name.replace(' ', '-').replace('+', '')
        slug = str(obj.manufacturing_date.year) + '-' + variant_name_slug + '-driven-' + str(obj.mileage) + '-kms'
        return '/used-cars-in-' + obj.locality.city.name.lower() + '/' + slug + '-' + str(obj.id)

    car_link = serializers.SerializerMethodField(read_only=True)

    def get_car_info(self, obj):
        market_price = get_market_price(obj)
        return {
            'id': obj.id,
            'variant_name': get_variant_name(obj, self.context.get('source')),
            'model_name': get_model_name(obj),
            'price': unit_converter(obj.price),
            'market_price': unit_converter(market_price, shorthand_price=True) if market_price > 100000
            else unit_converter(market_price),
            'savings': unit_converter(market_price - obj.price, shorthand_price=True)
            if market_price and obj.price else None,
            'mileage': "{:,}".format(obj.mileage),
            'primary_fuel': obj.fuel_type_id_primary,
            'secondary_fuel': obj.fuel_type_id_secondary,
            'owner': ordinal(obj.owners),
            'localities': obj.locality.name,
            'is_inventory': obj.is_inventory,
            'rating': "{:.1f}".format(obj.rating) if obj.rating else "0.0",
            'rto': '{0} ({1}-{2})'.format(obj.rto.name, obj.rto.state_code, obj.rto.rto_code),
            'city_name': obj.locality.city.name,
        }

    car_info = serializers.SerializerMethodField(read_only=True)

    def get_image_urls(self, obj):
        attachments_qs = self.context.get('attachment_qs')
        caption_order = [2, 3, 5, 7, 8, 1, 18]
        attachment_of_listing = attachments_qs.filter(listing=obj)
        attachment_list_of_listing = sorted(attachment_of_listing, key=lambda i: caption_order.index(i.caption_id))

        cdn_url = self.context.get('cdn_url')
        image_urls = []
        for attachment in attachment_list_of_listing:
            url_first_half = cdn_url + str(obj.id) + '-' + str(attachment.file_number) + '-360.jpg'
            url = url_first_half if not obj.img_version else (url_first_half + '?v=' + str(obj.img_version))
            image_urls.append(url)

        show_one_url = self.context.get('show_one_url', None)
        if show_one_url:
            image_urls = [image_urls[0]] if len(image_urls) else []

        return image_urls

    image_urls = serializers.SerializerMethodField(read_only=True)

    def get_is_seller_contacted(self, obj):
        buyer = self.context.get('buyer', None)
        is_seller_contacted = False

        if buyer and BuyerListing.objects.filter(listing=obj, buyer=buyer, is_active=True).exists():
            is_seller_contacted = True

        self.context['is_seller_contacted'] = is_seller_contacted
        return is_seller_contacted

    is_seller_contacted = serializers.SerializerMethodField(read_only=True)

    def get_seller_info(self, obj):
        is_user_logged_in = self.context.get('is_user_logged_in', False)
        is_subscribed_buyer = self.context.get('is_subscribed_buyer', False)
        is_seller_contacted = self.context.get('is_seller_contacted', False)
        buyer = self.context.get('buyer', None)

        seller_details = None
        if is_user_logged_in and is_seller_contacted and (is_subscribed_buyer and obj.is_inventory is False) or \
                (buyer and validate_buyer_for_seller_data(buyer, obj.id)):
            seller_details = {'name': obj.seller.name, 'mobile': obj.seller.mobile}

        return seller_details

    seller_info = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_city_id(obj):
        return obj.locality.city_id

    city_id = serializers.SerializerMethodField(read_only=True)

    def get_is_offered_by_me(self, obj):
        buyer = self.context.get('buyer')
        is_offered_by_me = False
        if buyer:
            offer_on_listing = obj.offers.filter(buyer=buyer, is_active=True, is_verified=True).exists()

            if offer_on_listing:
                is_offered_by_me = True

        return is_offered_by_me

    is_offered_by_me = serializers.SerializerMethodField(read_only=True)

    get_is_shortlisted = is_car_shortlisted
    is_shortlisted = serializers.SerializerMethodField(read_only=True)

    def get_is_seen(self, obj):
        is_seen = False
        if self.context.get('is_user_logged_in'):
            request = self.context.get('request')
            is_seen = request.user.user_activities.filter(
                listing=obj
            ).order_by('-id').values_list('is_seen', flat=True).first() or False

        return is_seen

    is_seen = serializers.SerializerMethodField(read_only=True)

    def get_is_super_saver(self, obj):
        return obj.id in self.context.get('super_saver_car_ids', [])

    is_super_saver = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_interested_people_count(obj):
        offer_count = BuyerListingOffer.objects.filter(listing=obj, is_active=True, is_verified=True).count()
        crf_count = BuyerListing.objects.filter(listing=obj).count()
        offset = (obj.id % 4) + 4
        return offer_count + crf_count + offset

    interested_people_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('car_link', 'car_info', 'sort_by', 'image_urls', 'is_seller_contacted', 'expected_live_date',
                  'city_id', 'status', 'is_offered_by_me', 'seller_info', 'is_shortlisted', 'is_seen',
                  'is_super_saver', 'price', 'interested_people_count')


class DedicatedSerializer(serializers.ModelSerializer):
    """Serializer class for Listing model and for dedicated page"""

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('variant', 'seller', 'locality', 'RegionalTransportOffice')
        return queryset

    @staticmethod
    def get_overview(obj):
        info = {
            'Fuel': obj.fuel_type_id_primary,
            'Transmission': obj.transmission_type_id,
            'RC Type': 'Individual' if obj.rc_type == 1 else 'Dealer',
            'RTO': '{0} ({1}-{2})'.format(obj.rto.name, obj.rto.state_code, obj.rto.rto_code),
            'Secondary Fuel': {
                'type': obj.fuel_type_id_secondary,
                'comments': obj.secondary_fuel_comments,
                'rto_endorsed': 'Endorsed by RTO' if obj.rto_endorsed == 'Y' else 'Not endorsed by RTO',
            },
        }

        listing_additional_fields = ListingAdditionalFields.objects.filter(listing=obj)
        if listing_additional_fields.exists():
            info['Secondary Fuel']['license_expiration_date'] = \
                listing_additional_fields.first().secondary_fuel_license_expiration_date

        seller_info = {
            'Owner': ordinal(obj.owners),
            'Employment': obj.seller.employment_category.name if hasattr(obj.seller, 'employment_category') else None,
            'Locality': obj.locality.name,
            'Seller': 'Dealer' if obj.seller.type_id == 'Subscribed_Dealer' else obj.seller.type_id,
        }

        return_info = collections.OrderedDict()

        return_info["car_info"] = info
        return_info["seller_info"] = seller_info

        return return_info

    overview = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_city_details(obj):
        city = {
            'City Id': obj.locality.city_id,
            'City Name': obj.locality.city.name,
        }
        return city

    city_details = serializers.SerializerMethodField(read_only=True)

    def get_show_sample_inspection_report(self, obj):
        return True if not can_show_inspection_report(self.context.get('request'), obj) and self.context.get(
            'is_buyer_crf_exhausted') else False

    show_sample_inspection_report = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_first_error_photo_number(obj):
        listing_attachments = obj.listing_attachments.all()
        first_error_photo_file_number = None
        for attachment in listing_attachments:
            if attachment.inspection_section_id and not first_error_photo_file_number:
                first_error_photo_file_number = attachment.file_number

        return first_error_photo_file_number

    first_error_photo_number = serializers.SerializerMethodField(read_only=True)

    def get_inspection_report(self, obj):
        refurb_data = []
        refurb_amount = 0
        positive_comments_inferred = obj.comments.filter(type="global positive").values_list('text', flat=True)
        negative_comments_inferred = obj.comments.filter(type="global negative").values_list('text', flat=True)

        summary = {
            "positive_comments": positive_comments_inferred,
            "negative_comments": negative_comments_inferred
        }

        if (not can_show_inspection_report(self.context.get('request'), obj) and
           self.context.get('is_buyer_crf_exhausted')):
            sample_inspection_report = get_sample_inspection_report()
            sample_inspection_report['summary'] = summary
            return sample_inspection_report

        inspector_info = dict()
        if obj.car_inspector:
            inspector_info = {
                "id": obj.car_inspector.id,
                "name": obj.car_inspector.name,
                "experience": str(obj.car_inspector.experience) + "+ years of experience",
                "cars_inspected": str(obj.car_inspector.cars_listed) + "+ cars inspected",
                "url": get_asset_url() + "/images/inspectors/" + str(obj.car_inspector.id) + "-" +
                       str(obj.car_inspector.name.lower()) + ".jpg?v=1"
            }

        listing_error_qs = obj.listing_errors.prefetch_related('inspection_error', 'inspection_component')
        listing_component_errors = [str(listing_error.inspection_component.id) + "-" +
                                    str(listing_error.inspection_error.id) for listing_error in listing_error_qs]

        groove_percentage = get_groove_percentage(listing_error_qs)

        listing_attachments = obj.listing_attachments.all()

        positive_comments = list()
        listing_comments = obj.comments.filter(type__exact='positive').prefetch_related('inspection_section')
        for comment in listing_comments:
            positive_comments.append({
                'section_id': comment.inspection_section_id,
                'text': comment.text
            })

        report = collections.OrderedDict()

        component_error_comment_map = get_component_error_comment_map()
        cdn_url = get_cdn_url()

        inspection_category = InspectionCategory.objects.prefetch_related('inspection_sections')
        caption_file_number_qs = Attachment.objects.filter(listing=obj)
        len(caption_file_number_qs)
        for category in inspection_category:
            category_sections = []
            inspection_category_sections = category.inspection_sections.prefetch_related('inspection_components'). \
                order_by('name')
            count = 0
            for section in inspection_category_sections:
                if section.id == 21:
                    continue
                inspection_section_components = section.inspection_components.all()
                comment_negative = list()
                comment_neutral = list()
                for component in inspection_section_components:
                    for error in listing_component_errors:
                        if error.startswith(str(component.id) + "-"):
                            if error in component_error_comment_map.keys() and \
                                    component_error_comment_map[error].comment_negative:
                                if component_error_comment_map[error].is_neutral == 1:
                                    comment_neutral.append(component_error_comment_map[error].comment_negative)
                                else :
                                    comment_negative.append(component_error_comment_map[error].comment_negative)
                                if component_error_comment_map[error].comment_negative:
                                    refurb_data.append({"label": component_error_comment_map[error].comment_negative,
                                                        "value": component_error_comment_map[error].refurbishment_cost})
                                    if component_error_comment_map[error].refurbishment_cost:
                                        refurb_amount += component_error_comment_map[error].refurbishment_cost
                comment_positive = [comment['text'] for comment in positive_comments
                                    if comment['section_id'] == section.id]

                attachment_qs = listing_attachments.filter(inspection_section=section)

                images = []
                for attachment in attachment_qs:
                    images.append({
                        "url": (cdn_url + str(obj.id) + "-" + str(attachment.file_number) + "-125.jpg?v=" +
                                str(obj.img_version)),
                        "caption": attachment.caption.name if attachment.caption else component_error_comment_map
                        [str(attachment.inspection_component_id) + '-' + str(attachment.inspection_error_id)].comment_negative
                    })

                category_sections.append({
                    section.name: {
                        "positive_comments": comment_positive,
                        "negative_comments": comment_negative,
                        "neutral_comments": comment_neutral,
                        "images": images
                    }
                })

                if str(section.id) in groove_percentage.keys():
                    category_sections[count][section.name]['groove_percentage'] = groove_percentage[
                                                                                      str(section.id)] + "%"
                    caption_id = GROOVE_INSPECTION_COMPONENTS[section.name]['caption_id']
                    caption = caption_file_number_qs.filter(caption_id=caption_id).first()
                    if caption:
                        category_sections[count][section.name]['urls'] = [get_cdn_url() + str(obj.id) + "-" + \
                                                                          str(caption.file_number) + "-125.jpg?v=" + \
                                                                          str(obj.img_version)]

                    count += 1

            try:
                rating = CategoryRating.objects.get(category=category, listing=obj).rating
                report[category.name] = {
                    "rating": "{:.1f}".format(rating),
                    "details": category_sections
                }
            except CategoryRating.DoesNotExist:
                pass

        refurb_data.append({"label": "Total refurbishment cost", "value": refurb_amount})

        inspection_report = {
            "inspector_info": inspector_info,
            "summary": summary,
            "report": report,
            "refurb_details": refurb_data,
            "listing_negative_comments": filter(None, obj.negative_comments.split("|")) if obj.negative_comments else [],
            "listing_positive_comments": filter(None, obj.positive_comments.split("|")) if obj.positive_comments else []
        }

        return inspection_report

    inspection_report = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_rc_insurance_validity(obj):
        if not obj.is_rc_verified and not obj.is_insurance_verified:
            return {
                'showErrorRcInsurance': 'RC and Insurance not verified'
            }
        elif not obj.is_rc_verified:
            return {
                'showErrorRcInsurance': 'RC not verified'
            }
        elif not obj.is_insurance_verified:
            return {
                'showErrorRcInsurance': 'Insurance not verified'
            }

    rc_insurance_validity = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_warranty_eligibility(obj):
        if not obj.is_eligible_warranty:
            return {
                'isEligibleWarranty': obj.is_eligible_warranty
            }

    warranty_eligibility = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_model_data(obj):
        model_obj = Model.objects.get(variants__variant_listings=obj)

        round_price = int(round(obj.price / 100000)) if obj.price else None
        if 2 <= round_price <= 9:
            price_slug = str(round_price - 1) + '-lakh-to-' + str(round_price + 1) + '-lakh'
        elif round_price < 2:
            price_slug = 'under-2-lakh'
        else:
            price_slug = '10-lakh-above'

        return {
            'model_link': "/used-" + model_obj.name.replace(" ", "-").lower() + "-cars-in-" +
                          obj.seller.city.name.lower(),
            'price_link': '/used-' + price_slug + '-cars-in-' + obj.seller.city.name.lower(),
            'model_name': model_obj.name,
            'make_name': model_obj.make.name,
            'price_range': price_slug
        }

    model_data = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_features(obj):
        listing_feature_ids = obj.listing_features.all().values_list('feature_id', flat=True)
        unique_feature_types = Feature.objects.values_list('type', flat=True).distinct()
        active_features = Feature.objects.filter(id__in=listing_feature_ids)
        feature_order = [3, 9, 4, 2, 7, 5, 10, 12, 11, 25, 26, 14]

        preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(feature_order)])
        ordered_active_features = active_features.order_by(preserved)

        inactive_features = Feature.objects.exclude(id__in=ordered_active_features.values_list('id', flat=True))

        features = list()
        for feature_type in unique_feature_types:
            if feature_type:
                features.append({
                    feature_type: {
                        "Active": ordered_active_features.filter(type=feature_type).values_list('name', flat=True),
                        "Inactive": inactive_features.filter(type=feature_type).values_list('name', flat=True)
                    }
                })

        listing_accessory_ids = obj.listing_accessories.all().values_list('accessory_id', flat=True)
        active_accessories = Accessory.objects.filter(id__in=listing_accessory_ids).order_by('id')

        active_accessory_ids = active_accessories.values_list('id', flat=True)
        inactive_accessories = Accessory.objects.exclude(id__in=active_accessory_ids)

        features.append({
            "Accessories": {
                "Active": active_accessories.values_list('name', flat=True),
                "Inactive": inactive_accessories.values_list('name', flat=True)
            }
        })
        return features

    features = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_basic_info(obj):
        caption_order = SHOWCASE_IMAGES
        attachment_qs = Attachment.objects.filter(listing=obj, caption_id__in=SHOWCASE_IMAGES)
        preserved = Case(*[When(caption_id=pk, then=pos) for pos, pk in enumerate(caption_order)])
        file_number_list = attachment_qs.order_by(preserved).values_list('file_number', flat=True)
        info = {
            'variant_name': str(obj.manufacturing_date)[:4] + " " + str(obj.variant.name),
            'price': unit_converter(obj.price, shorthand_price=True),
            'mileage': "{:,}".format(obj.mileage),
            'rating': "{:.1f}".format(obj.rating) if obj.rating else "0.0",
            'model_id': str(obj.variant.model_id),
            'manufacturing_date': str(obj.manufacturing_date)
        }
        if obj.is_inventory:
            save_price = (get_market_price(obj) - obj.price) if obj.price else None
            if save_price > 99999:
                info['save_price'] = unit_converter(save_price, shorthand_price=True)
            elif save_price > 0:
                info['save_price'] = unit_converter(save_price)
            else:
                pass

        showcase_images = [(get_cdn_url() + str(obj.id) + "-" + str(file_number) + "-360.jpg?v=" + str(obj.img_version))
                           for file_number in file_number_list]
        info['showcase_image_urls'] = showcase_images

        attachment_qs_with_non_null_captions = obj.listing_attachments.exclude(caption_id__isnull=True)
        info['listing_image_count'] = attachment_qs_with_non_null_captions.exclude(caption_id__in=
                                                                                   RC_INSURANCE_CAPTION_IDS).count() - 2

        return info

    basic_info = serializers.SerializerMethodField(read_only=True)

    def get_buyer_listing_offer(self, obj):
        return get_buyer_offer(obj, self.context.get('buyer'), self.context.get('invalid_numbers'))

    buyer_listing_offer = serializers.SerializerMethodField(read_only=True)

    def get_is_seller_contacted(self, obj):
        buyer = self.context.get('buyer', None)
        is_seller_contacted = False

        if buyer and BuyerListing.objects.filter(listing=obj, buyer=buyer, is_active=True).exists():
            is_seller_contacted = True

        self.context['is_seller_contacted'] = is_seller_contacted
        return is_seller_contacted

    is_seller_contacted = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_emi(obj):
        car_price = obj.price
        car_manufacturing_year = str(obj.manufacturing_date)[:4]
        yearly_interest_rate = 0.145
        current_year = datetime.datetime.now().year
        loan_tenure_yearly = 3
        emi = 0
        car_age = current_year - int(car_manufacturing_year)
        if 0 < car_age < 8:
            if car_age > 5:
                loan_tenure_yearly = 8 - car_age
            monthly_interest_rate = yearly_interest_rate / 12
            loan_tenure_monthly = loan_tenure_yearly * 12
            if car_price and 0.6 * car_price >= 100000:
                car_loan_amount = 0.6 * car_price
            else:
                car_loan_amount = 100000

            power = math.pow((1 + monthly_interest_rate), loan_tenure_monthly)
            emi = (car_loan_amount * (monthly_interest_rate * power)) / (power - 1)
            emi = math.floor(emi)

            if car_age <= 3:
                car_age = 5
            else:
                car_age = 8 - car_age

        if car_age <= 0 or emi <= 0 or car_price < 125000:
            return None

        if emi > 99999:
            emi = unit_converter(emi, shorthand_price=True)
        else:
            emi = unit_converter(emi)

        return {
            "emi": emi,
            "car_age": car_age,
            "tenure": loan_tenure_yearly
        }

    emi = serializers.SerializerMethodField(read_only=True)

    get_is_shortlisted = is_car_shortlisted
    is_shortlisted = serializers.SerializerMethodField(read_only=True)

    def get_is_super_saver(self, obj):
        return obj.id in self.context.get('super_saver_car_ids', [])

    is_super_saver = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_interested_people_count(obj):
        offer_count = BuyerListingOffer.objects.filter(listing=obj, is_active=True, is_verified=True).count()
        crf_count = BuyerListing.objects.filter(listing=obj).count()
        offset = (obj.id % 4) + 4
        return offer_count + crf_count + offset

    interested_people_count = serializers.SerializerMethodField(read_only=True)

    def get_market_place_interested_people_count(self, obj):
        if self.context.get('is_user_logged_in') and not obj.is_inventory:
            crf_count = BuyerListing.objects.filter(listing=obj).count()
            offset = (obj.id % 6) + 5
            count = max((2 * crf_count), offset)
            return count

    market_place_interested_people_count = serializers.SerializerMethodField(read_only=True)

    def get_interested_people_count_last_week(self, obj):
        if self.context.get('is_user_logged_in') and obj.is_inventory:
            current_date = get_timezone_aware_current_datetime().date()
            before_seven_days = current_date + timedelta(days=-7)
            visit_booking_count = BuyerVisit.objects.filter(
                buyer_visit_listings__listing_id=obj, visit_date__range=(before_seven_days, current_date)
            ).count()
            offset = (obj.id % 11) + 10
            count = max((2 * visit_booking_count), offset)
            return count

    interested_people_count_last_week = serializers.SerializerMethodField(read_only=True)

    def get_interested_people_count_next_week(self, obj):
        if self.context.get('is_user_logged_in') and obj.is_inventory:
            current_date = get_timezone_aware_current_datetime().date()
            after_seven_days = current_date + timedelta(days=7)
            visit_booking_count = BuyerVisit.objects.filter(
                buyer_visit_listings__listing_id=obj, visit_date__range=(current_date, after_seven_days)
            ).count()
            offset = (obj.id % 11) + 15
            count = max((3 * visit_booking_count), offset)
            return count

    interested_people_count_next_week = serializers.SerializerMethodField(read_only=True)

    def get_seller_contact_details(self, obj):
        seller_contact_details = dict()
        is_seller_contacted = self.context.get('is_seller_contacted', False)
        buyer = self.context.get('buyer', None)

        if self.context.get('is_user_logged_in') and (self.context.get('is_subscribed_buyer') and not obj.is_inventory)\
                or (is_seller_contacted and buyer and validate_buyer_for_seller_data(buyer, obj.id)):
            seller_contact_details = {
                'name': obj.seller.name,
                'mobile': obj.seller.mobile
            }

        return seller_contact_details

    seller_contact_details = serializers.SerializerMethodField(read_only=True)

    def get_is_buyer_crf_exhausted(self, obj):
        return self.context.get('is_buyer_crf_exhausted')

    is_buyer_crf_exhausted = serializers.SerializerMethodField(read_only=True)

    def get_instaveritas_verification_details(self, obj):
        if obj.is_inventory or self.context.get('is_auction_app_user', False) or (
                self.context.get('is_user_logged_in') and (
                self.context.get('is_subscribed_buyer') or not self.context.get('is_buyer_crf_exhausted'))):
            car_history = []
            registration_number = obj.registration_number
            try:
                instaveritas_details = InstaveritasVerification.objects.filter(registration_number=
                                                                               registration_number).latest('id')
            except InstaveritasVerification.DoesNotExist:
                instaveritas_details = None

            if (instaveritas_details and instaveritas_details.registration_date) or obj.registration_date:
                registration_date = instaveritas_details.registration_date if instaveritas_details and\
                                instaveritas_details.registration_date else obj.registration_date
                car_history.append(
                    {'Registration Date': registration_date})

            if instaveritas_details:
                if instaveritas_details.registering_authority:
                    car_history.append({'Registration Authority': instaveritas_details.registering_authority})

                if instaveritas_details.json_payload:
                    instaveritas_data = json.loads(instaveritas_details.json_payload)

                    if 'blacklisted' in instaveritas_data:
                        blacklisted = 'No'
                        if instaveritas_data['blacklisted'] is True:
                            blacklisted = 'Yes'
                        elif instaveritas_data['blacklisted'] == 'NA':
                            blacklisted = 'NA'

                        car_history.append({'Blacklisted': blacklisted})

                        if blacklisted == 'Yes' and 'blacklisted_reason' in instaveritas_data:
                            car_history.append({'Blacklisted Reason':
                                                instaveritas_data['blacklisted_reason']})

                    if 'expiry_date' in instaveritas_data:
                        car_history.append({'Registration Expiry Date': instaveritas_data['expiry_date']})

            if obj.policy_validity:
                car_history.append({'Insurance Validity': obj.policy_validity})

            if obj.policy_type:
                car_history.append({'Insurance Type': obj.policy_type})

            return car_history

    instaveritas_verification_details = serializers.SerializerMethodField(read_only=True)

    def get_depicted_market_price(self, obj):
        return get_market_price(obj)

    depicted_market_price = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('overview', 'inspection_report', 'rc_insurance_validity', 'warranty_eligibility', 'status',
                  'model_data', 'features', 'is_inventory', 'basic_info', 'emi', 'price', 'depicted_market_price',
                  'show_sample_inspection_report', 'is_shortlisted', 'is_super_saver', 'buyer_listing_offer',
                  'interested_people_count', 'market_place_interested_people_count', 'interested_people_count_last_week',
                  'interested_people_count_next_week', 'expected_live_date', 'is_seller_contacted', 'first_error_photo_number',
                  'city_details', 'carwale_price', 'inventory_offered_price', 'inspection_requested_user_id',
                  'seller_contact_details', 'instaveritas_verification_details', 'is_buyer_crf_exhausted')


class FiltersSerializer(serializers.Serializer):
    """
    Serializer class to get filters data

    To get data from this serializer, perform `.get_data()` rather
    than the usual way by accessing `.data`.
    """

    # info for unrelated model serializers
    NESTED_FIELD_KWARGS = {
        'body_type': {'model': BodyType, 'serializer': BodyTypeSerializer, 'context': 'filter_body_types'},
        'colors': {'model': Color, 'serializer': ColorSerializer, 'context': 'filter_colors'},
        'fuel': {'model': FuelType, 'serializer': FuelTypeSerializer, 'context': 'filter_fuel_types'},
        'features': {'model': Feature, 'serializer': FeatureSerializer, 'context': 'filter_features'},
        'transmission': {'model': TransmissionType, 'serializer': TransmissionTypeSerializer,
                         'context': 'filter_transmission'},
    }

    def get_regions(self, obj):
        """
        Returns serialized regions data.

        Set `filter_by_city` to true in serializer context,
        if the regions are to be filtered by city
        """

        filter_city = self.context.get('filter_city', False)
        filter_regions = self.context.get('filter_regions', '')

        if filter_city:
            city_regions = Region.objects.filter(city=obj)
        else:
            city_regions = Region.objects.all()

        region_serializer = RegionSerializer(city_regions, many=True, context={'filter_regions': filter_regions})
        return region_serializer.data

    regions = serializers.SerializerMethodField(read_only=True)

    def get_data(self, obj):
        serializer_data = self.data or dict()

        filter_city = self.context.get('filter_city', True)
        filter_makes = self.context.get('filter_makes', '')
        filter_models = self.context.get('filter_models', '')
        filter_prices = self.context.get('filter_prices', '')
        filter_truebil_direct = self.context.get('filter_truebil_direct', '')
        filter_value_for_money = self.context.get('filter_value_for_money', '')
        filter_km_driven = self.context.get('filter_km_driven', '')
        filter_truescore = self.context.get('filter_truescore', '')
        filter_owners = self.context.get('filter_owners', '')
        filter_years = self.context.get('filter_years', '')
        city_name = self.context.get('city_name', '')
        is_filter_selected = self.context.get('is_filter_selected', False)

        city = None
        if filter_city:
            city = obj

        # context for unrelated model serializers
        nested_fields_context = {
            'filter_body_types': self.context.get('filter_body_types', ''),
            'filter_colors': self.context.get('filter_colors', ''),
            'filter_fuel_types': self.context.get('filter_fuel_types', ''),
            'filter_features': self.context.get('filter_features', ''),
            'filter_transmission': self.context.get('filter_transmission', '')
        }

        serializer_data['make'] = get_active_makes_models(city, filter_makes, filter_models)
        serializer_data['prices'] = get_prices_data(filter_prices)
        serializer_data['is_inventory'] = get_truebil_direct(filter_truebil_direct)
        serializer_data['value_for_money'] = get_value_for_money(filter_value_for_money)
        serializer_data['mileage'] = get_km_drivens(filter_km_driven)
        serializer_data['truescores'] = get_truescores(filter_truescore)
        serializer_data['owners'] = get_owners(filter_owners)
        serializer_data['years'] = get_years(filter_years)
        serializer_data['is_filter_selected'] = is_filter_selected

        # data from other unrelated model serializers
        for nested_field in self.NESTED_FIELD_KWARGS:
            model_name = self.NESTED_FIELD_KWARGS[nested_field]['model']
            serializer_class = self.NESTED_FIELD_KWARGS[nested_field]['serializer']
            model_context = self.NESTED_FIELD_KWARGS[nested_field]['context']
            model_data = model_name.objects.filter(is_active=True) if model_name.__name__ in ['FuelType', 'Feature'] \
                else model_name.objects.all()
            nested_field_data = serializer_class(
                model_data,
                many=True,
                context={model_context: nested_fields_context[model_context], 'city_name': city_name}
            ).data
            serializer_data[nested_field] = nested_field_data

        return serializer_data


class SearchFiltersSerializer(serializers.Serializer):
    """
    Serializer class to get filters for search

    To get data from this serializer, perform `.get_data()` rather
    than the usual way by accessing `.data`.
    """

    def get_data(self, obj):
        serializer_data = self.data or dict()

        filter_city = self.context.get('filter_city', True)
        filter_makes = self.context.get('filter_makes', '')
        filter_models = self.context.get('filter_models', '')
        filter_prices = self.context.get('filter_prices', '')
        filter_fuel_types = self.context.get('filter_fuel_types', '')
        city_name = self.context.get('city_name', '')

        city = None
        if filter_city:
            city = obj

        serializer_data['make'] = get_active_makes_models(city, filter_makes, filter_models)
        serializer_data['prices'] = get_prices_data(filter_prices)
        serializer_data['fuel'] = FuelTypeSerializer(
            FuelType.objects.filter(is_active=True), many=True,
            context={'filter_fuel_types': filter_fuel_types, 'city_name': city_name}
        ).data
        serializer_data['popular_models'] = POPULAR_MODELS

        return serializer_data


class SubheaderFiltersSerializer(serializers.Serializer):
    """
    Serializer class to get filters for subheader

    To get data from this serializer, perform `.get_data()` rather
    than the usual way by accessing `.data`.
    """

    def get_data(self, obj):
        serializer_data = self.data or dict()
        filter_city = self.context.get('filter_city', True)
        city_name = self.context.get('city_name', '')

        city = None
        if filter_city:
            city = obj

        context = {'city_name': city_name}

        serializer_data['body_type'] = BodyTypeSerializer(BodyType.objects.all(), many=True, context=context).data
        serializer_data['mileage'] = get_km_drivens('', city_name=city_name, url=True)
        serializer_data['fuel'] = FuelTypeSerializer(FuelType.objects.filter(is_active=True), many=True,
                                                     context=context).data
        serializer_data['prices'] = get_prices_data(['', ''], city_name=city_name, url=True)[0]['price_range']
        active_makes = get_active_makes_models(city, filter_models='', url=True)
        serializer_data['make'] = subheader_active_makes(active_makes, city_name=city_name)
        serializer_data['years'] = get_year_data_for_subheaders(city_name)

        return serializer_data


class UnavilableCarSerializer(ListingSerializer):
    """Serializer class for Listing model"""

    def get_image_url(self, obj):
        cdn_url = self.context['cdn_url']

        attachment_obj = Attachment.objects.get(listing_id=obj.id, caption_id=2)
        url_first_half = cdn_url + str(obj.id) + '-' + str(attachment_obj.file_number) + '-360.jpg'
        url = url_first_half if not obj.img_version else (url_first_half + '?v=' + str(obj.img_version))

        return url

    image_urls = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('car_info', 'car_link', 'image_urls',)


class DashboardSerializer(ListingSerializer):
    """Serializer class for Listing model and for Dashboard APIs"""

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('variant', 'seller', 'locality')
        return queryset

    def get_buyer_listing_offer(self, obj):
        return get_buyer_offer(obj, self.context.get('buyer'), self.context.get('invalid_numbers'))

    buyer_listing_offer = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_model_id(obj):
        return obj.variant.model_id

    model_id = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('car_link', 'car_info', 'sort_by', 'image_urls', 'is_seller_contacted', 'expected_live_date',
                  'city_id', 'status', 'buyer_listing_offer', 'seller_info', 'is_shortlisted', 'is_seen',
                  'is_super_saver', 'price', 'interested_people_count', 'carwale_price', 'inventory_offered_price',
                  'model_id')


class SellerDashboardSerializer(DashboardSerializer):
    """Serializer class for Seller Dashboard APIs"""

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('variant', 'seller', 'locality')
        return queryset

    @staticmethod
    def get_model_id(obj):
        return obj.variant.model_id

    model_id = serializers.SerializerMethodField(read_only=True)

    def get_total_visitors(self, obj):
        total_user_activity_on_car = UserActivity.objects.filter(listing=obj).count()

        invalid_numbers = self.context.get('invalid_numbers')
        buyers_interested_in_inventory = self.context.get('buyers_interested_in_inventory')
        result_limit = 16
        now = timezone.now()
        crfs_created_within_twenty_days = timezone.now() - timedelta(days=20)

        total_crfs = BuyerListing.objects.\
            filter(listing=obj, is_active=True, created_at__lte=now, created_at__gte=crfs_created_within_twenty_days).\
            order_by('buyer__mobile', '-created_at')

        total_crfs_of_relevant_buyers = \
            total_crfs.exclude(buyer__mobile__in=invalid_numbers).values_list('buyer__mobile',
                                                                              flat=True).distinct()[:result_limit]

        crfs_created_within_thirty_days = timezone.now() - timedelta(days=30)
        crfs_created_within_nine_days = timezone.now() - timedelta(days=9)

        all_buyers_interested_in_same_model = BuyerListing.objects.\
            filter(listing__variant__model=obj.variant.model, listing__locality=obj.locality,
                   created_at__gte=crfs_created_within_thirty_days, created_at__lte=crfs_created_within_nine_days,
                   is_active=True).order_by('buyer__mobile', '-created_at')

        all_relevant_buyers_interested_in_same_model = \
            all_buyers_interested_in_same_model.exclude(buyer__mobile__in=(invalid_numbers,
                                                                           buyers_interested_in_inventory)).\
                values_list('buyer__mobile', flat=True).distinct()[:result_limit]

        buyers_interested_in_same_model = list(set(all_relevant_buyers_interested_in_same_model) -
                                               set(total_crfs_of_relevant_buyers))

        number_of_buyers_interested_in_same_model = len(buyers_interested_in_same_model)

        if total_user_activity_on_car > 5:
            total_visitors = total_user_activity_on_car
        elif len(total_crfs_of_relevant_buyers) + (number_of_buyers_interested_in_same_model/2) > 5:
            total_visitors = len(total_crfs_of_relevant_buyers) + math.ceil(number_of_buyers_interested_in_same_model/2)
        else:
            total_visitors = total_user_activity_on_car + 5

        return total_visitors

    total_visitors = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_online_since(obj):
        car_listed_since = obj.created_at if obj.created_at else obj.inspection_date
        pretty_date = pretty.date(car_listed_since.replace(tzinfo=None)).replace('ago', '')

        if 'hour' in pretty_date:
            pretty_date = 'Today'
        if pretty_date == '1 day ago':
            pretty_date = 'Yesterday'
        return pretty_date

    online_since = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_is_seller_interested_in_inventory(obj):
        return True if SellerInventoryRequest.objects.filter(listing=obj, seller=obj.seller).exists() else False

    is_seller_interested_in_inventory = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_truebil_recommended_price(obj):
        try:
            recommended_price = get_recommended_car_price(obj.is_eligible_inventory, obj.carwale_price)
            return str(recommended_price['min'])+'-'+str(recommended_price['max'])
        except Exception as e:
            return None

    truebil_recommended_price = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('car_link', 'car_info', 'image_urls', 'city_id', 'status', 'seller_info', 'price', 'model_id',
                  'total_visitors', 'online_since', 'is_eligible_inventory', 'inventory_offered_price',
                  'is_seller_interested_in_inventory', 'registration_number', 'truebil_recommended_price')


class CarsAddedToTestDriveSerializer(UnavilableCarSerializer, DashboardSerializer):
    """Serializer class for Buyer Visit Listings"""

    def get_buyer_visit_listing(self, obj):
        buyer_visit = self.context['buyer_visit']

        return {
            "buyer_visit_listing_id": BuyerVisitListing.objects.get(buyer_visit=buyer_visit, listing=obj).id
        }

    buyer_visit_listing = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('car_info', 'car_link', 'image_urls', 'buyer_visit_listing', 'buyer_listing_offer', 'status',
                  'city_id', 'is_seller_contacted', 'is_shortlisted', 'seller_info', 'expected_live_date', 'price')


class BuyerOfferSerializer(serializers.ModelSerializer):
    """Serializer class for Listing model and for rendering Buyer Listing Offer data"""

    def get_buyer_listing_offer(self, obj):
        return get_buyer_offer(obj, self.context.get('buyer'), self.context.get('invalid_numbers'))

    buyer_listing_offer = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Listing
        fields = ('buyer_listing_offer',)
