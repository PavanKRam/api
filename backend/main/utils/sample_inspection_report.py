SAMPLE_INSPECTION_REPORT_ERROR_IMGS = {
    'category_id': 7,
    'category_name': 'Sample Error Photos',
    'images': [
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-46-360.jpg?v=1',
            'caption': 'Dent in Right quarter panel'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-39-360.jpg?v=1',
            'caption': 'Scratch on front bumper'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-45-360.jpg?v=1',
            'caption': 'Scratch on back bumper'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-50-360.jpg?v=1',
            'caption': 'Left mirror is damaged'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-51-360.jpg?v=1',
            'caption': 'Scratch on Right Headlight'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-44-360.jpg?v=1',
            'caption': 'Crack in left tail light'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-48-360.jpg?v=1',
            'caption': 'Scratch on Right Fender'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-40-360.jpg?v=1',
            'caption': 'Dent in Left Fender'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-47-360.jpg?v=1',
            'caption': 'Scratch on Right Rear Door'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-49-360.jpg?v=1',
            'caption': 'Scratch on front bumper'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-43-360.jpg?v=1',
            'caption': 'Dent in Left quarter panel'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-41-360.jpg?v=1',
            'caption': 'Scratch on Right Headlight'
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-42-360.jpg?v=1',
            'caption': 'Scratch on Left Rear Door'
        }
    ]
}

SAMPLE_INSPECTION_REPORT_TYRE_IMGS = {
    'category_id': 8,
    'category_name': 'Sample Tyre Photos',
    'images': [
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-10-360.jpg?v=1',
            'caption': 'Front Left Tyre',
            'comments': {
                'positive': [
                    'No cut marks or hardening is observed in tyre.',
                    'No scratch, rusting and damage observed in rim.'
                ],
                'negative': None
            }
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-12-360.jpg?v=1',
            'caption': 'Rear Left Tyre',
            'comments': {
                'positive': [
                    'No cut marks or hardening is observed in tyre.',
                    'No scratch, rusting and damage observed in rim.'
                ],
                'negative': None
            }
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-14-360.jpg?v=1',
            'caption': 'Rear Right Tyre',
            'comments': {
                'positive': [
                    'No cut marks or hardening is observed in tyre.',
                    'No scratch, rusting and damage observed in rim.'
                ],
                'negative': None
            }
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-16-360.jpg?v=1',
            'caption': 'Front Right Tyre',
            'comments': {
                'positive': [
                    'No cut marks or hardening is observed in tyre.',
                    'No scratch, rusting and damage observed in rim.'
                ],
                'negative': None
            }
        },
        {
            'url': '//d1bxm722pxmpby.cloudfront.net/2336-34-360.jpg?v=1',
            'caption': 'Spare Tyre',
            'comments': {
                'positive': [
                    'Spare tyre is present.'
                ],
                'negative': None
            }
        }
    ]
}

SAMPLE_INSPECTOR_INFO = {
    "cars_inspected": "6400+ cars inspected",
    "experience": "11+ years of experience",
    "id": 1,
    "name": "Ajay",
    "url": "//dw745fgl22f1q.cloudfront.net/images/inspectors/1-ajay.jpg?v=1"
}

SAMPLE_REPORT = {
    "Exterior": {
        "rating": "3.2",
        "details": [
            {
                "Body": {
                    "images": [
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-47-125.jpg?v=1",
                            "caption": "Scratch on Right Rear Door"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-43-125.jpg?v=1",
                            "caption": "Dent in Left quarter panel"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-48-125.jpg?v=1",
                            "caption": "Scratch on Right Fender"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-49-125.jpg?v=1",
                            "caption": "Scratch on front bumper"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-42-125.jpg?v=1",
                            "caption": "Scratch on Left Rear Door"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-40-125.jpg?v=1",
                            "caption": "Dent in Left Fender"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-41-125.jpg?v=1",
                            "caption": "Scratch on Left Front Door"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-46-125.jpg?v=1",
                            "caption": "Dent in Right quarter panel"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-39-125.jpg?v=1",
                            "caption": "Scratch on front bumper"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-45-125.jpg?v=1",
                            "caption": "Scratch on back bumper"
                        }
                    ],
                    "positive_comments": [
                        "No Rusting on outer body panels.",
                        "Right side pillar A, B & C has no damage and factory seal intact.",
                        "Left side pillar A, B & C has no damage and factory seal intact."
                    ],
                    "negative_comments": [
                        "Scratch on front bumper",
                        "Scratch on back bumper",
                        "Scratch on Left Front Door",
                        "Scratch on Right Rear Door",
                        "Scratch on Left Rear Door",
                        "Scratch on Right Fender",
                        "Dent in Left Fender",
                        "Dent in Right quarter panel",
                        "Scratch on right quarter panel",
                        "Dent in Left quarter panel",
                        "Scratch on Left quarter panel"
                    ],
                    "neutral_comments": [],
                }
            },
            {
                "Exterior Components": {
                    "images": [
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-50-125.jpg?v=1",
                            "caption": "Left mirror is damaged"
                        }
                    ],
                    "positive_comments": [
                        "All door handles are operating properly.",
                        "Wipers are working properly."
                    ],
                    "negative_comments": [
                        "Left mirror is damaged"
                    ],
                    "neutral_comments": [],
                }
            },
            {
                "Glasses": {
                    "images": [],
                    "positive_comments": [
                        "No scratch & cracks on windshields and windows."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Lights": {
                    "images": [
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-51-125.jpg?v=1",
                            "caption": "Scratch on Right Headlight"
                        },
                        {
                            "url": "//d1bxm722pxmpby.cloudfront.net/2336-44-125.jpg?v=1",
                            "caption": "Crack in left tail light"
                        }
                    ],
                    "positive_comments": [
                        "Head lights, Tail lights and other lights functioning normally.",
                        "Head lights don't have any moisture formation."
                    ],
                    "negative_comments": [
                        "Scratch on Right Headlight",
                        "Crack in left tail light"
                    ],
                    "neutral_comments": [],
                }
            }
        ]
    },
    "Interior": {
        "rating": "3.2",
        "details": [
            {
                "Air Conditioner": {
                    "images": [],
                    "positive_comments": [
                        "Vent air flow operate as designed.",
                        "Heating & Air Cooling condition operating as designed."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Body Panel": {
                    "images": [],
                    "positive_comments": [
                        "No scratch, dent or discoloration in interior body panels.",
                        "No Rusting in trunk floor."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Interior Components": {
                    "images": [],
                    "positive_comments": [
                        "Interior door handles are operating properly.",
                        "All knobs and switches are operating smoothly.",
                        "Horn is functioning properly."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Seats": {
                    "images": [],
                    "positive_comments": [
                        "No stain, wear & tear in seats."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Windows": {
                    "images": [],
                    "positive_comments": [],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            }
        ]
    },
    "Engine": {
        "rating": "4.2",
        "details": [
            {
                "Battery": {
                    "images": [],
                    "positive_comments": [
                        "Battery condition is good."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Fixtures": {
                    "images": [],
                    "positive_comments": [
                        "No bodywork or rusting in the firewall.",
                        "No rusting or bodywork in apron.",
                        "Factory bolts and seals of bonnet frame are intact."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            },
            {
                "Health": {
                    "images": [],
                    "positive_comments": [
                        "Engine is intact and no overhauling done before.",
                        "No blowback observed in the Engine.",
                        "Engine is free of oil leakage."
                    ],
                    "negative_comments": [],
                    "neutral_comments": [],
                }
            }
        ]
    },
    "Road Test": {
        "rating": "4.2",
        "details": [
            {
                "Malfunction": {
                    "images": [],
                    "positive_comments": [
                        "No abnormal noise from engine.",
                        "No abnormal noise from tyre.",
                        "No abnormal noise from gear box.",
                        "No noise or leakage from axle."
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            },
            {
                "Pedal": {
                    "images": [],
                    "positive_comments": [
                        "Transmission/ clutch operates properly with no slipping.",
                        "Brake is functioning properly with no slipping and free of noise."
                    ],
                    "negative_comments": [
                        "Clutch is Spongy"
                    ],
                    "neutral_comments": [],
                }
            },
            {
                "Stability": {
                    "images": [],
                    "positive_comments": [
                        "Vehicle doesn't pull to the side when brake is applied.",
                        "Vehicle drives straight on level surface."
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            },
            {
                "Suspension": {
                    "images": [],
                    "positive_comments": [
                        "No abnormal noise or vibration from suspension."
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            }
        ]
    },
    "Tyres": {
        "rating": "5.0",
        "details": [
            {
                "Front Left Tyre": {
                    "images": [],
                    "groove_percentage": "80%",
                    "positive_comments": [
                        "No cut marks or hardening is observed in tyre.",
                        "No scratch, rusting and damage observed in rim."
                    ],
                    "urls": [
                        "//d1bxm722pxmpby.cloudfront.net/2336-10-125.jpg?v=1"
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            },
            {
                "Front Right Tyre": {
                    "images": [],
                    "groove_percentage": "80%",
                    "positive_comments": [
                        "No cut marks or hardening is observed in tyre.",
                        "No scratch, rusting and damage observed in rim."
                    ],
                    "urls": [
                        "//d1bxm722pxmpby.cloudfront.net/2336-16-125.jpg?v=1"
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            },
            {
                "Rear Left Tyre": {
                    "images": [],
                    "groove_percentage": "80%",
                    "positive_comments": [
                        "No cut marks or hardening is observed in tyre.",
                        "No scratch, rusting and damage observed in rim."
                    ],
                    "urls": [
                        "//d1bxm722pxmpby.cloudfront.net/2336-12-125.jpg?v=1"
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            },
            {
                "Rear Right Tyre": {
                    "images": [],
                    "groove_percentage": "80%",
                    "positive_comments": [
                        "No cut marks or hardening is observed in tyre.",
                        "No scratch, rusting and damage observed in rim."
                    ],
                    "urls": [
                        "//d1bxm722pxmpby.cloudfront.net/2336-14-125.jpg?v=1"
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            },
            {
                "Spare Tyre": {
                    "images": [],
                    "groove_percentage": "30%",
                    "positive_comments": [
                        "Spare tyre is present."
                    ],
                    "urls": [
                        "//d1bxm722pxmpby.cloudfront.net/2336-34-125.jpg?v=1"
                    ],
                    "negative_comments": [],
                    "neutral_comments": []
                }
            }
        ]
    }
}

SAMPLE_SUMMARY = {
    "positive_comments": [
        "No structural damage in the car. The car is 100% free from accidental & major repair work.",
        "Engine is in perfect condition and no oil leakage found.",
        "Interior of the car is very clean and maintained.",
        "Car is loaded with safety Airbags.",
        "Car has alloy wheels."
    ],
    "negative_comments": [
        "Scratch on some outer body panels.",
        "Insurance has lapsed. You will have to take the insurance."
    ]
}

SAMPLE_REFURB_DETAILS = [
    {
        "label": "Scratch on front bumper",
        "value": 1500
    },
    {
        "label": "Scratch on back bumper",
        "value": 1500
    },
    {
        "label": "Scratch on Left Front Door",
        "value": 1500
    },
    {
        "label": "Scratch on Right Rear Door",
        "value": 1500
    },
    {
        "label": "Scratch on Left Rear Door",
        "value": 1500
    },
    {
        "label": "Scratch on Right Fender",
        "value": 1500
    },
    {
        "label": "Dent in Left Fender",
        "value": 1800
    },
    {
        "label": "Dent in Right quarter panel",
        "value": 1800
    },
    {
        "label": "Scratch on right quarter panel",
        "value": 1500
    },
    {
        "label": "Dent in Left quarter panel",
        "value": 1800
    },
    {
        "label": "Scratch on Left quarter panel",
        "value": 1500
    },
    {
        "label": "Left mirror is damaged",
        "value": 1500
    },
    {
        "label": "Scratch on Right Headlight",
        "value": 500
    },
    {
        "label": "Crack in left tail light",
        "value": 2000
    },
    {
        "label": "Clutch is Spongy",
        "value": 1500
    },
    {
        "label": "Total refurbishment cost",
        "value": 22900
    }
]

def get_sample_inspection_report():
    return {
        "inspector_info": SAMPLE_INSPECTOR_INFO,
        "listing_negative_comments": [],
        "listing_positive_comments": [],
        "refurb_details": SAMPLE_REFURB_DETAILS,
        "summary": SAMPLE_SUMMARY,
        "report": SAMPLE_REPORT
    }