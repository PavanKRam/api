LISTING_ACTIVE_STATUS = ['active', 'refurbishing']
MAKE_NAMES_ORDER = {'Maruti Suzuki': 0, 'Hyundai': 1, 'Honda': 2, 'Toyota': 3, 'Tata': 4, 'Mahindra': 5}
TYRE_IMAGES = [10, 12, 14, 16, 33]
EXCLUDED_CAPTIONS_FOR_GALLERY = [36, 37]
TYRE_CAPTION_SECTION_MAP = {10: 18, 12: 19, 14: 17, 16: 16, 33: 20}
TYRE_CAPTION_COMPONENT_MAP = {10: 132, 12: 134, 14: 130, 16: 128, 33: 136}
TRENDING_MODELS = [217, 471, 209, 468, 182, 76, 242, 464, 132, 463, 306, 260, 438, 314, 379, 202, 248, 449, 301, 363]
GROOVE_INSPECTION_ERROR = 41
SHOWCASE_IMAGES = [2, 3, 1, 5, 18]
RC_INSURANCE_CAPTION_IDS = [10, 12, 14, 16, 33, 36, 37]
LISTING_STATUS_CHOICES = (
    ('incomplete', 'incomplete'),
    ('inactive', 'inactive'),
    ('active', 'active'),
    ('expired', 'expired'),
    ('truebiled', 'truebiled'),
    ('booked', 'booked'),
    ('soldByOthers', 'soldByOthers'),
    ('requestedInspection', 'requestedInspection'),
    ('refurbishing', 'refurbishing')
)

URL_SLUG = '/used-{}cars-in-{}'
MAX_SUPER_SAVER_PERCENT = 0.2
DAYS_TO_SHOW_IN_CALENDAR = 21
DAYS_TO_SHOW_IN_SELLER_INSPECTION_CALENDAR = 14
CAPTION_IDS = [1, 2, 3, 5, 7, 8, 18]
TOP_ORDER_LISTINGS_STATUSES = ['active', 'refurbishing']
LOW_ORDER_LISTINGS_STATUSES = ['incomplete', 'inactive', 'expired', 'truebiled', 'booked', 'soldByOthers',
                               'requestedInspection']
SELLER_LISTING_STATUSES = ['active', 'inactive', 'soldByOthers']

LISTING_ACTIVE_QUERY = """(select listings.id, localities.city_id, ifnull(price, 0)<200000 as price_b_2,
                       ifnull(price, 0) between 100000 and 300000 as price_bw_1_3, ifnull(price, 0) between 200000
                       and 400000 as price_bw_2_4, ifnull(price, 0) between 300000 and 500000 as price_bw_3_5,
                       ifnull(price, 0) between 400000 and 600000 as price_bw_4_6,ifnull(price, 0) between 500000 
                       and 700000 as price_bw_5_7,ifnull(price, 0) between 600000 and 800000 as price_bw_6_8, 
                       ifnull(price, 0) between 700000 and 900000 as price_bw_7_9, ifnull(price, 0)>900000 as 
                       price_a_9, ifnull(listings.body_type_id=1,0) as hatchback, ifnull(listings.body_type_id=2, 0)
                       as sedan,  ifnull(listings.body_type_id=3, 0) as mpv, ifnull(listings.body_type_id=4, 0) 
                       as suv, listings.fuel_type_id_primary=1 as petrol, listings.fuel_type_id_primary=2 as diesel,
                       ifnull(cast(listings.is_inventory as unsigned), 0) as is_inventory, price, created_at from
                       listings join  localities on listings.locality_id = localities.id where listings.status in
                       {0} and localities.city_id = %s) union (select listings.id, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1,
                       0, 0, 0, 1, 0, 0, 0, null from listings where locality_id is null) order by id=%s desc"""

LISTING_LOW_ORDER_QUERY = """(select listings.id, localities.city_id, ifnull(price, 0)<200000 as price_b_2,
        ifnull(price, 0) between 100000 and 300000 as price_bw_1_3, ifnull(price, 0)
        between 200000 and 400000 as price_bw_2_4, ifnull(price, 0) between 300000 and 500000 as price_bw_3_5,
        ifnull(price, 0) between 400000 and 600000 as price_bw_4_6,ifnull(price, 0) between 500000 and 700000
        as price_bw_5_7,ifnull(price, 0) between 600000 and 800000 as price_bw_6_8, ifnull(price, 0)
        between 700000 and 900000 as price_bw_7_9, ifnull(price, 0)>900000 as price_a_9,
        ifnull(listings.body_type_id=1,0) as hatchback, ifnull(listings.body_type_id=2, 0) as sedan, 
        ifnull(listings.body_type_id=3, 0) as mpv, ifnull(listings.body_type_id=4, 0) as suv, 
        listings.fuel_type_id_primary=1 as petrol, listings.fuel_type_id_primary=2 as diesel,
        ifnull(cast(listings.is_inventory as unsigned), 0) as is_inventory, price, created_at from listings join 
        localities on listings.locality_id = localities.id where listings.status in {0} and listings.id = %s and
        localities.city_id = %s) union (select id, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, null
        from listings where locality_id is null)"""

CARS_FOR_DASHBOARD = 3
MINIMUM_BALANCE_FOR_BIDDING = 5000
SECURITY_DEPOSIT_DEDUCTION_AMOUNT = 5000
POST_SALE_SECURITY_DEPOSIT_AMOUNT = 2500
PARKING_CHARGE_PER_DAY = 0
DELIVERY_CHARGE = 600
