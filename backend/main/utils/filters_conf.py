"""
Filters data
"""

VALUE_FOR_MONEY = [{"id": "1", "name": "Value for money cars"}]
KM_DRIVENS = [
    {"id": "20000", "name": "Less than 20,000 km"},
    {"id": "40000", "name": "Less than 40,000 km"},
    {"id": "60000", "name": "Less than 60,000 km"},
    {"id": "80000", "name": "Less than 80,000 km"},
    {"id": "80001", "name": "Above 80,000 km"}
]
TRUESCORES = [
    {"id": "4.5", "name": "More than 4.5"},
    {"id": "4.0", "name": "More than 4.0"},
    {"id": "3.5", "name": "More than 3.5"},
    {"id": "3.0", "name": "More than 3.0"}
]
OWNERS = [
    {"id": "1", "name": "1st Owner"},
    {"id": "2", "name": "2nd Owner"},
    {"id": "3", "name": "2+ Owners"}
]
YEARS = [
    {"id": "2015-01-01", "name": "2015 or later"},
    {"id": "2014-01-01", "name": "2014 or later"},
    {"id": "2013-01-01", "name": "2013 or later"},
    {"id": "2012-01-01", "name": "2012 or later"},
    {"id": "2011-12-31", "name": "Before 2012"}
]
TRUEBIL_DIRECT = [{"id": "1", "name": "Truebil Direct"}]
PRICE_RANGES = [
    {"id": "1", "price_min": "50000", "price_max": "200000", "name": "Under 2 Lakhs"},
    {"id": "2", "price_min": "200000", "price_max": "300000", "name": "2 - 3 Lakhs"},
    {"id": "3", "price_min": "300000", "price_max": "400000", "name": "3 - 4 Lakhs"},
    {"id": "4", "price_min": "400000", "price_max": "500000", "name": "4 - 5 Lakhs"},
    {"id": "5", "price_min": "500000", "price_max": "800000", "name": "5 - 8 Lakhs"},
    {"id": "6", "price_min": "800000", "price_max": "3000000", "name": "Above 8 Lakhs"}
]
POPULAR_MODELS = [
    {"id": "217", "name": "Swift", "make_name": "Maruti Suzuki", "make_id": "17", "similar_models": None},
    {"id": "471", "name": "Wagon R", "make_name": "Maruti Suzuki", "make_id": "17", "similar_models": "174"},
    {"id": "464", "name": "Alto", "make_name": "Maruti Suzuki", "make_id": "17", "similar_models": "184,177"},
    {"id": "209", "name": "Swift Dzire", "make_name": "Maruti Suzuki", "make_id": "17", "similar_models": None},
    {"id": "468", "name": "i20", "make_name": "Hyundai", "make_id": "20", "similar_models": "241"},
    {"id": "260", "name": "i10", "make_name": "Hyundai", "make_id": "20", "similar_models": "265"},
    {"id": "76", "name": "Honda City", "make_name": "Honda", "make_id": "9", "similar_models": None}
]
GROOVE_INSPECTION_COMPONENTS = {
    "Front Right Tyre": {
        "id": 128,
        "caption_id": "16",
        "inspection_section": "16"
    },
    "Rear Right Tyre": {
        "id": 130,
        "caption_id": "14",
        "inspection_section": "17"
    },
    "Front Left Tyre": {
        "id": 132,
        "caption_id": "10",
        "inspection_section": "18"
    },
    "Rear Left Tyre": {
        "id": 134,
        "caption_id": "12",
        "inspection_section": "19"
    },
    "Spare Tyre": {
        "id": 136,
        "caption_id": "33",
        "inspection_section": "20"
    }
}
SUBHEADER_POPULAR_MODELS = ['Chevrolet Beat', 'Chevrolet Spark', 'Fiat Linea', 'Fiat Punto', 'Ford EcoSport',
                            'Ford Fiesta', 'Ford Figo', 'Honda Amaze', 'Honda City', 'Honda Civic', 'Honda Jazz',
                            'Hyundai Eon', 'Hyundai Getz', 'Hyundai Grand i10', 'Hyundai i10', 'Hyundai i20',
                            'Hyundai Santro Xing', 'Hyundai Verna', 'Mahindra Scorpio', 'Mahindra XUV500',
                            'Mahindra Xylo', 'Maruti Suzuki Alto', 'Maruti Suzuki Eeco', 'Maruti Suzuki Ertiga',
                            'Maruti Suzuki Estilo', 'Maruti Suzuki Ritz', 'Maruti Suzuki Swift',
                            'Maruti Suzuki Swift DZire', 'Maruti Suzuki SX4', 'Maruti Suzuki Wagon R', 'Skoda Fabia',
                            'Skoda Rapid', 'Tata Indica Vista', 'Tata Indigo Manza', 'Tata Nano', 'Toyota Etios',
                            'Toyota Innova', 'Volkswagen Polo', 'Volkswagen Vento']

HIGH_DEMAND_MODELS = [217, 471, 76, 468, 260, 209, 480, 363, 132, 464, 182, 177, 184, 464, 379, 380, 373, 121, 123,
                      126, 249]

RTO_STATE_CODE = [{"id": "MH", "name": "Maharashtra"},
                  {"id": "GA", "name": "Goa"},
                  {"id": "DL", "name": "Delhi"},
                  {"id": "KA", "name": "Karnataka"},
                  {"id": "HR", "name": "Haryana"},
                  {"id": "UP", "name": "Uttar Pradesh"}
                  ]
