# -*- coding: utf-8 -*-
"""
Helper methods for serializers
"""

from django.contrib.auth.models import AnonymousUser
from django.db.models import Prefetch
from django.utils import timezone

from auction_app.auction_helpers import is_auction_app
from main.models import Make, Model, Variant, ComponentError, InspectionCategory, SuperSaverListing, FuelType
from main.quick_ops import get_listing_min_max_price, find_between, get_price_range_from_slug, decode_to_utf_8
from truebil_management.models import SeoKeyword
from truebil_management.utils.constants import WHITELIST
from user.models import BuyerListing, InvalidNumber, City, Locality, Region
from user.user_helpers import get_user_subscriptions, get_client_ip
from .constants import (LISTING_ACTIVE_STATUS, MAKE_NAMES_ORDER, URL_SLUG, GROOVE_INSPECTION_ERROR)
from .filters_conf import *


def can_show_inspection_report(request, listing_obj):
    is_subscribed_buyer = False
    is_user_logged_in = False
    is_car_owner = False
    is_truebil_user = get_client_ip(request.META) in WHITELIST
    if type(request.user) is not AnonymousUser:
        is_user_logged_in = True
        is_subscribed_buyer = get_user_subscriptions(request.user.id)['buyer']
        is_car_owner = request.user.mobile == listing_obj.seller.mobile

    return (is_user_logged_in and is_subscribed_buyer) or is_truebil_user or is_car_owner or listing_obj.is_inventory \
           or is_auction_app(request.path_info)


def get_serializer_data(request, serializer_class, city, filter_city):
    filter_makes = decode_to_utf_8(request.query_params.get('make', '')).split(',')
    filter_models = decode_to_utf_8(request.query_params.get('model', '')).split(',')
    filter_min_price = request.query_params.get('price_min', '')
    filter_max_price = request.query_params.get('price_max', '')
    filter_truebil_direct = request.query_params.get('is_inventory', '')
    filter_value_for_money = request.query_params.get('value_for_money', '')
    filter_km_driven = request.query_params.get('mileage', '')
    filter_truescore = request.query_params.get('truescores', '')
    filter_owners = decode_to_utf_8(request.query_params.get('owners', '')).split(',')
    filter_years = request.query_params.get('years', '')
    filter_regions = decode_to_utf_8(request.query_params.get('regions', '')).split(',')
    filter_body_types = decode_to_utf_8(request.query_params.get('body_type', '')).split(',')
    filter_features = decode_to_utf_8(request.query_params.get('features', '')).split(',')
    filter_fuel_types = decode_to_utf_8(request.query_params.get('fuel', '')).split(',')
    filter_transmission = decode_to_utf_8(request.query_params.get('transmission', '')).split(',')
    filter_colors = decode_to_utf_8(request.query_params.get('colors', '')).split(',')
    filter_slug = request.query_params.get('slug', '')

    context = {
        'filter_city': filter_city,
        'filter_makes': filter_makes,
        'filter_models': filter_models,
        'filter_prices': [filter_min_price, filter_max_price],
        'filter_truebil_direct': filter_truebil_direct,
        'filter_value_for_money': filter_value_for_money,
        'filter_km_driven': filter_km_driven,
        'filter_truescore': filter_truescore,
        'filter_owners': filter_owners,
        'filter_years': filter_years,
        'filter_regions': filter_regions,
        'filter_body_types': filter_body_types,
        'filter_features': filter_features,
        'filter_fuel_types': filter_fuel_types,
        'filter_transmission': filter_transmission,
        'filter_colors': filter_colors,
    }

    if filter_slug:
        slug_city, context = apply_slug_filters(context=context, filter_slug=filter_slug)
        city = slug_city or city

    context['is_filter_selected'] = is_filter_selected(context=context)

    try:
        context['city_name'] = city.name.lower()
    except AttributeError:
        context['city_name'] = 'india'

    serializer = serializer_class(city, context=context)
    return serializer.get_data(city)


def get_years(filter_years):
    for year in YEARS:
        is_checked = False

        if year['id'] == filter_years:
            is_checked = True

        year['is_checked'] = is_checked

    return YEARS


def get_year_data_for_subheaders(city_name):
    for year in YEARS:
        year['url'] = URL_SLUG.format('', city_name) + '?years=' + year['id']

    return YEARS


def get_owners(filter_owners):
    for owner in OWNERS:
        is_checked = False
        if owner['id'] in filter_owners:
            is_checked = True

        owner['is_checked'] = is_checked

    return OWNERS


def get_truescores(filter_truescore):
    for truescore in TRUESCORES:
        is_checked = False
        if truescore['id'] == filter_truescore:
            is_checked = True

        truescore['is_checked'] = is_checked

    return TRUESCORES


def get_km_drivens(filter_km_driven, city_name='india', url=False):
    for km_driven in KM_DRIVENS:
        is_checked = False
        if km_driven['id'] == filter_km_driven:
            is_checked = True

        km_driven['is_checked'] = is_checked

        if url:
            km_driven['url'] = URL_SLUG.format('', city_name) + '?mileage=' + km_driven['id']

    return KM_DRIVENS


def get_value_for_money(filter_value_for_money):
    for vfm in VALUE_FOR_MONEY:
        is_checked = False
        if vfm['id'] == filter_value_for_money:
            is_checked = True

        vfm['is_checked'] = is_checked

    return VALUE_FOR_MONEY


def get_truebil_direct(filter_truebil_direct):
    for tb in TRUEBIL_DIRECT:
        is_checked = False
        if tb['id'] == filter_truebil_direct:
            is_checked = True

        tb['is_checked'] = is_checked

    return TRUEBIL_DIRECT


def get_prices_data(filter_prices, city_name='india', url=False):
    min_price, max_price = get_listing_min_max_price()
    filtered_price_min, filtered_price_max = filter_prices[0] or '', filter_prices[1] or ''

    if filtered_price_min:
        if int(filtered_price_min) < min_price:
            filtered_price_min = str(min_price)
        if int(filtered_price_min) > max_price:
            filtered_price_min = str(max_price)
    else:
        filtered_price_min = str(min_price)

    if filtered_price_max:
        if int(filtered_price_max) > max_price:
            filtered_price_max = str(max_price)
        if int(filtered_price_max) < min_price:
            filtered_price_max = str(min_price)
    else:
        filtered_price_max = str(max_price)

    prices_data = [{
        "min_price": min_price,
        "max_price": max_price,
        "filtered_price_min": filtered_price_min,
        "filtered_price_max": filtered_price_max,
        "price_range": []
    }]

    PRICE_RANGES[0]['price_min'] = str(min_price)
    PRICE_RANGES[5]['price_max'] = str(max_price)

    for price_range in PRICE_RANGES:
        is_checked = False

        if price_range['price_min'] == filtered_price_min and price_range['price_max'] == filtered_price_max:
            is_checked = True

        price_range['is_checked'] = is_checked

        if url:
            url_price = ''
            if int(price_range['price_min']) < 200000:
                url_price = 'under-2-lakh-'
            elif int(price_range['price_min']) < 800000:
                price_min_in_lakh = int(price_range['price_min']) / 100000
                price_max_in_lakh = int(price_range['price_max']) / 100000
                url_price = '{}-lakh-to-{}-lakh-'.format(price_min_in_lakh, price_max_in_lakh)
            else:
                url_price = '8-lakh-above-'

            price_range['url'] = URL_SLUG.format(url_price, city_name)

    prices_data[0]['price_range'] = PRICE_RANGES

    return prices_data


def subheader_active_makes(active_makes, city_name='india'):
    """
    Formats active make models based on subheader popular models.
    """

    for active_make in active_makes:
        other_model_ids = []
        subheader_models = []
        for active_model in active_make['model']:
            active_make_model_name = active_make['name'] + active_model['name']
            if active_make_model_name not in SUBHEADER_POPULAR_MODELS:
                other_model_ids.append(str(active_model['id']))
            else:
                subheader_models.append(active_model)

        active_make['model'] = subheader_models

        if len(other_model_ids):
            active_make['model'].append({
                'id': -1,
                'name': 'other ' + (active_make['name']).lower(),
                'url': URL_SLUG.format('', city_name) + '?model=' + ','.join(other_model_ids)
            })

    return active_makes


def get_active_makes(city=None):
    """
    Fetches distinct makes for the active listings ordered by `MAKE_NAMES_ORDER`
    with prefetched distinct active models ordered by model name

    :param: city: Filters the active makes by city
            required: false
            type: City
    """

    model_queries = {'variants__variant_listings__status__in': LISTING_ACTIVE_STATUS}
    if city:
        model_queries['variants__variant_listings__locality__city'] = city

    makes_qs = Make.objects.prefetch_related(
        Prefetch('models', queryset=Model.objects.filter(**model_queries).distinct().order_by('name'),
                 to_attr='active_models')
    )

    queries = {'models__variants__variant_listings__status__in': LISTING_ACTIVE_STATUS}
    if city:
        queries['models__variants__variant_listings__locality__city'] = city

    active_makes_city_qs = makes_qs.filter(**queries).distinct()
    ordered_makes_qs = sorted(active_makes_city_qs, key=lambda x: MAKE_NAMES_ORDER.get(x.name, 1000))

    return ordered_makes_qs


def build_url_slug(obj_name, city_name):
    return URL_SLUG.format((obj_name).lower() + '-', city_name)


def is_model_checked(model_id, filter_models=''):
    if str(model_id) in filter_models:
        return True

    return False


def get_similar_models_if_popular(model_id):
    """
    Returns True, `similar_models` if the model_id is in popular models
    else False, None
    """

    for popular_model in POPULAR_MODELS:
        if popular_model['id'] == str(model_id):
            return True, popular_model['similar_models']

    return False, None


def get_active_models_by_make(active_make, city_name, filter_models='', url=False, is_make_selected=False):
    """
    Returns a list of active models of the `active_make`
    """

    active_models_by_make = active_make.active_models

    models_dict = []
    for active_model_by_make in active_models_by_make:
        model_dict = dict()
        is_popular, similar_models = get_similar_models_if_popular(active_model_by_make.id)

        model_dict['id'] = active_model_by_make.id
        model_dict['name'] = active_model_by_make.name.replace(active_make.name, '')
        model_dict['is_checked'] = is_model_checked(active_model_by_make.id, filter_models) or is_make_selected
        model_dict['is_popular'] = is_popular
        model_dict['similar_models'] = similar_models
        model_dict['body_type_id'] = active_model_by_make.body_type_id

        if url:
            url_model = '-'.join((active_model_by_make.name).lower().split(' ')) + '-'
            model_dict['url'] = URL_SLUG.format(url_model, city_name)

        models_dict.append(model_dict)

    return models_dict


def get_active_makes_models(city, filter_makes='', filter_models='', url=False):
    """
    Returns a list of active makes with nested active models

    --
    Example output:
        [{
            'id': 1,
            'name': 'XYZ',
            'models': [{
                'id': 101,
                'name': 'ABC',
                'is_checked': true,
                'is_popular': true,
                'similar_models': '56,70'
            },
            {
                ...
            }]
        },
        {
            ...
        }]
    """

    active_make_models = []
    active_makes = get_active_makes(city)

    city_name = 'india'
    if city:
        city_name = city.name

    for active_make in active_makes:
        is_make_selected = True if str(active_make.id) in filter_makes else False
        make_dict = dict()
        make_dict['id'] = active_make.id
        make_dict['name'] = active_make.name
        make_dict['is_checked'] = is_make_selected
        make_dict['model'] = get_active_models_by_make(
            active_make, city_name=(city_name).lower(), filter_models=filter_models, url=True,
            is_make_selected=is_make_selected
        )
        if url:
            url_make = '-'.join((active_make.name).lower().split(' ')) + '-'
            make_dict['url'] = URL_SLUG.format(url_make, (city_name).lower())

        active_make_models.append(make_dict)

    return active_make_models


def get_component_error_comment_map():
    component_error_qs = ComponentError.objects.prefetch_related('inspection_component', 'inspection_error')
    component_error_comment_map = dict()
    for component_error_obj in component_error_qs:
        component_error_comment_map[str(component_error_obj.inspection_component.id) + "-" + str(component_error_obj.inspection_error.id)] = component_error_obj
    return component_error_comment_map


def get_inspection_category_ids():
    return InspectionCategory.objects.values_list('id', flat=True)


def get_inspection_category_names():
    return InspectionCategory.objects.values_list('name', flat=True)


def get_groove_percentage(qs):
    groove_percentage = dict()
    for listing_error in qs:
        listing_inspection_error_id = listing_error.inspection_error.id
        listing_inspection_component_id = listing_error.inspection_component.id
        comment = listing_error.comment
        if listing_inspection_error_id == GROOVE_INSPECTION_ERROR:
            if listing_inspection_component_id == GROOVE_INSPECTION_COMPONENTS['Front Right Tyre']['id']:
                groove_percentage[GROOVE_INSPECTION_COMPONENTS['Front Right Tyre']['inspection_section']] = comment
            elif listing_inspection_component_id == GROOVE_INSPECTION_COMPONENTS['Rear Right Tyre']['id']:
                groove_percentage[GROOVE_INSPECTION_COMPONENTS['Rear Right Tyre']['inspection_section']] = comment
            elif listing_inspection_component_id == GROOVE_INSPECTION_COMPONENTS['Front Left Tyre']['id']:
                groove_percentage[GROOVE_INSPECTION_COMPONENTS['Front Left Tyre']['inspection_section']] = comment
            elif listing_inspection_component_id == GROOVE_INSPECTION_COMPONENTS['Rear Left Tyre']['id']:
                groove_percentage[GROOVE_INSPECTION_COMPONENTS['Rear Left Tyre']['inspection_section']] = comment
            elif listing_inspection_component_id == GROOVE_INSPECTION_COMPONENTS['Spare Tyre']['id']:
                groove_percentage[GROOVE_INSPECTION_COMPONENTS['Spare Tyre']['inspection_section']] = comment
    return groove_percentage


def is_car_shortlisted(serializer_obj, listing_obj):
    is_shortlisted = False
    if serializer_obj.context.get('is_user_logged_in', False):
        request = serializer_obj.context.get('request')
        is_shortlisted = request.user.user_activities.filter(
            listing=listing_obj
        ).order_by('-id').values_list('is_shortlisted', flat=True).first() or False

    return is_shortlisted


def get_super_saver_car_ids():
    today = timezone.now().date()
    tomorrow = today + timezone.timedelta(days=1)

    # fetches SuperSaverListing listing ids filtered by today's date
    # `__date` is not used as the DB doesn't support timezone info
    # for MySQL's `convert_tz` function.
    return set(SuperSaverListing.objects.filter(
        created_at__gte=today, created_at__lt=tomorrow
    ).values_list('listing_id', flat=True))


def get_invalid_numbers():
    return set(InvalidNumber.objects.all().values_list('mobile', flat=True))


def get_buyer_interested_in_inventory():
    return BuyerListing.objects.filter(listing__is_inventory = True).values_list('buyer__mobile', flat=True).distinct()


def get_variant_name(car, source=None):
    if source == 'desktop':
        return str(car.manufacturing_date)[:4] + '' + get_car_name_without_make_name(car)
    else:
        return str(car.manufacturing_date)[:4] + ' ' + car.variant.name


def get_model_name(car):
    car_model = car.variant.model
    return car_model.name.replace(car_model.make.name, '').lstrip()


def get_car_name_without_make_name(car):
    return str(car.variant.name).replace(car.variant.model.make.name, '')


def apply_slug_filters(context, filter_slug):
    """
    This function process slug string and add related filter to context
    :param context: context dict
    :param filter_slug: slug string
    :return: city object and processed context data
    """
    split_by_city_name = filter_slug.rsplit('-', 1)
    city_name = split_by_city_name[1]
    city = City.objects.filter(name=city_name.capitalize()).first()
    if city:
        context['filter_city'] = True

    relevant_slug = find_between(split_by_city_name[0], 'used-', '-cars-in')
    if relevant_slug:
        filter_list = str(relevant_slug).split(' ')
        seo_filters = SeoKeyword.objects.filter(filter_name__in=filter_list)
        if seo_filters:
            context = apply_seo_filters(context, seo_filters)
        else:
            price_range = get_price_range_from_slug(relevant_slug)
            context['filter_prices'] = [str(price_range['price_min']), str(price_range['price_max'])]

    return city, context


def apply_seo_filters(context, seo_filters):
    """
    This function apply related filters from SeoKeywords table
    :param context: context dict
    :param seo_filters: SeoKeywords queryset
    :return:
    """
    slug_context_map = {
        'make': 'filter_makes',
        'model': 'filter_models',
        'body_type': 'filter_body_types',
        'fuel_type': 'filter_fuel_types',
        'is_inventory': 'filter_truebil_direct',
        'is_value_for_money': 'filter_value_for_money'
    }

    for seo_filter in seo_filters:
        if seo_filter.filter_category in ['model', 'make', 'body_type']:
            context[slug_context_map[seo_filter.filter_category]].append(str(seo_filter.filter_id))
        elif seo_filter.filter_category == 'fuel_type':
            fuel_id = FuelType.objects.get(name=seo_filter.filter_id).id
            context[slug_context_map[seo_filter.filter_category]].append(str(fuel_id))
        elif seo_filter.filter_category in ['is_value_for_money', 'is_inventory']:
            context[slug_context_map[seo_filter.filter_category]] = (str(seo_filter.filter_id))

    return context


def is_filter_selected(context):
    min_price, max_price = get_listing_min_max_price()

    if context['filter_prices'][0] == str(min_price):
        context['filter_prices'][0] = ''
    if context['filter_prices'][1] == str(max_price):
        context['filter_prices'][1] = ''

    for key, value in context.iteritems():
        if type(value) is str and value:
            return True
        if type(value) is list:
            for filter_value in value:
                if filter_value:
                    return True

    return False


def get_make_model_variants():
    makes = list(Make.objects.values('id', 'name').all().order_by('name'))
    models = list(Model.objects.values('id', 'name', 'make_id').all())
    variants = list(Variant.objects.values('id', 'name', 'model_id').all())
    makes = sorted(makes, key=lambda x: MAKE_NAMES_ORDER.get(x['name'], 1000))

    for make in makes:
        model_data = list()
        for model in models:
            if 'make_id' in model and model['make_id'] == make['id']:
                variant_data = list()
                for variant in variants:
                    if 'model_id' in variant and variant['model_id'] == model['id']:
                        variant.pop('model_id')
                        variant['name'] = remove_substring(variant['name'], model['name'])
                        variant_data.append(variant)
                model.pop('make_id')
                model['variant'] = sorted(variant_data, key=lambda k: k['name'].lower())
                model['name'] = remove_substring(model['name'], make['name'])
                model_data.append(model)
        make['model'] = sorted(model_data, key=lambda k: k['name'].lower())

    return makes


def get_region_localities(city_id):
    regions = list(Region.objects.filter(city_id=city_id).values('id', 'name').order_by('name'))
    localities = list(Locality.objects.filter(city_id=city_id).values('id', 'name', 'region_id').order_by('name'))

    region_map = dict()
    for locality in localities:
        if locality['region_id'] not in region_map:
            region_map[locality['region_id']] = list()
        region_map[locality['region_id']].append(locality)

    others = {'id': -1, 'name': 'Others'}
    for region in regions:
        region_map[region['id']].append(others)
        region['localities'] = region_map[region['id']]

    localities.append(others)
    return regions, localities


def remove_substring(string, substring):
    return string.replace(substring, '').lstrip()
