import numpy as np
from django.contrib.auth.models import AnonymousUser

from user.models import City, Buyer
from .data_science.ds_helpers import pandas_cached_query
from .models import SuperSaverListing
from .quick_ops import decode_to_utf_8
from .utils.constants import LISTING_ACTIVE_QUERY, TOP_ORDER_LISTINGS_STATUSES, LISTING_LOW_ORDER_QUERY, \
    LOW_ORDER_LISTINGS_STATUSES


def get_city_super_saver_listings(city_id):
    """
        :param city_id: city_id (integer)
        :return: super saver listing's ids of that city

    """
    city_super_saver_listings = SuperSaverListing.objects.filter(listing__status__in=TOP_ORDER_LISTINGS_STATUSES,
                                                                 listing__locality_id__city_id=city_id).values_list(
                                                                 'listing__id', flat=True).distinct()

    return city_super_saver_listings


def get_city_listing_details(city_id, **kwargs):
    """
    :param city_id: city_id (integer)
    :return: listing features of listing_id passed(if none then all active listings features)

    """
    if 'status' in kwargs:
        listing_features = pandas_cached_query(LISTING_LOW_ORDER_QUERY.format(tuple(LOW_ORDER_LISTINGS_STATUSES)) %
                                               (kwargs['listing_id'], city_id))
        return np.array(listing_features[0:1])[0]
    else:
        listing_id = kwargs['listing_id'] if 'listing_id' in kwargs else 'NULL'
        listings_features = pandas_cached_query(LISTING_ACTIVE_QUERY.format(tuple(TOP_ORDER_LISTINGS_STATUSES)) %
                                                (city_id, listing_id))
        return np.array(listings_features)


def get_recommendation_params(request):
    """
    :param request: request data
    :return: buyer, city_id and seen car ids

    """
    buyer_id = request.query_params.get('buyer_id', None)
    city_id = request.query_params.get('city_id', None)
    slug = request.query_params.get('slug', None)
    seen_car_ids = request.query_params.get('cars_seen', None)

    if not buyer_id and type(request.user) is not AnonymousUser:
        try:
            buyer_id = Buyer.objects.values('id').get(mobile=request.user.mobile)['id']
        except Buyer.DoesNotExist:
            buyer_id = None

    if slug:
        city_name = slug.split('-')[-1]
        city_id = City.objects.values_list('id', flat=True).get(name=city_name)

    if seen_car_ids:
        seen_car_ids = [int(v) for v in decode_to_utf_8(seen_car_ids).split(',')]

    return {'buyer_id': buyer_id, 'city_id': city_id, 'seen_car_ids': seen_car_ids}
