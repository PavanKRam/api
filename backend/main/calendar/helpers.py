
def get_visit_time_range(slot):
    return str(slot.start_time.strftime("%-I%p").lower()) + ' - ' + str(slot.end_time.strftime("%-I%p").lower())


def get_permissible_visit_time_range(visit_time_slots):
    date_time = list()
    for slot in visit_time_slots:
        date_time.append({
            "range": get_visit_time_range(slot),
            "id": slot.id
        })
    return date_time
