from datetime import datetime, timedelta

from __builtin__ import reduce
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView

from main.calendar.helpers import get_permissible_visit_time_range
from main.models import Listing
from main.quick_ops import get_refurbished_date, get_timezone_aware_current_datetime
from main.utils.constants import DAYS_TO_SHOW_IN_CALENDAR, DAYS_TO_SHOW_IN_SELLER_INSPECTION_CALENDAR
from user.models import VisitTimeSlot, CarInspectorAvailability


@api_view(['GET'])
def calendar(request):
    """
    API View to get the calendar for visit booking

    ---
    parameters:
        - name: listing_id
          description: Listing id
          required: false
          type: string

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    listing_id = request.query_params.get('listing_id', None)
    calendar_datetime = request.query_params.get('calendar_datetime', None)

    refurbishment_counter = 0
    days_to_show = DAYS_TO_SHOW_IN_CALENDAR
    active_visit_time_slots = VisitTimeSlot.objects.filter(is_active=True)
    is_present_day_over = False
    is_car_under_refurbishment = False

    if listing_id is not None:
        listing = Listing.objects.get(id=listing_id)
        listing_status = listing.status
        is_car_under_refurbishment = listing_status == 'refurbishing'
        if is_car_under_refurbishment:
            expected_live_date = get_refurbished_date(listing.expected_live_date)['date'].date()
            present_date = timezone.now().date()
            date_diff = (expected_live_date - present_date).days
            if date_diff != 0:
                refurbishment_counter = date_diff

            if (days_to_show - refurbishment_counter) > 5:
                days_to_show -= refurbishment_counter
            else:
                days_to_show = 8

    current_time = get_timezone_aware_current_datetime()
    adjusted_time = current_time.hour + 2

    visit_booking_last_slot_hour = active_visit_time_slots.latest('start_time').start_time.hour
    if (not is_car_under_refurbishment) and adjusted_time >= visit_booking_last_slot_hour:
        is_present_day_over = True

    if calendar_datetime:
        calendar_datetime = datetime.strptime(calendar_datetime, "%Y-%m-%d")
        calendar_date = calendar_datetime.date()

        if calendar_date > current_time.date():
            date_time = get_permissible_visit_time_range(active_visit_time_slots)
        elif calendar_date == current_time.date():
            visit_time_slots_left = active_visit_time_slots if is_present_day_over else \
                active_visit_time_slots.filter(start_time__gt=current_time)

            date_time = get_permissible_visit_time_range(visit_time_slots_left)
        else:
            date_time = "Invalid date time"

        return Response({'status': True, 'time': date_time}, status=status.HTTP_200_OK)

    if is_present_day_over:
        delta = refurbishment_counter + 1
    else:
        delta = refurbishment_counter

    date_list = list()
    base = datetime.today()
    for day_delta in range(0, days_to_show):
        date = base + timedelta(days=delta+day_delta)
        date_dict = {
            "day": "Today" if (date == base) else date.strftime("%A"),
            "date": (date.strftime("%-d")) + ' ' + date.strftime("%B")[:3],
            "db_date": str(date.date())
        }
        date_list.append(date_dict)

    if is_car_under_refurbishment:
        date_time = get_permissible_visit_time_range(active_visit_time_slots)
    else:
        visit_time_slots_left = active_visit_time_slots if is_present_day_over else \
            active_visit_time_slots.filter(start_time__gt=current_time)

        date_time = get_permissible_visit_time_range(visit_time_slots_left)

    return Response({'status': True, 'date': date_list, 'time': date_time}, status=status.HTTP_200_OK)


class SellerCarInspectionCalender(APIView):
    """
        API View to get the calendar for seller inspection booking

        ---
        parameters:
            - name: locality_id
              description: Locality id
              required: false
              type: string

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml

        """
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request):
        response = dict()
        locality_id = request.query_params.get('locality_id', None)
        calendar_datetime = request.query_params.get('calendar_datetime', None)

        delta = 0
        days_to_show = DAYS_TO_SHOW_IN_SELLER_INSPECTION_CALENDAR
        active_visit_time_slots = VisitTimeSlot.objects.filter(is_active=True)
        is_present_day_over = False
        current_time = get_timezone_aware_current_datetime()
        adjusted_time_hour = current_time.hour + 2

        visit_booking_last_slot_hour = active_visit_time_slots.latest('start_time').start_time.hour
        if adjusted_time_hour >= visit_booking_last_slot_hour:
            is_present_day_over = True

        visit_time_slots_left = active_visit_time_slots if is_present_day_over else \
            active_visit_time_slots.filter(start_time__gt=current_time)
        date_time = get_permissible_visit_time_range(visit_time_slots_left)

        if calendar_datetime:
            calendar_datetime = datetime.strptime(calendar_datetime, "%Y-%m-%d")
            calendar_date = calendar_datetime.date()
            if calendar_date > current_time.date() + timedelta(days=14) or calendar_date < current_time.date():
                date_time = "Invalid date time"
            elif calendar_date > current_time.date():
                date_time = get_permissible_visit_time_range(active_visit_time_slots)
            elif calendar_date == current_time.date():
                visit_time_slots_left = active_visit_time_slots if is_present_day_over else \
                    active_visit_time_slots.filter(start_time__gt=current_time)

                date_time = get_permissible_visit_time_range(visit_time_slots_left)

            return Response({'status': True, 'time_slots': date_time}, status=status.HTTP_200_OK)

        inspectors = CarInspectorAvailability.objects.filter(locality_id=locality_id).values_list('available_days',
                                                                                                  flat=True)
        if not inspectors.exists() or locality_id == '-1':
            response['status'] = True
            base = current_time
            available_dates = list()
            for day_delta in range(1, days_to_show):
                date = base + timedelta(day_delta)
                date_dict = {
                    "day": "Today" if (date == base) else date.strftime("%A"),
                    "date": (date.strftime("%-d")) + ' ' + date.strftime("%B")[:3],
                    "db_date": str(date.date())
                }
                available_dates.append(date_dict)
            date_time = get_permissible_visit_time_range(active_visit_time_slots)
            response['dates'] = available_dates
            response['time_slots'] = date_time
            return Response(response)

        if inspectors.count() == 1:
            available_days = inspectors[0]
        else:
            available_days = bin(reduce(lambda x, y: int(x, 2) | int(y, 2), inspectors))[2:]
            available_days = ('0' * (7 - len(available_days))) + available_days

        base = current_time
        for day_delta in range(0, days_to_show):
            date = base + timedelta(day_delta)
            if available_days[date.weekday()] == '1':
                start_date = date
                break

        start_date = start_date.replace(hour=0, minute=0, second=0, microsecond=0)
        if start_date > current_time:
            date_time = get_permissible_visit_time_range(active_visit_time_slots)

        if is_present_day_over:
            delta = 1

        available_dates = list()
        base = get_timezone_aware_current_datetime()
        for day_delta in range(0, days_to_show):
            date = base + timedelta(delta + day_delta)
            if available_days[date.weekday()] == '1':
                date_dict = {
                    "day": "Today" if (date == base) else date.strftime("%A"),
                    "date": (date.strftime("%-d")) + ' ' + date.strftime("%B")[:3],
                    "db_date": str(date.date())
                }
                available_dates.append(date_dict)

        response['status'] = True
        response['time_slots'] = date_time
        response['dates'] = available_dates
        return Response(response)
