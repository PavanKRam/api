import threading
from datetime import datetime
from pickle import loads as ploads

import redis

from server.settings import REDIS_HOST_DS


class ModelData:
    user_features = 'user_features'
    item_features = 'item_features'
    trained_model = 'model'
    popular_listings = 'popular_listings'
    max_buyer_id = 'max_buyer_id'
    active_listings = 'active_listings'


def get_trained_model():
    pool = redis.ConnectionPool().from_url(REDIS_HOST_DS)
    redis_connection = redis.Redis(connection_pool=pool)

    ModelData.trained_model = ploads(redis_connection.get('model'))
    ModelData.user_features = ploads(redis_connection.get('user_features'))
    ModelData.item_features = ploads(redis_connection.get('item_features'))
    ModelData.popular_listings = ploads(redis_connection.get('popular_listings'))
    ModelData.max_buyer_id = redis_connection.get('max_buyer_id')
    ModelData.active_listings = ploads(redis_connection.get('active_listings'))


def get_time_interval():
    """
    :return: number of seconds after which trained model should be reloaded

    """
    current_minute_in_utc = datetime.now().minute
    if current_minute_in_utc == 45:
        return 3600
    elif current_minute_in_utc < 45:
        return (45 - current_minute_in_utc) * 60
    else:
        return (60 - current_minute_in_utc) * 60


def train_model_thread():
    get_trained_model()
    threading.Timer(get_time_interval(), train_model_thread).start()


train_model_thread()
