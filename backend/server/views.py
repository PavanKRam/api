from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response


@api_view(['GET'])
def index(request):
    response = {
        'status': True,
        'message': 'Welcome to Truebil REST API'
    }
    return Response(response, status.HTTP_200_OK)


@api_view(['GET'])
def bad_request(request):
    response = {
        'status': False,
        'message': 'Bad request.'
    }
    return Response(response, status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def not_found(request):
    response = {
        'status': False,
        'message': 'Not Found.'
    }
    return Response(response, status.HTTP_404_NOT_FOUND)


@api_view(['GET'])
def server_error(request):
    response = {
        'status': False,
        'message': 'A server error occurred. Please contact the administrator.'
    }
    return Response(response, status.HTTP_500_INTERNAL_SERVER_ERROR)
