"""api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import include, url
from fcm_django.api.rest_framework import FCMDeviceAuthorizedViewSet
from rest_framework.routers import DefaultRouter

from .views import index, bad_request, not_found, server_error

router = DefaultRouter()
router.register(r'fcm_device', FCMDeviceAuthorizedViewSet)

handler400 = bad_request
handler404 = not_found
handler500 = server_error

urlpatterns = [
    url(r'^$', index),
    url(r'^', include('main.urls')),
    url(r'^users/', include('user.urls')),
    url(r'^', include('authn.urls')),
    url(r'^', include('truebil_management.urls')),
    url(r'^auction_app/', include('auction_app.urls')),
] + router.urls

if settings.DEBUG:
    import debug_toolbar
    from rest_framework_swagger.views import get_swagger_view

    schema_view = get_swagger_view(title="Truebil API")

    urlpatterns[0] = url(r'^$', schema_view)

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
