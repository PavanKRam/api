import os
from django.core.wsgi import get_wsgi_application
import newrelic.agent

os.environ['DJANGO_SETTINGS_MODULE'] = 'server.apache.override'
application = get_wsgi_application()

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
newrelic.agent.initialize(os.path.join(BASE_DIR, 'newrelic.ini'))
application = newrelic.agent.WSGIApplicationWrapper(application)
