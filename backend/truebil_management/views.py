# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.models import AnonymousUser
from django.db.models import Count
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Listing
from main.utils.constants import LISTING_ACTIVE_STATUS
from user.models import City, UserActivity, Locality
from user.user_helpers import get_user_subscriptions, get_active_subscriptions_info, get_user_info, \
    get_buyer_visit_booking_info, get_seller_info, get_city_for_anonymous_user, get_user_source, check_seller_listing_statuses
from user.utils.messages import INVALID_CITY, INVALID_LISTING
from user.utils.response import get_error_response
from .models import InventoryLocation
from .truebil_management_helper import get_inventory_info


class InventoryInfoAPIView(APIView):
    """
    Inventory Info Api View Class
    """

    def get_inventory_city_ids(self, city_id):
        """
        Returns city ids where inventory cars > 0
        """

        city_filter = {}
        if city_id:
            city_filter = {'locality__city': city_id}

        inventory_cars = Listing.objects.filter(
            is_inventory=True, status__in=LISTING_ACTIVE_STATUS, locality__city__is_active=True, **city_filter
        ).values('locality__city').annotate(count=Count('id'))

        inventory_city_ids = [car['locality__city'] for car in inventory_cars if car['count'] > 0]
        return inventory_city_ids

    def get(self, request):
        """
        Returns City Inventory Info

        ---

        parameters:
            - name: city_id
              description: City id of user
              required: false
              type: string

        responseMessages:
            - code: 400
              message: City does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        city_id = request.query_params.get('city_id', None)
        if city_id:
            try:
                city = City.objects.get(id=city_id)
            except (City.DoesNotExist, ValueError):
                response = get_error_response(INVALID_CITY)
                return Response(response, status=status.HTTP_400_BAD_REQUEST)
        else:
            city = None

        inventory_city_ids = self.get_inventory_city_ids(city_id)
        inventory_locations = InventoryLocation.objects.filter(city__id__in=inventory_city_ids).values(
            'location_url', 'address', 'poc_name', 'poc_mobile', 'city_id', 'locality__name', 'shop_name'
        )

        resp = {}
        for location in inventory_locations:
            city_id = location.pop('city_id')
            location['locality_name'] = location.pop('locality__name')
            resp[city_id] = location

        return Response(resp)


class ConfigApiView(APIView):
    def get(self, request):
        """
        Returns configurations of a user

        ---

        parameters:
            - name: mobile
              description: Mobile of user
              required: false
              type: number
            - name: city_id
              description: City id of user
              required: false
              type: number
        responseMessages:
            - code: 200
            - message: dictionary of configurations

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        user = request.user
        mobile = request.query_params.get('mobile', None)
        city_id = request.query_params.get('city_id', None)
        response = {
            'is_user_logged_in': False,
            'is_subscribed_buyer': False,
            'is_subscribed_seller': False,
            'user_info': {
                'subscription_info': {
                    'buyer': None,
                    'seller': None
                }
            },
            'subscriptions_info': get_active_subscriptions_info(get_user_source(request.META)),
            'shortlist_count': None,
            'buyer_visit_info': None
        }

        if type(user) is not AnonymousUser:
            mobile = user.mobile
            response['is_user_logged_in'] = True
            response['shortlist_count'] = UserActivity.objects.filter(user=user, is_shortlisted=True).count()

            user_subscriptions = get_user_subscriptions(user.id, None, True)
            if user_subscriptions:
                if user_subscriptions.get('buyer', False):
                    response['is_subscribed_buyer'] = True

                if user_subscriptions.get('seller', False):
                    response['is_subscribed_seller'] = True

                if user_subscriptions.get('buyer_subscription', False):
                    response['user_info']['subscription_info']['buyer'] = user_subscriptions['buyer_subscription']

                if user_subscriptions.get('seller_subscription', False):
                    response['user_info']['subscription_info']['seller'] = user_subscriptions['seller_subscription']

                response['buyer_visit_info'] = get_buyer_visit_booking_info(user.mobile)
        city_not_found = False
        if mobile:
            user_info = get_user_info(mobile)
            try:
                user_info['city_name'] = City.objects.get(id=user_info['city_id']).name
            except (KeyError, City.DoesNotExist):
                city_not_found = True
            response['user_info'].update(user_info)
        else:
            city_not_found = True
        if city_not_found:
            city_detail = get_city_for_anonymous_user(request.META)
            if city_detail:
                response['user_info'].update(city_detail)

        seller_info = dict()
        if mobile or type(user) is not AnonymousUser:
            seller_info = get_seller_info(mobile)

        seller = {
            'is_seller': True if seller_info is not None and seller_info.get('type') == 'Individual' and
                                 check_seller_listing_statuses(mobile) else False,
            'is_dealer': True if seller_info is not None and seller_info.get('type') == 'Dealer' else False,
            'is_subscribed_dealer': True if seller_info is not None and seller_info.get('type') == 'Subscribed_Dealer'
            else False
        }

        if seller['is_seller']:
            response['user_info']['seller_listings'] = Listing.objects.filter(seller__mobile=mobile).\
                values_list('id', flat=True)

        response.update(seller)

        if not city_id and 'city_id' in response['user_info']:
            city_id = response['user_info']['city_id']

        response['inventory_info'] = get_inventory_info(city_id) if city_id else None

        return Response(response)


class TrackingInfoApiView(APIView):
    def get(self, request):
        """
        Returns city and car information.

        ---

        parameters:
            - name: listing_id
              description: Listing id of car
              required: false
              type: number
        responseMessages:
            - code: 200
            - message: dictionary of tracking information

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """
        response = dict()

        city_info = get_city_for_anonymous_user(request.META)
        if city_info:
            response['city_info'] = city_info

        listing_id = request.query_params.get('listing_id', None)
        if listing_id:
            try:
                listing_detail = Listing.objects.get(id=listing_id)
                response['car_info'] = {
                    'car_id': listing_detail.id,
                    'price': listing_detail.price,
                    'is_inventory': listing_detail.is_inventory
                }

                if not city_info:
                    try:
                        locality_info = Locality.objects.get(id=listing_detail.locality_id)
                        city_info = {
                            'city_id': locality_info.city.id,
                            'city_name': locality_info.city.name
                        }
                        response['city_info'] = city_info
                    except Locality.DoesNotExist:
                        pass
            except Listing.DoesNotExist:
                response = get_error_response(INVALID_LISTING)
                return Response(response, status=status.HTTP_400_BAD_REQUEST)

        return Response(response)
