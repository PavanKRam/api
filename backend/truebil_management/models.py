# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from user.models import BaseModel
from connect.signals import push_truebil_data_to_crm


class Config(models.Model):
    key = models.CharField(primary_key=True, max_length=100)
    value = models.CharField(max_length=100)

    class Meta:
        db_table = 'configs'

    def __unicode__(self):
        return '%s - %s' % (self.key, self.value)


class InventoryLocation(models.Model):
    city = models.ForeignKey(to='user.City', related_name='inventory_location', blank=True, null=True)
    locality = models.ForeignKey(to='user.Locality', blank=True, null=True)
    location_url = models.CharField(max_length=50, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    poc_name = models.CharField(max_length=50, blank=True, null=True)
    poc_mobile = models.CharField(max_length=20, blank=True, null=True)
    uber_url = models.CharField(max_length=20, blank=True, null=True)
    shop_name = models.CharField(max_length=50, blank=True, null=True)
    opening_time = models.TimeField()
    closing_time = models.TimeField()

    class Meta:
        db_table = 'inventory_locations'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.locality)


class UtmParameter(BaseModel):
    utm_source = models.CharField(max_length=100, blank=True, null=True)
    utm_medium = models.CharField(max_length=100, blank=True, null=True)
    utm_campaign = models.CharField(max_length=100, blank=True, null=True)
    utm_content = models.CharField(max_length=100, blank=True, null=True)
    gclid = models.CharField(max_length=100, blank=True, null=True)
    hash = models.CharField(max_length=32, blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'utm_parameters'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.utm_source)


class SeoKeyword(models.Model):
    filter_id = models.CharField(max_length=20, blank=True, null=True)
    filter_name = models.CharField(primary_key=True, max_length=45)
    filter_category = models.CharField(max_length=20, blank=True, null=True)

    class Meta:
        db_table = 'seo_keywords'

    def __unicode__(self):
        return '%s - %s' % (self.filter_name, self.filter_category)


class Source(models.Model):
    name = models.CharField(max_length=45)

    class Meta:
        db_table = 'sources'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)
