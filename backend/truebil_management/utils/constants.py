WHITELIST = ['111.119.251.90', '114.79.147.226', '114.79.147.227', '106.51.128.248', '124.155.247.203', '182.75.187.38',
             '114.79.162.195']

SUPPORT_MOBILE_NO = '+91-9619022022'

FREEMIUM_LIMIT = 3

PLATFORM_NAME = {
    'cakePHP': 'desktop',
    'api.truebil.com': 'pwa',
    'api.oldhonk.co.in': 'pwa',
    '127.0.0.1:8000': 'pwa'
}
AUCTION_APP_SALES_PERSON_NAME = 'Sonali Sinha'
AUCTION_APP_SALES_PERSON_MOBILE = '9619022022'

TRUEBIL_BANK_DETAIL = {
    'beneficiary_name' : 'PAIX TECHNOLOGY PVT LTD',
    'bank_name' : 'ICICI Bank Ltd',
    'acc_no' : '002005033341',
    'ifsc' :'ICIC0000020',
    'branch_name' : 'ICICI Powai'
}


