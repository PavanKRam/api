from django.conf.urls import url
from .views import InventoryInfoAPIView, ConfigApiView, TrackingInfoApiView


urlpatterns = [
    url(r'^inventory_info/$', InventoryInfoAPIView.as_view()),
    url(r'^configurations/$', ConfigApiView.as_view()),
    url(r'^tracking_info/$', TrackingInfoApiView.as_view())
]
