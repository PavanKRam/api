from .models import InventoryLocation


def get_inventory_info(city_id):
    inventory_location = InventoryLocation.objects.filter(city__id=city_id).values('location_url', 'address',
                                                                                   'poc_name', 'poc_mobile',
                                                                                   'city_id', 'locality__name',
                                                                                   'shop_name')
    if inventory_location:
        inventory_location = list(inventory_location)[0]
        inventory_location['locality_name'] = inventory_location.pop('locality__name')
        return inventory_location
    else:
        return None
