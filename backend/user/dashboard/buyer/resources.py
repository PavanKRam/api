from collections import OrderedDict

from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from authn.permissions import IsAuthenticated
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from main.models import InspectionRequest
from truebil_management.utils.constants import WHITELIST
from user.models import Buyer, UserSubscription
from user.user_helpers import get_user_subscriptions, get_client_ip
from user.utils.constants import SAMPLE_USER_SUBSCRIPTION_DETAILS
from user.utils.messages import UNPAID_USER, SUBSCRIPTION_INACTIVE
from user.utils.response import get_error_response


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def user_subscription(request):
    """
    API View to get the user subscription details

    ---

    responseMessages:
        - code: 200
          message: User has no active subscription.
          message: User has no subscription history.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    user = request.user

    client_ip = get_client_ip(request.META)

    user_subscription_obj = UserSubscription.objects.filter(user=user, is_active=1, subscription__type='buyer').\
        order_by('-end_date').first()

    if user_subscription_obj:
        today = timezone.now()

        if today <= user_subscription_obj.end_date:
            response_data = OrderedDict()
            response_data['Transaction ID'] = "#TBP" + str(261721 + int(user_subscription_obj.transaction_id)) if \
                user_subscription_obj.transaction_id else "None (Cash payment)"
            response_data['Subscription Status'] = 'Active'
            response_data['Order Date'] = user_subscription_obj.start_date.strftime('%d %b %Y')
            response_data['Valid Upto'] = user_subscription_obj.end_date.strftime('%d %b %Y')
            response_data['Order Total'] = user_subscription_obj.subscription.price
            response_data['Payment Status'] = 'Paid'

            return Response({'status': True, 'data': response_data}, status=status.HTTP_200_OK)
        else:
            return Response(get_error_response(SUBSCRIPTION_INACTIVE), status=status.HTTP_200_OK)
    elif client_ip in WHITELIST:
        return Response({'status': True, 'data': SAMPLE_USER_SUBSCRIPTION_DETAILS}, status=status.HTTP_200_OK)
    else:
        return Response(get_error_response(UNPAID_USER), status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def request_inspection(request):
    """
    API View to post inspection request

    ---

    responseMessages:
        - code: 200
          message: User has no subscription history.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    user = request.user

    is_user_paid = get_user_subscriptions(user.id, 'buyer')

    if is_user_paid:
        InspectionRequest.objects.create(user=user)

        try:
            buyer = Buyer.objects.get(mobile=user.mobile)
            post_data_email = {
                'sendTo': ['buyer.inspection.request.group@truebil.com'],
                'subject': 'Request for inspection',
                'message': "Hi all, Please find buyer info. "
                           "Buyer Name : %s "
                           "Buyer Mobile : %s "
                           "time : %s" % (buyer.name, buyer.mobile, timezone.now())
            }

            post_data_email = [post_data_email]
            mailer_data = prepare_mailer_data(email_data=post_data_email)
            add_gearman_data('mailer', mailer_data)

        except Buyer.DoesNotExist:
            pass

        return Response({'status': True}, status=status.HTTP_200_OK)
    else:
        return Response(get_error_response(UNPAID_USER), status=status.HTTP_200_OK)
