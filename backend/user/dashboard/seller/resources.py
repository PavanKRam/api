from django.db.models import Case, When, Value, IntegerField
from django.db.models import Q
from django.utils import timezone
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from authn.permissions import IsAuthenticated
from connect.email import get_email_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from connect.sms import get_sms_data
from main.models import Listing, Attachment, ListingSoldReportLog, ListingStatusTrack
from main.quick_ops import get_cdn_url, unit_converter, get_timezone_aware_current_datetime
from main.serializers import SellerDashboardSerializer
from main.utils.constants import CAPTION_IDS, SELLER_LISTING_STATUSES
from main.utils.serializer_helpers import get_invalid_numbers, get_buyer_interested_in_inventory
from user.models import Seller, VwBuyersInterestedInInventory, SellerInventoryRequest, \
    BuyerListing, BuyerListingOffer
from user.user_helpers import get_user_subscriptions
from user.utils.messages import INVALID_SELLER, INVALID_LISTING, REQUIRED_FIELDS_ABSENT, UNAUTHORIZED_SELLER
from user.utils.response import get_error_response
from datetime import timedelta


class SellerDashboardCars(APIView):

    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        API View to get the seller dashboard car details
        """

        user = request.user
        try:
            seller = Seller.objects.get(mobile=user.mobile, parent_id__isnull=True)
        except Seller.DoesNotExist:
            return Response(get_error_response(INVALID_SELLER), status=status.HTTP_400_BAD_REQUEST)

        seller_listed_cars = Listing.objects.filter(seller=seller, status__in=SELLER_LISTING_STATUSES).extra(
            select={'manual': "FIELD(listings.status, 'active', 'inactive', 'soldByOthers')"},
        ).order_by('manual', '-created_at')

        total_seller_listed_cars = seller_listed_cars.count()
        attachments_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
        buyers_interested_in_inventory = set(
            VwBuyersInterestedInInventory.objects.all().values_list('mobile', flat=True))

        seller_listed_cars_data = SellerDashboardSerializer(seller_listed_cars,
                                                            many=True,
                                                            context={'show_one_url': True, 'cdn_url': get_cdn_url(),
                                                                     'attachment_qs': attachments_qs,
                                                                     'invalid_numbers': get_invalid_numbers(),
                                                                     'buyers_interested_in_inventory':
                                                                         buyers_interested_in_inventory}).data

        return Response({'results': seller_listed_cars_data, 'count': total_seller_listed_cars})


class SellerInventoryRequestView(APIView):

    permission_classes = [IsAuthenticated]

    def post(self, request, format=None):
        """
        API View to add seller's request of adding car to inventory
        """

        listing_id = request.data.get('listing_id', None)
        variant_name = request.data.get('variant_name', None)
        truebil_offered_price = request.data.get('inventory_offered_price', None)
        registration_number = request.data.get('registration_number', None)

        user = request.user
        try:
            seller = Seller.objects.get(mobile=user.mobile)
        except Seller.DoesNotExist:
            return Response(get_error_response(INVALID_SELLER), status=status.HTTP_400_BAD_REQUEST)

        sms_data = list()
        email_data = list()

        try:
            SellerInventoryRequest.objects.create(seller=seller, listing_id=listing_id)
            is_seller_inventory_request = True

            sms_data.append(get_sms_data(type_='car_sell_request', mobile=user.mobile, name=user.name))

            email_data.append(
                get_email_data(type_='car_sell_request', send_to='inventory.sell.request.group@truebil.com',
                               subject='Request for selling car to inventory', name=user.name,
                               mobile=user.mobile, car_id=listing_id, variant_name=variant_name,
                               registration_number=registration_number,
                               truebil_offered_price=truebil_offered_price, time=timezone.now()))

            mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data)
            add_gearman_data('mailer', mailer_data)

        except Exception as e:
            return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

        return Response({'status': is_seller_inventory_request})


class SellerCarInterests(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        API View to get the list of people who have shown interest to the seller's car
        """
        listing_id = request.query_params.get('listing_id', None)
        user = request.user

        if listing_id is None:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        try:
            Seller.objects.get(mobile=user.mobile, parent_id__isnull=True)
        except Seller.DoesNotExist:
            return Response(get_error_response(INVALID_SELLER), status=status.HTTP_400_BAD_REQUEST)

        listing = Listing.objects.get(id=listing_id)
        listing_variant_model = listing.variant.model
        listing_locality_city_id = listing.locality.city_id
        listing_body_type = listing.body_type_id
        price = listing.price
        min_price = round(price * 0.9, -3)
        max_price = round(price * 1.1, -3)

        result_limit = 16
        now = timezone.now()
        last_twenty_days = now - timedelta(days=20)
        last_thirty_days = now - timedelta(days=30)
        last_nine_days = now - timedelta(days=9)

        is_seller_subscribed = get_user_subscriptions(user.id, 'seller')

        def get_offered_price(price):
            if is_seller_subscribed:
                return unit_converter(price)
            else:
                if price > 99994:
                    temp = unit_converter(price)
                    temp_int = ((temp.split(' ')[0]).split('.'))[0]
                    temp_string = temp.split(' ')[1]
                    return str(temp_int) + '.XX' + ' ' + temp_string
                else:
                    temp = unit_converter(price, shorthand_price=True)
                    temp_int = ((temp.split(' ')[0]).split('.'))[0]
                    return str(temp_int) + '.XX' + ' ' + 'Thousand'

        def get_distinct_values(data, limit=result_limit):
            distinct_data = list()
            distinct_set = set()
            for each in data:
                mobile = each[0]
                name = each[1]
                if limit == 0:
                    break
                if mobile in distinct_set:
                    continue
                else:
                    distinct_set.add(mobile)
                    modified_data = dict()
                    if is_seller_subscribed:
                        modified_data['mobile'] = mobile
                    else:
                        modified_data['mobile'] = 'X'*10
                    modified_data['name'] = name.title()
                    if len(each) == 3:
                        modified_data['offered_price'] = get_offered_price(each[2])
                    distinct_data.append(modified_data)
                    limit -= 1
            return distinct_data

        buyer_details_interested = get_distinct_values( \
            BuyerListing.objects \
                .values_list('buyer__mobile', 'buyer__name') \
                .filter(
                    ~Q(buyer__name=None),
                    ~Q(buyer__mobile=None),
                    listing_id=listing_id,
                    is_active=True,
                    created_at__gte=last_twenty_days
                ) \
                .order_by('-created_at', 'buyer__mobile') \
                .exclude(buyer__mobile__in=get_invalid_numbers()) \
                .distinct()
        )

        buyer_details_top_offers = get_distinct_values( \
            BuyerListingOffer.objects \
                .values_list('buyer__mobile', 'buyer__name', 'offered_price') \
                .filter(
                    ~Q(buyer__name=None),
                    ~Q(buyer__mobile=None),
                    listing_id=listing_id,
                    is_active=True,
                    is_verified=True,
                    created_at__gte=last_twenty_days
                ) \
                .order_by('-created_at', 'buyer__mobile') \
                .exclude(buyer__mobile__in=get_invalid_numbers()) \
                .distinct()
        )

        buyer_details_similar_model = get_distinct_values( \
            BuyerListing.objects \
                .values_list('buyer__mobile', 'buyer__name') \
                .filter(
                    ~Q(buyer__name=None),
                    ~Q(buyer__mobile=None),
                    is_active=True,
                    listing__variant__model=listing_variant_model,
                    listing__locality__city_id=listing_locality_city_id,
                    created_at__gte=last_thirty_days,
                ) \
                .order_by('-created_at', 'buyer__mobile') \
                .exclude(buyer__mobile__in=get_invalid_numbers()) \
                .exclude(buyer__mobile__in=get_buyer_interested_in_inventory()) \
                .distinct()
        )

        buyer_details_similar_price = get_distinct_values( \
            BuyerListing.objects \
                .values_list('buyer__mobile', 'buyer__name') \
                .filter(
                    ~Q(buyer__name=None),
                    ~Q(buyer__mobile=None),
                    is_active=True,
                    listing__price__range=(min_price, max_price),
                    listing__body_type_id__name=listing_body_type,
                    listing__locality_id__city_id=listing_locality_city_id,
                    created_at__gte=last_nine_days,
                ) \
                .exclude(buyer__mobile__in=get_invalid_numbers()) \
                .exclude(buyer__mobile__in=get_buyer_interested_in_inventory()) \
                .distinct()
        )

        response = dict()

        response['interested_buyers'] = buyer_details_interested
        response['top_offers'] = buyer_details_top_offers
        response['similar_model_buyer'] = buyer_details_similar_model
        response['similar_price_buyer'] = buyer_details_similar_price

        return Response(response)


class SellerDashboardListingMarkAsSold(APIView):

    permission_classes = [IsAuthenticated]

    def patch(self, request):
        """
            API View to update listing status to sold from seller dashboard.
        """
        listing_id = request.data.get('listing_id', None)
        user = request.user

        try:
            listing = Listing.objects.values('seller__mobile', 'status').get(pk=listing_id)
        except Listing.DoesNotExist:
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

        mobile = str(request.user.mobile)

        if mobile != listing['seller__mobile']:
            return Response(get_error_response(UNAUTHORIZED_SELLER), status=status.HTTP_401_UNAUTHORIZED)

        if str(listing['status']) == 'active':
            update_status = Listing.objects.filter(id=listing_id).update(status='soldByOthers')
            ListingSoldReportLog.objects.create(listing_id=listing_id, user_id=user.id, status='soldByOthers',
                                                source='seller_dashboard')
            ListingStatusTrack.objects.create(listing_id=listing_id, status_to='soldByOthers', source='seller_dashboard')
            sms_data = [get_sms_data(type_='car_sold', mobile=user.mobile, seller_name=user.name,
                                     seller_mobile=user.mobile)]
            time = get_timezone_aware_current_datetime().now().strftime("%Y-%m-%d %H:%M:%S")
            email_data = [
                get_email_data(type_='car_sold', send_to=['sold.car.group@truebil.com'],
                               subject='Sold car report from individual seller', seller_name=user.name,
                               seller_mobile=user.mobile,
                               car_id=listing_id, time=time)]
            mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data)
            add_gearman_data('mailer', mailer_data)
            message = 'Car marked sold successfully'
        else:
            update_status = True
            message = 'Car status is already sold'

        return Response({'status': bool(update_status), 'message': message})
