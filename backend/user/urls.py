from django.conf.urls import url as URL
from rest_framework.routers import DefaultRouter

from user.crf.resources import add_inventory_leads, add_exit_lead, make_price_offer, get_seller_contacts, \
    verify_buyer_offer, add_leads
from user.dashboard.buyer.resources import user_subscription, request_inspection
from user.dashboard.seller.resources import SellerDashboardCars, SellerInventoryRequestView, SellerCarInterests, \
    SellerDashboardListingMarkAsSold
from user.feedback.resources import feedbacks, seller_contact_feedbacks, SellerDashboardListingSoldFeedback
from user.payment.resource import payment
from user.profile.resources import edit_profile, truebil_premium_user
from user.rsa.resource import claim_rsa
from user.service.resources import ServiceAPIView
from user.service.url_kwargs import (CAR_LOAN_KWARGS, INSURANCE_KWARGS, PAPER_TRANSFER_KWARGS, INTERESTED_SELLER_KWARGS)
from .views import *

router = DefaultRouter()
router.register(r'^cities', CityViewSet)
router.register(r'^contacts_requested', ContactRequestedViewSet)
router.register(r'^offers_made', OffersMadeViewSet)
router.register(r'^shortlisted_cars', ShortlistedCarsViewSet)
router.register(r'^inspections_requested', InspectionRequestedCarsViewSet)
router.register(r'^recently_visited', RecentlyVisitedViewSet)
# router.register(r'^listing_offer', BuyerListingOfferViewSet)

urlpatterns = router.urls
urlpatterns += [
    URL(r'^add_inventory_leads/', add_inventory_leads),
    URL(r'^add_exit_lead/', add_exit_lead),
    URL(r'^make_price_offer/', make_price_offer),
    URL(r'^get_seller_contacts/', get_seller_contacts),
    URL(r'^verify_buyer_offer/', verify_buyer_offer),
    URL(r'^feedbacks/', feedbacks),
    URL(r'^subscription_detail/', user_subscription),
    URL(r'^edit_profile/', edit_profile),
    URL(r'^check_premium/', truebil_premium_user),
    URL(r'^request_inspection/', request_inspection),
    URL(r'^payment/', payment),
    # URL(r'^recently_visited_cars/(?P<id>\d+)', RecentlyVisitedCars.as_view()),
    URL(r'^book_visit/$', VisitBooking.as_view()),
    URL(r'^book_visit/(?P<pk>\d+)$', VisitBooking.as_view()),
    URL(r'^test_drive_car/$', VisitListing.as_view()),
    URL(r'^test_drive_car/(?P<pk>\d+)$', VisitListing.as_view()),
    URL(r'^add_leads/', add_leads),
    URL(r'^claim_rsa/', claim_rsa),
    URL(r'^listing_offer/$', BuyerOffer.as_view()),
    URL(r'^buyer_dashboard_cars', BuyerDashboardCars.as_view()),
    URL(r'^seller_dashboard_cars', SellerDashboardCars.as_view()),
    URL(r'^request_seller_inventory', SellerInventoryRequestView.as_view()),
    URL(r'^interested_buyers', SellerCarInterests.as_view()),
    URL(r'^seller_listing_sold_feedback', SellerDashboardListingSoldFeedback.as_view()),
    URL(r'^seller_listing_mark_as_sold', SellerDashboardListingMarkAsSold.as_view()),
    URL(r'^seller_visit/$', SellerVisitBooking.as_view()),
    URL(r'^seller_inspection_locality/$', SellerInspectionLocality.as_view()),
    URL(r'^seller_contact_feedbacks/', seller_contact_feedbacks)
]

api_view = ServiceAPIView.as_view()

urls = [
    ['loan_applicants', CAR_LOAN_KWARGS],
    ['insurance_applicants', INSURANCE_KWARGS],
    ['paper_transfer', PAPER_TRANSFER_KWARGS],
    ['interested_sellers', INTERESTED_SELLER_KWARGS],
]

urlpatterns += [URL(r'^{url_name}/'.format(url_name=url[0]), api_view, url[1]) for url in urls]
