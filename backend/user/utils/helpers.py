# -*- coding: utf-8 -*-
"""
Helpers for user app
"""
import pytz


def is_user_paid(user):
    """
    Checks user's subscription status
    """

    # this method is dependent on certain attributes of user,
    # which does not exist currently.
    return False


def localize_timezone(datetime):
    return pytz.timezone('Asia/Kolkata').localize(datetime)
