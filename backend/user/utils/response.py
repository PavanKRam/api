def get_error_response(message):
    return {
        'status': False,
        'message': message
    }