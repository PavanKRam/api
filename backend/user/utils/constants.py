# -*- coding: utf-8 -*-

BASIC = {
    'desktop': {
        'positive': [
            {'text': 'Unlimited Seller Contacts', 'bold': True},
            {'text': 'RTO verified complete car history'},
            {'text': 'Complete inspection report of all cars'},
            {'text': 'Photos of dents, rust, scratches, etc.'}
        ],
        'negative': [
            {'text': 'Relationship Manager - Negotiates with owners, car buying expertise, etc.'},
            {'text': 'Free 6 months Comprehensive Service Warranty worth ' + u'₹' + ' 7000 on all cars'}
        ]
    },
    'pwa': {
        'positive': [
            {'text': 'Unlimited Seller Contacts', 'bold': True},
            {'text': 'RTO verified complete car history'},
            {'text': 'Complete inspection report of all cars'},
            {'text': 'Photos of dents, rust, scratches, etc.'}
        ],
        'negative': [
            {'text': 'Relationship Manager - Negotiates with owners, car buying expertise, etc.'},
            {'text': 'Free 6 months Comprehensive Service Warranty worth ' + u'₹' + ' 7000 on all cars'}
        ]
    }
}

ESSENTIAL = {
    'desktop': {
        'positive': [
            {'text': 'Unlimited Seller Contacts'},
            {'text': 'RTO verified complete car history'},
            {'text': 'Complete inspection report of all cars'},
            {'text': 'Photos of dents, rust, scratches, etc.'},
            {'text': 'Relationship Manager - Negotiates with owners, car buying expertise, etc.', 'bold': True}
        ],
        'negative': [
            {'text': 'Free 6 months Comprehensive Service Warranty worth ' + u'₹' + ' 7000 on all cars'}
        ]
    },
    'pwa': {
        'positive': [
            {'text': 'Relationship Manager - Negotiates with owners, car buying expertise, etc.',
             'bold': True},
            {'text': 'Unlimited Seller Contacts'},
            {'text': 'RTO verified complete car history'},
            {'text': 'Complete inspection report of all cars'},
            {'text': 'Photos of dents, rust, scratches, etc.'}
        ],
        'negative': [
            {'text': 'Free 6 months Comprehensive Service Warranty worth ' + u'₹' + ' 7000 on all cars'}
        ]
    }
}

PREMIUM = {
    'desktop': {
        'positive': [
            {'text': 'Unlimited Seller Contacts'},
            {'text': 'RTO verified complete car history'},
            {'text': 'Complete inspection report of all cars'},
            {'text': 'Photos of dents, rust, scratches, etc.'},
            {'text': 'Relationship Manager - Negotiates with owners, car buying expertise, etc.'},
            {'text': 'Free 6 months Comprehensive Service Warranty worth ' + u'₹' + ' 7000 on all cars', 'bold': True}
        ],
        'negative': [
        ]
    },
    'pwa': {
        'positive': [
            {'text': 'Free 6 months Comprehensive Service Warranty worth ' + u'₹' + ' 7000 on all cars', 'bold': True},
            {'text': 'Unlimited Seller Contacts'},
            {'text': 'RTO verified complete car history'},
            {'text': 'Complete inspection report of all cars'},
            {'text': 'Photos of dents, rust, scratches, etc.'},
            {'text': 'Relationship Manager - Negotiates with owners, car buying expertise, etc.'}
        ],
        'negative': [
        ]
    }
}

SUBSCRIPTION_MAP = {
    8: BASIC,
    9: ESSENTIAL,
    10: PREMIUM
}

SHOWCASE_PRICE = {
    8: 999,
    9: 1999,
    10: 8999
}

SUBSCRIPTIONS_HAVING_RELATIONSHIP_MANAGERS = [9, 10]

SAMPLE_USER_SUBSCRIPTION_DETAILS = {
    'Valid Upto': '08-04-2028',
    'Order Date': '08-02-2028',
    'relationship_manager': {
        'mobile': '02242480910',
        'name': 'Jayant Chauhan'
    },
    'Subscription Status': 'Active',
    'Order Total': 499,
    'Payment Status': 'Paid',
    'Transaction ID': 'TBP261721truebil'
}

INSPECTOR_MOBILE = '9619022022'
