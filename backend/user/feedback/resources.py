import json

from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView

from authn.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from connect.email import get_email_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from connect.sms import get_sms_data
from main.models import Listing
from main.quick_ops import valid_mobile_number
from main.utils.messages import INVALID_LISTING
from user.crf.helpers import should_communicate_to_seller
from user.models import Feedback, City, UserListingFeedback, SellerListingSoldFeedback
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_CITY, INVALID_MOBILE, INVALID_LISTING
from user.utils.response import get_error_response


@api_view(['POST'])
def feedbacks(request):
    """
    API View that records the feedback of users

    ---

    parameters:
        - name: name
          description: Name of the applicant
          required: true
          type: string
        - name: email
          description: Email of the applicant
          required: false
          type: string
        - name: mobile
          description: Mobile number of the applicant
          required: true
          type: integer
        - name: query
          description: Query of user
          required: true
          type: string
        - name: text
          description: Feedback text
          required: true
          type: string
        - name: city_id
          description: City ID of the applicant
          required: false
          type: integer

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    name = request.data.get('name', None)
    mobile = request.data.get('mobile', None)
    email = request.data.get('email', None)
    query = request.data.get('query', None)
    message = request.data.get('text', None)
    city_id = request.data.get('city_id', None)

    if name is None or mobile is None or query is None or message is None or city_id is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)
    else:

        if city_id:
            try:
                city = City.objects.get(id=city_id)
            except City.DoesNotExist:
                return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)

        if valid_mobile_number(mobile):
            Feedback.objects.create(name=name, mobile=mobile, email=email, message=message, query=query, city=city)

            feedback_ticket_data = {
                'description': message,
                'subject': 'Customer feedback ' + mobile + ' ' + message[0:20],
                'name': name,
                'phone': mobile,
                'custom_fields': {
                    'direction': 'Inbound',
                    'source': 'Product feedback',
                    'category': query,
                    'city': city.name if city else 'India',
                    'department_ticket': 'Miscellaneous ticket',
                    'queries_1': 'Others',
                    'queries_2': 'Others',
                    'subject': 'N/A'
                }
            }

            if email:
                feedback_ticket_data['email'] = email

            ticket_data = {
                'createTicket': [json.dumps(feedback_ticket_data)]
            }

            add_gearman_data('freshdesk', ticket_data)

            response = {
                'message': 'Feedback saved successfully!',
                'status': True
            }

            return Response(response, status=status.HTTP_201_CREATED)
        else:
            return Response(get_error_response(INVALID_MOBILE), status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def seller_contact_feedbacks(request):
    """
    API View that records the seller contact feedback of users

    ---

    parameters:
        - name: listing_id
          description: listing ID of the car.
          required: true
          type: integer
        - name: feedback
          description: Feedback text
          required: true
          type: string
        - name: feedback_id
          description: Feedback ID
          required: false
          type: integer

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    listing_id = request.data.get('listing_id', None)
    feedback = request.data.get('feedback', None)
    feedback_id = request.data.get('feedback_id', None)

    if not (listing_id and feedback):
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    try:
        listing = Listing.objects.select_related('seller').values('seller__mobile', 'seller__type_id',
                                                                  'is_inventory').get(id=listing_id)
    except Listing.DoesNotExist:
        return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

    user = request.user
    UserListingFeedback.objects.create(user=user, listing_id=listing_id, feedback=feedback)

    seller_mobile = listing['seller__mobile']
    seller_type = listing['seller__type_id']
    time = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    comm_seller = should_communicate_to_seller(request, user.mobile, listing['is_inventory'], seller_type)

    buyer_message = seller_message = None
    city_name = user.city.name.lower()
    truebil_direct_link = 'https://m.truebil.com/used-truebil-direct-cars-in-' + city_name
    buyerUTM = '?utm_source=sms&utm_medium=buyer'

    if feedback_id == 0:
        send_to = ['demand.group@truebil.com']
    elif feedback_id == 1:
        buyerUTM += '&utm_campaign=negofailed_directcars'
        truebil_direct_link += buyerUTM
        buyer_message = get_sms_data(type_='negotiation_unsuccessful', mobile=user.mobile, name=user.name,
                                     url=truebil_direct_link, perform_shortener=True)
        send_to = ['demand.group@truebil.com', 'inventory.crf.group@truebil.com']
    elif feedback_id == 2:
        buyerUTM += '&utm_campaign=sold_notavailable_directcars'
        truebil_direct_link += buyerUTM
        seller_dashboard_link = \
            "http://m.truebil.com/user/seller?utm_source=sms&utm_medium=seller&utm_campaign=notreachable" \
            "&loginText=Please login to see the seller dashboard&emailListingId=" + str(listing_id) + \
            "&referrer=sellerEmail&action=soldCarEmail&openLogin=1&postLoginPage=/user/seller?" \
            "utm_source=sms%26utm_medium=seller%26utm_campaign=notreachable%26referrer=sellerEmail%26" \
            "action=soldCarEmail%26emailListingId=" + str(listing_id)

        buyer_message = get_sms_data(type_='seller_not_available_buyer', mobile=user.mobile, name=user.name,
                                     url=truebil_direct_link, perform_shortener=True)
        seller_message = get_sms_data(type_='seller_not_available_seller', mobile=seller_mobile,
                                      url=seller_dashboard_link, perform_shortener=True)

        send_to = ['demand.group@truebil.com', 'inventory.crf.group@truebil.com', 'sold.car.group@truebil.com']

    elif feedback_id == 3:
        buyerUTM += '&utm_campaign=sold_notavailable_directcars'
        truebil_direct_link += buyerUTM
        seller_dashboard_link = \
            "http://m.truebil.com/user/seller?utm_source=sms&utm_medium=seller&utm_campaign=confirmsold" \
            "&loginText=Please login to see the seller dashboard&emailListingId=" + str(listing_id) + \
            "&referrer=sellerEmail&action=soldCarEmail&openLogin=1&postLoginPage=/user/seller?utm_source=sms" \
            "%26utm_medium=seller%26utm_campaign=confirmsold%26referrer=sellerEmail%26action=soldCarEmail" \
            "%26emailListingId=" + str(listing_id)

        buyer_message = get_sms_data(type_='car_not_available_buyer', mobile=user.mobile, name=user.name,
                                     url=truebil_direct_link, perform_shortener=True)
        seller_message = get_sms_data(type_='car_not_available_seller', mobile=seller_mobile,
                                      url=seller_dashboard_link, perform_shortener=True)

        send_to = ['demand.group@truebil.com', 'inventory.crf.group@truebil.com', 'sold.car.group@truebil.com']

    else:
        send_to = ['demand.group@truebil.com']

    send_to.append('product.manager.group@truebil.com')

    email_data = list()
    sms_data = list()
    email_data.append(
        get_email_data(type_='feedback_mail', send_to=send_to,
                       subject='Premium Buyer Response', user_id=user.id, user_name=user.name,
                       user_mobile=user.mobile,
                       car_id=listing_id, feedback=feedback, time=time))

    if buyer_message:
        sms_data.append(buyer_message)
    if seller_message and comm_seller:
        sms_data.append(seller_message)

    mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data)
    add_gearman_data('mailer', mailer_data)

    response = {
        'message': 'Feedback saved successfully!',
        'status': True
    }

    return Response(response, status=status.HTTP_201_CREATED)


class SellerDashboardListingSoldFeedback(APIView):
    """
        API View that records the feedback of Seller from the Seller Dashboard
    """
    permission_classes = [IsAuthenticatedOrReadOnly]

    def post(self, request):

        listing_id = request.data.get('listing_id', None)

        if listing_id is None:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        try:
            Listing.objects.get(pk=listing_id)
        except Listing.DoesNotExist:
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

        try:
            SellerListingSoldFeedback.objects.get(listing_id=listing_id)
        except SellerListingSoldFeedback.DoesNotExist:
            SellerListingSoldFeedback.objects.create(listing_id=listing_id)

        experience = request.data.get('experience', None)
        recommended_price_feedback = request.data.get('recommended_price_feedback', None)
        sold_price = request.data.get('sold_price', None)

        data = dict()
        key, value = None, None

        if experience:
            key = 'experience'
            value = experience
        elif recommended_price_feedback:
            key = 'recommended_price_feedback'
            value = recommended_price_feedback
        elif sold_price:
            key = 'sold_price'
            value = sold_price
        else:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        data[key] = value
        SellerListingSoldFeedback.objects.filter(listing_id=listing_id).update(**data)

        return Response({'status': 'success'})

    def get(self, request):
        listing_id = request.query_params.get('listing_id', None)
        try:
            Listing.objects.get(pk=listing_id)
        except Listing.DoesNotExist:
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)

        try:
            feedback = SellerListingSoldFeedback.objects.get(listing=listing_id)
        except SellerListingSoldFeedback.DoesNotExist:
            return Response({'exists': False})

        return \
            Response({
                'exists': True,
                'experience': feedback.experience,
                'recommended_price_feedback': feedback.recommended_price_feedback,
                'sold_price': feedback.sold_price
            })
