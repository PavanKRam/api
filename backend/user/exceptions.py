# -*- coding: utf-8 -*-
"""
Module for exception classes
"""

class ParameterValidationError(Exception):
    """Parameter Validation Error"""
    pass

class RequestValidationError(Exception):
    """Parameter Validation Error"""
    pass