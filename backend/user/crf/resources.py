from django.contrib.auth.models import AnonymousUser
from django.forms.models import model_to_dict
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from authn.utils.helpers import create_otp, get_login_communications
from connect.email import get_email_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from connect.sms import get_sms_data
from connect.spreadsheet import get_spreadsheet_data
from main.models import Listing
from truebil_management.models import Source
from truebil_management.utils.constants import PLATFORM_NAME
from user.models import BuyerInventory, Buyer, BuyerExitLead, BuyerListingOffer, Seller, \
    DealerInventoryInterest, BuyerListing, BuyerFilter, City
from user.user_helpers import is_interested_seller
from user.utils.messages import INVALID_CITY, USER_NOT_LOGGED_IN
from user.utils.response import get_error_response
from .helpers import (get_buyer_crf_count_of_today, get_seller_contact_communications,
                      get_test_drive_request_communications, get_buyer_offer_communications, process_request,
                      is_subscribed_dealer, is_freemium_eligible)


@api_view(['POST'])
def add_inventory_leads(request):
    """
    API View to store inventory test drive leads

    ---

    parameters:
        - name: name
          description: Name of the applicant
          required: true
          type: string
        - name: mobile
          description: Mobile number of the applicant
          required: true
          type: integer
        - name: city_id
          description: City of the applicant
          required: false
          type: integer
        - name: utm
          description: UTM parameters
          required: false
          type: objects

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    parameters_info = {
      'all_params': ['name', 'mobile', 'city_id', 'utm'],
      'required_params': ['name', 'mobile', 'city_id']
    }

    try:
        post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer = process_request(request,
                                                                                                   parameters_info)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    BuyerInventory.objects.create(buyer=buyer)

    sms_data, spreadsheet_data = get_test_drive_request_communications(buyer, is_user_logged_in, is_subscribed_buyer)
    mailer_data = prepare_mailer_data(sms_data=sms_data, spreadsheet_data=spreadsheet_data)
    add_gearman_data('mailer', mailer_data)

    response = {
        'message': 'Inventory lead added successfully!',
        'buyer_id': buyer.id,
        'status': True,
        'buyer_exists': existing_buyer,
        'seller_exists': is_interested_seller(post_data['mobile'])
    }

    return Response(response, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def make_price_offer(request):
    """
    API View to store details of offer made on listings

    ---

    parameters:
        - name: name
          description: Name of the applicant
          required: true
          type: string
        - name: mobile
          description: Mobile number of the applicant
          required: true
          type: integer
        - name: city_id
          description: City of the applicant
          required: false
          type: integer
        - name: listing_id
          description: Listing Id
          required: true
          type: integer
        - name: offered_price
          description: Offered Price
          required: true
          type: integer
        - name: utm
          description: UTM parameters
          required: false
          type: objects

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    parameters_info = {
      'all_params': ['name', 'mobile', 'city_id', 'listing_id', 'offered_price', 'utm'],
      'required_params': ['name', 'mobile', 'listing_id', 'offered_price', 'city_id']
    }

    try:
        post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer = process_request(request,
                                                                                                   parameters_info)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    listing_obj = Listing.objects.get(id=post_data['listing_id'])
    offered_price = post_data['offered_price']

    BuyerListingOffer.objects.filter(buyer=buyer, listing=listing_obj).update(is_active=False)
    BuyerListingOffer.objects.create(buyer=buyer, listing=listing_obj, offered_price=offered_price,
                                     is_verified=is_user_logged_in)

    existing_seller = is_interested_seller(post_data['mobile'])
    if is_user_logged_in:
        subscribed_dealer = False
        
        try:
            seller_obj = Seller.objects.get(mobile=post_data['mobile'], parent_id=None)
            subscribed_dealer = is_subscribed_dealer(post_data['mobile'])

            if subscribed_dealer:
                DealerInventoryInterest.objects.create(dealer=seller_obj, listing=listing_obj,
                                                       offered_price=offered_price)
        except Seller.DoesNotExist:
            pass

        sms_data, email_data = get_buyer_offer_communications(request, buyer, listing_obj, is_user_logged_in,
                                                              is_subscribed_buyer, offered_price, subscribed_dealer)
        mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data)
    else:
        sms_data, spreadsheet_data = get_login_communications(mobile=post_data['mobile'],
                                                              otp=create_otp(post_data['mobile']))
        mailer_data = prepare_mailer_data(sms_data=[sms_data], spreadsheet_data=[spreadsheet_data])

    add_gearman_data('mailer', mailer_data)

    response = {
        'message': 'Offer added successfully!',
        'buyer_id': buyer.id,
        'status': True,
        'buyer_exists': existing_buyer,
        'seller_exists': existing_seller
    }

    return Response(response, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def get_seller_contacts(request):
    """
    API View for get seller contacts on listings

    ---

    parameters:
        - name: name
          description: Name of the applicant
          required: true
          type: string
        - name: mobile
        - name: email
          description: Email of the applicant
          required: false
          type: string
          description: Mobile number of the applicant
          required: true
          type: integer
        - name: city_id
          description: City of the applicant
          required: false
          type: integer
        - name: listing_id
          description: Listing Id
          required: true
          type: integer
        - name: utm
          description: UTM parameters
          required: false
          type: objects

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    parameters_info = {
      'all_params': ['name', 'email', 'mobile', 'city_id', 'listing_id', 'utm'],
      'required_params': ['name', 'mobile', 'listing_id', 'city_id']
    }

    try:
        post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer = process_request(request,
                                                                                                   parameters_info)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    listing_id = post_data['listing_id']
    listing_obj = Listing.objects.get(id=listing_id)
    is_inventory = listing_obj.is_inventory

    crf_count = 0
    freemium_eligible = False
    if is_user_logged_in and is_subscribed_buyer and not is_inventory:
        crf_count = get_buyer_crf_count_of_today(buyer)
    elif is_user_logged_in and not is_subscribed_buyer:
        crf_count = get_buyer_crf_count_of_today(buyer)
        freemium_eligible = is_freemium_eligible(buyer.id)

    if crf_count <= 10:
        source_host = PLATFORM_NAME.get(request.get_host(), None)
        source = None
        if source_host:
            source = Source.objects.get(name=source_host)
        BuyerListing.objects.create(buyer=buyer, listing_id=listing_id, source=source)

    sms_data, email_data, spreadsheet_data = get_seller_contact_communications(request, buyer, listing_obj,
                                                                               crf_count, is_user_logged_in,
                                                                               is_subscribed_buyer)

    mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data, spreadsheet_data=spreadsheet_data)
    add_gearman_data('mailer', mailer_data)

    response = {
        'message': 'CRF Recorded successfully!',
        'buyer_id': buyer.id,
        'max_limit_reached': crf_count > 10,
        'buyer_limit_reached': not freemium_eligible,
        'status': True,
        'buyer_exists': existing_buyer,
        'seller_exists': is_interested_seller(post_data['mobile'])
    }

    if is_user_logged_in and not is_inventory and (is_subscribed_buyer or (not is_subscribed_buyer and
                                                                           freemium_eligible)) and \
            crf_count <= 10:
        seller_details = listing_obj.seller
        response['seller_info'] = {
            'name': seller_details.name,
            'mobile': seller_details.mobile
        }

    return Response(response, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def add_leads(request):
    """
    API View to store leads

    ---

    parameters:
        - name: name
          description: name
          required: true
          type: string
        - name: mobile
          description: mobile
          required: true
          type: string
        - name: city_id
          description: city_id
          required: true
          type: string
        - name: filters
          description: filters
          required: false
          type: string

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    parameters_info = {
      'all_params': ['name', 'mobile', 'city_id', 'filters'],
      'required_params': ['name', 'mobile', 'city_id']
    }

    try:
        post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer = process_request(request,
                                                                                                   parameters_info)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    city_name = 'india'
    if 'city_id' in post_data and post_data['city_id'] is not None:
        try:
            city_name = City.objects.get(id=post_data['city_id']).name.lower()
        except City.DoesNotExist:
            return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)

    buyer_info = model_to_dict(buyer)
    buyer_info['carid'] = 'http://www.truebil.com/used-cars-in-' + city_name
    if 'filters' in post_data and post_data['filters'] is not None:
        BuyerFilter.objects.create(buyer=buyer, filter=post_data['filters'])
        buyer_info['carid'] += post_data['filters']

    buyer_info['created_at'] = str(buyer_info['created_at'].strftime('%Y-%m-%d %H:%M:%S')) if 'created_at' in  \
                                                                                              buyer_info else ''
    buyer_info['source'] = 'Results Page'

    spreadsheet_data = [get_spreadsheet_data(buyer_info, 'BuyerLead', 'BuyerSync', city_name,
                                             is_user_logged_in, is_subscribed_buyer)]

    sms_data = [get_sms_data(type_='sold_car_crf', mobile=buyer.mobile, name=buyer.name[:20])]

    mailer_data = prepare_mailer_data(sms_data=sms_data, spreadsheet_data=spreadsheet_data)
    add_gearman_data('mailer', mailer_data)

    response = {
        'status': True,
        'buyer_exists': existing_buyer,
        'seller_exists': is_interested_seller(post_data['mobile']),
        'buyer_id': buyer_info['id']
    }

    return Response(response, status=status.HTTP_200_OK)


@api_view(['POST'])
def add_exit_lead(request):
    """
        API View to add exit lead

        ---

        parameters:
            - name: mobile
              description: Mobile number of the applicant
              required: true
              type: integer

            - name: city_id
              description: city_id of the applicant
              required: true
              type: integer

        responseMessages:
            - code: 400
              message: Required field not entered.
              message: Mobile Number is not correct.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml

        """

    parameters_info = {
        'all_params': ['name', 'mobile', 'city_id', 'utm'],
        'required_params': ['city_id', 'mobile']
    }

    try:
        post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer = process_request(request,
                                                                                                   parameters_info)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    buyer_info = model_to_dict(buyer)

    city_name = str(post_data['city']) if post_data['city'] else ''
    buyer_info['carid'] = 'http://www.truebil.com/used-cars-in-' + city_name

    BuyerExitLead.objects.create(buyer_id=buyer.id)

    buyer_obj = Buyer.objects.get(id=buyer.id)
    buyer_info['created_at'] = str(buyer_obj.created_at.strftime('%Y-%m-%d %H:%M:%S'))
    buyer_info['source'] = 'Results Page'

    sms_data = [get_sms_data(type_='exit_lead', mobile=post_data['mobile'])]
    spreadsheet_data = [get_spreadsheet_data(buyer_info, 'BuyerLead', 'BuyerSync', city_name,
                                             is_user_logged_in, is_subscribed_buyer)]

    email_data = []
    email_data.append(
        get_email_data(
            type_='exit_lead',
            send_to=['jit.buyer.crf.group@truebil.com'],
            subject='Buyer list while exiting',
            city=city_name,
            mobile=post_data['mobile'],
            time=timezone.now().strftime("%Y-%m-%d %H:%M:%S")
        )
    )
    mailer_data = prepare_mailer_data(sms_data=sms_data, spreadsheet_data=spreadsheet_data, email_data=email_data)
    add_gearman_data('mailer', mailer_data)

    response = {
        'buyer_exists': existing_buyer,
        'buyer_id': buyer.id,
        'status': True,
    }

    return Response(response, status=status.HTTP_201_CREATED)


@api_view(['POST'])
def verify_buyer_offer(request):
    """
    API View to verify buyer offer

    ---

    parameters:
        - name: listing_id
          description: Listing Id
          required: true
          type: integer
        - name: offered_price
          description: offered price
          required: true
          type: integer

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    parameters_info = {
      'all_params': ['listing_id', 'offered_price'],
      'required_params': ['listing_id', 'offered_price']
    }

    if type(request.user) is AnonymousUser:
        return Response(get_error_response(USER_NOT_LOGGED_IN), status=status.HTTP_400_BAD_REQUEST)

    try:
        post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer = process_request(request,
                                                                                                   parameters_info)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    listing_obj = Listing.objects.get(id=post_data['listing_id'])

    BuyerListingOffer.objects.filter(buyer=buyer, listing_id=post_data['listing_id'], is_active=True).\
        update(is_verified=True)

    sms_data, email_data = get_buyer_offer_communications(request, buyer, listing_obj,
                                                          is_user_logged_in, is_subscribed_buyer,
                                                          post_data['offered_price'],
                                                          is_subscribed_dealer(post_data['mobile']))
    mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data)
    add_gearman_data('mailer', mailer_data)

    return Response({'status': True}, status=status.HTTP_200_OK)
