# -*- coding: utf-8 -*-
import datetime
import hashlib

from django.contrib.auth.models import AnonymousUser
from django.forms.models import model_to_dict

from authn.main_auth_helpers import post_verification_process
from authn.utils.helpers import create_otp, get_login_communications
from connect.sms import get_sms_data
from connect.spreadsheet import get_spreadsheet_data
from main.quick_ops import get_refurbished_date, get_recommended_car_price, get_cdn_url
from main.quick_ops import valid_mobile_number
from main.utils.serializer_helpers import get_client_ip
from truebil_management.models import Source, UtmParameter, InventoryLocation
from truebil_management.utils.constants import FREEMIUM_LIMIT, WHITELIST
from user.exceptions import ParameterValidationError, RequestValidationError
from user.models import Buyer, VwBuyersInterestedInInventory, InvalidNumber, BuyerListing, City, User, Seller
from user.user_helpers import get_user_subscriptions, get_user_source
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_CITY, INVALID_MOBILE


def get_post_parameters(request, parameters):
    """
    Returns the post parameters from request form data.
    """

    post_data = request.data

    post_request_data = {}
    for parameter in parameters:
        post_request_data[parameter] = post_data.get(parameter, None)

    return post_request_data


def validate_empty_parameters(post_data, required_params):
    """
    Validates the required post parameters are empty or not.
    Sets data if data is correct.
    Raises ParameterValidationError when empty required parameter.
    """

    for parameter in post_data:
        if parameter in required_params and post_data[parameter] is None:
            raise ParameterValidationError


def process_request(request, parameters_info):
    post_data = get_post_parameters(request, parameters_info['all_params'])

    try:
        validate_empty_parameters(post_data, parameters_info['required_params'])
    except ParameterValidationError:
        raise RequestValidationError(REQUIRED_FIELDS_ABSENT)

    if 'city_id' in post_data and post_data['city_id'] is not None:
        try:
            post_data['city'] = City.objects.get(id=post_data['city_id'])
        except City.DoesNotExist:
            raise RequestValidationError(INVALID_CITY)

    is_user_logged_in = type(request.user) is not AnonymousUser
    is_subscribed_buyer = False

    if is_user_logged_in:
        mobile = request.user.mobile
        user = User.objects.filter(mobile=mobile).order_by('-created_at').first()

        post_data['mobile'] = user.mobile
        user_name = user.name

        if user_name:
            post_data['name'] = user_name
        else:
            post_data['name'] = post_verification_process(mobile=user.mobile, name=post_data.get('name', None),
                                                          city=post_data.get('city', None))['user_info']['name']

        post_data['email'] = user.email if user.email else post_data.get('email', None)
        post_data['city'] = user.city if user.city else post_data.get('city', None)

        is_subscribed_buyer = get_user_subscriptions(user.id, 'buyer')

    if valid_mobile_number(post_data['mobile']):
        buyer, existing_buyer = add_buyer(get_buyer_info(post_data, get_user_source(request.META)))

        return post_data, buyer, existing_buyer, is_user_logged_in, is_subscribed_buyer
    else:
        raise RequestValidationError(INVALID_MOBILE)


def get_buyer_info(post_data, host_name):
    buyer_info = {
        'name': post_data['name'],
        'mobile': post_data['mobile'],
    }

    if host_name:
        buyer_info['source'] = Source.objects.get(name=host_name)

    if 'utm' in post_data and post_data['utm'] is not None:
        utm_parameters = save_utm_parameters(post_data['utm'])
        buyer_info['utm_parameter_id'] = utm_parameters.id

    if 'city' in post_data and post_data['city'] is not None:
        buyer_info['city'] = post_data['city']

    if 'email' in post_data and post_data['email'] is not None:
        buyer_info['email'] = post_data['email']

    return buyer_info


def is_buyer_interested_in_inventory(mobile):
    try:
        VwBuyersInterestedInInventory.objects.get(mobile=mobile)
        return True
    except VwBuyersInterestedInInventory.DoesNotExist:
        return False


def is_invalid_number(mobile):
    try:
        InvalidNumber.objects.get(mobile=mobile)
        return True
    except InvalidNumber.DoesNotExist:
        return False


def add_buyer(buyer_info):
    existing_buyer = False
    try:
        buyer = Buyer.objects.get(mobile=buyer_info['mobile'])
        existing_buyer = True
    except Buyer.DoesNotExist:
        buyer = Buyer.objects.create(**buyer_info)
    return buyer, existing_buyer


def save_utm_parameters(utm_parameters):
    hash_string = ''
    for parameter in utm_parameters:
        hash_string += utm_parameters[parameter]

    hash = hashlib.md5(hash_string.encode('utf')).hexdigest()
    utm_parameters['hash'] = hash

    utm_parameters_obj = UtmParameter.objects.filter(hash=hash).first()
    if utm_parameters_obj is None:
        utm_parameters_obj = UtmParameter.objects.create(**utm_parameters)

    return utm_parameters_obj


def get_buyer_crf_count_of_today(buyer):
    today = datetime.date.today()
    return BuyerListing.objects.filter(buyer=buyer, created_at__gt=today, is_active=True, listing__is_inventory=False).\
        values_list('listing_id').distinct().count()


def should_communicate_to_seller(request, buyer_mobile, is_inventory, seller_type):
    return get_client_ip(request.META) not in WHITELIST and not is_buyer_interested_in_inventory(buyer_mobile) and \
           not is_invalid_number(buyer_mobile) and not is_inventory and seller_type == 'Individual'


def get_seller_email_data(listing_obj, buyer_info, is_crf=True):
    seller = listing_obj.seller
    listing_id = listing_obj.id

    view_vars = {
        'is_crf': is_crf,
        'car_link': 'https:' + get_cdn_url() + str(listing_id) + '-2-360.jpg?v=' + str(listing_obj.img_version),
        'buyer_name': buyer_info['name'],
        'seller_name': seller.name,
        'email_listing_id': listing_id,
        'user_city_lower': buyer_info['city']
    }

    is_eligible_inventory = listing_obj.is_eligible_inventory
    if is_eligible_inventory:
        view_vars['is_inventory_offered'] = is_eligible_inventory
        carwale_price = listing_obj.inventory_offered_price
    else:
        if listing_obj.carwale_price:
            carwale_price = listing_obj.carwale_price
        else:
            carwale_price = listing_obj.carwale_price_good_individual

    recommended_price = get_recommended_car_price(is_eligible_inventory, carwale_price)

    if recommended_price['max'] < listing_obj.price:
        view_vars['show_price_recommendation'] = True
        view_vars['min_offered_price'] = "₹" + "{:,}".format(recommended_price['min'])
        view_vars['max_offered_price'] = "₹" + "{:,}".format(recommended_price['max'])

    if is_crf is False:
        view_vars['buyer_offered_price'] = buyer_info['buyer_offered_price']

    return {
        'sendTo': seller.email,
        'subject': 'New buyer on Truebil for your car!' if is_crf else 'You have a new offer on Truebil for your car!',
        'viewVars': view_vars,
        'template': 'seller_crf_offer'
    }


def get_seller_contact_communications(request, buyer, listing_obj, crf_count, is_user_logged_in, is_subscribed_buyer):
    listing_id = listing_obj.id
    variant_name = listing_obj.variant.name[:20]
    is_inventory = listing_obj.is_inventory
    car_status = listing_obj.status
    is_refurbishing = car_status == 'refurbishing'
    is_sold = not is_refurbishing and car_status != 'active'
    listing_city = listing_obj.locality.city
    seller = listing_obj.seller
    seller_type = seller.type_id
    seller_email = seller.email
    seller_name = seller.name[:20]
    seller_mobile = seller.mobile

    buyer_mobile = buyer.mobile
    buyer_name = buyer.name[:20]
    buyer_city = buyer.city.name if buyer.city else ''
    buyer_email = buyer.email
    today = str(datetime.datetime.today())

    sms = []
    spreadsheet = []
    email = []

    if not is_user_logged_in:
        sms_data, spreadsheet_data = get_login_communications(buyer_mobile, otp=create_otp(buyer_mobile))
        sms.append(sms_data)
        spreadsheet.append(spreadsheet_data)

    if crf_count <= 10:
        email_data = {
            'sendTo': ['jit.buyer.crf.group@truebil.com'],
            'subject': 'Request Callback carid, User name, mobile, email, city id, Services ' + today,
            'message': str(listing_id) + ', ' + str(buyer_mobile) + ', ' + buyer_name + ', ' + str(buyer_email) + ', '
                       + buyer_city
        }

        email.append(email_data)

        if is_sold:
            sms.append(get_sms_data(type_='sold_car_crf', mobile=buyer_mobile, name=buyer_name))
        else:
            if not is_subscribed_buyer and not is_inventory:
                sms.append(get_sms_data(type_='crf_pitch_premium', mobile=buyer_mobile, name=buyer_name,
                                        variant_name=variant_name, listing_id=listing_id))

            send_comm_to_seller = should_communicate_to_seller(request, buyer_mobile, is_inventory, seller_type)

            if send_comm_to_seller:
                sms.append(get_sms_data(type_='crf_alert_to_seller', mobile=seller.mobile, s_name=seller_name,
                                        b_name=buyer_name))

                if seller_email:
                    buyer_info = {
                        'name': buyer_name,
                        'city': buyer_city.lower()
                    }
                    email.append(get_seller_email_data(listing_obj, buyer_info))

            if is_user_logged_in and is_subscribed_buyer and not is_inventory:
                sms.append(get_sms_data(type_='seller_contact_msg', mobile=buyer_mobile, b_name=buyer_name,
                                        variant_name=variant_name, s_name=seller_name, s_mobile=seller_mobile))

            buyer_info = model_to_dict(buyer)
            if 'created_at' in buyer_info: del buyer_info['created_at']
            buyer_info['carid'] = listing_id
            buyer_info['source'] = 'Inventory_crf' if is_inventory else 'Truebil-Crf'
            spreadsheet.append(get_spreadsheet_data(buyer_info, 'BuyerLead', 'BuyerSync', listing_city.name,
                                                    is_user_logged_in, is_subscribed_buyer))

            if is_inventory:
                inventory_info = InventoryLocation.objects.get(city_id=listing_city.id)
                email_data = {
                    'sendTo': ['inventory.crf.group@truebil.com'],
                    'subject': 'Inventory car CRF',
                    'message': "Hi , FYI \
                               Car Id: " + str(listing_id) + " \
                               Car Name: " + variant_name + " \
                               Buyer Name: " + buyer_name + " \
                               Buyer Mobile: " + str(buyer_mobile) + " \
                               City: " + buyer_city + " \
                               time: " + today

                }

                email.append(email_data)
                onwards = get_refurbished_date(listing_obj.expected_live_date)['formatted_date'] + ' onwards ' if \
                    is_refurbishing else ''

                sms.append(get_sms_data(type_='inventory_crf_msg', mobile=buyer_mobile, name=buyer_name,
                                        variant_name=variant_name, expert_mobile=inventory_info.poc_mobile, onwards=onwards,
                                        inventory_address=inventory_info.shop_name, locality=inventory_info.locality.name , 
                                        inventory_location_url=inventory_info.location_url, store_timings=
                                        inventory_info.opening_time.strftime("%I:%M %p") + ' to ' +
                                        inventory_info.closing_time.strftime("%I:%M %p")))

    else:
        email_data = {
            'sendTo': ['product.manager.group@truebil.com', 'jerrin.joy@truebil.com'],
            'subject': 'Max Limit reached for CRF',
            'message': "Hi , FYI \
                       Buyer Name: " + buyer_name + " \
                       Buyer Mobile: " + str(buyer_mobile) + " \
                       Buyer Email: " + str(buyer_email)

        }

        email.append(email_data)

    return sms, email, spreadsheet


def get_test_drive_request_communications(buyer, is_user_logged_in, is_subscribed_buyer):
    city_id = buyer.city.id if buyer.city else 1
    inventory_info = InventoryLocation.objects.get(city_id=city_id)
    sms_data = [get_sms_data(type_='test_drive_request', mobile=buyer.mobile, name=buyer.name,
                            expert_mobile=inventory_info.poc_mobile)]

    buyer.source = 'inventory_testdrive'
    buyer_city_name = buyer.city.name if buyer.city else ''
    buyer_info = model_to_dict(buyer)
    if 'created_at' in buyer_info: del buyer_info['created_at']
    spreadsheet_data = [get_spreadsheet_data(buyer_info, 'BuyerLead', 'BuyerSync', buyer_city_name,
                                             is_user_logged_in, is_subscribed_buyer)]
    
    return sms_data, spreadsheet_data


def get_buyer_offer_communications(request, buyer, listing_obj, is_user_logged_in, is_subscribed_buyer, offered_price,
                                   subscribed_dealer):
    listing_id = listing_obj.id
    variant_name = listing_obj.variant.name[:20]
    offer_value = "{:n}".format(float(offered_price))
    is_inventory = listing_obj.is_inventory
    buyer_city_name = buyer.city.name if buyer.city else ''
    listing_city = listing_obj.locality.city
    is_refurbishing = listing_obj.status == 'refurbishing'
    seller = listing_obj.seller
    seller_type = seller.type_id
    seller_email = seller.email
    seller_name = seller.name[:20]
    seller_mobile = seller.mobile
    buyer_mobile = buyer.mobile
    buyer_name = buyer.name[:20]
    buyer_city = buyer.city.name if buyer.city else ''
    today = str(datetime.datetime.today())

    sms = []
    email = []

    if is_inventory:
        inventory_info = InventoryLocation.objects.get(city_id=listing_city.id)
        subject = 'Dealer interest on inventory: Offer made' if subscribed_dealer else 'Inventory Offer made'
        email_group = 'ajinkya.bhasagare@truebil.com' if subscribed_dealer else 'inventory.crf.group@truebil.com'

        email_data = {
            'sendTo': [email_group],
            'subject': subject,
            'message': "Hi , FYI Car Id : %s Car Name: %s Buyer Name : %s Buyer Mobile : %s Offered Price : %s City :"
                       " %s time : %s" % (str(listing_id), variant_name, buyer_name, str(buyer_mobile), str(offer_value),
                                          buyer_city_name, today)
        }
        email.append(email_data)

        if subscribed_dealer:
            sms.append(get_sms_data(type_='subscribed_dealer_offer_inventory', mobile=buyer_mobile,
                                    offer_value=offer_value, variant_name=variant_name,
                                    inventory_location_url=inventory_info.location_url, dealer_poc='Ajinkya, 8291853765'))
        else:
            sms.append(get_sms_data(type_='buyer_offer_inventory', mobile=buyer_mobile, name=buyer_name,
                                    offer_value=offer_value, variant_name=variant_name, expert_mobile=inventory_info.poc_mobile))

            onwards = get_refurbished_date(listing_obj.expected_live_date)['formatted_date'] + ' onwards ' if \
                is_refurbishing else ''
            inventory_info = InventoryLocation.objects.get(city_id=listing_city.id)
            sms.append(get_sms_data(type_='test_drive', mobile=buyer_mobile, onwards=onwards,
                                    inventory_address=inventory_info.address,
                                    store_timings=inventory_info.opening_time.strftime("%I:%M %p") + ' to ' +
                                    inventory_info.closing_time.strftime("%I:%M %p"),
                                    inventory_location_url=inventory_info.location_url))

    else:
        if is_subscribed_buyer:
            sms.append(get_sms_data(type_='subscribed_buyer_offer', mobile=buyer_mobile, name=buyer_name,
                                    offer_value=offer_value, variant_name=variant_name))
        else:
            sms.append(get_sms_data(type_='buyer_offer', mobile=buyer_mobile, name=buyer_name, offer_value=offer_value,
                                    variant_name=variant_name, url='goo.gl/fZ9X3A'))

        send_comm_to_seller = should_communicate_to_seller(request, buyer_mobile, is_inventory, seller_type)

        if send_comm_to_seller:
            sms.append(get_sms_data(type_='seller_offer_alert', mobile=seller_mobile, name=seller_name,
                                   variant_name=variant_name, url='goo.gl/wD7IDe'))

            if seller_email:
                buyer_info = {
                    'name': buyer_name,
                    'city': buyer_city.lower(),
                    'buyer_offered_price': offer_value
                }
                email.append(get_seller_email_data(listing_obj, buyer_info, False))

    return sms, email


def is_subscribed_dealer(mobile):
    try:
        return Seller.objects.get(mobile=mobile, parent_id=None).type_id == 'Subscribed_Dealer'
    except Seller.DoesNotExist:
        return False


def is_freemium_eligible(buyer_id):
    """
    Validates if the non subscribed buyer is eligible for freemium plan or not
    checks count on the unique listings
    parameters : buyer_id
    """
    return BuyerListing.objects.values('listing_id').filter(buyer_id=buyer_id).distinct().count() <= FREEMIUM_LIMIT


def validate_buyer_for_seller_data(buyer, listing_id):
    listings = map(int, BuyerListing.objects.filter(buyer=buyer, is_active=True).order_by('created_at').values_list(
        'listing_id', flat=True)[:FREEMIUM_LIMIT])

    if listing_id in listings:
        return True
    else:
        return False


def get_or_create_buyer(request):
    user = request.user
    try:
        buyer = Buyer.objects.get(mobile=user.mobile)
    except Buyer.DoesNotExist:
        buyer_info = dict()
        buyer_info['mobile'] = user.mobile
        buyer_info['name'] = user.name
        buyer_info['email'] = user.email
        buyer_info['city_id'] = user.city_id
        buyer_info['source_id'] = Source.objects.values('id').get(name=get_user_source(request.META))['id']
        utm = request.data.get('utm', None)
        if utm:
            utm_parameters = save_utm_parameters(utm)
            buyer_info['utm_parameter_id'] = utm_parameters.id
        buyer = Buyer.objects.create(**buyer_info)

    return buyer
