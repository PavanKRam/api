# -*- coding: utf-8 -*-
import collections
import socket
from datetime import datetime

from dateutil import parser
from django.contrib.gis.geoip2 import GeoIP2
from django.db.models import Case, When, Value, IntegerField
from django.utils import timezone

from main.models import Listing, CarInspector, Attachment
from main.quick_ops import ordinal, haversine, get_asset_url, get_cdn_url
from main.utils.constants import TOP_ORDER_LISTINGS_STATUSES, LOW_ORDER_LISTINGS_STATUSES, SELLER_LISTING_STATUSES
from truebil_management.models import InventoryLocation
from user.models import UserSubscription, User, Buyer, Seller, Subscription, BuyerVisit, BuyerVisitListing, \
    VisitTimeSlot, City, InterestedSeller, RazorpayPayment
from user.utils.constants import SUBSCRIPTION_MAP, SHOWCASE_PRICE, INSPECTOR_MOBILE


def get_user_subscriptions(user_id, subscription_type=None, show_all=None):
    """
    Function returns user subscription details
    :param user_id: user id
    :param subscription_type: buyer or seller
    :param show_all: True or False
    :return: a dictionary of subscription status
    """

    user_subscriptions = {
        'seller': False,
        'buyer': False
    }

    user_subscriptions_qs = UserSubscription.objects.select_related('subscription').filter(user_id=user_id,
                                                                                           is_active=True)

    if user_subscriptions_qs:
        today = timezone.now().date()
        for user_subscription in user_subscriptions_qs:
            if today <= user_subscription.end_date.date():
                user_subscriptions[user_subscription.subscription.type] = True
                if show_all:
                    user_subscriptions[user_subscription.subscription.type + '_subscription'] = \
                        get_subscription_data(user_subscription)
            else:
                user_subscriptions[user_subscription.subscription.type] = False

    if subscription_type is not None:
        return user_subscriptions[subscription_type]
    else:
        return user_subscriptions


def get_subscription_data(subscribed_subscription):
    return {
        'subscription_id': subscribed_subscription.subscription.id,
        'paid_amount': subscribed_subscription.subscription.price,
        'paid_date': subscribed_subscription.start_date.date().strftime("%d-%m-%Y"),
        'payment_through': get_payment_method(subscribed_subscription.transaction_id),
        'relationship_manager': get_relationship_manager(subscribed_subscription)
    }


def get_payment_method(transaction_id):
    try:
        payment_method = RazorpayPayment.objects.get(id=transaction_id).method
        return payment_method
    except RazorpayPayment.DoesNotExist:
        return None


def get_relationship_manager(user_subscription):
    if user_subscription.relationship_manager:
        return {
            'name': user_subscription.relationship_manager.name,
            'mobile': user_subscription.relationship_manager.mobile
        }
    else:
        return None


def get_active_subscriptions_info(source=None):
    """
    Returns active subscriptions info
    """

    active_subscriptions_info = collections.defaultdict(list)
    subscriptions_qs = Subscription.objects.filter(is_active=True)

    for subscription in subscriptions_qs:
        user_type = subscription.type
        active_subscriptions_info[user_type].append(
            {
                'id': subscription.id,
                'name': subscription.name,
                'description': subscription.description,
                'price': subscription.price,
                'active_days': subscription.active_days,
                'features': SUBSCRIPTION_MAP[subscription.id][
                    source] if user_type == 'buyer' and subscription.id in SUBSCRIPTION_MAP else '',
                'is_popular': True if subscription.name == 'Essential' else False if user_type == 'buyer' else False,
                'showcase_price': SHOWCASE_PRICE[subscription.id] if user_type == 'buyer' and subscription.id in
                SHOWCASE_PRICE else ''
            }
        )

    return active_subscriptions_info


def get_user_info(mobile):
    user = User.objects.filter(mobile=mobile).order_by('-created_at'). \
        values('id', 'name', 'mobile', 'email', 'city_id').first()

    if not user:
        user_info = get_buyer_seller_info(mobile)
        user_info['mobile'] = mobile
        return user_info

    return user


def get_buyer_seller_info(mobile):
    """
    Check for user info in existing buyer and seller
    """
    user_name = None
    user_email = None

    buyer_info = get_buyer_info(mobile)

    if buyer_info is not None:
        user_name = buyer_info['name']
        user_email = buyer_info['email']
    else:
        seller_info = get_seller_info(mobile)

        if seller_info is not None:
            user_name = seller_info['name']
            user_email = seller_info['email']

    return {'name': user_name, 'email': user_email}


def get_buyer_info(mobile):
    """
    Get buyer record
    :param mobile: Mobile number of user
    :return: name and email if buyer exists
    """
    try:
        buyer_info = Buyer.objects.get(mobile=mobile)
        return {'name': buyer_info.name, 'email': buyer_info.email}
    except Buyer.DoesNotExist:
        return None


def get_seller_info(mobile):
    """
    Get seller record
    :param mobile: Mobile number of user
    :return: name and email if seller exists
    """
    try:
        seller_info = Seller.objects.get(mobile=mobile, parent_id__isnull=True)
        return {
            'name': seller_info.name,
            'email': seller_info.email,
            'type': seller_info.type_id
        }
    except Seller.DoesNotExist:
        return None


def check_seller_listing_statuses(mobile):
    """
    Checks statuses of listings of seller present in SELLER_LISTING_STATUSES
    :param mobile: Mobile number of user
    :return: name and email if seller exists
    """
    try:
        seller = Seller.objects.get(mobile=mobile, parent_id__isnull=True)
        return Listing.objects.filter(seller=seller, status__in=SELLER_LISTING_STATUSES).exists()
    except Seller.DoesNotExist:
        return False


def is_interested_seller(mobile):
    """
    Get Interested seller record
    :param mobile: Mobile number of user
    :return: name and email if seller exists
    """

    return True if InterestedSeller.objects.filter(mobile=mobile).exists() else False


def add_listing_to_visit(listing, buyer_visit):
    """
    The function aims to add a listing to a buyer visit or update the status of the listing, if it already exists for
    that visit
    """

    try:
        buyer_visit_listing = BuyerVisitListing.objects.get(listing=listing, buyer_visit=buyer_visit)
        buyer_visit_listing.is_active = True
        buyer_visit_listing.save(update_fields=['is_active'])
    except BuyerVisitListing.DoesNotExist:
        buyer_visit_listing = BuyerVisitListing.objects.create(listing=listing, buyer_visit=buyer_visit)

    return buyer_visit_listing


def remove_listing_from_visit(buyer_visit_listing):
    """
    The function toggles the status of listing added to a buyer visit
    """
    buyer_visit_listing.is_active = False
    buyer_visit_listing.save()


def format_date_time(date):
    if isinstance(date, unicode):
        return parser.parse(date)
    else:
        return date


def get_visit_date_time(visit_date, visit_time_slot_id):
    date = get_date_ordinal_month(datetime.strptime(str(visit_date), '%Y-%m-%d'))

    visit_time_slot = VisitTimeSlot.objects.get(id=visit_time_slot_id)
    time = visit_time_slot.start_time.strftime("%I") + "-" + visit_time_slot.end_time.strftime("%I%p")

    return {"date": date, "time": time}


def get_date_ordinal_month(date):
    date = format_date_time(date)
    return ordinal(int(date.strftime("%d"))) + ' ' + date.strftime("%B")


def get_buyer_visit_booking_info(mobile):
    """
    Get buyer's visit booking details
    :param mobile: buyer mobile number
    :return:
    """

    try:
        buyer = Buyer.objects.get(mobile=mobile)

        buyer_visit = BuyerVisit.objects.filter(buyer=buyer).order_by('-created_at').first()
        buyer_visit_listings = BuyerVisitListing.objects.filter(buyer_visit=buyer_visit, is_active=True)
        visit_listings = buyer_visit_listings.values_list('listing_id', flat=True)
        inventory = InventoryLocation.objects.get(city=buyer_visit.city)
        all_cities_of_listings_in_buyer_visit = Listing.objects.filter(
            id__in=visit_listings, locality__city__isnull=False).values_list('locality__city', flat=True)
        distinct_city_ids = set(all_cities_of_listings_in_buyer_visit)
        distinct_city_qs = City.objects.filter(id__in=distinct_city_ids)

        data = {
            "visit_timestamp": buyer_visit.visit_date,
            "visit_id": buyer_visit.id,
            "visit_date": (buyer_visit.visit_date.strftime("%-d")) + ' ' + buyer_visit.visit_date.strftime("%B")[:3],
            "visit_day": buyer_visit.visit_date.strftime("%A"),
            "visit_time": str(buyer_visit.visit_time_slot.start_time.strftime("%-I%p").lower()) + ' - '
                          + str(buyer_visit.visit_time_slot.end_time.strftime("%-I%p").lower()),
            "status": buyer_visit.status,
            "active_city_name": buyer_visit.city.name,
            "active_city_id": buyer_visit.city.id,
            "visit_listings": visit_listings,
            "visit_listings_count": len(visit_listings),
            "inventory_location": inventory.address,
            "inventory_url": inventory.location_url
        }

        city_list = list()
        for city in distinct_city_qs:
            city_list.append({
                "id": city.id,
                "name": city.name
            })
        data['buyer_visit_cities'] = city_list

        return data
    except:
        return None


def get_inventory_info(city):
    try:
        inventory_data = InventoryLocation.objects.get(city=city)

        return {
            'city': inventory_data.city.id,
            'location_url': inventory_data.location_url,
            'address': inventory_data.address,
            'poc_name': inventory_data.poc_name,
            'poc_mobile': inventory_data.poc_mobile,
            'store_opening_time': inventory_data.opening_time,
            'store_closing_time': inventory_data.closing_time
        }
    except InventoryLocation.DoesNotExist:
        return dict()


def get_sorted_dashboard_cars(listing_qs):
    """
    The function sorts the listings that appear in the buyer dashboard.
    """
    return listing_qs.annotate(
        custom_order=Case(
            When(status__in=TOP_ORDER_LISTINGS_STATUSES, then=Value(1)),
            When(status__in=LOW_ORDER_LISTINGS_STATUSES, then=Value(2)),
            output_field=IntegerField(),
        )
    ).order_by('custom_order')


def get_city_id_from_ip(ip_address):
    """
    Return from city id if ip address belongs to address near 50km of
    active city location
    :param ip_address:
    :return: city id if it's near active city
    """
    geo_ip = GeoIP2()
    max_distance = 150
    try:
        user_coords = geo_ip.coords(ip_address)
    except (RuntimeError, socket.gaierror):
        return None
    cities = list(City.objects.filter(is_active=True))
    for city in cities:
        distance = haversine((city.longitude, city.latitude), user_coords)
        if distance < max_distance:
            return {
                'city_id': city.id,
                'city_name': city.name
            }
    return None


def get_client_ip(request_meta):
    """
    :param request_meta: object
    :return: ip address extracted from meta information
    """

    if request_meta.get('HTTP_X_USER_IP'):
        ip = request_meta.get('HTTP_X_USER_IP')
    elif request_meta.get('HTTP_X_FORWARDED_FOR'):
        x_forwarded_for = request_meta.get('HTTP_X_FORWARDED_FOR')
        ip = x_forwarded_for.split(',')[-1].strip()
    elif request_meta.get('HTTP_X_REAL_IP'):
        ip = request_meta.get('HTTP_X_REAL_IP')
    else:
        ip = request_meta.get('REMOTE_ADDR')
    return ip


def get_city_for_anonymous_user(request_meta):
    client_ip = get_client_ip(request_meta)
    return get_city_id_from_ip(client_ip)


def get_user_source(request_meta):
    if 'HTTP_X_USER_IP' in request_meta:
        return 'desktop'
    else:
        return 'pwa'


def get_seller_visit_info(seller_visit_obj):
    """
    Returns Seller Inspection details
    :param seller_visit_obj:
    :return: dicitionary of seller inspection details and information of the car inspector

    """
    date_temp = datetime.strptime(str(seller_visit_obj.visit_date), "%Y-%m-%d")
    slot_temp = seller_visit_obj.visit_time_slot
    start_time = slot_temp.start_time.strftime('%I') + slot_temp.start_time.strftime('%p').lower()
    end_time = slot_temp.end_time.strftime('%I') + slot_temp.end_time.strftime('%p').lower()
    time = start_time + ' - ' + end_time

    data = {
        'seller_inspection_details': {
            'day': date_temp.strftime('%A'),
            'month': date_temp.strftime('%b'),
            'date': date_temp.strftime('%e'),
            'time': time,
            'location': seller_visit_obj.location
        }
    }
    if seller_visit_obj.inspector_id:
        car_inspector_obj = CarInspector.objects.get(id=seller_visit_obj.inspector_id)
        data['seller_inspection_details']['inspector_info'] = {
            'id': car_inspector_obj.id,
            'name': car_inspector_obj.name,
            'mobile': INSPECTOR_MOBILE,
            'experience': str(car_inspector_obj.experience) + "+ years of experience",
            'url': get_asset_url() + "/images/inspectors/" + str(car_inspector_obj.id) + "-" +
                   str(car_inspector_obj.name.lower()) + ".jpg?v=1"
        }
    if seller_visit_obj.locality:
        data['seller_inspection_details']['locality'] = seller_visit_obj.locality.name
        data['seller_inspection_details']['city'] = seller_visit_obj.locality.city.name

    return data


def get_sellerpage_listing_image(mobile):
    """
        Returns image url for seller page if seller's car is in seller dashboard

        :param mobile :

    """

    listing = Listing.objects.filter(seller__mobile=mobile).order_by('-created_at')
    if listing.exists():
        listing_obj = listing.first()
        attachment_obj = Attachment.objects.get(listing=listing_obj, caption_id=2)
        cdn_url = get_cdn_url()

        url_first_half = cdn_url + str(listing_obj.id) + '-' + str(attachment_obj.file_number) + '-360.jpg'
        url = url_first_half if not listing_obj.img_version else (url_first_half + '?v=' + str(listing_obj.img_version))
        return url
    else:
        return {}
