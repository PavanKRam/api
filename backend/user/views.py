# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, timedelta

from django.contrib.auth.models import AnonymousUser
from django.db.models import Case, When, Count
from rest_framework import pagination, mixins
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from authn.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from connect.buyer_visit import prepare_buyer_visit_before_save_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from connect.sms import get_sms_data
from connect.spreadsheet import get_seller_spreadsheet_data
from main.models import Listing, Attachment, InterestedSellerListings
from main.quick_ops import get_car_details, get_cars_unavailable_for_test_drive, get_cdn_url
from main.serializers import ListingSerializer, UnavilableCarSerializer, CarsAddedToTestDriveSerializer, \
    DashboardSerializer, BuyerOfferSerializer
from main.utils.constants import CAPTION_IDS
from main.utils.messages import INVALID_PATCH_REQUEST
from main.utils.serializer_helpers import get_invalid_numbers, get_region_localities
from main.views import ListingParentViewSet
from truebil_management.models import InventoryLocation
from truebil_management.utils.constants import SUPPORT_MOBILE_NO
from user.crf.helpers import get_or_create_buyer
from user.models import BuyerListing, BuyerListingOffer, UserActivity, City, Buyer, BuyerVisitListing, BuyerVisit, \
    SellerVisit, Locality, CarInspectorAvailability
from user.serializers import BuyerListingSerializer, BuyerListingOfferSerializer, UserActivitySerializer, CitySerializer \
    , BuyerVisitSerializer
from user.user_helpers import get_seller_visit_info
from user.user_helpers import get_visit_date_time, add_listing_to_visit, get_date_ordinal_month, get_inventory_info, \
    remove_listing_from_visit, get_sorted_dashboard_cars
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_LISTING
from user.utils.response import get_error_response


class CityViewSet(viewsets.GenericViewSet, mixins.ListModelMixin, mixins.RetrieveModelMixin):
    """Viewset class for City model"""

    queryset = City.objects.filter(is_active=True)
    serializer_class = CitySerializer


class DashboardPagination(pagination.PageNumberPagination):
    page_size = 8
    page_size_query_param = 'page_size'
    max_page_size = 100


class DashboardParentViewSet(ListingParentViewSet, mixins.UpdateModelMixin):
    """Parent Viewset class for dashboard viewsets"""

    permission_classes = [IsAuthenticated]
    pagination_class = DashboardPagination

    def get_serializer_context(self):
        context_dict = ListingParentViewSet.get_serializer_context(self)
        context_dict['show_one_url'] = True
        return context_dict

    def get_serializer_class(self):
        if self.action == 'list':
            return DashboardSerializer


class ContactRequestedViewSet(DashboardParentViewSet):
    """Viewset class for BuyerListing model"""

    def get_serializer_class(self):
        serializer = DashboardParentViewSet.get_serializer_class(self)
        if self.action == 'partial_update':
            serializer = BuyerListingSerializer
        return serializer

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """

        if self.action == 'list':
            buyer_listing_ids = BuyerListing.objects.filter(buyer__mobile=self.request.user.mobile,
                                                            listing__isnull=False,
                                                            is_active=True).values_list('listing_id', flat=True)

            listing_qs = Listing.objects.filter(id__in=buyer_listing_ids)
            return ListingSerializer.setup_eager_loading(get_sorted_dashboard_cars(listing_qs))

        elif self.action == 'partial_update':
            return BuyerListing.objects.all()

    def partial_update(self, request, *args, **kwargs):
        buyer_listing = BuyerListing.objects.get(buyer__mobile=self.request.user.mobile,
                                                 listing_id=request.data['listing'])
        buyer_listing.is_active = False
        buyer_listing.save()
        serializer = self.get_serializer(buyer_listing)
        return Response(serializer.data)


class RecentlyVisitedViewSet(DashboardParentViewSet):
    """Viewset class for UserActivity model"""

    permission_classes = [IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """

        if self.action == 'list':
            cars_seen = self.request.query_params.get('cars_seen', None)
            city_id = self.request.query_params.get('city_id', None)
            if type(self.request.user) is AnonymousUser:
                if cars_seen is None:
                    return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

                seen_car_ids = [int(car_id) for car_id in cars_seen.split(',')]
                listing_qs = Listing.objects.filter(id__in=seen_car_ids).order('-created_at')
            else:
                user_activities = self.request.user.user_activities. \
                    filter(is_seen=True, listing__locality__city_id=city_id).order_by('-created_at')
                listing_qs = Listing.objects.filter(id__in=list(user_activities.values_list('listing', flat=True)))

            return ListingSerializer.setup_eager_loading(get_sorted_dashboard_cars(listing_qs))

    def partial_update(self, request, *args, **kwargs):
        """
        View to remove seen car for user.

        Methods:
             - PATCH:
                parameters:
                    - name: id
                      description: Listing id
                      required: false
                      type: list
        """

        listing_id = kwargs.get('pk', None)
        if listing_id:
            user_activity_qs = UserActivity.objects.filter(user=self.request.user, listing_id=listing_id)
            if user_activity_qs.exists():
                user_activity = user_activity_qs.first()
                user_activity.is_seen = not user_activity.is_seen
                user_activity.save()
            else:
                user_activity = UserActivity.objects.create(user=self.request.user, listing_id=listing_id,
                                                            is_seen=True)

            return Response(UserActivitySerializer(user_activity).data)
        else:
            return Response(get_error_response(INVALID_PATCH_REQUEST), status=status.HTTP_400_BAD_REQUEST)


class OffersMadeViewSet(DashboardParentViewSet):
    """Viewset class for BuyerListingOffer model"""

    def get_serializer_class(self):
        serializer = DashboardParentViewSet.get_serializer_class(self)
        if self.action == 'partial_update':
            serializer = BuyerListingOfferSerializer
        return serializer

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """
        if self.action == 'list':

            buyer_listing_ids = BuyerListingOffer.objects.filter(buyer__mobile=self.request.user.mobile, is_active=True,
                                                                 is_verified=True).values_list('listing_id', flat=True)

            listing_qs = Listing.objects.filter(id__in=buyer_listing_ids)
            return ListingSerializer.setup_eager_loading(get_sorted_dashboard_cars(listing_qs))

        elif self.action == 'partial_update':
            return BuyerListingOffer.objects.all()

    def partial_update(self, request, *args, **kwargs):
        buyer_listing_offer = BuyerListingOffer.objects.get(buyer__mobile=self.request.user.mobile,
                                                            listing_id=request.data['listing'], is_active=True)
        buyer_listing_offer.is_active = False
        buyer_listing_offer.save()
        serializer = self.get_serializer(buyer_listing_offer)
        return Response(serializer.data)


class ShortlistedCarsViewSet(DashboardParentViewSet):
    """Viewset class for UserActivity model"""

    def get_serializer_class(self):
        serializer = DashboardParentViewSet.get_serializer_class(self)
        if self.action == 'partial_update':
            serializer = UserActivitySerializer
        return serializer

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """

        if self.action == 'list':

            shortlisted_car_ids = list(UserActivity.objects.filter(user=self.request.user, is_shortlisted=True).
                                       order_by('-modified_at').values_list('listing_id', flat=True))

            preserved = Case(*[When(pk=listing_id, then=position)
                               for position, listing_id in enumerate(shortlisted_car_ids)])

            listing_qs = Listing.objects.filter(id__in=shortlisted_car_ids).order_by(preserved)
            return ListingSerializer.setup_eager_loading(get_sorted_dashboard_cars(listing_qs))

        elif self.action == 'partial_update':
            return UserActivity.objects.all()

    def partial_update(self, request, *args, **kwargs):
        user_activity_qs = UserActivity.objects.filter(user=self.request.user, listing_id=request.data['listing'])
        if user_activity_qs.exists():
            user_activity = user_activity_qs.first()
            user_activity.is_shortlisted = not user_activity.is_shortlisted
            user_activity.save()
        else:
            user_activity = UserActivity.objects.create(user=self.request.user, listing_id=request.data['listing'],
                                                        is_shortlisted=True)
        serializer = self.get_serializer(user_activity)
        return Response(serializer.data)


class InspectionRequestedCarsViewSet(DashboardParentViewSet):
    """Viewset class for Listing model"""

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """
        inspection_requested_cars = Listing.objects.filter(inspection_requested_user=self.request.user)

        return ListingSerializer.setup_eager_loading(get_sorted_dashboard_cars(inspection_requested_cars))


class VisitBooking(APIView):
    """
    An API View to book, cancel or reschedule visit booking
    """

    permission_classes = [IsAuthenticated]

    def get_serializer_context(self):
        context_dict = ListingParentViewSet.get_serializer_context(self)
        return context_dict

    def post(self, request, format=None):
        """
        An API View to book visit.

        ---

        Methods:
             - POST:
                parameters:
                    - name: listing_id
                      description: listing id
                      required: false
                      type: string
                    - name: visit_date
                      description: visit date
                      required: true
                      type: string
                    - name: visit_time_slot_id
                      description: visit time slot id
                      required: true
                      type: string
                    - name: is_confirmed
                      description: is confirmed
                      required: false
                      type: string
                    - name: city_id
                      description: city id
                      required: true
                      type: string

                consumes:
                    - application/json
                    - application/xml
                produces:
                    - application/json
                    - application/xml

            """
        listing_id = request.data.get('listing_id', None)
        visit_date = request.data.get('visit_date', None)
        visit_time_slot_id = request.data.get('visit_time_slot_id', None)
        city_id = request.data.get('city_id', None)

        user = request.user

        buyer = get_or_create_buyer(request)

        buyer_visit = BuyerVisit.objects.filter(buyer_id=buyer.id, status__in=['booked', 'missed'])

        sms = []
        if not buyer_visit.exists():
            prepare_buyer_visit_before_save_data(None)
            buyer_visit_obj = BuyerVisit.objects.create(buyer=buyer, visit_date=visit_date,
                                                        visit_time_slot_id=visit_time_slot_id, status='booked',
                                                        city_id=city_id)

            address = InventoryLocation.objects.get(city_id=city_id).address
            location_url = InventoryLocation.objects.get(city_id=city_id).location_url
            if buyer_visit_obj:
                visit_date_time = get_visit_date_time(buyer_visit_obj.visit_date, visit_time_slot_id)
                date_time = visit_date_time['date'] + ", " + visit_date_time['time']
                test_drive_car_count = BuyerVisitListing.objects.filter(buyer_visit=buyer_visit_obj, is_active=True,
                                                                        buyer_visit__city=buyer_visit_obj.city).values(
                    'listing').aggregate(total=Count('listing'))['total']

                if listing_id:
                    listing = Listing.objects.get(id=listing_id)
                    add_listing_to_visit(listing, buyer_visit_obj)

                    if get_car_details(listing_id)['listing_status'] == 'refurbishing':
                        sms.append(get_sms_data(type_='refurbished_car_added_to_test_drive', mobile=user.mobile,
                                                name=buyer.name, date_time=date_time, address=address,
                                                car_name=get_car_details(listing_id)['car_name'][:20],
                                                available_date=get_date_ordinal_month(listing.expected_live_date)))

                    else:
                        sms.append(get_sms_data(type_='car_added_to_test_drive', mobile=user.mobile, name=buyer.name,
                                                date_time=date_time, address=address,
                                                car_name=get_car_details(listing_id)['car_name'][:20]))

                else:
                    if buyer_visit.exists():
                        sms.append(get_sms_data(type_='test_drive_rescheduled', mobile=user.mobile, name=buyer.name,
                                                date_time=date_time, test_drive_car_count=test_drive_car_count,
                                                address=address, inventory_location_url=location_url))
                    else:
                        sms.append(get_sms_data(type_='test_drive_scheduled', mobile=user.mobile, name=buyer.name,
                                                date_time=date_time, address=address))

                mailer_data = prepare_mailer_data(sms_data=sms)
                add_gearman_data('mailer', mailer_data)

                return Response({"status": True, "data": BuyerVisitSerializer(buyer_visit_obj).data}, status=status.
                                HTTP_201_CREATED)
        else:
            return Response({"status": False, "message": "An active or missed booking already exists. Please reschedule"
                             }, status=status.HTTP_405_METHOD_NOT_ALLOWED)

    def patch(self, request, *args, **kwargs):
        """
        Method to reschedule and cancel the visit booking

        Methods:
             - PATCH:
                parameters:
                    - name: visit_date
                      description: visit date
                      required: false
                      type: string
                    - name: visit_time_slot_id
                      description: visit time slot id
                      required: false
                      type: string
                    - name: is_confirmed
                      description: is confirmed
                      required: false
                      type: string
                    - name: cancel
                      description: cancel booking
                      required: false
                      type: boolean
                    - name: city_id
                      description: city id
                      required: true
                      type: string

        """

        cancel_booking = request.data.get('cancel', None)
        visit_date = request.data.get('visit_date', None)
        visit_time_slot_id = request.data.get('visit_time_slot_id', None)
        is_confirmed = request.data.get('is_confirmed', False)
        city_id = request.data.get('city_id', None)

        buyer_visit_id = kwargs.get('pk', None)
        buyer_visit = BuyerVisit.objects.get(id=buyer_visit_id)

        if cancel_booking:
            try:
                prepare_buyer_visit_before_save_data(buyer_visit)
                buyer_visit.status = "cancelled"
                buyer_visit.save(update_fields=['status'])
                return Response({"status": True, "data": BuyerVisitSerializer(buyer_visit).data}, status=status.
                                HTTP_200_OK)
            except BuyerVisit.DoesNotExist:
                return Response(get_error_response("Invalid Buyer Visit ID"), status=status.HTTP_400_BAD_REQUEST)
        else:
            cars_unavailable_for_test_drive = get_cars_unavailable_for_test_drive(
                buyer_visit.buyer_visit_listings.filter(is_active=True).values_list('listing_id', flat=True),
                buyer_visit.visit_date)

            if cars_unavailable_for_test_drive and is_confirmed is False:
                attachment_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
                return Response({"status": True, "is_car_unavailable": True,
                                 "data": UnavilableCarSerializer(cars_unavailable_for_test_drive, many=True,
                                                                 context={'cdn_url': get_cdn_url(),
                                                                          'attachment_qs': attachment_qs}).data},
                                status=status.HTTP_200_OK)

            if (cars_unavailable_for_test_drive and is_confirmed) or (not cars_unavailable_for_test_drive.exists()):
                try:
                    buyer_visit = BuyerVisit.objects.get(id=buyer_visit_id)
                    prepare_buyer_visit_before_save_data(buyer_visit)
                    buyer_visit.status = 'booked'
                    if visit_date is not None:
                        buyer_visit.visit_date = visit_date
                    if visit_time_slot_id is not None:
                        buyer_visit.visit_time_slot_id = visit_time_slot_id
                    if city_id is not None:
                        buyer_visit.city_id = city_id

                    buyer_visit.save(update_fields=['status', 'visit_date', 'visit_time_slot_id', 'city_id'])
                except BuyerVisit.DoesNotExist:
                    return Response(get_error_response("Invalid Buyer Visit ID"), status=status.HTTP_400_BAD_REQUEST)

            return Response(
                {"status": True, "data": BuyerVisitSerializer(BuyerVisit.objects.get(id=buyer_visit_id)).data},
                status=status.HTTP_200_OK)


class VisitListing(APIView):
    """
    An API View to get, add or remove listing from buyer visit
    """

    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        An API view to list all the active listings in the visit
        """

        user = request.user

        buyer = Buyer.objects.get(mobile=user.mobile)
        buyer_visit = BuyerVisit.objects.filter(buyer_id=buyer.id, status__in=['booked', 'missed'])
        buyer_visit_obj = buyer_visit.latest('created_at')

        buyer_visit_listing_qs = BuyerVisitListing.objects.filter(buyer_visit=buyer_visit_obj, is_active=True)

        all_listings_in_buyer_visit = Listing.objects.filter(id__in=buyer_visit_listing_qs.values_list('listing_id',
                                                                                                       flat=True))

        listings_in_buyer_visit_city = all_listings_in_buyer_visit.filter(locality__city=buyer_visit_obj.city)
        attachment_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)

        return Response({"status": True,
                         "data": CarsAddedToTestDriveSerializer(listings_in_buyer_visit_city, many=True,
                                                                context={'cdn_url': get_cdn_url(),
                                                                         'buyer_visit': buyer_visit_obj,
                                                                         'attachment_qs': attachment_qs,
                                                                         'invalid_numbers': get_invalid_numbers()}).data
                         }, status=status.HTTP_200_OK)

    def post(self, request, format=None):
        """
        An API View to book visit.

        ---

        Methods:
             - POST:
                parameters:
                    - name: listing_id
                      description: listing id
                      required: false
                      type: string

                consumes:
                    - application/json
                    - application/xml
                produces:
                    - application/json
                    - application/xml

            """
        listing_id = request.data.get('listing_id', None)

        user = request.user

        buyer = get_or_create_buyer(request)

        buyer_visit = BuyerVisit.objects.filter(buyer_id=buyer.id, status__in=['booked', 'missed'])

        sms = []
        listing = Listing.objects.get(id=listing_id)
        if buyer_visit.exists():
            buyer_visit_obj = buyer_visit.latest('created_at')
            add_listing_to_visit(listing, buyer_visit_obj)

            car_city = listing.locality.city
            is_listing_from_different_city = True if car_city.name != buyer_visit_obj.city.name else False

            visit_date_time = get_visit_date_time(buyer_visit_obj.visit_date, buyer_visit_obj.visit_time_slot.id)
            date_time = visit_date_time['date'] + ", " + visit_date_time['time']
            inventory_details = get_inventory_info(listing.locality.city)
            sms.append(get_sms_data(type_='listing_added_to_buyer_visit', mobile=user.mobile,
                                    name=buyer.name, car_name=get_car_details(listing_id)['car_name'][:20],
                                    expert_name=inventory_details.get('poc_name'),
                                    expert_number=inventory_details.get('poc_mobile')))

            sms.append(get_sms_data(type_='test_drive_detail', mobile=user.mobile,
                                    name=buyer.name, date_time=date_time, address=inventory_details['address'],
                                    inventory_location_url=inventory_details['location_url'], store_timings=
                                    inventory_details['store_opening_time'].strftime("%I:%M %p") + ' to ' +
                                    inventory_details['store_closing_time'].strftime("%I:%M %p")))

            mailer_data = prepare_mailer_data(sms_data=sms)
            add_gearman_data('mailer', mailer_data)

            return Response({"status": True, "is_listing_from_different_city": is_listing_from_different_city,
                             "car_city_id": car_city.id, "car_city_name": car_city.name},
                            status=status.HTTP_201_CREATED)
        else:
            return Response({"status": False}, status=status.HTTP_400_BAD_REQUEST)

    def patch(self, request, *args, **kwargs):
        """
        Method to remove car from visit booking
        """

        buyer_visit_listing_id = kwargs.get('pk', None)

        user = request.user
        buyer = Buyer.objects.get(mobile=user.mobile)

        try:
            buyer_visit_listing = BuyerVisitListing.objects.get(pk=buyer_visit_listing_id)
            buyer_visit_obj = buyer_visit_listing.buyer_visit

            sms = []
            remove_listing_from_visit(buyer_visit_listing)
            visit_date_time = get_visit_date_time(buyer_visit_obj.visit_date, buyer_visit_obj.visit_time_slot.id)
            date_time = visit_date_time['date'] + ", " + visit_date_time['time']

            sms.append(get_sms_data(type_='car_removed_from_test_drive', mobile=user.mobile,
                                    name=buyer.name, date_time=date_time,
                                    car_name=get_car_details(buyer_visit_listing.listing.id)['car_name'][:20]))

            mailer_data = prepare_mailer_data(sms_data=sms)
            add_gearman_data('mailer', mailer_data)

            return Response({"status": True}, status=status.HTTP_200_OK)

        except BuyerVisitListing.DoesNotExist:
            return Response(get_error_response("Listing does not exist in the buyer visit"),
                            status=status.HTTP_400_BAD_REQUEST)


class BuyerOffer(APIView):
    """View class for buyer listing offer"""

    serializer_class = BuyerOfferSerializer

    def get(self, request, format=None):
        """
        Returns buyer listing offer details

        ---

        parameters:
            - name: listing_id
              description: Listing ID
              required: true
              type: integer
            - name: buyer_id
              description: Buyer ID
              required: true
              type: integer

        responseMessages:
            - code: 400
              message: Listing does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        listing_id = request.query_params.get('listing_id', None)

        if not listing_id:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        buyer = None
        if type(self.request.user) is not AnonymousUser:
            mobile = self.request.user.mobile
            buyer = Buyer.objects.filter(mobile=mobile).order_by('-created_at').first()
        else:
            buyer_id = self.request.query_params.get('buyer_id', None)
            if buyer_id:
                try:
                    buyer = Buyer.objects.get(id=buyer_id)
                except Buyer.DoesNotExist:
                    pass

        try:
            listing = Listing.objects.get(id=listing_id)
            return Response(BuyerOfferSerializer(listing, context={'buyer': buyer,
                                                                   'invalid_numbers': get_invalid_numbers()}).data,
                            status=status.HTTP_200_OK)
        except Listing.DoesNotExist:
            return Response(get_error_response(INVALID_LISTING), status=status.HTTP_400_BAD_REQUEST)


# TODO remove this api functionality after recommendation logic implemented in api
class BuyerDashboardCars(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        """
        An API view to list all the listing ids associated with each activity in buyer dashboard tabs
        """

        city_id = self.request.query_params.get('city_id', None)
        if city_id is None:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        contacts_requested_car_ids = list(BuyerListing.objects.
                                          filter(buyer__mobile=request.user.mobile, listing__isnull=False,
                                                 is_active=True).values_list('listing_id', flat=True))

        offers_made_car_ids = list(BuyerListingOffer.objects.
                                   filter(buyer__mobile=request.user.mobile, is_active=True, is_verified=True)
                                   .values_list('listing_id', flat=True))

        shortlisted_car_ids = list(UserActivity.objects.filter(user=request.user, is_shortlisted=True)
                                   .order_by('-modified_at').values_list('listing_id', flat=True))

        recently_visited_car_ids = list(request.user.user_activities.
                                        filter(is_seen=True, listing__locality__city_id=city_id).order_by('-created_at')
                                        .values_list('listing_id', flat=True))

        return Response({
            'seenCarIds': recently_visited_car_ids,
            'contactedSellerCarIds': contacts_requested_car_ids,
            'shortlistedCarIds': shortlisted_car_ids,
            'madeOfferCarList': offers_made_car_ids
        }, status=status.HTTP_200_OK)


class SellerVisitBooking(APIView):
    """
        An API View to book and reschedule Seller visit Booking
    """
    permission_classes = [IsAuthenticatedOrReadOnly]

    def post(self, request):
        """
        An API View to schedule an inspection or seller's car
        ---

        parameters:
            - name: visit_date
              description: Date for the inspection
              required: true
              type: Date
            - name: visit_time_slot_id
              description: Time slot
              required: true
              type: integer
            - name: location
              description: Address of the seller
              required: true
              type: text
            - name: locality_id
              description: Locality
              required: true
              type: integer

        responseMessages:
            - code: 201
              status: true
            - code: 400
              status: false
              message: Seller does not have any active enquiry
            - code: 400
              message: Invalid Locality
            - code: 403
              status: false
              message: An active or missed booking already exists.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        response = dict()
        visit_date = request.data.get('visit_date', None)
        visit_time_slot_id = request.data.get('visit_time_slot_id', None)
        location = request.data.get('location', None)
        locality_id = request.data.get('locality_id', None)
        user = request.user

        locality = None
        try:
            if locality_id != '-1':
                locality = Locality.objects.get(id=locality_id)
        except Locality.DoesNotExist:
            return Response(get_error_response('Invalid Locality'), status.HTTP_400_BAD_REQUEST)

        ten_day_before = datetime.now() - timedelta(days=10)
        seller_visit_in_last_ten_days = InterestedSellerListings.objects.filter(
            seller_visit__isnull=False,
            interested_seller__mobile=user.mobile,
            seller_visit__status__in=['booked', 'missed'],
            created_at__gt=ten_day_before
        )

        if seller_visit_in_last_ten_days.exists():
            response['status'] = False
            response['message'] = 'An active or missed booking already exists.'
            return Response(response, status=status.HTTP_403_FORBIDDEN)

        interested_seller_listing = InterestedSellerListings.objects.filter(
            interested_seller__mobile=user.mobile,
            seller_visit__isnull=True
        ).order_by('-created_at').first()

        if interested_seller_listing is None:
            message = 'Seller does not have any active enquiry'
            return Response(get_error_response(message), status.HTTP_400_BAD_REQUEST)

        inspector_id = None
        visit_date = datetime.strptime(str(visit_date), "%Y-%m-%d")
        if locality:
            inspectors = CarInspectorAvailability.objects.filter(locality=locality)
            available_inspectors = list()
            for inspector in inspectors:
                if inspector.available_days[visit_date.weekday()] == '1':
                    available_inspectors.append(inspector)

            inspector_assigned = available_inspectors[user.id % len(available_inspectors)] \
                if len(available_inspectors) else None

            if inspector_assigned:
                inspector_id = inspector_assigned.car_inspector_id

        seller_visit = SellerVisit.objects.create(
            visit_date=visit_date.date(),
            visit_time_slot_id=visit_time_slot_id,
            inspector_id=inspector_id,
            locality=locality,
            location=location,
            status='booked'
        )

        interested_seller_listing.seller_visit = seller_visit
        interested_seller_listing.save(update_fields=['seller_visit'])

        spreadsheet_data = list()
        sms_data = [get_sms_data(type_='interested_seller', mobile=user.mobile, name=user.name,
                                 support_mobile=SUPPORT_MOBILE_NO)]
        spreadsheet_data.append(get_seller_spreadsheet_data(user, interested_seller_listing, seller_visit))
        mailer_data = prepare_mailer_data(sms_data=sms_data, spreadsheet_data=spreadsheet_data)
        add_gearman_data('mailer', mailer_data)

        response['status'] = True
        response['data'] = get_seller_visit_info(seller_visit)

        return Response(response, status=status.HTTP_201_CREATED)


class SellerInspectionLocality(APIView):
    permission_classes = [IsAuthenticatedOrReadOnly]

    def get(self, request):
        """
            An API View to schedule an inspection or seller's car
            ---

            parameters:
                - name: city_id
                  description: City Id
                  required: true
                  type: Date

            responseMessages:
                - code: 200
                  status: true
                - code: 400
                  status: false
                  message: Required field(s) not entered.
            consumes:
                - application/json
                - application/xml
            produces:
                - application/json
                - application/xml
        """

        city_id = request.query_params.get('city_id', None)
        response = dict()

        if city_id is None:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)
        else:
            response['status'] = True
            regions, locality = get_region_localities(city_id)
            response['regions'] = regions
            response['locality'] = locality
            return Response(response, status=status.HTTP_200_OK)
