import datetime
from datetime import timedelta

import razorpay
from django.forms.models import model_to_dict
from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from authn.permissions import IsAuthenticated
from connect.email import get_email_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from connect.sms import get_sms_data
from connect.spreadsheet import get_spreadsheet_data
from server.settings import env
from truebil_management.models import Source
from user.models import UserSubscription, RazorpayPayment, Subscription, City
from user.user_helpers import get_user_subscriptions, get_user_source
from user.utils.constants import SUBSCRIPTIONS_HAVING_RELATIONSHIP_MANAGERS
from user.utils.messages import INVALID_SUBSCRIPTION
from user.utils.response import get_error_response


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def payment(request):
    """
    API View to perform functions as soon as the user completes payment

    ---
    parameters:
        - name: subscription_id
          description: subscription_id
          required: true
          type: string
        - name: razorpay_payment_id
          description: razorpay_payment_id
          required: true
          type: string
        - name: city_id
          description: city_id
          required: true
          type: string

    responseMessages:
        - code: 200
          message: User has no active subscription.
          message: User has no subscription history.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    user = request.user

    client = razorpay.Client(auth=(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET')))

    subscription_id = request.data.get('subscription_id', None)
    razorpay_payment_id = request.data.get('razorpay_payment_id', None)
    city_id = request.data.get('city_id', None)
    city_name = "india"
    source = get_user_source(request.META)
    source_id = Source.objects.get(name=source).id

    if city_id:
        try:
            city_name = City.objects.get(id=city_id).name.lower()
        except City.DoesNotExist:
            pass

    try:
        payment_data = client.payment.fetch(razorpay_payment_id)
    except Exception as e:
        return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

    try:
        subscription_record = Subscription.objects.get(id=subscription_id)
    except Subscription.DoesNotExist:
        return Response(get_error_response(INVALID_SUBSCRIPTION), status=status.HTTP_400_BAD_REQUEST)

    payment_record = RazorpayPayment.objects.filter(txn_id=razorpay_payment_id)

    email_data = list()
    sms_data = list()
    payment_status = True

    if not payment_record.exists():
        amount_paid = payment_data['amount']/100
        new_payment = RazorpayPayment(txn_id=razorpay_payment_id, amount=amount_paid,
                                      order_id=payment_data['order_id'], method=payment_data['method'],
                                      bank=payment_data['bank'], wallet=payment_data['wallet'],
                                      error_description=payment_data['error_description'],
                                      payment_time=datetime.datetime.fromtimestamp(payment_data['created_at']).
                                      strftime('%Y-%m-%d %H:%M:%S'), status=payment_data['status'])
        new_payment.save()

        if not user.email:
            user.email = payment_data['email']
            user.save()

        user_info = model_to_dict(user)
        user_info['source'] = 'Subscribed'
        user_info['created_at'] = str(user_info['created_at'])
        user_info['modified_at'] = str(user_info['modified_at'])
        spreadsheet_data = [get_spreadsheet_data(user_info, 'BuyerLead', 'BuyerSync', city_name, True,
                                                 get_user_subscriptions(user_info['id'], 'buyer'))]

        email_data.append(get_email_data(type_='payment_status', send_to=['payment.status.group@truebil.com'],
                                         subject='Payment Status', status=payment_data['status'],
                                         sub_type=subscription_record.type, payment_id=razorpay_payment_id,
                                         city_name=city_name, user_name=user_info['name'], user_mobile=user_info['mobile'],
                                         user_email=user_info['email'], time=timezone.now().strftime("%Y-%m-%d %H:%M:%S"),
                                         subscription_id=subscription_id, source=source))

        if subscription_record.price != amount_paid:
            email_data.append(get_email_data(type_='fraud_transaction', send_to=['developers@truebil.com',
                                                                                 'product.manager.group@truebil.com'],
                                             subject='Fraud transaction', status=payment_data['status'],
                                             sub_type=subscription_record.type, payment_id=razorpay_payment_id,
                                             subscription_id=subscription_id, city_name=city_name, user_name=user_info['name'],
                                             user_mobile=user_info['mobile'], user_email=user_info['email'],
                                             amount=amount_paid, time=timezone.now().strftime("%Y-%m-%d %H:%M:%S"),
                                             source=source))

            payment_status = False
        else:
            try:
                payment_data = client.payment.capture(razorpay_payment_id, payment_data['amount'])
            except Exception as e:
                return Response(get_error_response(str(e)), status=status.HTTP_400_BAD_REQUEST)

            if payment_data['status'] == "captured" and payment_data['error_code'] is None:
                RazorpayPayment.objects.filter(txn_id=razorpay_payment_id).update(status=payment_data['status'],
                                                                                  captured_at=timezone.now())
                start_date = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
                end_date = (timezone.now() + timedelta(days=subscription_record.active_days)).strftime("%Y-%m-%d %H:%M:%S")
                UserSubscription.objects.filter(user=user, subscription_id=subscription_id).update(is_active=False)
                user_subscription = UserSubscription.objects.create(user=user, subscription_id=subscription_id,
                                                                    start_date=start_date, end_date=end_date,
                                                                    transaction_id=new_payment.id, is_active=True,
                                                                    source_id=source_id)

                view_vars = { 'name': user.name, 'amount': subscription_record.price, 'source': source,
                              'subscription_id': subscription_id }
                              
                if subscription_record.type is 'seller':
                    seller_url = 'http://goo.gl/ID5yxg'

                    sms_data.append(get_sms_data(type_='seller_payment_success', mobile=user.mobile,
                                                 s_name=user.name[:20], sub_price=subscription_record.price,
                                                 url=seller_url))

                    email_data.append(get_email_data(type_='seller_payment_success', send_to=[user.email],
                                                     subject='Payment to Truebil successful', viewVars=view_vars,
                                                     template='sellerPaymentSuccess'))
                else:
                    if subscription_id in SUBSCRIPTIONS_HAVING_RELATIONSHIP_MANAGERS:
                        subscription_data = {'user_subscription_id': user_subscription.id, 'city_id': user.city_id}
                        add_gearman_data('relationship', subscription_data)

                    sms_data.append(get_sms_data(type_='buyer_payment_success', mobile=user.mobile,
                                                 sub_price=subscription_record.price))

                    email_data.append(get_email_data(type_='payment_success', send_to=[user.email],
                                                     subject='Payment to Truebil successful', viewVars=view_vars,
                                                     template='paymentSuccess'))

            else:
                email_data.append(get_email_data(type_='payment_error', send_to=['developers@truebil.com',
                                                                                 'product.manager.group@truebil.com'],
                                                 subject='Error in capturing payment', payment_id=razorpay_payment_id,
                                                 user_name=user.name, user_mobile=user.mobile, user_email=user.email,
                                                 payment_data=payment_data, subscription_id=subscription_id, source=source))

                RazorpayPayment.objects.filter(txn_id=razorpay_payment_id).\
                    update(status=payment_data['status'], error_description=payment_data['error_description'])

                payment_status = False

        mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data, spreadsheet_data=spreadsheet_data)
        add_gearman_data('mailer', mailer_data)

    return Response({'payment_status': payment_status, 'subscription_type': subscription_record.type},
                    status=status.HTTP_200_OK)
