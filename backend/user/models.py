# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class BaseModel(models.Model):

    class Meta:
        abstract = True

    restricted_fields = ['created_at', 'created', 'modified_at', 'changed_at']

    def _do_insert(self, manager, using, fields, update_pk, raw):
        return super(BaseModel, self). \
            _do_insert(manager, using, [field for field in fields if
                                        field.attname not in self.restricted_fields],
                       update_pk, raw)

    def _do_update(self, base_qs, using, pk_val, values, update_fields, forced_update):
        return super(BaseModel, self)._do_update(
            base_qs,
            using,
            pk_val,
            [value for value in values if value[0].attname not in self.restricted_fields],
            update_fields,
            forced_update
        )


class EmploymentType(models.Model):
    name = models.CharField(max_length=128)

    class Meta:
        db_table = 'employment_types'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class EmploymentCategory(models.Model):
    name = models.CharField(max_length=128)
    employment_type = models.ForeignKey(to='EmploymentType', related_name='employment_categories')

    class Meta:
        db_table = 'employment_categories'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class City(models.Model):
    name = models.CharField(max_length=128)
    is_active = models.BooleanField(default=True)
    latitude = models.FloatField(default=None)
    longitude = models.FloatField(default=None)

    class Meta:
        db_table = 'cities'
        verbose_name_plural = 'cities'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Region(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)
    city = models.ForeignKey(to='City', related_name='regions')

    class Meta:
        db_table = 'regions'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class RegionalTransportOffice(models.Model):
    name = models.CharField(max_length=128, null=True, blank=True)
    city = models.ForeignKey(to='City', related_name='rto')
    state_code = models.CharField(max_length=64, null=True, blank=True)
    rto_code = models.CharField(max_length=64, null=True, blank=True)

    class Meta:
        db_table = 'regional_transport_offices'
        unique_together = (('state_code', 'rto_code'),)

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Locality(models.Model):
    city = models.ForeignKey(to='City', related_name='localities')
    name = models.CharField(max_length=128)
    region = models.ForeignKey(to='Region', related_name='region_localities')
    regional_transport_office = models.ForeignKey(to='RegionalTransportOffice', related_name='rto_localities')

    class Meta:
        db_table = 'localities'
        verbose_name_plural = 'localities'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class Seller(BaseModel):

    SELLER_TYPE_CHOICES = (
        ('Individual', 'Individual'),
        ('Dealer', 'Dealer'),
        ('Subscribed_Dealer', 'Subscribed_Dealer')
    )

    SELLER_AGE_CHOICES = (
        ('0-25', '0-25'),
        ('25-30', '25-30'),
        ('30-35', '30-35'),
        ('35-40', '35-40'),
        ('40-50', '40-50'),
        ('50+', '50+')
    )

    SELLER_INCOME_CHOICES = (
        ('0-5', '0-5'),
        ('5-10', '5-10'),
        ('10-15', '10-15'),
        ('15+', '15+')
    )

    SELLER_GENDER_CHOICES = (
        ('Male', 'Male'),
        ('Female', 'Female'),
    )

    SELLER_CAR_EXPERIENCE_CHOICES = (
        ('0-2', '0-2'),
        ('2-5', '2-5'),
        ('5+', '5+'),
    )

    name = models.CharField(max_length=128, null=True, blank=True)
    mobile = models.IntegerField(null=True, blank=True)
    email = models.CharField(max_length=128, null=True, blank=True)
    employment_category = models.ForeignKey(to='EmploymentCategory', related_name='sellers')
    city = models.ForeignKey(to='City', related_name='sellers')
    locality = models.ForeignKey(to='Locality', related_name='sellers_in_locality')
    address = models.TextField(null=True, blank=True)
    pincode = models.CharField(max_length=16, null=True, blank=True)
    type_id = models.CharField(max_length=16, choices=SELLER_TYPE_CHOICES, null=True, blank=True)
    age = models.CharField(max_length=16, choices=SELLER_AGE_CHOICES, null=True, blank=True)
    income = models.CharField(max_length=16, choices=SELLER_INCOME_CHOICES, null=True, blank=True)
    gender = models.CharField(max_length=16, choices=SELLER_GENDER_CHOICES, null=True, blank=True)
    car_usage_experience = models.CharField(max_length=16, choices=SELLER_CAR_EXPERIENCE_CHOICES, null=True, blank=True)
    mobile_secondary = models.IntegerField(null=True, blank=True)
    firm_name = models.CharField(max_length=128, null=True, blank=True)
    created_at = models.DateTimeField()
    email_secondary = models.CharField(max_length=128, null=True, blank=True)
    parent_id = models.IntegerField(null=True, blank=True)

    class Meta:
        db_table = 'sellers'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class LoanApplicant(BaseModel):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    quoted_amount = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()
    city = models.ForeignKey(to='City', related_name='loan_applicants', blank=True, null=True)
    utm_parameter = models.ForeignKey(to='truebil_management.UtmParameter', related_name='utm_parameter_loan_applicants',
                                      blank=True, null=True)

    class Meta:
        db_table = 'loan_applicants'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InsuranceApplicant(BaseModel):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    city = models.ForeignKey(to='City', related_name='insurance_applicants', blank=True, null=True)
    utm_parameter = models.ForeignKey(to='truebil_management.UtmParameter', related_name='utm_parameter_insurance_applicants',
                                      blank=True, null=True)

    class Meta:
        db_table = 'insurance_applicants'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class PaperTransferApplicant(BaseModel):
    name = models.CharField(max_length=100)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    created_at = models.DateTimeField()
    city = models.ForeignKey(to='City', related_name='paper_transfer_applicants', blank=True, null=True)
    utm_parameter = models.ForeignKey(to='truebil_management.UtmParameter', related_name='utm_parameter_paper_transfer',
                                      blank=True, null=True)

    class Meta:
        db_table = 'paper_transfer_applicants'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class InterestedSeller(BaseModel):
    name = models.CharField(max_length=45, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    created = models.DateTimeField(null=True)
    city = models.ForeignKey(to='City', related_name='interested_sellers', blank=True, null=True)
    utm_parameter = models.ForeignKey(to='truebil_management.UtmParameter', related_name='utm_parameter_sellers', blank=True,
                                      null=True)

    class Meta:
        db_table = 'interested_sellers'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class User(BaseModel):
    name = models.CharField(max_length=50, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    mobile = models.BigIntegerField(unique=True)
    city = models.ForeignKey(to='City', related_name='users', blank=True, null=True)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()
    utm_parameter = models.ForeignKey(to='truebil_management.UtmParameter', related_name='utm_parameter_users', blank=True,
                                      null=True)

    class Meta:
        db_table = 'users'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class UserActivity(BaseModel):
    user = models.ForeignKey(to='User', related_name='user_activities')
    listing = models.ForeignKey(to='main.Listing', related_name='user_listing_activities')
    is_seen = models.BooleanField(default=False)
    is_shortlisted = models.BooleanField(default=False)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    class Meta:
        db_table = 'user_activities'
        unique_together = (('listing', 'user',),)

    def __unicode__(self):
        return '%s - %s' % (self.user.id, self.listing.variant.name)


class UserRsaClaim(BaseModel):
    user = models.ForeignKey(to='User', related_name='rsa_claims')
    bought_from = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'user_rsa_claims'

    def __unicode__(self):
        return '%s - %s' % (self.user.id, self.bought_from)


class Buyer(BaseModel):
    name = models.CharField(max_length=100, blank=True, null=True)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    email = models.CharField(max_length=100, blank=True, null=True)
    city = models.ForeignKey(to='City', related_name='buyers', blank=True, null=True)
    utm_parameter = models.ForeignKey(to='truebil_management.UtmParameter', related_name='utm_parameter_buyers', blank=True,
                                      null=True)
    created_at = models.DateTimeField()
    source = models.ForeignKey(to='truebil_management.Source', related_name='buyer_source', blank=True, null=True)

    class Meta:
        db_table = 'buyers'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)

class BuyerExitLead(BaseModel):
    buyer = models.ForeignKey(to='Buyer',related_name='buyer_exit_lead',blank=False,null=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'buyer_exit_leads'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)

class BuyerInventory(BaseModel):
    buyer = models.ForeignKey(to='Buyer', related_name='buyer_inventories', blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'buyer_inventories'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class RelationshipManager(models.Model):
    name = models.CharField(max_length=50, blank=True, null=True)
    mobile = models.CharField(max_length=20, blank=True, null=True)
    city = models.ForeignKey(to='City', related_name='relationship_managers')
    is_active = models.BooleanField(default=True)

    class Meta:
        db_table = 'relationship_managers'

    def __unicode__(self):
        return '%s - %s' % (self.name, self.mobile)


class Subscription(BaseModel):

    SUBSCRIPTION_TYPE_CHOICES = (
        ('buyer', 'buyer'),
        ('seller', 'seller'),
    )

    name = models.CharField(max_length=50)
    type = models.CharField(max_length=16, choices=SUBSCRIPTION_TYPE_CHOICES)
    description = models.TextField(blank=True, null=True)
    is_active = models.IntegerField(blank=True, null=True)
    price = models.IntegerField()
    active_days = models.IntegerField()
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    class Meta:
        db_table = 'subscriptions'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class UserSubscription(models.Model):
    user = models.ForeignKey(to='User', related_name='user_subscriptions')
    subscription = models.ForeignKey(to='Subscription', related_name='subscribed_users')
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    is_active = models.BooleanField(default=True)
    transaction_id = models.IntegerField()
    is_suspended = models.BooleanField(default=False)
    relationship_manager = models.ForeignKey(to='RelationshipManager', null=True, blank=True, related_name=
                                             'user_assigned')
    source = models.ForeignKey(to='truebil_management.Source', related_name='user_subscription_source', blank=True)

    class Meta:
        db_table = 'user_subscriptions'

    def __unicode__(self):
        return '%s - %s' % (self.user.id, self.subscription.name)


class BuyerListingOffer(BaseModel):
    buyer = models.ForeignKey(to='Buyer', related_name='offers_made', blank=True, null=True)
    listing = models.ForeignKey(to='main.Listing', related_name='offers', blank=True, null=True)
    offered_price = models.IntegerField(blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_verified = models.BooleanField(default=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'buyer_listing_offers'

    def __unicode__(self):
        return '%s - %s' % (self.buyer.name, self.listing.id)


class BuyerFilter(BaseModel):
    buyer = models.ForeignKey(to='Buyer', related_name='filters', blank=True, null=True)
    filter = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'buyer_filters'

    def __unicode__(self):
        return '%s - %s' % (self.buyer.name, self.filter)


class SellerInventoryRequest(BaseModel):
    seller = models.ForeignKey(to='Seller', related_name='seller_inventory_requests')
    listing = models.ForeignKey(to='main.Listing', related_name='listing_inventory_requests')
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'seller_inventory_requests'

    def __unicode__(self):
        return '%s - %s' % (self.seller.name, self.listing.registration_number)


class VwBuyersInterestedInInventory(models.Model):
    mobile = models.IntegerField(primary_key=True)

    class Meta:
        managed = False
        db_table = 'vw_buyers_interested_in_inventories'


class InvalidNumber(models.Model):
    mobile = models.CharField(primary_key=True, max_length=10)

    class Meta:
        db_table = 'invalid_numbers'

    def __unicode__(self):
        return '%s' % self.mobile


class DealerInventoryInterest(BaseModel):
    dealer = models.ForeignKey(to='Seller', related_name='dealer_inventory_interests')
    listing = models.ForeignKey(to='main.Listing', related_name='interested_dealers', blank=True, null=True)
    offered_price = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'dealer_inventory_interests'

    def __unicode__(self):
        return '%s - %s' % (self.dealer.name, self.offered_price)


class BuyerListing(BaseModel):
    listing = models.ForeignKey(to='main.Listing', related_name='listing_buyers', blank=True, null=True)
    buyer = models.ForeignKey(to='Buyer', related_name='buyer_listings', blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField()
    source = models.ForeignKey(to='truebil_management.Source', related_name='buyer_listing_source', blank=True, null=True)

    class Meta:
        db_table = 'buyer_listings'

    def __unicode__(self):
        return '%s - %s' % (self.listing.variant.name, self.buyer.name)


class Feedback(BaseModel):
    name = models.CharField(max_length=45)
    mobile = models.CharField(max_length=10, blank=True, null=True)
    email = models.CharField(max_length=45, blank=True, null=True)
    message = models.TextField(blank=True, null=True)
    query = models.CharField(max_length=100, blank=True, null=True)
    city = models.ForeignKey(to='City', related_name='feedback', blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'feedback'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class SellerListingSoldFeedback(BaseModel):
    listing = models.ForeignKey(to='main.Listing', related_name='seller_listing_sold_feedback')
    experience = models.CharField(max_length=128, null=True)
    recommended_price_feedback = models.CharField(max_length=128, null=True)
    sold_price = models.IntegerField(null=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'seller_listing_sold_feedbacks'

    def __unicode__(self):
        return '%s' % self.listing


class UserListingFeedback(BaseModel):
    user = models.ForeignKey(to='User', related_name='user_listing_feedbacks')
    listing = models.ForeignKey(to='main.Listing', related_name='user_listing_feedbacks')
    feedback = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'user_listing_feedbacks'

    def __unicode__(self):
        return '%s - %s' % (self.user, self.listing)


class RazorpayPayment(BaseModel):
    txn_id = models.CharField(max_length=30, blank=True, null=True)
    amount = models.IntegerField(blank=True, null=True)
    order_id = models.IntegerField(blank=True, null=True)
    method = models.CharField(max_length=10, blank=True, null=True)
    bank = models.CharField(max_length=30, blank=True, null=True)
    status = models.CharField(max_length=10, blank=True, null=True)
    wallet = models.CharField(max_length=30, blank=True, null=True)
    error_code = models.TextField(blank=True, null=True)
    error_description = models.TextField(blank=True, null=True)
    payment_time = models.DateTimeField()
    created_at = models.DateTimeField()
    captured_at = models.DateTimeField()

    class Meta:
        db_table = 'razorpay_payments'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.amount)


class BuyerVisit(BaseModel):

    VISIT_STATUS_CHOICES = (
        ('booked', 'booked'),
        ('completed', 'completed'),
        ('cancelled', 'cancelled'),
        ('missed', 'missed'),
    )

    enjay_id = models.CharField(max_length=36, blank=True, null=True)
    buyer = models.ForeignKey(to='Buyer', related_name='buyer_visits')
    visit_date = models.DateField()
    visit_time_slot = models.ForeignKey(to='VisitTimeSlot', related_name='buyer_visit_slot')
    status = models.CharField(max_length=9, choices=VISIT_STATUS_CHOICES, blank=True, null=True)
    city = models.ForeignKey(to='City', related_name='buyer_visit_city')
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    class Meta:
        db_table = 'buyer_visits'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.visit_date)


class BuyerVisitListing(BaseModel):
    buyer_visit = models.ForeignKey(to='BuyerVisit', related_name='buyer_visit_listings')
    listing = models.ForeignKey(to='main.Listing', related_name='buyer_visit')
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    class Meta:
        unique_together = (('buyer_visit', 'listing'),)
        db_table = 'buyer_visit_listings'

    def __unicode__(self):
        return '%s - %s' % (self.buyer_visit.id, self.listing.id)


class VisitTimeSlot(BaseModel):

    TIME_GROUP_CHOICES = (
        ('Morning', 'Morning'),
        ('Afternoon', 'Afternoon'),
        ('Evening', 'Evening'),
    )

    start_time = models.TimeField(blank=True, null=True)
    end_time = models.TimeField(blank=True, null=True)
    time_group = models.CharField(max_length=9, choices=TIME_GROUP_CHOICES, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    class Meta:
        db_table = 'visit_time_slots'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.time_group)


class SellerVisit(BaseModel):

    VISIT_STATUS_CHOICES = (
        ('booked', 'booked'),
        ('completed', 'completed'),
        ('cancelled', 'cancelled'),
        ('missed', 'missed'),
    )
    visit_date = models.DateField()
    visit_time_slot = models.ForeignKey(to='user.VisitTimeSlot', related_name='seller_visits')
    inspector = models.ForeignKey(null=True, to='main.CarInspector', related_name='seller_visits')
    locality = models.ForeignKey(null=True, to='user.Locality', related_name='seller_visits')
    location = models.TextField()
    status = models.CharField(max_length=9, choices=VISIT_STATUS_CHOICES, blank=True, null=True)
    created_at = models.DateTimeField()
    modified_at = models.DateTimeField()

    class Meta:
        db_table = 'seller_visits'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.visit_date)


class CarInspectorAvailability(BaseModel):
    car_inspector = models.ForeignKey(to='main.CarInspector', related_name='inspector_availabilities')
    locality = models.ForeignKey(to='user.Locality', related_name='inspector_availabilities')
    available_days = models.CharField(max_length=7)

    class Meta:
        db_table = 'car_inspector_availabilities'

    def __unicode__(self):
        return '%s - %s' % (self.car_inspector.name, self.locality)