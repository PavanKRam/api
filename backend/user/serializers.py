"""
Module holding serializer classes for models
in truebil user module.
"""

from dateutil import parser
from rest_framework import serializers

from main.models import Listing
from truebil_management.models import InventoryLocation
from user.models import BuyerListing, BuyerListingOffer, City, Region, UserActivity, Buyer, BuyerVisit
import types
from user_helpers import format_date_time


class CitySerializer(serializers.ModelSerializer):
    """Serializer class for City model"""

    class Meta:
        model = City
        fields = ('id', 'name')


class RegionSerializer(serializers.ModelSerializer):
    """Serializer class for Region model"""

    def get_is_checked(self, obj):
        filter_regions = self.context.get('filter_regions', '')

        is_checked = False
        if str(obj.id) in filter_regions:
            is_checked = True

        return is_checked

    is_checked = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Region
        fields = ('id', 'name', 'is_checked')


class BuyerListingSerializer(serializers.ModelSerializer):
    """Serializer class for BuyerListing model"""
    listing = serializers.PrimaryKeyRelatedField(queryset=Listing.objects.all(), required=False)
    buyer = serializers.PrimaryKeyRelatedField(queryset=Buyer.objects.all(), required=False)

    class Meta:
        model = BuyerListing
        fields = ('id', 'listing', 'buyer', 'is_active')


class BuyerListingOfferSerializer(serializers.ModelSerializer):
    """Serializer class for BuyerListingOffer model"""
    listing = serializers.PrimaryKeyRelatedField(queryset=Listing.objects.all(), required=False)
    buyer = serializers.PrimaryKeyRelatedField(queryset=Buyer.objects.all(), required=False)

    class Meta:
        model = BuyerListingOffer
        fields = ('id', 'listing', 'buyer', 'is_active', 'offered_price', 'is_verified')


class UserActivitySerializer(serializers.ModelSerializer):
    """Serializer class for UserActivity model"""
    listing = serializers.PrimaryKeyRelatedField(queryset=Listing.objects.all(), required=False)
    user = serializers.PrimaryKeyRelatedField(queryset=Buyer.objects.all(), required=False)

    class Meta:
        model = UserActivity
        fields = ('id', 'listing', 'user', 'is_shortlisted', 'is_seen')


class BuyerVisitSerializer(serializers.ModelSerializer):
    """Serializer class for BuyerVisit model"""

    @staticmethod
    def get_visit_details(obj):
        buyer_visit_listings = obj.buyer_visit_listings.filter(is_active=True)
        inventory = InventoryLocation.objects.get(city=obj.city)
        buyer_visit_date = format_date_time(obj.visit_date)

        return {
            "visit_id": obj.id,
            "enjay_id": obj.enjay_id,
            "visit_date": (buyer_visit_date.strftime("%-d")) + ' ' + buyer_visit_date.strftime("%B")[:3],
            "visit_time": str(obj.visit_time_slot.start_time.strftime("%-I%p").lower()) + ' - ' +
                          str(obj.visit_time_slot.end_time.strftime("%-I%p").lower()),
            "visit_day": buyer_visit_date.strftime("%A"),
            "status": obj.status,
            "visit_city": obj.city.name,
            "visit_listings": buyer_visit_listings.values_list('listing_id', flat=True),
            "visit_listings_count": buyer_visit_listings.count(),
            "inventory_location": inventory.address,
            "inventory_url": inventory.location_url
        }

    visit_details = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = BuyerVisit
        fields = ('visit_date', 'visit_details')
