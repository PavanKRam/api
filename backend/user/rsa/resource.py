from django.utils import timezone
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from authn.permissions import IsAuthenticated
from connect.email import get_email_data
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from user.models import UserRsaClaim
from user.user_helpers import get_user_subscriptions
from user.utils.messages import REQUIRED_FIELDS_ABSENT
from user.utils.messages import UNPAID_USER
from user.utils.response import get_error_response


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def claim_rsa(request):
    """
    API View to post requests of users who wish to claim RSA

    ---
        parameters:
        - name: purchase_source
          description: source of the car purchase
          required: true
          type: string

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    source_of_car_purchase = request.data.get('purchase_source', None)

    if source_of_car_purchase is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)
    else:
        user = request.user
        is_user_paid = get_user_subscriptions(user.id, 'buyer')

        if is_user_paid:
            UserRsaClaim.objects.create(user=user, bought_from=source_of_car_purchase)
            email_data = [get_email_data(type_='rsa', send_to=['sold.car.group@truebil.com', 'demand.group@truebil.com'],
                                         subject='Bought car report from subscribed user', name=user.name,
                                         mobile=user.mobile, source=source_of_car_purchase,
                                         time=timezone.now().strftime("%Y-%m-%d %H:%M:%S"))]

            mailer_data = prepare_mailer_data(email_data=email_data)

            add_gearman_data('mailer', mailer_data)

            return Response({'status': True}, status=status.HTTP_201_CREATED)
        else:
            return Response(get_error_response(UNPAID_USER), status=status.HTTP_200_OK)
