"""
Helper functions for value add resources
"""

from connect.utils.helpers import import_connect_components
from user.crf.helpers import save_utm_parameters
from user.models import User, Buyer
from user.user_helpers import is_interested_seller
from user.utils.messages import INVALID_MOBILE
from user.utils.response import get_error_response

exec import_connect_components(sms=True, email=True, spreadsheet=True, gearman_helpers=True,
                              gearman_component=True, exceptions=True)


def sync_user_details(user, kwargs):
    user_mobile = user.mobile
    try:
        existing_user = User.objects.get(mobile=user_mobile)
        existing_user.name = kwargs['name']
        existing_user.city = kwargs['city_id']
        if 'email' in kwargs:
            existing_user.email = kwargs['email']
        existing_user.save()
    except User.DoesNotExist as e:
        print e

    try:
        existing_buyer = Buyer.objects.get(mobile=user_mobile)
        existing_buyer.name = kwargs['name']
        existing_buyer.city = kwargs['city_id']
        if 'email' in kwargs:
            existing_buyer.email = kwargs['email']
        existing_buyer.save()
    except Buyer.DoesNotExist as e:
        print e


def build_keyword_args(*args):
    kwarg = dict()
    for arg in args:
        kwarg[arg] = arg
    return kwarg


def get_service_sms_data(type_, mobile, **kwargs):
    return {'type': type_, 'mobile': mobile, 'kwargs': kwargs}


def get_service_spreadsheet_data(user, city_name, is_login, is_paid):
    return {
        'user': user,
        'worksheet_name': 'postSell',
        'spreadsheet_name': 'BuyerSync',
        'city': city_name,
        'is_login': is_login,
        'is_paid': is_paid
    }


def applicant_application(mobile_number, model_name, **kwargs):
    if mobile_number:
        applicant = model_name.objects.create(name=kwargs['name'], mobile=mobile_number[0],
                                              email=kwargs.get('email', None), city=kwargs['city'])
        utm_params = kwargs.get('utm', None)
        if utm_params:
            utm_obj = save_utm_parameters(utm_params)
            applicant.utm_parameter = utm_obj
            applicant.save()

        if applicant.__class__.__name__ == 'LoanApplicant':
            applicant.quoted_amount = kwargs['loan_amount']
            applicant.save()

        response = {
            'message': 'Service applied successfully!',
            'status': True
        }

        if applicant.__class__.__name__ == 'InterestedSeller':
            seller_exists = is_interested_seller(mobile_number[0])
            try:
                buyer_exists = True if Buyer.objects.get(mobile=mobile_number[0]) else False
            except Buyer.DoesNotExist:
                buyer_exists = False

            response['seller_exists'] = seller_exists
            response['buyer_exists'] = buyer_exists

        return response
    else:
        return get_error_response(INVALID_MOBILE)


def connect_to_gearman(sms=None, email=None, spreadsheet=None):
    sms_data, email_data, spreadsheet_data = None, None, None

    try:
        if sms:
            sms_data = [get_sms_data(type_=sms['type'], mobile=sms['mobile'], **sms['kwargs'])]
        if email:
            email_data = [get_email_data(type_=email['type'], send_to=email['send_to'],
                                        subject=email['subject'], **email['kwargs'])]
        if spreadsheet:
            spreadsheet_data = [get_spreadsheet_data(
                spreadsheet['user'], spreadsheet['worksheet_name'], spreadsheet['spreadsheet_name'],
                spreadsheet['city'], spreadsheet['is_login'], spreadsheet['is_paid']
            )]

        mailer_data = prepare_mailer_data(sms_data=sms_data, email_data=email_data,
                                          spreadsheet_data=spreadsheet_data)

        add_gearman_data('mailer', mailer_data)
    except ConnectValidationError:
        return False

    return True
