# Service specific URL keyword args

from user.models import LoanApplicant, InsuranceApplicant, PaperTransferApplicant, InterestedSeller
from truebil_management.utils.constants import SUPPORT_MOBILE_NO

CAR_LOAN_KWARGS = {
    'all_params': ['name', 'mobile', 'city_id', 'email', 'utm', 'loan_amount'],
    'required_params': ['name', 'mobile', 'city_id'],
    'extra_params': None,
    'sms_params': ['name'],
    'service_model': LoanApplicant,
    'service_type': 'car_loan',
    'send_sms': True,
    'send_spreadsheet': True,
    'send_email': False,
}

INSURANCE_KWARGS = {
    'all_params': ['name', 'mobile', 'city_id', 'email', 'utm'],
    'required_params': ['name', 'mobile', 'city_id'],
    'extra_params': None,
    'sms_params': ['name'],
    'service_model': InsuranceApplicant,
    'service_type': 'insurance',
    'send_sms': True,
    'send_spreadsheet': True,
    'send_email': False,
}

PAPER_TRANSFER_KWARGS = {
    'all_params': ['name', 'mobile', 'city_id', 'email', 'utm'],
    'required_params': ['name', 'mobile', 'city_id'],
    'extra_params': None,
    'sms_params': ['name'],
    'service_model': PaperTransferApplicant,
    'service_type': 'paper_transfer',
    'send_sms': True,
    'send_spreadsheet': True,
    'send_email': False,
}

INTERESTED_SELLER_KWARGS = {
    'all_params': ['name', 'mobile', 'city_id', 'email', 'utm'],
    'required_params': ['name', 'mobile', 'city_id'],
    'extra_params': {'support_mobile': SUPPORT_MOBILE_NO},
    'sms_params': ['name', 'support_mobile'],
    'service_model': InterestedSeller,
    'service_type': 'interested_seller',
    'send_sms': True,
    'send_spreadsheet': True,
    'send_email': False,
}
