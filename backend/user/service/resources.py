# -*- coding: utf-8 -*-
"""
Resources containing API views for service pages.
"""

from django.contrib.auth.models import AnonymousUser
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from main.quick_ops import valid_mobile_number
from user.models import City
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_CITY
from user.utils.helpers import is_user_paid
from user.utils.response import get_error_response
from user.exceptions import ParameterValidationError
from .helpers import (applicant_application, connect_to_gearman, get_service_sms_data,
                      get_service_spreadsheet_data)


class ServiceAPIView(APIView):
    def initialize(self, **kwargs):
        """
        Initializes the APIView with the specific service type information.
        """

        self.all_params = kwargs.get('all_params', [])
        self.required_params = kwargs.get('required_params', [])
        self.extra_params = kwargs.get('extra_params', {})
        self.sms_params = kwargs.get('sms_params', [])
        self.service_model = kwargs.get('service_model', '')
        self.service_type = kwargs.get('service_type', '')
        self.send_sms = kwargs.get('send_sms', False)
        self.send_spreadsheet = kwargs.get('send_spreadsheet', False)
        self.send_email = kwargs.get('send_email', False)


    def get_post_parameters(self, request):
        """
        Returns the post parameters from request form data.
        """

        post_data = request.data

        post_request_data = {}
        for parameter in self.all_params:
            post_request_data[parameter] = post_data.get(parameter, None)

        return post_request_data


    def validate_empty_parameters(self, post_data):
        """
        Validates the required post parameters are empty or not.
        Sets data if data is correct.
        Raises ParameterValidationError when empty required parameter.
        """

        for parameter in post_data:
            if parameter in self.required_params and post_data[parameter] is None:
                raise ParameterValidationError


    def is_login(self, user):
        """
        Checks if user is logged in.
        """

        is_login = False
        if type(user) is not AnonymousUser:
            is_login = True

        return is_login


    def is_paid(self, user):
        """
        Checks if logged in user is paid.
        """

        is_paid = is_user_paid(user)
        return is_paid


    def save_application(self, data):
        """
        Saves the application in database.
        """

        db_model = self.service_model
        mobile = data['mobile']

        return applicant_application(valid_mobile_number(mobile), db_model, **data)


    def build_mailer_data(self, data, response, _user):
        """
        Builds mailer data
        """

        mobile = data['mobile']
        city_name = data['city'].name
        email = data.get('email', None)
        user = response.copy()
        is_login = self.is_login(_user)
        is_paid = is_login and self.is_paid(_user)
        service_type = self.service_type
        extra_params = self.extra_params

        mailer_data = {}
        if self.send_sms:
            sms_kwargs = {}
            for parameter in self.sms_params:
                sms_kwargs[parameter] = data.get(parameter, None) or \
                                        extra_params.get(parameter, None)

            mailer_data['sms'] = get_service_sms_data(service_type, mobile, **sms_kwargs)

        if self.send_spreadsheet:
            mailer_data['spreadsheet'] = get_service_spreadsheet_data(user, city_name, is_login, is_paid)

        if self.send_email and email is not None:
            # Note: None of the services built is expected to send emails.
            #       This block is written for future purposes.
            #       Value `None` is to be replaced with relevant content.
            mailer_data['email'] = None

        return mailer_data


    def send_mailer_data(self, response, mailer_data):
        """
        Sends processed mailer data to gearman.
        Sets the response status after sending
        """

        status = False
        if mailer_data:
            status = connect_to_gearman(**mailer_data)

        return status


    def post(self, request, **kwargs):
        """
        Service API View

        ---

        parameters:
            - name: name
              description: Name of the applicant
              required: true
              type: string
            - name: email
              description: Email of the applicant
              required: false
              type: string
            - name: mobile
              description: Mobile number of the applicant
              required: true
              type: integer
            - name: city_id
              description: City ID of the applicant
              required: true
              type: integer
            - name: utm
              description: UTM parameters
              required: false
              type: objects

        responseMessages:
            - code: 400
              message: Required field not entered.
              message: City does not exist.

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """

        self.initialize(**kwargs)
        post_data = self.get_post_parameters(request)

        try:
            self.validate_empty_parameters(post_data)
        except ParameterValidationError:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

        try:
            post_data['city'] = City.objects.get(id=post_data['city_id'])
        except City.DoesNotExist:
            return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)
        else:
            response = self.save_application(post_data)
            if response['status']:
                response_status = status.HTTP_201_CREATED
                mailer_data = self.build_mailer_data(post_data, response, request.user)
                response['status'] = self.send_mailer_data(response, mailer_data)
            else:
                response_status = status.HTTP_200_OK

            return Response(response, status=response_status)
