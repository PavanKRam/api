from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from user.models import City, User, Seller
from truebil_management.models import UtmParameter
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_CITY, USER_NOT_LOGGED_IN, INVALID_USER_TYPE, \
    INVALID_MOBILE
from user.utils.response import get_error_response
from django.contrib.auth.models import AnonymousUser
from user.user_helpers import get_user_subscriptions
from main.quick_ops import valid_mobile_number


@api_view(['POST'])
def edit_profile(request):
    """
    API View that records the feedback of users

    ---

    parameters:
        - name: name
          description: Name of the applicant
          required: true
          type: string
        - name: email
          description: Email of the applicant
          required: true
          type: string
        - name: mobile
          description: Mobile number of the applicant
          required: true
          type: integer
        - name: city_id
          description: City ID of the applicant
          required: true
          type: integer

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    name = request.data.get('name', None)
    mobile = request.data.get('mobile', None)
    email = request.data.get('email', None)
    city_id = request.data.get('city_id', None)

    if name is None or mobile is None or email is None or city_id is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)
    else:
        if type(request.user) is not AnonymousUser:
            mobile = request.user.mobile

            try:
                city = City.objects.get(id=city_id)
            except City.DoesNotExist:
                return Response(get_error_response(INVALID_CITY), status=status.HTTP_400_BAD_REQUEST)

            user = User.objects.filter(mobile=mobile).order_by('-created_at').first()

            user.name = name
            user.city = city
            user.email = email
            user.save()

            user_info = {
                'name': name,
                'email': email,
                'mobile': str(mobile),
                'city_id': city_id
            }
  
            response_data = {
                'message': 'User updated successfully!',
                'user_info': user_info,
                'status': True
            }
            return Response(response_data)
        else:
            return Response(get_error_response(USER_NOT_LOGGED_IN), status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def truebil_premium_user(request):
    """
    API View that checks if a user is premium or not and if not, registers them
    as user or seller.

    ---

    parameters:
        - name: user_type
          description: Whether user is buyer or seller
          required: true
          type: string
        - name: email
          description: Email of the applicant
          required: true
          type: string
        - name: mobile
          description: Mobile number of the applicant
          required: true
          type: integer
        - name: city_id
          description: City of the applicant
          required: false
          type: integer
        - name: utm
          description: UTM parameters
          required: false
          type: objects

    responseMessages:
        - code: 400
          message: Required field not entered.
          message: City does not exist.

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    user_type = request.data.get('user_type', None)
    mobile = request.data.get('mobile', None)
    email = request.data.get('email', None)
    city_id = request.data.get('city_id', None)
    utm = request.data.get('utm', None)

    if user_type is None or mobile is None or city_id is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)
    else:
        if not valid_mobile_number(mobile):
            return Response(get_error_response(INVALID_MOBILE), status=status.HTTP_400_BAD_REQUEST)

        user = User.objects.filter(mobile=mobile).first()

        if user_type == 'buyer':
            if email is None:
                return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

            is_buyer_premium = False
            if user:
                is_buyer_premium = get_user_subscriptions(user.id, 'buyer')
            else:
                if utm is not None:
                    utm_obj = UtmParameter.objects.create(**utm)
                else:
                    utm_obj = None

                User.objects.create(mobile=mobile, email=email, city_id=city_id, utm_parameter=utm_obj)

            return Response({'status': True, 'is_buyer_premium': is_buyer_premium}, status=status.HTTP_200_OK)

        elif user_type == 'seller':
            seller = Seller.objects.filter(mobile=mobile, parent_id=None, type_id="Individual").first()
            response_data = {
                'is_seller': True if seller else False,
                'email': seller.email if seller else None,
                'is_seller_premium': get_user_subscriptions(user.id, 'seller') if user else False,
                'status': True
            }

            return Response(response_data, status=status.HTTP_200_OK)
        else:
            return Response(get_error_response(INVALID_USER_TYPE), status=status.HTTP_400_BAD_REQUEST)
