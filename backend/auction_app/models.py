# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from user.models import BaseModel


class Wallet(BaseModel):
    amount = models.IntegerField(null=False, blank=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'wallets'
        verbose_name_plural = 'wallets'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.amount)


class Dealer(BaseModel):
    user = models.OneToOneField(to='user.User', unique=True)
    wallet = models.OneToOneField(to='Wallet', unique=True)
    is_verified_from_admin = models.BooleanField(default=False, null=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'dealers'
        verbose_name_plural = 'dealers'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.user.name)


class DealerListingBid(BaseModel):
    listing_auction = models.ForeignKey(to='ListingAuction')
    dealer = models.ForeignKey(to='Dealer')
    amount = models.IntegerField(null=False, blank=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'dealer_listing_bids'
        verbose_name_plural = 'dealer_listing_bids'
        unique_together = ('listing_auction', 'amount',)

    def __unicode__(self):
        return '%s - %s' % (self.id, self.listing_auction)


class ListingAuction(BaseModel):
    listing = models.ForeignKey(to='main.Listing')
    start_time = models.DateTimeField(null=False, blank=False)
    end_time = models.DateTimeField(null=False, blank=False)
    min_bidding_amount = models.IntegerField(null=False, blank=False)
    suggested_price = models.IntegerField(null=False, blank=False)
    auction_status = models.ForeignKey(to='AuctionStatus', null=False, blank=False, default=1)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'listing_auctions'
        verbose_name_plural = 'listing_auctions'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.listing_id)


class WalletTransaction(BaseModel):
    wallet = models.ForeignKey(to='Wallet')
    listing_auction = models.ForeignKey(to='ListingAuction')
    amount = models.IntegerField(null=False, blank=False)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'wallet_transactions'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.wallet_id)


class AuctionStatus(models.Model):
    name = models.TextField(blank=False, null=False)

    class Meta:
        db_table = 'auction_statuses'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.name)


class AuctionActivity(BaseModel):
    listing_auction = models.ForeignKey(to='ListingAuction')
    auction_status = models.ForeignKey(to='AuctionStatus')
    dealer_id = models.IntegerField(null=True, blank=True)
    transaction_amount = models.IntegerField(null=True, blank=True)
    changed_at = models.DateTimeField(null=False, blank=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'auction_activities'

    def __unicode__(self):
        return '%s - %s' % (self.id, self.listing_auction)


class AuctionWinner(BaseModel):
    listing_auction = models.ForeignKey(to='ListingAuction')
    dealer = models.ForeignKey(to='Dealer')
    auction_status = models.ForeignKey(to='AuctionStatus')
    transaction_amount = models.IntegerField(null=False, blank=False)
    created_at = models.DateTimeField()

    class Meta:
        db_table = 'auction_winners'
        unique_together = ('listing_auction', 'dealer',)

    def __unicode__(self):
        return '%s - %s - %s' % (self.id, self.listing_auction, self.dealer)
