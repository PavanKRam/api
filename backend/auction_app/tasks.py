from __future__ import absolute_import, unicode_literals

from django.db.models import F
from django.utils import timezone
from fcm_django.models import FCMDevice
from django.core.cache import cache

from main.utils.constants import SECURITY_DEPOSIT_DEDUCTION_AMOUNT
from server.celery_app import app
from .auction_helpers import get_auction_status_id
from .models import ListingAuction, DealerListingBid, Wallet, WalletTransaction, Dealer, AuctionActivity, AuctionWinner
from .utils.messages import SECURITY_DEPOSIT, AUCTION_NOT_FOUND


@app.task
def clean_auction(auction_id):
    try:
        auction = ListingAuction.objects.get(pk=auction_id, auction_status_id=1)
    except ListingAuction.DoesNotExist:
        return AUCTION_NOT_FOUND

    status, dealer = end_auction(auction)
    if status and dealer:
        status = deduct_security_deposit(auction.id, dealer)
        send_notification(dealer, auction)
    cache.delete('clean_auction_' + str(auction_id))
    return status


def end_auction(auction):
    dealer = None
    auction_data = {}
    highest_bid_rec = DealerListingBid.objects.filter(listing_auction_id=auction.id).order_by('-amount').first()
    if highest_bid_rec:
        status_id = auction.auction_status_id = get_auction_status_id('negotiation')
        dealer = Dealer.objects.values_list('id').get(pk=highest_bid_rec.dealer_id)
        auction_data = {
                    'auction_status_id': status_id,
                    'dealer_id': dealer['id'],
                    'transaction_amount': highest_bid_rec.amount
                }
    else:
        auction_data['auction_status_id'] = auction.auction_status_id = get_auction_status_id('closed')

    auction_data['listing_auction_id'] = auction.id
    auction_data['changed_at'] = timezone.now().strftime("%Y-%m-%d %H:%M:%S")
    auction.save()
    AuctionActivity.objects.create(**auction_data)

    if dealer:
        AuctionWinner.objects.create(auction_status_id=auction.auction_status_id, listing_auction_id=auction.id,
                                     dealer_id=dealer['id'], transaction_amount=highest_bid_rec.amount)

    return True, dealer


def deduct_security_deposit(auction_id, dealer):
    wallet_id = dealer.wallet_id
    Wallet.objects.filter(pk=wallet_id).update(amount=F('amount') - SECURITY_DEPOSIT_DEDUCTION_AMOUNT)
    reason_for_transaction = SECURITY_DEPOSIT
    WalletTransaction.objects.create(wallet_id=wallet_id, amount=(-1 * SECURITY_DEPOSIT_DEDUCTION_AMOUNT),
                                     description=reason_for_transaction, listing_auction_id=auction_id)

    return True


def send_notification(dealer, auction):
    device = FCMDevice.objects.filter(user_id=dealer.user_id)
    device.send_data_message(data_message={"type": "Won", 'auction_id': auction.id, 'listing_id': auction.listing.id, 'message': "Hi, You have won bid on {0}".format(auction.listing.variant.name)})


def add_celery_job(auction_id, auction_end_time):
    task_id = str(auction_id) + '-' + str(auction_end_time.time())
    cache.set('clean_auction_' + str(auction_id), task_id)
    return clean_auction.apply_async(args=(auction_id,), eta=auction_end_time, task_id=task_id)
