# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, timedelta

import django_filters
import pytz
from celery.task.control import revoke
from django.core.cache import cache
from django.db import IntegrityError
from django.db.models import Max, Q
from django.http import HttpResponseForbidden
from django_filters import rest_framework as filters
from rest_framework import mixins, status, pagination, viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.views import APIView

from authn.permissions import IsVerifiedFromAdmin, IsAuthenticated
from main.data_science.functions import (get_listing_potential_dealers, listing_price_prediction,
                                         get_listing_procurement_score, get_listing_start_bidding_price,
                                         get_listing_top_offers, get_listing_seller_price)
from main.models import Attachment, Listing
from main.quick_ops import get_cdn_url
from main.utils.constants import CAPTION_IDS, MINIMUM_BALANCE_FOR_BIDDING
from main.utils.serializer_helpers import get_invalid_numbers
from truebil_management.utils.constants import TRUEBIL_BANK_DETAIL
from user.crf.helpers import get_post_parameters, validate_empty_parameters
from user.exceptions import ParameterValidationError
from user.models import City
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_LISTING
from user.utils.response import get_error_response
from .auction_helpers import (get_sales_person_detail, get_auction_status_id, get_server_time, get_firebase_connection,
                              post_failed_bid_log)
from .models import Dealer, Wallet, WalletTransaction, DealerListingBid, ListingAuction, AuctionStatus, AuctionWinner
from .serializer_helper import get_auction_filters_serializer_data, get_wallet_amount
from .serializers import (DealerSerializer, WalletTransactionSerializer, ListingAuctionSerializer,
                          DedicatedAuctionSerializer, AuctionFiltersSerializer)
from .tasks import clean_auction, add_celery_job
from .utils.constants import WINNING_STATUS, LOSING_STATUS, HISTORY_STATUS, ACTIVE_STATUS, AUCTION_DELAY_TIME
from .utils.messages import (AMOUNT_LESS_THAN_MIN_BID_AMOUNT, BID_PLACED, AMOUNT_SHOULD_BE_GREATER_THAN_PREVIOUS_BID,
                             LOW_BALANCE, BIDDING_NOT_ALLOWED, INVALID_AUCTION, FIREBASE_CONNECTION_ERROR,
                             FIREBASE_NOT_UPDATED, BID_FOR_THIS_AMOUNT_ALREADY_EXISTS)


class AuctionPagination(pagination.CursorPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100
    ordering = 'end_time'


class TraversalFilter(django_filters.Filter):
    """Custom filter class for implementing '__' lookup type"""

    def filter(self, qs, value):
        if value not in (None, ''):
            integers = [v for v in value.split(',')]
            return qs.filter(**{'%s__%s' % (self.name, self.lookup_expr): integers})
        return qs


class ListingAuctionFilter(filters.FilterSet):
    """Custom filter class for filtering listings"""
    city_id = TraversalFilter(name='listing__locality_id__city_id', lookup_expr='in')
    state_code = TraversalFilter(name='listing__rto_id__state_code', lookup_expr='in')
    auction_status_id = TraversalFilter(name='auction_status_id', lookup_expr='in')

    class Meta:
        model = ListingAuction
        fields = ('city_id', 'state_code', 'auction_status_id')


class ListingAuctionParentViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = ListingAuction.objects.all()
    serializer_class = ListingAuctionSerializer

    def get_serializer_context(self):
        attachments_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
        dealer = Dealer.objects.get(user=self.request.user)
        return {
            'request': self.request,
            'dealer': dealer,
            'buyer': None,
            'cdn_url': get_cdn_url(),
            'attachment_qs': attachments_qs,
            'invalid_numbers': get_invalid_numbers()
        }


class ListingAuctionViewSet(ListingAuctionParentViewSet, mixins.RetrieveModelMixin):
    """"""
    pagination_class = AuctionPagination
    filter_backends = (filters.DjangoFilterBackend,)
    permission_classes = [IsVerifiedFromAdmin]
    filter_class = ListingAuctionFilter

    def get_serializer_class(self):
        if self.action == 'list':
            return ListingAuctionSerializer
        elif self.action == 'retrieve':
            return DedicatedAuctionSerializer
        else:
            return ListingAuctionSerializer

    def get_queryset(self):
        """
        Queryset function to handle availability of row based
        on request method, request user and their respective permissions.
        """
        if self.action == 'list':
            listing_auctions_qs = self.queryset
            auction_status_ids = self.request.query_params.get('auction_status_id', None)
            post_won_filters = ('negotiation', 'dealer_cancelled', 'seller_cancelled', 'payment_waiting',
                                'payment_done', 'waiting_for_procurement')

            if auction_status_ids:
                try:
                    auction_status_ids_list = [ids for ids in auction_status_ids.split(',')]

                    status_names = set(AuctionStatus.objects.values_list('name', flat=True)\
                                       .filter(id__in=auction_status_ids_list))

                    if status_names.issubset(post_won_filters):
                        cars_won_by_dealer = AuctionWinner.objects.values('listing_auction_id')\
                            .filter(dealer__user_id=self.request.user.id)
                        listing_auctions_qs = listing_auctions_qs\
                            .filter(id__in=[a['listing_auction_id'] for a in cars_won_by_dealer])
                except AuctionStatus.DoesNotExist:
                    pass

            return listing_auctions_qs

        elif self.action == 'retrieve':
            return self.queryset
        else:
            return HttpResponseForbidden()

    def list(self, request, *args, **kwargs):
        response = super(ListingAuctionViewSet, self).list(request, args, kwargs)
        response.data['server_time'] = get_server_time()
        return response

    def retrieve(self, request, *args, **kwargs):
        response = super(ListingAuctionViewSet, self).retrieve(request, args, kwargs)
        response.data['server_time'] = get_server_time()
        return response


class AuctionFiltersAPIView(APIView):
    def get(self, request, format=None):
        serializer_class = AuctionFiltersSerializer
        filters_data = get_auction_filters_serializer_data(request, serializer_class)
        response_data = {'success': True, 'filters': filters_data}
        return Response(response_data)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def dealer_status(request):
    """
    Dealer status api

    ---

    responseMessages:
        - code: 401
          message: Not authenticated
        - code: 200
          message: Dealer found

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml
    """

    user = request.user
    dealer = Dealer.objects.get(user_id=user.id)

    dealer_info = (DealerSerializer(dealer)).data
    del dealer_info['user_id']
    user_info = {
        'name': user.name,
        'mobile': user.mobile,
        'email': user.email,
        'city_id': user.city_id,
    }

    dealer_info.update(user_info)
    print(user_info['city_id'])
    if user_info['city_id'] is not None:
        city_name = City.objects.get(id=user.city_id, )
        dealer_info['city_name'] = city_name.name

    sales_rep_info = get_sales_person_detail()
    dealer_info['sales_rep_info'] = sales_rep_info

    dealer_info['truebil_bank_detail'] = TRUEBIL_BANK_DETAIL

    response = dict()
    response['status'] = True
    response['message'] = 'Dealer details'
    response['details'] = dealer_info
    return Response(response, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes([IsVerifiedFromAdmin])
def dealer_bid(request):
    """
    API View to store dealer bid

    ---

    parameters:
        - name: listing_auction_id
          description: auction id of listing
          required: true
          type: integer
        - name: dealer_id
          description: id of dealer
          required: true
          type: integer
        - name: amount
          description: bidding amount
          required: true
          type: integer
        - name: end_time
          description: end time of auction
          required: true
          type: datetime

    responseMessages:
        - code: 400
          message: Required field not entered.
        - code: 200
          message: Bid successfully placed

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml

    """

    parameters_info = {
        'all_params': ['listing_auction_id', 'amount'],
        'required_params': ['listing_auction_id', 'amount']
    }

    post_data = get_post_parameters(request, parameters_info['all_params'])
    try:
        validate_empty_parameters(post_data, parameters_info['required_params'])
    except ParameterValidationError:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    auction_id = post_data['listing_auction_id']

    all_auction_bid = DealerListingBid.objects.filter(listing_auction_id=auction_id)
    max_bid = all_auction_bid.aggregate(Max('amount'))['amount__max']

    listing_auction = ListingAuction.objects.get(pk=auction_id)

    min_bidding_amount = listing_auction.min_bidding_amount
    auction_end_time = listing_auction.end_time
    auction_status = listing_auction.auction_status.name
    current_timestamp = datetime.now().replace(tzinfo=pytz.utc)
    bid_amount = int(post_data['amount'])
    response = {}

    if auction_status != 'active' or auction_end_time < current_timestamp:
        return Response(get_error_response(BIDDING_NOT_ALLOWED), status=status.HTTP_400_BAD_REQUEST)

    if bid_amount <= min_bidding_amount:
        return Response(get_error_response(AMOUNT_LESS_THAN_MIN_BID_AMOUNT), status=status.HTTP_400_BAD_REQUEST)

    if bid_amount <= max_bid:
        response = get_error_response(AMOUNT_SHOULD_BE_GREATER_THAN_PREVIOUS_BID)
        response.update({'code': 100})
        return Response(response, status=status.HTTP_400_BAD_REQUEST)

    post_data['dealer_id'], wallet_id = Dealer.objects.values_list('id', 'wallet__id').get(user_id=request.user.id)

    dealer_wallet_balance = Wallet.objects.values_list('amount', flat=True).get(pk=wallet_id)

    if dealer_wallet_balance >= MINIMUM_BALANCE_FOR_BIDDING:
        try:
            latest_dealer_bid = DealerListingBid.objects.create(**post_data)
        except IntegrityError:
            response = get_error_response(BID_FOR_THIS_AMOUNT_ALREADY_EXISTS)
            response.update({'code': 100})
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
    else:
        err_response = get_error_response(LOW_BALANCE)
        err_response['minimum_balance_req'] = MINIMUM_BALANCE_FOR_BIDDING
        return Response(err_response, status=status.HTTP_400_BAD_REQUEST)

    new_timestamp = (current_timestamp + timedelta(**AUCTION_DELAY_TIME)).replace(microsecond=0)
    if current_timestamp < auction_end_time <= new_timestamp:
        old_task_id = cache.get('clean_auction_' + str(auction_id))
        if not old_task_id:
            old_task_id = str(auction_id) + '-' + str(auction_end_time.time())
        listing_auction.end_time = new_timestamp
        listing_auction.save()
        # revoke celery job
        revoke(old_task_id, terminate=True)
        # create new celery job
        add_celery_job(auction_id, new_timestamp)
        response['auction_end_time'] = new_timestamp.strftime('%Y-%m-%d %H:%M:%S')

    try:
        firebase_db = get_firebase_connection()

        if firebase_db is None:
            err_response = get_error_response(FIREBASE_CONNECTION_ERROR)
            return Response(err_response, status=status.HTTP_424_FAILED_DEPENDENCY)

        firebase_db.child('BidInfo/' + str(auction_id)).update({
            'user_id': post_data['dealer_id'],
            'highest_bid': bid_amount,
            'bid_end_time': listing_auction.end_time.strftime('%Y-%m-%d %H:%M:%S'),
            post_data['dealer_id']: bid_amount
        })

        response['status'] = True
        response['message'] = BID_PLACED
        response['server_time'] = get_server_time()
        response_status = status.HTTP_200_OK
        return Response(response, status=response_status)
    except:
        latest_dealer_bid.delete()
        post_failed_bid_log(post_data)
        err_response = get_error_response(FIREBASE_NOT_UPDATED)
        return Response(err_response, status=status.HTTP_424_FAILED_DEPENDENCY)


@api_view(['GET'])
@permission_classes([IsVerifiedFromAdmin])
def my_account(request):
    user = request.user
    wallet_id = Dealer.objects.values_list('wallet_id', flat=True).get(user_id=user.id)
    response = {}

    transaction_qs = WalletTransaction.objects.filter(wallet_id=wallet_id).order_by('-created_at')
    serializer = WalletTransactionSerializer(transaction_qs, many=True)

    response['current_balance'] = get_wallet_amount(wallet_id)
    response['transactions'] = {'all': serializer.data}
    return Response({'status': True, 'message': 'Account Details', 'details': response}, status=status.HTTP_200_OK)


@api_view(['POST'])
def post_new_auction(request):
    """
        API View to do work post new auction add
        - add job in celery
        - Send new auction notification to matching criteria dealer

        ---
        parameters:
            - name: auction_ids
              description: string of auction ids separated with comma
              required: true
              type: string
        responseMessages:
            - code: 400
              message: Required field not entered.
            - code: 200
              message: Added celery job successfully.
              job_ids: Enqueued jobs array with keys as auction_id and value as job_id.
        """

    data = request.data.get('auction_ids', None)
    if data is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    auction_ids = [int(v) for v in data.split(',')]
    auctions = ListingAuction.objects.values('end_time', 'id').filter(pk__in=auction_ids)

    if auctions.count() != len(auction_ids):
        return Response(get_error_response(INVALID_AUCTION), status=status.HTTP_400_BAD_REQUEST)

    job_ids = {}
    for auction in auctions:
        job_id = add_celery_job(auction['id'], auction['end_time'])
        job_ids[auction['id']] = str(job_id)

    response_data = {'status': True, 'message': 'Added celery job successfully.', 'job_ids': job_ids}
    return Response(response_data, status=status.HTTP_200_OK)


class MyBidsAPIView(APIView):
    permission_classes = [IsVerifiedFromAdmin]

    def get(self, request, format=None):
        pagination_class = AuctionPagination
        paginator = pagination_class()

        bid_status = request.query_params.get('status', HISTORY_STATUS)
        dealer = Dealer.objects.get(user_id=request.user.id)
        active_status_id = get_auction_status_id(ACTIVE_STATUS)
        if bid_status in [WINNING_STATUS, LOSING_STATUS]:
            dealer_involved_cars_qs = DealerListingBid.objects.values('listing_auction_id').annotate(id=Max('id')) \
                .filter(dealer_id=dealer.id, listing_auction__auction_status_id=active_status_id)

            auction_winners = DealerListingBid.objects.values('listing_auction_id').annotate(id=Max('id')) \
                .filter(listing_auction__auction_status_id=active_status_id)
            auction_winner_dealer_ids = [a['id'] for a in auction_winners]
            if bid_status == WINNING_STATUS:
                dealer_involved_cars_qs = dealer_involved_cars_qs.filter(
                    id__in=auction_winner_dealer_ids).values_list('listing_auction_id', flat=True)
            elif bid_status == LOSING_STATUS:
                dealer_involved_cars_qs = dealer_involved_cars_qs.filter(
                    ~Q(id__in=auction_winner_dealer_ids)).values_list('listing_auction_id', flat=True)
        else:
            dealer_involved_cars_qs = DealerListingBid.objects.values_list('listing_auction_id', flat=True).filter(
                created_at__gte=datetime.now() - timedelta(days=7)) \
                .filter(dealer_id=dealer.id).filter(~Q(listing_auction__auction_status_id=active_status_id)).distinct()

        listing_auction_qs = ListingAuction.objects.filter(id__in=dealer_involved_cars_qs)
        listing_auction_paginated_qs = paginator.paginate_queryset(listing_auction_qs, self.request)
        attachments_qs = Attachment.objects.filter(caption_id__in=CAPTION_IDS)
        cdn_url = get_cdn_url()
        listing_auction_serializer = ListingAuctionSerializer(listing_auction_paginated_qs,
                                                              context={'request': self.request,
                                                                       'dealer': dealer,
                                                                       'buyer': None,
                                                                       'cdn_url': cdn_url,
                                                                       'attachment_qs': attachments_qs,
                                                                       'invalid_numbers': get_invalid_numbers()},
                                                              many=True)

        paginated_data = paginator.get_paginated_response(listing_auction_serializer.data)
        paginated_data.data['server_time'] = get_server_time()
        return paginated_data


class PotentialDealerAPIView(APIView):
    def get(self, request, format=None):
        """
        An API View to potential dealer list

        ---

        parameters:
            - name: listing_id
              description: ID of the listing
              required: true
              type: integer

        responseMessages:
            - code: 400
              status: false
              message: Required field not entered.
            - code: 200
              status: true
              message: dict containing potential dealers, procurement score, top offers, price engine price,
              start bidding price, seller show price

        consumes:
            - application/json
            - application/xml
        produces:
            - application/json
            - application/xml
        """
        listing_id = request.query_params.get('listing_id', None)

        if not listing_id:
            return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status.HTTP_400_BAD_REQUEST)

        if not Listing.objects.filter(pk=listing_id).exists():
            return Response(get_error_response(INVALID_LISTING), status.HTTP_400_BAD_REQUEST)

        top_potential_dealers = get_listing_potential_dealers(listing_id)
        price_engine_price = listing_price_prediction(listing_id)
        procurement_score_data = get_listing_procurement_score(listing_id)
        seller_show_price = get_listing_seller_price(listing_id)
        start_bidding_price = get_listing_start_bidding_price(listing_id)
        top_offers = get_listing_top_offers(listing_id)

        response = {
            'potential_dealers': top_potential_dealers,
            'procurement_score': procurement_score_data[1],
            'top_offers': top_offers,
            'price_engine_price': price_engine_price,
            'start_bidding_price': start_bidding_price,
            'seller_show_price': seller_show_price
        }

        return Response(response)


@api_view(['PUT'])
def end_auction(request):
    """
    API View to end auction
    ---
    parameters:
        - name: auction_id
          description: auction id of listing
          required: true
          type: integer

    responseMessages:
        - code: 400
          message: Required field not entered.
        - code: 200
          message: Auction ended.
    """
    parameters_info = {
        'all_params': ['auction_id'],
        'required_params': ['auction_id']
    }
    post_data = get_post_parameters(request, parameters_info['all_params'])

    try:
        validate_empty_parameters(post_data, parameters_info['required_params'])
    except ParameterValidationError:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    clean_auction.delay(post_data['auction_id'])
    return Response({'status': True, 'message': 'Auction ended.'}, status=status.HTTP_200_OK)
