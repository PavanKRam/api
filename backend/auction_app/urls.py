from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from authn.main_auth import generate_otp, verify_otp
from main.views import AlbumImagesAPIVIew
from .views import dealer_bid, dealer_status, my_account, post_new_auction, ListingAuctionViewSet, \
    AuctionFiltersAPIView, MyBidsAPIView, end_auction, PotentialDealerAPIView

router = DefaultRouter()
router.register(r'^auction_listings', ListingAuctionViewSet)

urlpatterns = [
                  url(r'my_account/$', my_account),
                  url(r'filters/$', AuctionFiltersAPIView.as_view()),
                  url(r'my_bids/$', MyBidsAPIView.as_view()),
                  url(r'generate_otp/$', generate_otp),
                  url(r'verify_otp/$', verify_otp),
                  url(r'dealer_bid/$', dealer_bid),
                  url(r'dealer_status/$', dealer_status),
                  url(r'potential_dealer/$', PotentialDealerAPIView.as_view()),
                  url(r'^post_new_auction/$', post_new_auction),
                  url(r'^end_auction/$', end_auction),
                  url(r'^album_images/$', AlbumImagesAPIVIew.as_view()),
              ] + router.urls
