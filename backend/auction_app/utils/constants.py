ACTIVE_STATUS = 'active'
NEGOTIATION_STATUS = 'negotiation'
PAYMENT_WAITING_STATUS = 'payment_waiting'
PAYMENT_DONE_STATUS = 'payment_done'
SELLER_CANCELLED_STATUS = 'seller_cancelled'
DEALER_CANCELLED_STATUS = 'dealer_cancelled'
WINNING_STATUS = 'winning'
LOSING_STATUS = 'losing'
WON_STATUS = 'won'
LOST_STATUS = 'lost'
HISTORY_STATUS = 'history'
AUCTION_DELAY_TIME = {
    'seconds': 5,
    'minutes': 2
}
