# -*- coding: utf-8 -*-
import logging
import re

import firebase_admin
from django.utils import timezone
from fcm_django.models import FCMDevice
from firebase_admin import db

from authn.utils.helpers import generate_jwt
from server.settings import FIREBASE_CONNECTION_URL, FIREBASE_SERVICE_ACCOUNT_CREDENTIALS_JSON_PATH
from truebil_management.utils.constants import AUCTION_APP_SALES_PERSON_MOBILE, AUCTION_APP_SALES_PERSON_NAME
from user.models import User
from .models import Dealer, AuctionStatus


def is_auction_app(request_url):
    if re.search('/auction_app/', request_url):
        return True
    return False


def post_verification_dealer(**kwargs):
    user = User.objects.filter(mobile=kwargs.get('mobile')).order_by('-created_at').first()
    if user:
        try:
            dealer = Dealer.objects.get(user_id=user.id)
        except Dealer.DoesNotExist:
            dealer = Dealer.objects.create(user_id=user.id)
    else:
        user = User.objects.create(name=kwargs.get('name'), mobile=kwargs.get('mobile'), city=kwargs.get('city'))
        dealer = Dealer.objects.create(user_id=user.id)

    token = generate_jwt(user)
    dealer_info = {
        'dealer_id': dealer.id,
        'name': user.name,
        'mobile': str(user.mobile),
        'is_verified_from_admin': dealer.is_verified_from_admin
    }

    response_data = {
        'message': 'Dealer verified successfully!',
        'token': token,
        'dealer_info': dealer_info,
        'sales_person': get_sales_person_detail(),
        'status': True
    }

    return response_data


def get_sales_person_detail():
    return {
        'Name': AUCTION_APP_SALES_PERSON_NAME,
        'Mobile': AUCTION_APP_SALES_PERSON_MOBILE
    }


def send_new_auction_notification(auction):
    device = FCMDevice.objects.all()
    response_data = {
        'type': 'New auction',
        'auction_id': auction.id,
        'listing_id': auction.listing.id,
        'state_code': auction.listing.rto.state_code,
        'city_id': auction.listing.locality.city_id,
        'heading': '{0} added'.format(auction.listing.variant.model.name),
        'message': '{0} has been added for auction'.format(auction.listing.variant.model.name)
    }
    return device.send_data_message(data_message=response_data)


def get_auction_status_id(name):
    return AuctionStatus.objects.values_list('id', flat=True).get(name=name)


def get_firebase_connection():
    try:
        firebase_auction_app = firebase_admin.get_app(name='truebil-auction-db')
        firebase_db = db.reference('/', firebase_auction_app)
    except ValueError:
        cred = firebase_admin.credentials.Certificate(FIREBASE_SERVICE_ACCOUNT_CREDENTIALS_JSON_PATH)
        firebase_auction_app = firebase_admin.initialize_app(cred, {
            'databaseURL': FIREBASE_CONNECTION_URL
        }, name='truebil-auction-db')
        firebase_db = db.reference('/', firebase_auction_app)

    return firebase_db


def get_server_time():
    return timezone.now().replace(microsecond=0).isoformat(' ')


def post_failed_bid_log(data):
    logging_data = 'Firebase connection timeout: Auction ID {} Dealer ID {} Amount {} '.format(
        data['listing_auction_id'],
        data['dealer_id'],
        data['amount']
    )
    logger = logging.getLogger(__name__)
    logger.warning(logging_data)
