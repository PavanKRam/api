from main.utils.constants import SECURITY_DEPOSIT_DEDUCTION_AMOUNT
from main.utils.filters_conf import RTO_STATE_CODE
from user.models import City
from .models import Wallet, DealerListingBid, AuctionWinner
from .utils.constants import ACTIVE_STATUS, WINNING_STATUS, WON_STATUS, LOSING_STATUS, LOST_STATUS


def get_auction_filters_serializer_data(request, serializer_class):
    filter_state_code = request.query_params.get('state_code', '').split(',')
    filter_city = request.query_params.get('city_id', '').split(',')

    context = {
        'filter_state_code': filter_state_code,
        'filter_city': filter_city,
    }

    serializer = serializer_class(context=context)
    return serializer.get_data()


def get_state_codes(filter_state_code):
    for state in RTO_STATE_CODE:
        is_checked = False
        if state['id'] in filter_state_code:
            is_checked = True

        state['is_checked'] = is_checked

    return RTO_STATE_CODE


def get_city(filter_city):
    cities = list(City.objects.filter(is_active=True))
    cities_array = []
    for city in cities:
        is_checked = False
        if str(city.id) in filter_city:
            is_checked = True
        cities_array.append({'id': city.id, 'name': city.name, 'is_checked': is_checked})
    return cities_array


def get_wallet_amount(wallet_id):
    wallet_amount = Wallet.objects.values_list('amount', flat=True).get(pk=wallet_id)
    return {'label': 'Balance amount', 'value': wallet_amount}


def get_auction_details_data(dealer, listing_auction):
    auction_status = listing_auction.auction_status.name
    dealer_auction_status = ''
    dealer_bid_amount = None
    highest_bid_amount = None
    min_bid_amount = listing_auction.min_bidding_amount

    highest_bid_qs = DealerListingBid.objects.filter(listing_auction_id=listing_auction.id).order_by('-amount')
    highest_auction_bid = highest_bid_qs.first()

    dealer_highest_bid_qs = highest_bid_qs.filter(dealer_id=dealer.id).first()

    if highest_bid_qs:
        highest_bid_amount = highest_auction_bid.amount

    if dealer_highest_bid_qs:
        if auction_status == ACTIVE_STATUS:
            dealer_bid_amount = dealer_highest_bid_qs.amount
            if highest_auction_bid.dealer_id == dealer.id:
                dealer_auction_status = WINNING_STATUS
            else:
                dealer_auction_status = LOSING_STATUS

        else:
            try:
                AuctionWinner.objects.get(listing_auction_id=listing_auction.id, dealer_id=dealer.id)
                dealer_auction_status = WON_STATUS
            except AuctionWinner.DoesNotExist:
                dealer_auction_status = LOST_STATUS

    return {
        'id': listing_auction.id,
        'listing_id': listing_auction.listing_id,
        'start_time': str(listing_auction.start_time),
        'end_time': str(listing_auction.end_time),
        'suggested_price': listing_auction.suggested_price,
        'highest_bid': highest_bid_amount,
        'dealer_bid': dealer_bid_amount,
        'min_bid_amount': min_bid_amount,
        'auction_status': auction_status,
        'dealer_auction_status': dealer_auction_status,
        'security_deposit_amount': SECURITY_DEPOSIT_DEDUCTION_AMOUNT
    }
