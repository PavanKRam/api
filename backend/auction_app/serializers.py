from datetime import datetime

from django.db.models import Max
from django.utils.timezone import utc
from rest_framework import serializers

from main.serializers import DedicatedSerializer
from main.serializers import ListingSerializer
from main.utils.constants import SECURITY_DEPOSIT_DEDUCTION_AMOUNT, POST_SALE_SECURITY_DEPOSIT_AMOUNT, \
    PARKING_CHARGE_PER_DAY, DELIVERY_CHARGE
from serializer_helper import get_wallet_amount
from .models import Dealer, WalletTransaction, ListingAuction, AuctionActivity, DealerListingBid, AuctionWinner
from .serializer_helper import get_city, get_auction_details_data, get_state_codes
from .utils.constants import NEGOTIATION_STATUS, PAYMENT_WAITING_STATUS, WON_STATUS, PAYMENT_DONE_STATUS, \
    SELLER_CANCELLED_STATUS, DEALER_CANCELLED_STATUS


class DealerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dealer
        fields = ('id', 'is_verified_from_admin', 'wallet_id', 'user_id', 'created_at')

    def to_representation(self, instance):
        representation = super(DealerSerializer, self).to_representation(instance)
        representation['created_at'] = str(instance.created_at)
        return representation


class WalletTransactionSerializer(serializers.ModelSerializer):
    @staticmethod
    def get_transactions(obj):
        variant_name = ''
        listing_id = None
        if obj.listing_auction_id:
            variant_name = obj.listing_auction.listing.variant.name
            listing_id = obj.listing_auction.listing_id

        return {
            'transaction_id': obj.id,
            'listing_auction_id': obj.listing_auction_id,
            'purpose': obj.description,
            'transaction_date': str(obj.created_at),
            'amount': obj.amount,
            'type': 'CR' if obj.amount > 0 else 'DB',
            'listing_id': listing_id,
            'variant_name': variant_name
        }

    transactions = serializers.SerializerMethodField(read_only=True)

    def to_representation(self, instance):
        # get the instance -> Dict of primitive data types
        representation = super(WalletTransactionSerializer, self).to_representation(instance)

        # manipulate returned dictionary as desired
        transaction_details = representation.pop('transactions')

        return transaction_details

    class Meta:
        model = WalletTransaction
        fields = ('transactions',)


class ListingAuctionSerializer(serializers.ModelSerializer):
    """Serializer class for ListingAuction model"""

    @staticmethod
    def setup_eager_loading(queryset):
        queryset = queryset.prefetch_related('listing')
        return queryset

    def get_listing_details(self, obj):
        listing_data = ListingSerializer(obj.listing, context={'request': self.context.get('request'),
                                                               'attachment_qs': self.context.get('attachment_qs'),
                                                               'cdn_url': self.context.get('cdn_url'),
                                                               'is_auction_app_user': True})
        return listing_data.data

    listing_details = serializers.SerializerMethodField(read_only=True)

    def get_auction_details(self, obj):
        return get_auction_details_data(self.context.get('dealer'), obj)

    auction_details = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ListingAuction
        fields = ('listing_details', 'auction_details')


class DedicatedAuctionSerializer(serializers.ModelSerializer):
    JOURNEY_TEXT = {
        'active': 'Auction start',
        'won': 'You won auction',
        'negotiation': 'Negotiate with seller',
        'seller_cancelled': 'Seller cancelled',
        'dealer_cancelled': 'Dealer cancelled',
        'closed': 'Auction closed',
        'payment_waiting': 'Waiting for payment',
        'payment_done': 'We have received payment',
    }

    def get_listing_details(self, obj):
        listings_data = DedicatedSerializer(obj.listing, context={'request': self.context.get('request'),
                                                                  'invalid_numbers': self.context.get(
                                                                      'invalid_numbers'),
                                                                  'buyer': None,
                                                                  'is_auction_app_user': True})
        return listings_data.data

    listing_details = serializers.SerializerMethodField(read_only=True)

    def get_invoice_details(self, obj):
        dealer = self.context.get('dealer')
        procured_date = AuctionActivity.objects.values_list('changed_at', flat=True) \
            .filter(listing_auction_id=obj.id, auction_status_id=2).first()

        dealer_highest_bid = DealerListingBid.objects.values('listing_auction_id').annotate(amount=Max('amount')) \
            .filter(listing_auction_id=obj.id, dealer_id=dealer.id).first()

        now = datetime.utcnow().replace(tzinfo=utc)
        if procured_date:
            parking_days = (now - procured_date).days
        else:
            parking_days = 0
        parking_charge = PARKING_CHARGE_PER_DAY * parking_days
        total_amount = dealer_highest_bid['amount'] + POST_SALE_SECURITY_DEPOSIT_AMOUNT + DELIVERY_CHARGE + parking_charge

        return {
            'car_price': {'value': dealer_highest_bid['amount'], 'label': 'Car price'},
            'ref_sec_deposit': {'value': POST_SALE_SECURITY_DEPOSIT_AMOUNT, 'label': 'Refundable security deposit'},
            'delivery_charge': {'value': DELIVERY_CHARGE, 'label': 'Delivery Charges'},
            'parking_charge': {'value': parking_charge, 'label': 'Parking Charges'},
            'total_amount': {'value': total_amount, 'label': 'Total amount'},
        }

    def get_security_deposit_status(self, obj):
        return {
            'is_refunded': True,
            'amount': SECURITY_DEPOSIT_DEDUCTION_AMOUNT,
            'label': 'We have refunded RS {0} to your bank account'.format(SECURITY_DEPOSIT_DEDUCTION_AMOUNT)
        }

    def get_auction_journey(self, obj):
        request = self.context.get('request')
        current_status = AuctionWinner.objects.values_list('auction_status__name', flat=True).get(
            dealer__user_id=request.user.id, listing_auction_id=obj.id)
        journey = list()
        won_state = {'status': 'processed', 'state': WON_STATUS, 'label': self.JOURNEY_TEXT[WON_STATUS]}
        journey.append(won_state)

        if current_status == NEGOTIATION_STATUS:
            state = {'status': 'processing', 'state': NEGOTIATION_STATUS,
                     'label': self.JOURNEY_TEXT[current_status]}
        else:
            state = {'status': 'processed', 'state': NEGOTIATION_STATUS,
                     'label': self.JOURNEY_TEXT[NEGOTIATION_STATUS]}

        journey.append(state)

        last_step_states = [PAYMENT_DONE_STATUS, SELLER_CANCELLED_STATUS, DEALER_CANCELLED_STATUS]
        if current_status in last_step_states:
            state = {'status': 'processed', 'state': WON_STATUS, 'label': self.JOURNEY_TEXT[current_status]}
        elif current_status == PAYMENT_WAITING_STATUS:
            state = {'status': 'processing', 'state': PAYMENT_WAITING_STATUS,
                     'label': self.JOURNEY_TEXT[current_status]}
        else:
            state = {'status': 'not-processed', 'state': PAYMENT_WAITING_STATUS,
                     'label': self.JOURNEY_TEXT[PAYMENT_WAITING_STATUS]}

        journey.append(state)

        return journey

    def get_auction_details(self, obj):
        auction_status = obj.auction_status.name
        details = get_auction_details_data(self.context.get('dealer'), obj)

        if details['dealer_auction_status'] is WON_STATUS:
            if auction_status == PAYMENT_WAITING_STATUS:
                invoice_details = self.get_invoice_details(obj)
                details['invoice_details'] = invoice_details

            if auction_status is SELLER_CANCELLED_STATUS:
                security_deposit_status = self.get_security_deposit_status(obj)
                details['security_deposit_status'] = security_deposit_status

            details['journey'] = self.get_auction_journey(obj)
        return details

    auction_details = serializers.SerializerMethodField(read_only=True)

    def get_wallet_details(self, obj):
        request = self.context.get('request')
        wallet_id = Dealer.objects.values_list('wallet_id', flat=True).get(user_id=request.user.id)
        return get_wallet_amount(wallet_id)

    wallet_details = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = ListingAuction
        fields = ('listing_details', 'auction_details', 'wallet_details')


class AuctionFiltersSerializer(serializers.Serializer):
    """
    Serializer class to get filters data

    To get data from this serializer, perform `.get_data()` rather
    than the usual way by accessing `.data`.
    """

    def get_data(self):
        serializer_data = self.data or dict()

        filter_city = self.context.get('filter_city', '')
        filter_state_code = self.context.get('filter_state_code', '')

        serializer_data['state_code'] = get_state_codes(filter_state_code)
        serializer_data['city'] = get_city(filter_city)
        return serializer_data
