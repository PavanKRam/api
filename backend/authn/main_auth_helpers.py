from main.models import Listing
from user.models import User, City
from user.utils.messages import INVALID_CITY
from .utils.helpers import generate_jwt


def post_verification_process(**kwargs):
    """
    The function takes care of the verification process of a user
    """

    city = None
    if kwargs.get('city_id'):
        try:
            city = City.objects.get(id=kwargs.get('city_id'))
        except City.DoesNotExist:
            return INVALID_CITY

    user = User.objects.filter(mobile=kwargs.get('mobile')).order_by('-created_at').first()

    if user is None:
        user = User.objects.create(name=kwargs.get('name'), mobile=kwargs.get('mobile'), city=city)
    else:
        if kwargs.get('name'):
            user.name = kwargs.get('name')
        if city:
            user.city = city
        user.save()

    token = generate_jwt(user)
    user_info = {
        'name': user.name,
        'email': user.email,
        'mobile': str(user.mobile),
        'city_id': user.city.id if user.city else None,
    }

    seller_listings = Listing.objects.filter(seller__mobile=kwargs.get('mobile')).first()
    if seller_listings:
        user_info['seller_type'] = seller_listings.seller.type_id

    response_data = {
        'message': 'User verified successfully!',
        'token': token,
        'user_info': user_info,
        'status': True
    }

    return response_data
