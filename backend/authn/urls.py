from django.conf.urls import url
from .main_auth import generate_otp, verify_otp, resend_otp, login_without_otp


urlpatterns = [
    url(r'generate_otp/$', generate_otp),
    url(r'verify_otp/$', verify_otp),
    url(r'resend_otp/$', resend_otp),
    url(r'login_without_otp/$', login_without_otp),
]