# -*- coding: utf-8 -*-
"""
Module for providing helper methods
"""

from rest_framework_jwt.settings import api_settings
from random import randrange
from authn.models import LoginOtpLog
from user.models import Seller, City
from .constants import MIN_OTP_VALUE, MAX_OTP_VALUE
from connect.spreadsheet import get_spreadsheet_data
from user.user_helpers import get_user_subscriptions
from connect.sms import get_sms_data
from user.user_helpers import get_user_info


def generate_jwt(user):
    """
    Generates JWT for the given user

    ---

    parameters:
        - name: user
          description: user instance
          required: true
          type: Object

    return: JWT
    """

    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)

    return token


def get_login_spreadsheet_data(user_info):
    user_info['is_seller'] = Seller.objects.filter(mobile=user_info['mobile'], parent_id=None)\
                                 .first() is not None
    
    is_user_paid = False
    if 'id' in user_info:
        is_user_paid = get_user_subscriptions(user_info['id'], 'buyer')

    user_info['source'] = 'otp_sent'
    city_name = str('' if user_info.get('city_id') is None else City.objects.get(id=user_info['city_id']).name)
    spreadsheet_data = get_spreadsheet_data(user_info, 'BuyerLead', 'BuyerSync', city_name, False, is_user_paid)

    return spreadsheet_data


def create_otp(mobile):
    otp = randrange(MIN_OTP_VALUE, MAX_OTP_VALUE + 1)
    LoginOtpLog.objects.create(mobile=mobile, otp=otp)

    return otp


def get_login_communications(mobile, otp):
    sms_data = get_sms_data(type_='otp', mobile=mobile, otp=otp)
    user_info = get_user_info(mobile)
    spreadsheet_data = get_login_spreadsheet_data(user_info)

    return sms_data, spreadsheet_data