# -*- coding: utf-8 -*-
"""
Module holding JWT authentication views.
"""

from __future__ import unicode_literals

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from auction_app.auction_helpers import is_auction_app, post_verification_dealer
from authn.main_auth_helpers import post_verification_process
from connect.gearman_component import add_gearman_data
from connect.gearman_helpers import prepare_mailer_data
from main.quick_ops import valid_mobile_number
from user.utils.messages import REQUIRED_FIELDS_ABSENT, INVALID_CITY
from user.utils.response import get_error_response
from user.user_helpers import get_city_for_anonymous_user
from .models import LoginOtpLog
from .utils.helpers import create_otp, get_login_communications
from .utils.messages import INVALID_MOBILE, OTP_DOES_NOT_MATCH, OTP_IS_NOT_A_NUMBER


@api_view(['POST'])
def generate_otp(request):
    """
    An API View to generate a new One Time Password (OTP)

    ---

    parameters:
        - name: mobile
          description: 10-digit valid Indian mobile number
          required: true
          type: integer
        - name: name
          description: name
          required: true
          type: string

    responseMessages:
        - code: 400
          success: false
          message: Required field not entered.
          message: Invalid Mobile Number.
        - code: 201
          success: true

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml
    """

    mobile = request.data.get('mobile', None)
    name = request.data.get('name', None)
    is_dealer = is_auction_app(request.path_info)

    if is_dealer:
        name = "Truebil Dealer"

    if mobile is None or name is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    if valid_mobile_number(mobile):
        sms_data, spreadsheet_data = get_login_communications(mobile, otp=create_otp(mobile))
        if is_dealer:
            mailer_data = prepare_mailer_data(sms_data=[sms_data])
        else:
            mailer_data = prepare_mailer_data(sms_data=[sms_data], spreadsheet_data=[spreadsheet_data])
        add_gearman_data('mailer', mailer_data)

        response_data = {
            'message': 'OTP sent successfully!',
            'status': True
        }
        return Response(response_data, status=status.HTTP_201_CREATED)
    else:
        return Response(get_error_response(INVALID_MOBILE), status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def verify_otp(request):
    """
    An API View to verify One Time Password (OTP) and generate JWT

    ---

    parameters:
        - name: mobile
          description: 10-digit valid Indian mobile number
          required: true
          type: integer
        - name: name
          description: name
          required: true
          type: string
        - name: otp
          description: One Time Password
          required: true
          type: integer
        - name: city_id
          description: City Id
          required: false
          type: integer
        - name: name
          description: user name
          required: true
          type: string

    responseMessages:
        - code: 400
          success: false
          message: Required field not entered.
          message: Invalid Mobile Number.
        - code: 200
          success: true
          token: JWT token value

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml
    """

    mobile = request.data.get('mobile', None)
    otp = request.data.get('otp', None)
    name = request.data.get('name', None)
    city_id = request.data.get('city_id', None)
    is_dealer = is_auction_app(request.path_info)

    if is_dealer:
        name = "Truebil Dealer"
        city_id = get_city_for_anonymous_user(request.META)
    kwargs = dict()

    if mobile is None or otp is None or name is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    if valid_mobile_number(mobile):
        try:
            otp = int(otp)
        except ValueError:
            return Response(get_error_response(OTP_IS_NOT_A_NUMBER), status=status.HTTP_400_BAD_REQUEST)

        user_otp_log = LoginOtpLog.objects.filter(mobile=mobile).order_by('-created_at').first()

        if user_otp_log.otp == otp:
            if city_id:
                kwargs['city_id'] = city_id
            kwargs['mobile'] = mobile
            kwargs['name'] = name

            if is_dealer:
                response_data = post_verification_dealer(**kwargs)
            else:
                response_data = post_verification_process(**kwargs)
            if response_data == INVALID_CITY:
                return Response(get_error_response(response_data), status=status.HTTP_400_BAD_REQUEST)
            else:
                return Response(response_data)
        else:
            response_data = get_error_response(OTP_DOES_NOT_MATCH)
            response_data['incorrect_otp'] = True
            return Response(response_data, status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(get_error_response(INVALID_MOBILE), status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def login_without_otp(request):
    """
    An API View to verify One Time Password (OTP) and generate JWT

    ---

    parameters:
        - name: mobile
          description: 10-digit valid Indian mobile number
          required: true
          type: integer
        - name: city_id
          description: City Id
          required: false
          type: integer

    responseMessages:
        - code: 400
          success: false
          message: Required field not entered.
          message: Invalid Mobile Number.
        - code: 200
          success: true
          token: JWT token value

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml
    """

    mobile = request.data.get('mobile', None)
    city_id = request.data.get('city_id', None)

    kwargs = dict()

    if mobile is None or city_id is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    if city_id:
        kwargs['city_id'] = city_id
    kwargs['mobile'] = mobile

    response_data = post_verification_process(**kwargs)
    if response_data == INVALID_CITY:
        return Response(get_error_response(response_data), status=status.HTTP_400_BAD_REQUEST)
    else:
        return Response(response_data)


@api_view(['POST'])
def resend_otp(request):
    """
    An API View to resend the One Time Password (OTP)

    ---

    parameters:
        - name: mobile
          description: 10-digit valid Indian mobile number
          required: true
          type: integer

    responseMessages:
        - code: 400
          success: false
          message: Required field not entered.
          message: Invalid Mobile Number.
        - code: 200
          success: true

    consumes:
        - application/json
        - application/xml
    produces:
        - application/json
        - application/xml
    """

    mobile = request.data.get('mobile', None)

    if mobile is None:
        return Response(get_error_response(REQUIRED_FIELDS_ABSENT), status=status.HTTP_400_BAD_REQUEST)

    if valid_mobile_number(mobile):
        user = LoginOtpLog.objects.filter(mobile=mobile).values('otp').order_by('-created_at').first()

        sms_data, spreadsheet_data = get_login_communications(mobile, otp=user['otp'])

        mailer_data = prepare_mailer_data(sms_data=[sms_data], spreadsheet_data=[spreadsheet_data])
        add_gearman_data('mailer', mailer_data)

        response_data = {
            'message': 'OTP sent successfully!',
            'status': True
        }
        return Response(response_data)
    else:
        return Response(get_error_response(INVALID_MOBILE), status=status.HTTP_400_BAD_REQUEST)
