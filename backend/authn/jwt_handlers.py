from calendar import timegm
from datetime import datetime, timedelta

from rest_framework_jwt.settings import api_settings


def jwt_payload_handler(user):
    """ Custom payload handler
    Token encrypts the dictionary returned by this function, and can be decoded by rest_framework_jwt.utils.jwt_decode_handler
    """
    expiry_date = datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA

    return {
        'mobile': user.mobile,
        'exp': expiry_date,
        'orig_iat': timegm(
            datetime.utcnow().utctimetuple()
        )
    }


def jwt_get_username_from_payload_handler(payload):
    """ Custom Username Get Handler """
    return payload.get('mobile')
