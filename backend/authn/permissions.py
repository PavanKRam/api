# -*- coding: utf-8 -*-

from django.contrib.auth.models import AnonymousUser
from rest_framework import permissions
from auction_app.models import Dealer


class IsAuthenticated(permissions.BasePermission):
    """
    Global permission check for authentication.
    """

    def has_permission(self, request, view):
        if type(request.user) == AnonymousUser:
            return False

        return request.user


class AllowAny(permissions.BasePermission):
    """
    Allow any access.
    This isn't strictly required, since you could use an empty
    permission_classes list, but it's useful because it makes the intention
    more explicit.
    """
    def has_permission(self, request, view):
        return True


class IsAuthenticatedOrReadOnly(permissions.BasePermission):
    """
    Global permission to allow read request and check for authentication for other requests
    """

    def has_permission(self, request, view):
        if type(request.user) != AnonymousUser or request.method == 'GET':
            return request.user

        return False


class IsVerifiedFromAdmin(permissions.BasePermission):
    """
    Permission check for admin verified dealer
    """

    def has_permission(self, request, view):
        if type(request.user) == AnonymousUser:
            return False
        else:
            try:
                dealer = Dealer.objects.get(user_id=request.user.id)
                if dealer.is_verified_from_admin:
                    return request.user
            except Dealer.DoesNotExist:
                return False
        return False
