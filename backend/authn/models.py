# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from user.models import BaseModel
from connect.signals import push_truebil_data_to_crm


class LoginOtpLog(BaseModel):
    mobile = models.BigIntegerField()
    otp = models.PositiveIntegerField()
    created_at = models.DateTimeField()

    def __unicode__(self):
        return "{0}, {1}".format(self.mobile, self.otp)

    class Meta:
        db_table = 'login_otp_logs'