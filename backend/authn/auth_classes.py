from rest_framework import exceptions
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from user.models import User


jwt_get_username_from_payload = api_settings.JWT_PAYLOAD_GET_USERNAME_HANDLER


class JSONWebTokenOtpAuthentication(JSONWebTokenAuthentication):
    def authenticate_credentials(self, payload):
        """
        Authenticates the user using the payload in the received JWT
        """

        mobile = jwt_get_username_from_payload(payload)

        if not mobile:
            msg = 'Invalid payload.'
            raise exceptions.AuthenticationFailed(msg)

        # get the last entry for the given mobile
        user = User.objects.filter(mobile=mobile).order_by('-created_at').first()
        if not user:
            msg = 'Invalid signature.'
            raise exceptions.AuthenticationFailed(msg)
        user.is_authenticated = True
        return user
