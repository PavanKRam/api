# -*- coding: utf-8 -*-
"""
Module for sms based communication
"""

from .message_dictionaries import SMS
from .exceptions import ConnectValidationError
from .utils.messages import INVALID_DATA


def get_message_text(type_, **kwargs):
    """Formats message text from the sms dictionary"""

    try:
        message_text = SMS[type_].format(**kwargs)
    except KeyError:
        raise ConnectValidationError(INVALID_DATA)

    return message_text


def get_sms_data(type_, mobile, **kwargs):
    """
    Processes information to get sms data
    :param type_: Email type
           required: true
           type: string
    :param mobile: 10-digit valid Indian mobile number
           required: true
           type: integer

    :return sms data dictionary
    """

    message_text = get_message_text(type_, **kwargs)
    sms_data = {
        'sendTo': mobile,
        'message': message_text
    }

    perform_shortener = kwargs.get('perform_shortener', False)

    if perform_shortener:
        sms_data['performShortener'] = True
        sms_data['longURL'] = kwargs.get('url')

    return sms_data
