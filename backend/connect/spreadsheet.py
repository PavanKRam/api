# -*- coding: utf-8 -*-
"""
Module for spreadsheet based communication
"""
from main.models import CarInspector
from main.quick_ops import get_timezone_aware_current_datetime
from user.models import Locality


def get_spreadsheet_data(user, worksheet_name, spreadsheet_name, city_name, is_login, is_paid):
    """
    Gets spreadsheet data dictionary

    ---

    parameters:
        - name: user
          description: user information
          required: true
          type: dict
        - name: worksheet_name
          description: Name of the worksheet
          required: true
          type: string
        - name: spreadsheet_name
          description: Name of the spreadsheet
          required: true
          type: string
        - name: city_name
          description: City Name of the user
          required: true
          type: string
        - name: is_login
          description: User is logged in or not
          required: true
          type: boolean
        - name: is_paid
          description: User has paid or not
          required: true
          type: boolean
    """

    now = get_timezone_aware_current_datetime()
    lead_time = now.strftime('%Y-%m-%d %H:%M:%S')

    spreadsheet_data = user

    spreadsheet_data['workSheetName'] = worksheet_name
    spreadsheet_data['spreadSheetName'] = spreadsheet_name
    spreadsheet_data['city'] = city_name
    spreadsheet_data['time'] = lead_time
    spreadsheet_data['lead generation date'] = lead_time
    spreadsheet_data['time'] = lead_time
    spreadsheet_data['islogin'] = is_login
    spreadsheet_data['ispaid'] = is_paid

    return spreadsheet_data


def get_seller_spreadsheet_data(user, interested_seller_listing, seller_visit=None):
    now = get_timezone_aware_current_datetime()
    lead_time = now.strftime('%Y-%m-%d %H:%M:%S')

    spreadsheet_data = dict()
    spreadsheet_data['name'] = user.name
    spreadsheet_data['mobile'] = user.mobile
    spreadsheet_data['Email'] = user.email
    spreadsheet_data['city'] = user.city.name

    spreadsheet_data['spreadSheetName'] = 'SellerSync'
    spreadsheet_data['workSheetName'] = 'SellerLeads'
    spreadsheet_data['source'] = 'LRF'
    spreadsheet_data['lead generation date'] = lead_time
    spreadsheet_data['time'] = lead_time

    model_obj = interested_seller_listing.variant.model
    make_name = model_obj.make.name
    spreadsheet_data['Make'] = make_name
    spreadsheet_data['Model'] = model_obj.name.replace(make_name, '').lstrip()
    spreadsheet_data['Year'] = int(interested_seller_listing.year)
    spreadsheet_data['Kilometer'] = str(interested_seller_listing.mileage)

    if seller_visit:
        if seller_visit.locality_id:
            locality_name = Locality.objects.values_list('name').get(pk=seller_visit.locality_id)[0]
            spreadsheet_data['Locality'] = locality_name
        extra_info = dict()
        extra_info['visit_date'] = seller_visit.visit_date
        extra_info['visit_time'] = seller_visit.visit_time_slot.start_time
        extra_info['address'] = seller_visit.location
        extra_info['owner'] = interested_seller_listing.owner
        if seller_visit.inspector_id:
            car_inspector = CarInspector.objects.get(pk=seller_visit.inspector_id)
            try:
                extra_info['engineer'] = ' '.join(car_inspector.email.split('@')[0].split('.'))
            except (KeyError, AttributeError):
                extra_info['engineer'] = car_inspector.name

        spreadsheet_data['URL'] = ''
        for key, value in extra_info.iteritems():
            spreadsheet_data['URL'] += key + ' : ' + str(value) + '\n'

    return spreadsheet_data
