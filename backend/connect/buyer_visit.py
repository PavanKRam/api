# -*- coding: utf-8 -*-
"""
Module for logging buyer visit changes
"""

from connect.enjay import parse_model_fields
from server.settings import env


BEFORE_SAVE_DATA = {}


def prepare_buyer_visit_before_save_data(model_object):
    global BEFORE_SAVE_DATA
    BEFORE_SAVE_DATA = parse_model_fields(model_object) if model_object else {}


def prepare_buyer_visit_log_data(model_object):
    """
    Prepares buyer visit log data
    """

    buyer_visit_data = dict()

    buyer_visit_data['oldState'] = BEFORE_SAVE_DATA
    buyer_visit_data['newState'] = parse_model_fields(model_object)
    buyer_visit_data['source'] = env('PLATFORM_NAME')
    if buyer_visit_data['newState']['status'] == 'cancelled':
        global BEFORE_SAVE_DATA
        BEFORE_SAVE_DATA = {}

    return buyer_visit_data
