# -*- coding: utf-8 -*-
"""
Module for exception classes
"""

class ConnectValidationError(Exception):
    """Validation Error"""
    pass