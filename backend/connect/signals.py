from django.db.models.signals import post_save
from django.dispatch import receiver

from connect.buyer_visit import prepare_buyer_visit_log_data
from connect.enjay import get_enjay_data
from connect.gearman_component import add_gearman_data
from user.models import BuyerVisit


@receiver(post_save)
def push_truebil_data_to_crm(sender, **kwargs):
    is_created = kwargs.get('created', None)
    enjay_data = get_enjay_data(is_created, kwargs.get('instance'))
    add_gearman_data('pushTruebilEnjay', enjay_data)


@receiver(post_save, sender=BuyerVisit)
def log_buyer_visit(sender, **kwargs):
    instance_id = kwargs.get('instance').id
    instance = BuyerVisit.objects.get(id=instance_id)
    visit_booking_log_data = prepare_buyer_visit_log_data(instance)
    add_gearman_data('visitBookingLog', visit_booking_log_data)
