# -*- coding: utf-8 -*-
"""
Module for email based communication
"""

from .message_dictionaries import EMAIL
from .exceptions import ConnectValidationError
from .utils.messages import INVALID_DATA


def get_email_vars(type_, **kwargs):
    """Get email variables"""

    email_vars = EMAIL[type_]

    if 'template' in email_vars.keys():
        template = email_vars['template']
        required_view_vars = email_vars['required_view_vars']

        return {
            'template': template,
            'required_view_vars': required_view_vars
        }
    else:
        _message = email_vars['message']

        try:
            message = _message.format(**kwargs)
        except KeyError:
            raise ConnectValidationError(INVALID_DATA)

        return {
            'message': message,
        }


def is_valid_view_vars(params, required_params):
    """
    Validates view vars passed to get email data
    Raises ConnectValidationError if arguments are missing
    """
    for param in required_params:
        if not (param in params):
            raise ConnectValidationError(INVALID_DATA)


def get_email_data(type_, send_to, subject, **kwargs):
    """
    Processes information to get email data

    :param type_: Email type
           required: true
           type: string
    :param send_to: Email recipient list
           required: true
           type: list
    :param subject: Email subject
           required: true
           type: string

    :return email data dictionary
    """

    email_vars = get_email_vars(type_, **kwargs)
    email_data = {
        'sendTo': send_to,
        'subject': subject,
    }

    if 'template' in email_vars.keys():
        email_data['template'] = email_vars['template']

        is_valid_view_vars(kwargs.get('viewVars'), email_vars['required_view_vars'])
        email_data['viewVars'] = kwargs.get('viewVars')
    else:
        email_data['message'] = email_vars['message']

    return email_data
