# -*- coding: utf-8 -*-
"""
Module for enjay based communication
"""
import datetime
import decimal
from itertools import chain

from django.db import models

from server.settings import env
from user.models import BaseModel


def get_enjay_data(is_created, model_object):
    """
    Prepares enjay data
    """

    enjay_data = dict()
    if not is_created:
        enjay_data['id'] = model_object.id
    refresh_restricted_fields_from_db(model_object)
    enjay_data['table'] = model_object._meta.db_table
    enjay_data['data'] = parse_model_fields(model_object)
    enjay_data['app_name'] = env('PLATFORM_NAME')
    enjay_data['action'] = 'save' if is_created else 'update'

    return enjay_data


def parse_model_fields(instance):
    """
    Returns a dict containing the data in ``instance``.
    IMP: The function is an extension of model_to_dict function
    """

    opts = instance._meta
    data = {}
    for f in chain(opts.concrete_fields, opts.private_fields, opts.many_to_many):
        value = f.value_from_object(instance)
        data[f.attname] = datetime_converter(value)

        if value and isinstance(value, (int, long, decimal.Decimal, object)):
            data[f.name] = unicode(value)
        else:
            data[f.name] = value

        if isinstance(f, models.ManyToManyField):
            data[f.name] = list(data[f.name])

    return data


def datetime_converter(o):
    return o.__str__() if isinstance(o, (datetime.datetime, datetime.date)) else o


def refresh_restricted_fields_from_db(model_object):
    fields = [f.attname for f in model_object._meta.concrete_fields if f.attname in BaseModel.restricted_fields]
    model_object.refresh_from_db(fields=fields)
