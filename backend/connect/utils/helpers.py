# -*- coding: utf-8 -*-
"""
Helpers for connect app
"""

def import_connect_components(sms=False, email=False, spreadsheet=False, gearman_helpers=False,
                              gearman_component=False, exceptions=False):
    """
    Imports the desired components based on arguments passed
    """

    import_string = ''

    if sms:
        import_string += 'from connect.sms import get_sms_data\n'
    if email:
        import_string += 'from connect.email import get_email_data\n'
    if spreadsheet:
        import_string += 'from connect.spreadsheet import get_spreadsheet_data\n'
    if gearman_helpers:
        import_string += 'from connect.gearman_helpers import prepare_mailer_data\n'
    if gearman_component:
        import_string += 'from connect.gearman_component import add_gearman_data\n'
    if exceptions:
        import_string += 'from connect.exceptions import *\n'

    return import_string
