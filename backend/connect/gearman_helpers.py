# -*- coding: utf-8 -*-
"""
Module for providing utilities for connect app
"""

def prepare_mailer_data(sms_data=None, email_data=None, spreadsheet_data=None):
    """
    Prepares mailer data for gearman client
    :param sms_data: SMS data array
    :param email_data: Email data array
    :param spreadsheet_data: Spreadsheet data array
    """
    mailer_data = {}

    if sms_data:
        mailer_data['sms'] = sms_data

    if email_data:
        mailer_data['email'] = email_data

    if spreadsheet_data:
        mailer_data['spreadSheet'] = spreadsheet_data

    return mailer_data