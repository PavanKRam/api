"""
Module containing message templates dictionaries for
sms, email and spreadsheet
"""

SMS = {
    'otp': '{otp} is your Truebil One time password (OTP)',
    'exit_lead': 'Hi! We have received your request for assistance on Truebil. We will get in touch soon. You can also call 09619022022 for any help. Visit us at goo.gl/M4hsjm.',
    'buyer_lead': "Hi {}, our experts will get in touch to help you find the best cars. We will also let you know if we add cars matching your requirements. Truebil",
    'seller_car_alert': "Hi {}, {} has shown an interest on your car on Truebil. Click here goo.gl/4M3fQk to see the contact. Call 09619022022 for any help.",
    'buyer_car_interest': "Hi {}, you requested the seller contact for {} on Truebil. Seller name: {} Contact: {} Happy Car buying!",
    'seller_payment_success': "Hi {s_name}, we have received your payment of Rs. {sub_price}. Now get more leads and see buyer contacts directly! Login at {s_url}",
    'buyer_payment_success': "Congrats! We have received your payment of Rs. {sub_price}. You are now a Truebil Premium User. Enjoy numerous benefits.",
    'user_payment_success': "Hi {}, we have successfully received your payment of Rs. {}. We assure you a great car buying experience at Truebil.",
    'test_drive_request': "Hi {name}, we have received your request for a test drive. We'll get in touch soon. You can call our car expert at {expert_mobile} for queries.",
    'sold_car_crf': "Hi {name}, our experts will get in touch to help you find the best cars. We will also let you know if we add cars matching your requirements. Truebil",
    'crf_pitch_premium': "Hi {name}, your request for {variant_name} (ref ID {listing_id}) has been recorded. Become a premium buyer to see contacts directly here goo.gl/gBasA6. Call 9619022022 for any help.",
    'crf_alert_to_seller': "Hi {s_name}, {b_name} has shown an interest on your car on Truebil. Click here goo.gl/4M3fQk to see the contact. Call 09619022022 for any help.",
    'seller_contact_msg': "Hi {b_name}, you requested the seller contact for {variant_name} on Truebil. Seller name: {s_name} Contact: {s_mobile} Happy Car buying!",
    'inventory_crf_msg': "Hi {name}, we have received your request for {variant_name}. Come for a test drive {onwards} at any time between {store_timings} all 7 days a week at {inventory_address}, {locality}. Click {inventory_location_url} to navigate. Contact our car expert at {expert_mobile} for any queries.",
    'test_drive': "You can come for a test drive {onwards} at any time between {store_timings} all 7 days a week at Truebil, {inventory_address}. Click {inventory_location_url} to navigate.",
    'car_loan': "Hi {name}, Your application for Car Loan has been recorded. A Relationship Manager will get in touch with you very shortly. Truebil care",
    'paper_transfer': "Hi {name}, Your application for Paper Transfer has been recorded. A Relationship Manager will get in touch with you very shortly. Truebil care",
    'insurance': "Hi {name}, Your application for Car Insurance has been recorded. A Relationship Manager will get in touch with you very shortly. Truebil care",
    'quicksell': "Hi {}, Thank you for choosing Truebil. Our representative will get in touch with you shortly to schedule inspection. For any queries call us at {}.",
    'buyer_offer_inventory': "Hi {name}, you have successfully made an offer of Rs. {offer_value} for {variant_name}. You can contact directly at {expert_mobile} for any queries.",
    'subscribed_buyer_offer': "Hi {name}, you have successfully made an offer of Rs. {offer_value} on {variant_name}. Your offer has been sent to the seller. We will let you know if he decreases the price. Call 09619022022 for any help.",
    'buyer_offer': "Hi {name}, you have successfully made an offer of Rs. {offer_value} on {variant_name}. Your offer has been sent to the seller. We will let you know if he decreases the price. You can also contact seller directly by becoming a premium buyer! Click {url}. Call 09619022022 for any help.",
    'seller_offer_alert': "Hi {name}, you have received an offer on your {variant_name} on Truebil. Click here {url} to see the buyer contacts. Call 022-30770255 for any help.",
    'login_confirmation': "Hello {}! You have successfully logged in to Truebil. We assure you a great car buying/selling experience. Visit us here: m.truebil.com",
    'interested_seller': "Hi {name}, Thank you for choosing \"Truebil car selling experience\". Our representative will get in touch for further communication. For any queries, reach us at {support_mobile}",
    'car_sell_request': "Hi {name}, we have received the request for selling your car. We'll get in touch with you shortly. You can also call our car expert, Anil Hemdev, for any queries at 022-62459780.",
    'car_sold': "Hi {seller_name}, congrats on selling your car, we have delisted it from Truebil. Checkout Truebil's collection of 100% verified cars at {seller_mobile}.",
    'price_change_request': "Hi {name}, we have received your request for the price change of your car. We will update the price on the website soon.",
    'subscribed_dealer_offer_inventory': "Hi! You have made an offer of Rs. {offer_value} on {variant_name}. You can visit Truebil Direct store {inventory_location_url} . Contact {dealer_poc} for any help",
    'refurbished_car_added_to_test_drive': "Hi {name}, your test drive visit is booked on {date_time} at Truebil, {address}. {car_name} is available {available_date) onwards. However you can reschedule earlier and test drive many other great cars. Reschedule at http://goo.gl/4y5qaa",
    'car_added_to_test_drive': "Hi {name}, your test drive visit is booked on {date_time} at Truebil, {address}. {car_name} is added to your test drive list. Add more cars to your test drive list at http://goo.gl/4y5qaa",
    'test_drive_rescheduled': "Hi {name}, you have successfully rescheduled your test drives to {date_time}. You would be test driving {test_drive_car_count} cars at Truebil, {address}. Click {inventory_location_url} to navigate.",
    'test_drive_scheduled': "Hi {name}, your test drive visit is booked on {date_time} at Truebil, {address}. Select cars you would like to test drive at http://goo.gl/4y5qaa",
    'listing_added_to_buyer_visit': "Hi {name}, {car_name} is successfully added to your test drive list. We will get back to you shortly. You can also contact our car expert at {expert_number} for any queries. Click http://goo.gl/vvFYV7 to manage booking.",
    'test_drive_detail': "Hi {name}, your test drive is scheduled on {date_time} at {address}. Open 7 days a week between {store_timings}. Click {inventory_location_url} to navigate.",
    'car_removed_from_test_drive': "Hi {name}, you have removed {car_name} from test drive list on {date_time}. Add more cars or manage cars you are interested to take test drive at http://goo.gl/4y5qaa",
    'negotiation_unsuccessful': "Hi {name}! Sorry that negotiations with the seller were unsuccessful. You can also check out high quality Truebil Direct cars for an assured great experience at {url}. Call 09619022022 for any help",
    'seller_not_available_buyer': "Hi {name}! Sorry that you could not go ahead with your interest on Truebil. You can check out high quality Truebil Direct cars for an assured great experience at {url}. Call 09619022022 for any help",
    'seller_not_available_seller': "Hi! An interested buyer on Truebil tried to reach you but could not. Please see your dashboard at {url} to see interested buyers. Call 09619022022 for any help",
    'car_not_available_buyer': "Hi {name}! Sorry that you could not go ahead with your interest on Truebil. You can check out high quality Truebil Direct cars for an assured great experience at {url}. Call 09619022022 for any help",
    'car_not_available_seller': "Hi! An interested buyer on Truebil marked your car as sold. If that is true, please confirm here {url}. Call 09619022022 for any help"
}

EMAIL = {
    'buyer_offer': {
        'message': "Hi , FYI \
          Car Id : {car_id} \
          Car Name: {car_name} \
          Buyer Name : {buyer_name} \
          Buyer Mobile : {buyer_mobile} \
          Offered Price : {offered_price} \
          City : {city} \
          time : {time}"
    },
    'seller_payment_success': {
        'required_view_vars': ['name', 'amount', 'subscription_id', 'source'],
        'template': 'sellerPaymentSuccess'
    },
    'interested_seller': {
        'required_view_vars': ['name'],
        'template': 'seller'
    },
    'payment_status': {
        'message': "Hi , PFA \
          Status : {status} \
          Payment Type : {sub_type} \
          Payment ID : {payment_id} \
          City : {city_name} \
          User Name : {user_name} \
          Mobile : {user_mobile} \
          Email : {user_email} \
          Time : {time} \
          Subscription ID : {subscription_id} \
          Source : {source}"
    },
    'fraud_transaction': {
        'message': "Hi , Developers \
          Status : {status} \
          Payment Type : {sub_type} \
          Payment ID : {payment_id} \
          Subscription ID : {subscription_id} \
          City : {city_name} \
          User Name : {user_name} \
          Mobile : {user_mobile} \
          Email : {user_email} \
          Amount : {amount} \
          Time : {time} \
          Source : {source}"
    },
    'payment_success': {
        'required_view_vars': ['name', 'amount', 'subscription_id', 'source'],
        'template': 'paymentSuccess'
    },
    'payment_error': {
        'message': "Hi Developers, PFA: \
          Payment ID : {payment_id} \
          User Name : {user_name} \
          Mobile : {user_mobile} \
          Email : {user_email} \
          Payment Data : {payment_data} \
          Subscription ID : {subscription_id} \
          Source : {source}"
    },
    'rsa': {
        'message': "Hi , PFA \
            User Name : {name} \
            User Number : {mobile} \
            Bought From : {source} \
            Time : {time}"
    },
    'exit_lead': {
        'message': "Hi , FYI \
            Buyer Mobile : {mobile} \
            City : {city} \
            Time : {time}"
    },
    'car_sell_request': {
        'message': "Hi all, Please find seller info. \
        Seller Name: {name} \
        Seller Mobile No.: {mobile} \
        Car Id: {car_id} \
        Car Name: {variant_name} \
        Registration Number: {registration_number} \
        Truebil Offered Price : {truebil_offered_price} \
        Time: {time}"
    },
    'price_decrease': {
        'message': "Hi , FYI \
        Car Id : {car_id} \
        Seller Name : {name} \
        Seller Mobile No.: {mobile} \
        Original Price : {original_price} \
        Requested Price : {new_price} \
        Updated Price : {updated_price} \
        Min Recommended Price : {min_recommended_price} \
        Max Recommended Price : {max_recommended_price}"
    },
    'feedback_mail': {
        'message': "Hi , FYI \
            User Id : {user_id} \
            User Name : {user_name} \
            User Mobile : {user_mobile} \
            Car Id : {car_id} \
            Response : {feedback} \
            Time : {time}"
    },
    'car_sold': {
        'message': "Hi All, following car has been reported as sold. \
              Car Id : {car_id} \
              Seller Name : {seller_name} \
              Seller Number : {seller_mobile} \
              time : {time}"
    }
}
