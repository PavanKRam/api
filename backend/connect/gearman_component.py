"""
Module for Gearman Client to send sms and emails
"""

import json
from gearman.client import GearmanClient
from django.conf import settings


GEARMAN_IP = settings.GEARMAN_IP


def add_gearman_data(process_name, params):
    """ Sends data to Gearman server """
    client = GearmanClient()
    client.add_connection(GEARMAN_IP)

    client.submit_job(str(process_name), json.dumps(params), background=True)
